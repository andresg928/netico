
const routes = [
  {
    path: '/',
    component: () => import('layouts/LayoutConductores.vue'),
    children: [
      { path: '/turnos', component: () => import('pages/logistica/Turnos.vue') },
      { path: '/gestion-despacho', component: () => import('pages/logistica/GestionDespacho.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/login/InicioSesion.vue') },
      { path: '/inicio', meta: { requiresAuth: true }, name: '/inicio', component: () => import('pages/MenuPrincipal.vue') },
      { path: '/rrhh', meta: { requiresAuth: true }, name: '/rrhh', component: () => import('pages/rrhh/AdminRRHH.vue') },
      { path: '/produccion', meta: { requiresAuth: true }, name: '/produccion', component: () => import('pages/produccion/AdminProduccion.vue') },
      { path: '/logistica', meta: { requiresAuth: true }, name: '/logistica', component: () => import('pages/logistica/AdminLogistica.vue') },
      { path: '/produccion-silos', meta: { requiresAuth: true }, name: '/produccion-silos', component: () => import('pages/produccion/Dashboard.vue') },
      { path: '/ventas', meta: { requiresAuth: true }, name: '/ventas', component: () => import('pages/ventas/AdminVentas.vue') },
      { path: '/admin', meta: { requiresAuth: true }, name: '/admin', component: () => import('pages/administrador/AdminAdministrador.vue') },
      { path: '/admin/mantenimiento', name: '/admin/mantenimiento', component: () => import('pages/administrador/Mantenimiento.vue') },
      { path: '/usuarios/login', name: '/usuarios/login', component: () => import('pages/login/InicioSesion.vue') },
      { path: '/usuarios/reset-pass', name: '/usuarios/reset-pass', meta: { requiresAuth: true }, component: () => import('pages/usuarios/ResetPass.vue') },
      { path: '/inicio/bienvenida', name: '/inicio/bienvenida', meta: { requiresAuth: true }, component: () => import('pages/ventas/MenuAppVendedores.vue') },
      { path: '/despachos/registro', name: '/despachos/registro', meta: { requiresAuth: true }, component: () => import('pages/despachos/RegistroDespachos.vue') },
      { path: '/despachos/historico', name: '/despachos/historico', meta: { requiresAuth: true }, component: () => import('pages/despachos/HistoricoDespachos.vue') },
      { path: '/pedidos/registro/:despacho/:placa/:conductor/:origen?/:id_pedido?', name: '/pedidos/registro', meta: { requiresAuth: true }, component: () => import('pages/pedidos/RegistroPedidos.vue') },
      { path: '/pedidos/historico/', name: '/pedidos/historico', meta: { requiresAuth: true }, component: () => import('pages/pedidos/HistoricoPedidos.vue') },
      { path: '/cartera/historico/', name: '/cartera/historico', meta: { requiresAuth: true }, component: () => import('pages/cartera/HistoricoCartera.vue') },
      { path: '/clientes/listado/', name: '/clientes/listado', meta: { requiresAuth: true }, component: () => import('pages/clientes/ListadoClientes.vue') },
      { path: '/mercado/registro/:registro?', name: '/mercado/registro', meta: { requiresAuth: true }, component: () => import('pages/mercado/Registro.vue') },
      { path: '/mercado/listado/', name: '/mercado/listado', meta: { requiresAuth: true }, component: () => import('pages/mercado/Listado.vue') },
      { path: '/informes/ventas-vendedor/', name: '/informes/ventas-vendedor', meta: { requiresAuth: true }, component: () => import('pages/ventas/VentasPorVendedor.vue') },
      { path: '/ventas/registro-visita-cliente/:estado?', name: '/ventas/registro-visita-cliente', meta: { requiresAuth: true }, component: () => import('pages/clientes/RegistrarVisita.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
