import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import IEcharts from 'vue-echarts-v3/src/full.js'
import Toast from 'vue-toastification'
import fullCalendar from 'vue-fullcalendar'
Vue.use(Toast)
// Import the CSS or use your own!
import 'vue-toastification/dist/index.css'
// intro
/* import IntroJS from 'intro.js'
import 'intro.js/introjs.css'
Vue.use(IntroJS) */
Vue.component('IEcharts', IEcharts)
Vue.use(VueRouter)
Vue.component('inicio-sesion', require('pages/login/InicioSesion.vue').default)
Vue.component('comentarios', require('pages/comentarios/Comentarios.vue').default)
Vue.component('ventas-dashboard', require('pages/ventas/Dashboard.vue').default)
Vue.component('ventas-despachos', require('pages/ventas/ListadoDespachos.vue').default)
Vue.component('ventas-presupuestos', require('pages/ventas/Presupuestos.vue').default)
Vue.component('ventas-presupuestos-seguimiento', require('pages/ventas/PresupuestoSeguimiento.vue').default)
Vue.component('ventas-asigna-presupuesto-vendedor', require('pages/ventas/AsignaPresupuestosVendedor.vue').default)
Vue.component('ventas-presupuesto-mes', require('pages/ventas/PresupuestosConsolidadoVentasMes.vue').default)
Vue.component('ventas-informe-notas-credito', require('pages/ventas/InformeNotasCredito.vue').default)
Vue.component('ventas-informe-cartera', require('pages/ventas/InformeCartera.vue').default)
Vue.component('ventas-informe-facturacion', require('pages/ventas/InformeFacturacion.vue').default)
Vue.component('ventas-presupuesto-mes-vendedor', require('pages/ventas/PresupuestosConsolidadoVentasMesVendedor.vue').default)
Vue.component('ventas-mercado', require('pages/ventas/MercadoDashboard.vue').default)
Vue.component('ventas-vendedor', require('pages/ventas/VentasPorVendedor.vue').default)
Vue.component('ventas-menu-app', require('pages/ventas/MenuAppVendedores.vue').default)
Vue.component('usuarios-admin', require('pages/administrador/AdminUsuarios.vue').default)
Vue.component('usuarios-no-autorizado', require('pages/administrador/NoAutorizado.vue').default)
Vue.component('usuarios-registro', require('pages/administrador/RegistroUsuarios.vue').default)
Vue.component('usuarios-listado', require('pages/administrador/ListadoUsuarios.vue').default)
Vue.component('produccion-maestro-silos', require('pages/produccion/MaestroSilos.vue').default)
Vue.component('produccion-dashboard', require('pages/produccion/Dashboard.vue').default)
Vue.component('produccion-compras-trigo', require('pages/produccion/ComprasTrigo.vue').default)
Vue.component('produccion-molienda', require('pages/produccion/InformeMolienda.vue').default)
Vue.component('produccion-ingresar-produccion', require('pages/produccion/IngresarProduccion.vue').default)
Vue.component('logistica-consulta-despacho', require('pages/logistica/ConsultarDespacho.vue').default)
Vue.component('logistica-gestion-turnos', require('pages/logistica/GestionTurnos.vue').default)
Vue.component('logistica-recepcion-envio-trigo', require('pages/logistica/RecepcionEnvioTrigo.vue').default)
Vue.component('logistica-maestros-productos', require('pages/logistica/MaestrosProductos.vue').default)
Vue.component('logistica-gestion-producciones', require('pages/logistica/GestionProducciones.vue').default)
Vue.component('logistica-consulta-entregas', require('pages/logistica/ConsultaEntrega.vue').default)
Vue.component('logistica-traza-despachos', require('pages/logistica/TrazabilidadDespacho.vue').default)
Vue.component('administrador-responsables', require('pages/administrador/AdminResponsables.vue').default)
Vue.component('rrhh-registro-hv', require('pages/rrhh/RegistroHV.vue').default)
Vue.component('rrhh-gestion-hv', require('pages/rrhh/GestionHV.vue').default)
Vue.component('gestion-vendedores', require('pages/ventas/GestionVendedores.vue').default)
Vue.component('tablero-diario', require('pages/ventas/TableroDiario.vue').default)
Vue.component('full-calendar', fullCalendar)
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach((to, from, next) => {
    // console.log(permisos[2])
    // check if the route requires authentication and user is not logged in
    if (to.matched.some(routes => routes.meta.requiresAuth) && !store.getters['store/isLoggedIn']) {
      // redirect to login page
      next({ name: '/usuarios/login' })
      return
    }

    // if logged in redirect to dashboard
    if (to.path === '/usuarios/login' && store.getters['store/isLoggedIn']) {
      next({ name: '/' })
      return
    }
    if (to.path === '/' && store.getters['store/isLoggedIn']) {
      next({ name: '/inicio' })
      return
    }
    if (to.path === '/' && store.getters['store/isLoggedIn'] === false) {
      next({ name: '/usuarios/login' })
      return
    }
    next()
  })

  return Router
}
