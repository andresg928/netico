import axios from 'axios'
// Obtener la fecha actual
const fechaActual = new Date()
// Obtener el año actual
const year = fechaActual.getFullYear()
const state = {
  url_api: window.location.hostname === '192.168.0.117' ? 'http://192.168.0.117:90' : 'http://181.204.220.50:9084',
  tipo_servidor: window.location.hostname === '192.168.0.117' ? '* DESARROLLO EN LOCAL' : '',
  version: '2024.08.24',
  permisos: [],
  clientes: [],
  cliente_registrado: 0,
  movimiento_registro: 0,
  habilita_menu: 0,
  derechos: year + ' © ICOHARINAS S.A.S - Powered by ICOHARINAS',
  rol: null,
  isLoggedIn: !!localStorage.getItem('token')
}
const mutations = {
  vehiculoIngresado (state) {
    state.movimiento_registro++
  },
  movimientoRegistro (state) {
    state.movimiento_registro++
  },
  habilitaMenu (state) {
    state.habilita_menu++
  },
  loginUser (state) {
    state.isLoggedIn = true
  },
  logoutUser (state) {
    state.isLoggedIn = false
  },
  obtenerPermisos (state, datos) {
    state.permisos = datos
  },
  rol (state, datos) {
    state.rol = datos
  },
  obtenerClientes (state, datos) {
    state.clientes = datos
  }
}
const actions = {
  vehiculoIngresado ({ commit }) {
    commit('vehiculoIngresado')
  },
  loginUser ({ commit }) {
    commit('loginUser')
  },
  logoutUser ({ commit }) {
    commit('logoutUser')
  },
  habilitaMenu ({ commit }) {
    commit('habilitaMenu')
  },
  movimientoRegistro ({ commit }) {
    commit('movimientoRegistro')
  },
  obtenerPermisos ({ commit }) {
    if (localStorage.getItem('token') && state.isLoggedIn === true) {
      axios.post(state.url_api + '/api/usuarios/obtener-usuario', {
        token: localStorage.getItem('token')
      }).then(response => {
        if (response.data.user.id) {
          commit('obtenerPermisos', response.data.permisos)
          commit('rol', response.data.rol)
        } else {
          commit('obtenerPermisos', [])
        }
      }).catch(error => {
        console.log(error)
        commit('obtenerPermisos', [])
      })
    }
  },
  obtenerClientes ({ commit }) {
    if (localStorage.getItem('token') && state.isLoggedIn === true) {
      // alert('asd')
      axios.get(state.url_api + '/api/clientes/listado/', {
        params: {
          token: localStorage.getItem('token'),
          nombre: '',
          sucursal: 'si'
        }
      }).then((response) => {
        commit('obtenerClientes', response.data)
      }).finally(function () {
        // commit('obtenerClientes', [])
      })
    }
  }
}
const getters = {
  cliente_registrado: (state) => {
    return state.cliente_registrado
  },
  habilita_menu: (state) => {
    return state.habilita_menu
  },
  isLoggedIn: (state) => {
    return state.isLoggedIn
  },
  movimiento_registro: (state) => {
    return state.movimiento_registro
  },
  url_api: (state) => {
    return state.url_api
  },
  obtener_permisos: (state) => {
    return state.permisos
  },
  obtener_clientes: (state) => {
    return state.clientes
  },
  obtener_rol: (state) => {
    return state.rol
  },
  derechos: (state) => {
    return state.derechos
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
