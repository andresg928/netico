<?php

return [
    'test' => true,
    'ip_websocket' => '192.168.0.117',
    'port_websocket' => '6001',
    'nombre_impresora_turnos' => 'TURNOS',
    'no_minutos_reportado' => 120,
    'no_minutos_en_espera' => 60,
    'no_minutos_cargue' => 180,
    'imprimir_ficho' => false,
    'grupos_producto' => [
        'paqueteo' => ['G008','G009','G010','G011','G012', 'G018'],
        'bulto' => ['G001','G002','G003','G004','G005','G006','G007','G013','G015'],
        'subproducto' => ['G014','G017']
    ],
    'productos_convertir_paquetes' => [
        '2154', '2114', '2124', '2106', '2108', '2088', '2116', '2091', '2117'
    ],
    // Controla fotos de perfil en el menu de vendedores
    'cedulas_vendedores' => [
        '7218299',
        '17323260',
        '35526901',
        '40450430',
        '63365258',
        '63366883',
        '71420482',
        '71526308',
        '91271392'
    ],
    'version' => '2024.04.30',
    'email_cartera' => 'l.herrera@icoharinas.com.co',
    'email_facturacion' => 'auxiliarcomercial@icoharinas.com.co',
    'email_rrhh' => 'l.serrano@icoharinas.com.co',
    'email_calidad' => 'a.bonilla@icoharinas.com.co',
    'copia_email_calidad' => 'auxiliar.logistico.icoharinas@gmail.com',
    'email_gerencia_ventas'=> 'jr.castillo@icoharinas.com.co',
    'email_lider_ventas'=> 'l.mendoza@icoharinas.com.co',
    'email_notif_entrega' => [
        'l.herrera@icoharinas.com.co',
        'l.mendoza@icoharinas.com.co',
        'jefelogistica@icoharinas.com.co'
    ],
    // RRHH
    'entrevista' => [
        'direccion' => 'CRA 30 # 27-10',
        'lugar' => 'TEMPORAL'
    ],
    'pruebas'=>[
        'direccion' => 'CRA 30 # 27-10',
        'lugar' => 'TEMPORAL PRUEBAS'
    ],
    'examen_medico' => [
        'direccion' => 'CRA 11 NO. 20-65 MEJORAS PUBLICAS',
        'lugar'=>'IPS TEMPORAL SALUDOCUPA'
    ],
    'datos_empresa'=>[
        'nombre' => 'ICOHARINAS SAS',
        'direccion'=> 'CARRERA 17 # 59-131 Palenque chimitá'
    ],
    'notifica_aspirante' => true,
    'periodo_prueba' => 2, // Meses
    'dias_notifica' => 10, // Dias que notitica antes que se finalice el periodo de prueba por la temporal
    'modo_mantenimiento' => false,
    'competencia' => [
                        ['nit' => '1', 'nombre' => 'HARINERA PARDO'],
                        ['nit' => '2', 'nombre' => 'MOLINOS SANTANDER'],
                        ['nit' => '3', 'nombre' => 'HARINAS LA ECONOMICA'],
                        ['nit' => '4', 'nombre' => 'LA NIEVE'],
                        ['nit' => '5', 'nombre' => 'ALMACENES EXITO']
    ],
    'marcas' => ['Doria', 'Gavassa'],
    'fabricantes' => [
        ['nit' => '1', 'nombre' => 'HARINERA PARDO'],
        ['nit' => '2', 'nombre' => 'MOLINOS SANTANDER'],
        ['nit' => '3', 'nombre' => 'HARINAS LA ECONOMICA'],
        ['nit' => '4', 'nombre' => 'LA NIEVE']
    ],
    'ciudades' => [
        'MEDELLIN',
        'ABEJORRAL',
        'ABRIAQUI',
        'ALEJANDRIA',
        'AMAGA',
        'AMALFI',
        'ANDES',
        'ANGELOPOLIS',
        'ANGOSTURA',
        'ANORI',
        'SANTAFE DE ANTIOQUIA',
        'ANZA',
        'APARTADO',
        'ARBOLETES',
        'ARGELIA',
        'ARMENIA',
        'BARBOSA',
        'BELMIRA',
        'BELLO',
        'BETANIA',
        'BETULIA',
        'CIUDAD BOLIVAR',
        'BRICEÑO',
        'BURITICA',
        'CACERES',
        'CAICEDO',
        'CALDAS',
        'CAMPAMENTO',
        'CAÑASGORDAS',
        'CARACOLI',
        'CARAMANTA',
        'CAREPA',
        'EL CARMEN DE VIBORAL',
        'CAROLINA',
        'CAUCASIA',
        'CHIGORODO',
        'CISNEROS',
        'COCORNA',
        'CONCEPCION',
        'CONCORDIA',
        'COPACABANA',
        'DABEIBA',
        'DON MATIAS',
        'EBEJICO',
        'EL BAGRE',
        'ENTRERRIOS',
        'ENVIGADO',
        'FREDONIA',
        'FRONTINO',
        'GIRALDO',
        'GIRARDOTA',
        'GOMEZ PLATA',
        'GRANADA',
        'GUADALUPE',
        'GUARNE',
        'GUATAPE',
        'HELICONIA',
        'HISPANIA',
        'ITAGUI',
        'ITUANGO',
        'JARDIN',
        'JERICO',
        'LA CEJA',
        'LA ESTRELLA',
        'LA PINTADA',
        'LA UNION',
        'LIBORINA',
        'MACEO',
        'MARINILLA',
        'MONTEBELLO',
        'MURINDO',
        'MUTATA',
        'NARIÑO',
        'NECOCLI',
        'NECHI',
        'OLAYA',
        'PEÐOL',
        'PEQUE',
        'PUEBLORRICO',
        'PUERTO BERRIO',
        'PUERTO NARE',
        'PUERTO TRIUNFO',
        'REMEDIOS',
        'RETIRO',
        'RIONEGRO',
        'SABANALARGA',
        'SABANETA',
        'SALGAR',
        'SAN ANDRES DE CUERQUIA',
        'SAN CARLOS',
        'SAN FRANCISCO',
        'SAN JERONIMO',
        'SAN JOSE DE LA MONTAÑA',
        'SAN JUAN DE URABA',
        'SAN LUIS',
        'SAN PEDRO',
        'SAN PEDRO DE URABA',
        'SAN RAFAEL',
        'SAN ROQUE',
        'SAN VICENTE',
        'SANTA BARBARA',
        'SANTA ROSA DE OSOS',
        'SANTO DOMINGO',
        'EL SANTUARIO',
        'SEGOVIA',
        'SONSON',
        'SOPETRAN',
        'TAMESIS',
        'TARAZA',
        'TARSO',
        'TITIRIBI',
        'TOLEDO',
        'TURBO',
        'URAMITA',
        'URRAO',
        'VALDIVIA',
        'VALPARAISO',
        'VEGACHI',
        'VENECIA',
        'VIGIA DEL FUERTE',
        'YALI',
        'YARUMAL',
        'YOLOMBO',
        'YONDO',
        'ZARAGOZA',
        'BARRANQUILLA',
        'BARANOA',
        'CAMPO DE LA CRUZ',
        'CANDELARIA',
        'GALAPA',
        'JUAN DE ACOSTA',
        'LURUACO',
        'MALAMBO',
        'MANATI',
        'PALMAR DE VARELA',
        'PIOJO',
        'POLONUEVO',
        'PONEDERA',
        'PUERTO COLOMBIA',
        'REPELON',
        'SABANAGRANDE',
        'SABANALARGA',
        'SANTA LUCIA',
        'SANTO TOMAS',
        'SOLEDAD',
        'SUAN',
        'TUBARA',
        'USIACURI',
        'BOGOTA, D.C.',
        'CARTAGENA',
        'ACHI',
        'ALTOS DEL ROSARIO',
        'ARENAL',
        'ARJONA',
        'ARROYOHONDO',
        'BARRANCO DE LOBA',
        'CALAMAR',
        'CANTAGALLO',
        'CICUCO',
        'CORDOBA',
        'CLEMENCIA',
        'EL CARMEN DE BOLIVAR',
        'EL GUAMO',
        'EL PEÑON',
        'HATILLO DE LOBA',
        'MAGANGUE',
        'MAHATES',
        'MARGARITA',
        'MARIA LA BAJA',
        'MONTECRISTO',
        'MOMPOS',
        'NOROSI',
        'MORALES',
        'PINILLOS',
        'REGIDOR',
        'RIO VIEJO',
        'SAN CRISTOBAL',
        'SAN ESTANISLAO',
        'SAN FERNANDO',
        'SAN JACINTO',
        'SAN JACINTO DEL CAUCA',
        'SAN JUAN NEPOMUCENO',
        'SAN MARTIN DE LOBA',
        'SAN PABLO',
        'SANTA CATALINA',
        'SANTA ROSA',
        'SANTA ROSA DEL SUR',
        'SIMITI',
        'SOPLAVIENTO',
        'TALAIGUA NUEVO',
        'TIQUISIO',
        'TURBACO',
        'TURBANA',
        'VILLANUEVA',
        'ZAMBRANO',
        'TUNJA',
        'ALMEIDA',
        'AQUITANIA',
        'ARCABUCO',
        'BELEN',
        'BERBEO',
        'BETEITIVA',
        'BOAVITA',
        'BOYACA',
        'BRICEÑO',
        'BUENAVISTA',
        'BUSBANZA',
        'CALDAS',
        'CAMPOHERMOSO',
        'CERINZA',
        'CHINAVITA',
        'CHIQUINQUIRA',
        'CHISCAS',
        'CHITA',
        'CHITARAQUE',
        'CHIVATA',
        'CIENEGA',
        'COMBITA',
        'COPER',
        'CORRALES',
        'COVARACHIA',
        'CUBARA',
        'CUCAITA',
        'CUITIVA',
        'CHIQUIZA',
        'CHIVOR',
        'DUITAMA',
        'EL COCUY',
        'EL ESPINO',
        'FIRAVITOBA',
        'FLORESTA',
        'GACHANTIVA',
        'GAMEZA',
        'GARAGOA',
        'GUACAMAYAS',
        'GUATEQUE',
        'GUAYATA',
        'GsICAN',
        'IZA',
        'JENESANO',
        'JERICO',
        'LABRANZAGRANDE',
        'LA CAPILLA',
        'LA VICTORIA',
        'LA UVITA',
        'VILLA DE LEYVA',
        'MACANAL',
        'MARIPI',
        'MIRAFLORES',
        'MONGUA',
        'MONGUI',
        'MONIQUIRA',
        'MOTAVITA',
        'MUZO',
        'NOBSA',
        'NUEVO COLON',
        'OICATA',
        'OTANCHE',
        'PACHAVITA',
        'PAEZ',
        'PAIPA',
        'PAJARITO',
        'PANQUEBA',
        'PAUNA',
        'PAYA',
        'PAZ DE RIO',
        'PESCA',
        'PISBA',
        'PUERTO BOYACA',
        'QUIPAMA',
        'RAMIRIQUI',
        'RAQUIRA',
        'RONDON',
        'SABOYA',
        'SACHICA',
        'SAMACA',
        'SAN EDUARDO',
        'SAN JOSE DE PARE',
        'SAN LUIS DE GACENO',
        'SAN MATEO',
        'SAN MIGUEL DE SEMA',
        'SAN PABLO DE BORBUR',
        'SANTANA',
        'SANTA MARIA',
        'SANTA ROSA DE VITERBO',
        'SANTA SOFIA',
        'SATIVANORTE',
        'SATIVASUR',
        'SIACHOQUE',
        'SOATA',
        'SOCOTA',
        'SOCHA',
        'SOGAMOSO',
        'SOMONDOCO',
        'SORA',
        'SOTAQUIRA',
        'SORACA',
        'SUSACON',
        'SUTAMARCHAN',
        'SUTATENZA',
        'TASCO',
        'TENZA',
        'TIBANA',
        'TIBASOSA',
        'TINJACA',
        'TIPACOQUE',
        'TOCA',
        'TOGsI',
        'TOPAGA',
        'TOTA',
        'TUNUNGUA',
        'TURMEQUE',
        'TUTA',
        'TUTAZA',
        'UMBITA',
        'VENTAQUEMADA',
        'VIRACACHA',
        'ZETAQUIRA',
        'MANIZALES',
        'AGUADAS',
        'ANSERMA',
        'ARANZAZU',
        'BELALCAZAR',
        'CHINCHINA',
        'FILADELFIA',
        'LA DORADA',
        'LA MERCED',
        'MANZANARES',
        'MARMATO',
        'MARQUETALIA',
        'MARULANDA',
        'NEIRA',
        'NORCASIA',
        'PACORA',
        'PALESTINA',
        'PENSILVANIA',
        'RIOSUCIO',
        'RISARALDA',
        'SALAMINA',
        'SAMANA',
        'SAN JOSE',
        'SUPIA',
        'VICTORIA',
        'VILLAMARIA',
        'VITERBO',
        'FLORENCIA',
        'ALBANIA',
        'BELEN DE LOS ANDAQUIES',
        'CARTAGENA DEL CHAIRA',
        'CURILLO',
        'EL DONCELLO',
        'EL PAUJIL',
        'LA MONTAÑITA',
        'MILAN',
        'MORELIA',
        'PUERTO RICO',
        'SAN JOSE DEL FRAGUA',
        'SAN VICENTE DEL CAGUAN',
        'SOLANO',
        'SOLITA',
        'VALPARAISO',
        'POPAYAN',
        'ALMAGUER',
        'ARGELIA',
        'BALBOA',
        'BOLIVAR',
        'BUENOS AIRES',
        'CAJIBIO',
        'CALDONO',
        'CALOTO',
        'CORINTO',
        'EL TAMBO',
        'FLORENCIA',
        'GUACHENE',
        'GUAPI',
        'INZA',
        'JAMBALO',
        'LA SIERRA',
        'LA VEGA',
        'LOPEZ',
        'MERCADERES',
        'MIRANDA',
        'MORALES',
        'PADILLA',
        'PAEZ',
        'PATIA',
        'PIAMONTE',
        'PIENDAMO',
        'PUERTO TEJADA',
        'PURACE',
        'ROSAS',
        'SAN SEBASTIAN',
        'SANTANDER DE QUILICHAO',
        'SANTA ROSA',
        'SILVIA',
        'SOTARA',
        'SUAREZ',
        'SUCRE',
        'TIMBIO',
        'TIMBIQUI',
        'TORIBIO',
        'TOTORO',
        'VILLA RICA',
        'VALLEDUPAR',
        'AGUACHICA',
        'AGUSTIN CODAZZI',
        'ASTREA',
        'BECERRIL',
        'BOSCONIA',
        'CHIMICHAGUA',
        'CHIRIGUANA',
        'CURUMANI',
        'EL COPEY',
        'EL PASO',
        'GAMARRA',
        'GONZALEZ',
        'LA GLORIA',
        'LA JAGUA DE IBIRICO',
        'MANAURE',
        'PAILITAS',
        'PELAYA',
        'PUEBLO BELLO',
        'RIO DE ORO',
        'LA PAZ',
        'SAN ALBERTO',
        'SAN DIEGO',
        'SAN MARTIN',
        'TAMALAMEQUE',
        'MONTERIA',
        'AYAPEL',
        'BUENAVISTA',
        'CANALETE',
        'CERETE',
        'CHIMA',
        'CHINU',
        'CIENAGA DE ORO',
        'COTORRA',
        'LA APARTADA',
        'LORICA',
        'LOS CORDOBAS',
        'MOMIL',
        'MONTELIBANO',
        'MOÑITOS',
        'PLANETA RICA',
        'PUEBLO NUEVO',
        'PUERTO ESCONDIDO',
        'PUERTO LIBERTADOR',
        'PURISIMA',
        'SAHAGUN',
        'SAN ANDRES SOTAVENTO',
        'SAN ANTERO',
        'SAN BERNARDO DEL VIENTO',
        'SAN CARLOS',
        'SAN PELAYO',
        'TIERRALTA',
        'VALENCIA',
        'AGUA DE DIOS',
        'ALBAN',
        'ANAPOIMA',
        'ANOLAIMA',
        'ARBELAEZ',
        'BELTRAN',
        'BITUIMA',
        'BOJACA',
        'CABRERA',
        'CACHIPAY',
        'CAJICA',
        'CAPARRAPI',
        'CAQUEZA',
        'CARMEN DE CARUPA',
        'CHAGUANI',
        'CHIA',
        'CHIPAQUE',
        'CHOACHI',
        'CHOCONTA',
        'COGUA',
        'COTA',
        'CUCUNUBA',
        'EL COLEGIO',
        'EL PEÑON',
        'EL ROSAL',
        'FACATATIVA',
        'FOMEQUE',
        'FOSCA',
        'FUNZA',
        'FUQUENE',
        'FUSAGASUGA',
        'GACHALA',
        'GACHANCIPA',
        'GACHETA',
        'GAMA',
        'GIRARDOT',
        'GRANADA',
        'GUACHETA',
        'GUADUAS',
        'GUASCA',
        'GUATAQUI',
        'GUATAVITA',
        'GUAYABAL DE SIQUIMA',
        'GUAYABETAL',
        'GUTIERREZ',
        'JERUSALEN',
        'JUNIN',
        'LA CALERA',
        'LA MESA',
        'LA PALMA',
        'LA PEÑA',
        'LA VEGA',
        'LENGUAZAQUE',
        'MACHETA',
        'MADRID',
        'MANTA',
        'MEDINA',
        'MOSQUERA',
        'NARIÑO',
        'NEMOCON',
        'NILO',
        'NIMAIMA',
        'NOCAIMA',
        'VENECIA',
        'PACHO',
        'PAIME',
        'PANDI',
        'PARATEBUENO',
        'PASCA',
        'PUERTO SALGAR',
        'PULI',
        'QUEBRADANEGRA',
        'QUETAME',
        'QUIPILE',
        'APULO',
        'RICAURTE',
        'SAN ANTONIO DEL TEQUENDAMA',
        'SAN BERNARDO',
        'SAN CAYETANO',
        'SAN FRANCISCO',
        'SAN JUAN DE RIO SECO',
        'SASAIMA',
        'SESQUILE',
        'SIBATE',
        'SILVANIA',
        'SIMIJACA',
        'SOACHA',
        'SOPO',
        'SUBACHOQUE',
        'SUESCA',
        'SUPATA',
        'SUSA',
        'SUTATAUSA',
        'TABIO',
        'TAUSA',
        'TENA',
        'TENJO',
        'TIBACUY',
        'TIBIRITA',
        'TOCAIMA',
        'TOCANCIPA',
        'TOPAIPI',
        'UBALA',
        'UBAQUE',
        'VILLA DE SAN DIEGO DE UBATE',
        'UNE',
        'UTICA',
        'VERGARA',
        'VIANI',
        'VILLAGOMEZ',
        'VILLAPINZON',
        'VILLETA',
        'VIOTA',
        'YACOPI',
        'ZIPACON',
        'ZIPAQUIRA',
        'QUIBDO',
        'ACANDI',
        'ALTO BAUDO',
        'ATRATO',
        'BAGADO',
        'BAHIA SOLANO',
        'BAJO BAUDO',
        'BOJAYA',
        'EL CANTON DEL SAN PABLO',
        'CARMEN DEL DARIEN',
        'CERTEGUI',
        'CONDOTO',
        'EL CARMEN DE ATRATO',
        'EL LITORAL DEL SAN JUAN',
        'ISTMINA',
        'JURADO',
        'LLORO',
        'MEDIO ATRATO',
        'MEDIO BAUDO',
        'MEDIO SAN JUAN',
        'NOVITA',
        'NUQUI',
        'RIO IRO',
        'RIO QUITO',
        'RIOSUCIO',
        'SAN JOSE DEL PALMAR',
        'SIPI',
        'TADO',
        'UNGUIA',
        'UNION PANAMERICANA',
        'NEIVA',
        'ACEVEDO',
        'AGRADO',
        'AIPE',
        'ALGECIRAS',
        'ALTAMIRA',
        'BARAYA',
        'CAMPOALEGRE',
        'COLOMBIA',
        'ELIAS',
        'GARZON',
        'GIGANTE',
        'GUADALUPE',
        'HOBO',
        'IQUIRA',
        'ISNOS',
        'LA ARGENTINA',
        'LA PLATA',
        'NATAGA',
        'OPORAPA',
        'PAICOL',
        'PALERMO',
        'PALESTINA',
        'PITAL',
        'PITALITO',
        'RIVERA',
        'SALADOBLANCO',
        'SAN AGUSTIN',
        'SANTA MARIA',
        'SUAZA',
        'TARQUI',
        'TESALIA',
        'TELLO',
        'TERUEL',
        'TIMANA',
        'VILLAVIEJA',
        'YAGUARA',
        'RIOHACHA',
        'ALBANIA',
        'BARRANCAS',
        'DIBULLA',
        'DISTRACCION',
        'EL MOLINO',
        'FONSECA',
        'HATONUEVO',
        'LA JAGUA DEL PILAR',
        'MAICAO',
        'MANAURE',
        'SAN JUAN DEL CESAR',
        'URIBIA',
        'URUMITA',
        'VILLANUEVA',
        'SANTA MARTA',
        'ALGARROBO',
        'ARACATACA',
        'ARIGUANI',
        'CERRO SAN ANTONIO',
        'CHIBOLO',
        'CIENAGA',
        'CONCORDIA',
        'EL BANCO',
        'EL PIÑON',
        'EL RETEN',
        'FUNDACION',
        'GUAMAL',
        'NUEVA GRANADA',
        'PEDRAZA',
        'PIJIÑO DEL CARMEN',
        'PIVIJAY',
        'PLATO',
        'PUEBLOVIEJO',
        'REMOLINO',
        'SABANAS DE SAN ANGEL',
        'SALAMINA',
        'SAN SEBASTIAN DE BUENAVISTA',
        'SAN ZENON',
        'SANTA ANA',
        'SANTA BARBARA DE PINTO',
        'SITIONUEVO',
        'TENERIFE',
        'ZAPAYAN',
        'ZONA BANANERA',
        'VILLAVICENCIO',
        'ACACIAS',
        'BARRANCA DE UPIA',
        'CABUYARO',
        'CASTILLA LA NUEVA',
        'CUBARRAL',
        'CUMARAL',
        'EL CALVARIO',
        'EL CASTILLO',
        'EL DORADO',
        'FUENTE DE ORO',
        'GRANADA',
        'GUAMAL',
        'MAPIRIPAN',
        'MESETAS',
        'LA MACARENA',
        'URIBE',
        'LEJANIAS',
        'PUERTO CONCORDIA',
        'PUERTO GAITAN',
        'PUERTO LOPEZ',
        'PUERTO LLERAS',
        'PUERTO RICO',
        'RESTREPO',
        'SAN CARLOS DE GUAROA',
        'SAN JUAN DE ARAMA',
        'SAN JUANITO',
        'SAN MARTIN',
        'VISTAHERMOSA',
        'PASTO',
        'ALBAN',
        'ALDANA',
        'ANCUYA',
        'ARBOLEDA',
        'BARBACOAS',
        'BELEN',
        'BUESACO',
        'COLON',
        'CONSACA',
        'CONTADERO',
        'CORDOBA',
        'CUASPUD',
        'CUMBAL',
        'CUMBITARA',
        'CHACHAGsI',
        'EL CHARCO',
        'EL PEÑOL',
        'EL ROSARIO',
        'EL TABLON DE GOMEZ',
        'EL TAMBO',
        'FUNES',
        'GUACHUCAL',
        'GUAITARILLA',
        'GUALMATAN',
        'ILES',
        'IMUES',
        'IPIALES',
        'LA CRUZ',
        'LA FLORIDA',
        'LA LLANADA',
        'LA TOLA',
        'LA UNION',
        'LEIVA',
        'LINARES',
        'LOS ANDES',
        'MAGsI',
        'MALLAMA',
        'MOSQUERA',
        'NARIÑO',
        'OLAYA HERRERA',
        'OSPINA',
        'FRANCISCO PIZARRO',
        'POLICARPA',
        'POTOSI',
        'PROVIDENCIA',
        'PUERRES',
        'PUPIALES',
        'RICAURTE',
        'ROBERTO PAYAN',
        'SAMANIEGO',
        'SANDONA',
        'SAN BERNARDO',
        'SAN LORENZO',
        'SAN PABLO',
        'SAN PEDRO DE CARTAGO',
        'SANTA BARBARA',
        'SANTACRUZ',
        'SAPUYES',
        'TAMINANGO',
        'TANGUA',
        'SAN ANDRES DE TUMACO',
        'TUQUERRES',
        'YACUANQUER',
        'CUCUTA',
        'ABREGO',
        'ARBOLEDAS',
        'BOCHALEMA',
        'BUCARASICA',
        'CACOTA',
        'CACHIRA',
        'CHINACOTA',
        'CHITAGA',
        'CONVENCION',
        'CUCUTILLA',
        'DURANIA',
        'EL CARMEN',
        'EL TARRA',
        'EL ZULIA',
        'GRAMALOTE',
        'HACARI',
        'HERRAN',
        'LABATECA',
        'LA ESPERANZA',
        'LA PLAYA',
        'LOS PATIOS',
        'LOURDES',
        'MUTISCUA',
        'OCAÑA',
        'PAMPLONA',
        'PAMPLONITA',
        'PUERTO SANTANDER',
        'RAGONVALIA',
        'SALAZAR',
        'SAN CALIXTO',
        'SAN CAYETANO',
        'SANTIAGO',
        'SARDINATA',
        'SILOS',
        'TEORAMA',
        'TIBU',
        'TOLEDO',
        'VILLA CARO',
        'VILLA DEL ROSARIO',
        'ARMENIA',
        'BUENAVISTA',
        'CALARCA',
        'CIRCASIA',
        'CORDOBA',
        'FILANDIA',
        'GENOVA',
        'LA TEBAIDA',
        'MONTENEGRO',
        'PIJAO',
        'QUIMBAYA',
        'SALENTO',
        'PEREIRA',
        'APIA',
        'BALBOA',
        'BELEN DE UMBRIA',
        'DOSQUEBRADAS',
        'GUATICA',
        'LA CELIA',
        'LA VIRGINIA',
        'MARSELLA',
        'MISTRATO',
        'PUEBLO RICO',
        'QUINCHIA',
        'SANTA ROSA DE CABAL',
        'SANTUARIO',
        'BUCARAMANGA',
        'AGUADA',
        'ALBANIA',
        'ARATOCA',
        'BARBOSA',
        'BARICHARA',
        'BARRANCABERMEJA',
        'BETULIA',
        'BOLIVAR',
        'CABRERA',
        'CALIFORNIA',
        'CAPITANEJO',
        'CARCASI',
        'CEPITA',
        'CERRITO',
        'CHARALA',
        'CHARTA',
        'CHIMA',
        'CHIPATA',
        'CIMITARRA',
        'CONCEPCION',
        'CONFINES',
        'CONTRATACION',
        'COROMORO',
        'CURITI',
        'EL CARMEN DE CHUCURI',
        'EL GUACAMAYO',
        'EL PEÑON',
        'EL PLAYON',
        'ENCINO',
        'ENCISO',
        'FLORIAN',
        'FLORIDABLANCA',
        'GALAN',
        'GAMBITA',
        'GIRON',
        'GUACA',
        'GUADALUPE',
        'GUAPOTA',
        'GUAVATA',
        'GsEPSA',
        'HATO',
        'JESUS MARIA',
        'JORDAN',
        'LA BELLEZA',
        'LANDAZURI',
        'LA PAZ',
        'LEBRIJA',
        'LOS SANTOS',
        'MACARAVITA',
        'MALAGA',
        'MATANZA',
        'MOGOTES',
        'MOLAGAVITA',
        'OCAMONTE',
        'OIBA',
        'ONZAGA',
        'PALMAR',
        'PALMAS DEL SOCORRO',
        'PARAMO',
        'PIEDECUESTA',
        'PINCHOTE',
        'PUENTE NACIONAL',
        'PUERTO PARRA',
        'PUERTO WILCHES',
        'RIONEGRO',
        'SABANA DE TORRES',
        'SAN ANDRES',
        'SAN BENITO',
        'SAN GIL',
        'SAN JOAQUIN',
        'SAN JOSE DE MIRANDA',
        'SAN MIGUEL',
        'SAN VICENTE DE CHUCURI',
        'SANTA BARBARA',
        'SANTA HELENA DEL OPON',
        'SIMACOTA',
        'SOCORRO',
        'SUAITA',
        'SUCRE',
        'SURATA',
        'TONA',
        'VALLE DE SAN JOSE',
        'VELEZ',
        'VETAS',
        'VILLANUEVA',
        'ZAPATOCA',
        'SINCELEJO',
        'BUENAVISTA',
        'CAIMITO',
        'COLOSO',
        'COROZAL',
        'COVEÑAS',
        'CHALAN',
        'EL ROBLE',
        'GALERAS',
        'GUARANDA',
        'LA UNION',
        'LOS PALMITOS',
        'MAJAGUAL',
        'MORROA',
        'OVEJAS',
        'PALMITO',
        'SAMPUES',
        'SAN BENITO ABAD',
        'SAN JUAN DE BETULIA',
        'SAN MARCOS',
        'SAN ONOFRE',
        'SAN PEDRO',
        'SAN LUIS DE SINCE',
        'SUCRE',
        'SANTIAGO DE TOLU',
        'TOLU VIEJO',
        'IBAGUE',
        'ALPUJARRA',
        'ALVARADO',
        'AMBALEMA',
        'ANZOATEGUI',
        'ARMERO',
        'ATACO',
        'CAJAMARCA',
        'CARMEN DE APICALA',
        'CASABIANCA',
        'CHAPARRAL',
        'COELLO',
        'COYAIMA',
        'CUNDAY',
        'DOLORES',
        'ESPINAL',
        'FALAN',
        'FLANDES',
        'FRESNO',
        'GUAMO',
        'HERVEO',
        'HONDA',
        'ICONONZO',
        'LERIDA',
        'LIBANO',
        'MARIQUITA',
        'MELGAR',
        'MURILLO',
        'NATAGAIMA',
        'ORTEGA',
        'PALOCABILDO',
        'PIEDRAS',
        'PLANADAS',
        'PRADO',
        'PURIFICACION',
        'RIOBLANCO',
        'RONCESVALLES',
        'ROVIRA',
        'SALDAÑA',
        'SAN ANTONIO',
        'SAN LUIS',
        'SANTA ISABEL',
        'SUAREZ',
        'VALLE DE SAN JUAN',
        'VENADILLO',
        'VILLAHERMOSA',
        'VILLARRICA',
        'CALI',
        'ALCALA',
        'ANDALUCIA',
        'ANSERMANUEVO',
        'ARGELIA',
        'BOLIVAR',
        'BUENAVENTURA',
        'GUADALAJARA DE BUGA',
        'BUGALAGRANDE',
        'CAICEDONIA',
        'CALIMA',
        'CANDELARIA',
        'CARTAGO',
        'DAGUA',
        'EL AGUILA',
        'EL CAIRO',
        'EL CERRITO',
        'EL DOVIO',
        'FLORIDA',
        'GINEBRA',
        'GUACARI',
        'JAMUNDI',
        'LA CUMBRE',
        'LA UNION',
        'LA VICTORIA',
        'OBANDO',
        'PALMIRA',
        'PRADERA',
        'RESTREPO',
        'RIOFRIO',
        'ROLDANILLO',
        'SAN PEDRO',
        'SEVILLA',
        'TORO',
        'TRUJILLO',
        'TULUA',
        'ULLOA',
        'VERSALLES',
        'VIJES',
        'YOTOCO',
        'YUMBO',
        'ZARZAL',
        'ARAUCA',
        'ARAUQUITA',
        'CRAVO NORTE',
        'FORTUL',
        'PUERTO RONDON',
        'SARAVENA',
        'TAME',
        'YOPAL',
        'AGUAZUL',
        'CHAMEZA',
        'HATO COROZAL',
        'LA SALINA',
        'MANI',
        'MONTERREY',
        'NUNCHIA',
        'OROCUE',
        'PAZ DE ARIPORO',
        'PORE',
        'RECETOR',
        'SABANALARGA',
        'SACAMA',
        'SAN LUIS DE PALENQUE',
        'TAMARA',
        'TAURAMENA',
        'TRINIDAD',
        'VILLANUEVA',
        'MOCOA',
        'COLON',
        'ORITO',
        'PUERTO ASIS',
        'PUERTO CAICEDO',
        'PUERTO GUZMAN',
        'LEGUIZAMO',
        'SIBUNDOY',
        'SAN FRANCISCO',
        'SAN MIGUEL',
        'SANTIAGO',
        'VALLE DEL GUAMUEZ',
        'VILLAGARZON',
        'SAN ANDRES',
        'PROVIDENCIA',
        'LETICIA',
        'EL ENCANTO',
        'LA CHORRERA',
        'LA PEDRERA',
        'LA VICTORIA',
        'MIRITI - PARANA',
        'PUERTO ALEGRIA',
        'PUERTO ARICA',
        'PUERTO NARIÑO',
        'PUERTO SANTANDER',
        'TARAPACA',
        'INIRIDA',
        'BARRANCO MINAS',
        'MAPIRIPANA',
        'SAN FELIPE',
        'PUERTO COLOMBIA',
        'LA GUADALUPE',
        'CACAHUAL',
        'PANA PANA',
        'MORICHAL',
        'SAN JOSE DEL GUAVIARE',
        'CALAMAR',
        'EL RETORNO',
        'MIRAFLORES',
        'MITU',
        'CARURU',
        'PACOA',
        'TARAIRA',
        'PAPUNAUA',
        'YAVARATE',
        'PUERTO CARREÑO',
        'LA PRIMAVERA',
        'SANTA ROSALIA',
        'CUMARIBO',
    ],
    'festivos' => [
            '2023-01-01',
            '2023-01-09',
            '2023-03-20',
            '2023-04-06',
            '2023-04-07',
            '2023-05-01',
            '2023-05-22',
            '2023-06-12',
            '2023-06-19',
            '2023-07-03',
            '2023-07-20',
            '2023-08-07',
            '2023-08-21',
            '2023-10-16',
            '2023-11-06',
            '2023-11-13',
            '2023-12-08',
            '2023-12-25',
            '2024-01-01',
            '2024-01-08',
            '2024-03-25',
            '2024-03-28',
            '2024-03-29',
            '2024-05-01',
            '2024-05-13',
            '2024-06-03',
            '2024-06-10',
            '2024-07-01',
            '2024-07-20',
            '2024-08-07',
            '2024-08-19',
            '2024-10-14',
            '2024-11-04',
            '2024-11-11',
            '2024-12-08',
            '2024-12-25'
    ]
]

?>