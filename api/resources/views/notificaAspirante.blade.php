<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
</body>
    <table align="center" width="700px" style="border:1px solid black;padding: 11px;background-color:#ffffff">
        @if($datos['accion'] == 'entrevista')
            <tr style="background-color:#225A3F;color:#FFFFFF">
                <td colspan="3" style="text-align: center"><b>:::: ENTREVISTA PROGRAMADA ::::</b></td>
            </tr>
            <tr>
                <td  style="text-align: left"><b>Aspirante</b></td>
                <td colspan="2" style="text-align: left">{{$datos['documento_identidad']}} - {{$datos['nombre_completo']}}</td>
            </tr>
            <tr>
                <td  style="text-align: left"><b>Lugar</b></td>
                <td colspan="2" style="text-align: left">{{$datos['lugar_pruebas']}}</td>
            </tr>
            <tr>
                <td  style="text-align: left"><b>Dirección</b></td>
                <td colspan="2" style="text-align: left">{{$datos['direccion_pruebas']}}</td>
            </tr>
            <tr>
                <td  style="text-align: left"><b>Fecha y hora</b></td>
                <td colspan="2" style="text-align: left">{{$datos['entrevista_fecha_cita']}} - {{$datos['entrevista_hora_cita']}} </td>
            </tr>
            <tr>
                <td  style="text-align: left"><b>Entrevistado por</b></td>
                <td colspan="2" style="text-align: left">{{$datos['entrevista_entrevistador']}}</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left">Se recomienda estar 15 minutos antes de la cita.</td>
            </tr>
        @endif
    </table>
</body>
</html>
