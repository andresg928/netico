<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
<body>
<br>
  <table width="100%" style="width:100%" border="0" >
      @if( $pesaje[0]['tipo_producto']=='trigo' )
        <tr>
          <td class="titulos">FACTURA DE VENTA</td>
          <td colspan="2" class="titulos2" align="right">{{$pesaje[0]['num_dcto']}}</td>
        </tr>
      @elseif( $pesaje[0]['tipo_producto'] == 'producto_terminado' )
        <tr>
            <td class="titulos">FACTURA DE VENTA</td>
            <td colspan="2" class="titulos2" align="right">{{$pesaje[0]['num_dcto']}}</td>
        </tr>
      @endif
      <tr>
          <td colspan="3"></td>
      </tr>
      <tr>
        <td colspan="3" class="letra_normal">{{$pesaje[0]['empresa']}}</td>
      </tr>
      <tr>
        <td colspan="3" style="font-size:10px;border-bottom:0.5pt solid black">NIT : {{$pesaje[0]['nit']}}<br>{{$pesaje[0]['direccion']}} <br> {{$pesaje[0]['telefono']}}</td>
      </tr>

      <tr>
        <td style="font-size:12px">Placa No : </td>
        <td colspan="2" style="font-size:20px;font-weight:bold;">{{$pesaje[0]['placa']}}</td>
      </tr>

      <tr>
        <td style="font-size:12px" align="left"></td>
        <td style="font-size:12px" align="left">Valor : </td>
        <td style="font-size:13px" align="right">{{number_format($pesaje[0]['subtotal'], 0, '.', ',')}}</td>
      </tr>

      <tr>
        <td style="font-size:12px" align="left"></td>
        <td style="font-size:12px" align="left">Iva : </td>
        <td style="font-size:13px" align="right">{{number_format($pesaje[0]['iva'], 0, '.', ',')}}</td>
      </tr>

      <tr >
        <td  align="left" style="border-bottom:0.5pt solid black"></td>
        <td  align="left" style="font-size:12px;border-bottom:0.5pt solid black">Total : </td>
        <td style="font-size:14px;font-weight:bold;border-bottom:0.5pt solid black" align="right">{{number_format($pesaje[0]['total'], 0, '.', ',')}}</td>
      </tr>

      <tr>
        <td colspan="3" style="border-bottom:0.5pt solid black">
          <table align="center" border="0" width="100%">
            <tr >
              <td></td>
              <td><b style="font-size:11px">ENTRADA</b></td>
              <td colspan="2"><b style="font-size:11px">SALIDA</b></td>
            </tr>

            <tr class="letra_normal">
              <td >Fecha</td>
              <td>{{$pesaje[0]['fecha_actual']}}</td>
              <td colspan="2">{{$pesaje[0]['fecha_trans_2']}}</td>
            </tr>

            <tr class="letra_normal">
              <td class="letra_normal">Hora</td>
              <td>{{$pesaje[0]['hora_trans_1']}}</td>
              <td colspan="2">{{$pesaje[0]['hora_trans_2']}}</td>
            </tr>

            <tr class="letra_normal">
              <td class="letra_normal">Peso</td>
              <td>{{number_format($pesaje[0]['peso_inicial'], 0, '.', ',')}} Kg</td>
              <td colspan="2">{{number_format($pesaje[0]['peso_final'], 0, '.', ',')}} Kg</td>
            </tr>

            <tr>
              <td class="letra_normal">Neto</td>
              <td colspan="3"><b>{{number_format($pesaje[0]['peso_neto'], 0, '.', ',')}} Kg</b></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="3" style="font-size:11px;">Girón - Santander, {{$pesaje[0]['fecha_impresion']}}</td>
      </tr>

      <tr style="border-bottom:0.5pt solid black">
        <td style="font-size:10px;" align="left">Imprimió :</td>
        <td colspan="2" style="font-size:10px;" align="left">{{$pesaje[0]['usuario']}}</td>
      </tr>

      @if($pesaje[0]['precio'] != 0)
        <tr>
          <td colspan="3" style="font-size:10px;" align="center">Resolución número</td>
        </tr>

        <tr>
          <td colspan="3" style="font-size:10px;" align="center">{{$pesaje[0]['resolucion']}} {{$pesaje[0]['fecha_inicio']}} - {{$pesaje[0]['fecha_final']}}</td>
        </tr>

        <tr>
          <td colspan="3" style="font-size:10px;" align="center">Autorización de Numeración</td>
        </tr>

        <tr>
          <td colspan="3" style="font-size:10px;" align="center">{{$pesaje[0]['rango_inicio']}} - {{$pesaje[0]['rango_final']}}</td>
        </tr>
      @endif
      <tr>
          <td colspan="3"><br><br></td>
      </tr>

      <tr >
        <td  align="center" style="font-size:10px;">_____________________</td>
        <td colspan="2" align="center" style="font-size:10px;">_____________________</td>
      </tr>

      <tr >
        <td  align="center" style="font-size:10px;">Firma operador báscula</td>
        <td colspan="2"   align="center" style="font-size:10px;">Firma conductor</td>
      </tr>

      @if($pesaje[0]['reimprimir']=='si')
        <tr>
          <td colspan="3" style="font-size:9px" align="center">----------- COPIA -------------</td>
        </tr>
      @endif

    </table></body></body>
</html>

<style>
    @page { size: 14cm 7cm landscape; }
    html { margin: 3px;font-family: Arial, Helvetica, sans-serif;}
    .titulos {
      font-size:10px;
      font-weight: bold; 
    }
    .titulos2 {
      font-size:14px;
      font-weight: bold; 
    }
    .letra_normal {
      font-size:12px;
    }
</style>