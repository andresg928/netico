<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
</body>
<table align="center" width="700px" style="border:1px solid black;padding: 11px;background-color:#225A40">
    @if($datos['accion'] == 'cerrar_despacho')
        <tr style="background-color:#ffffff;color:#067603">
            <td colspan="3" style="text-align: center"><b>:::: POR AUTORIZAR ::::</b></td>
        </tr>
    @endif
    @if($datos['accion'] == 'autorizar')
        <tr style="background-color:#ffffff;color:#067603">
            <td colspan="3" style="text-align: center"><b>:::::::::::::::: DESPACHO AUTORIZADO ::::::::::::::::</b></td>
        </tr>
    @endif
    @if($datos['accion'] == 'notifica_certificado_calidad')
        <tr style="background-color:#ffffff;color:#067603">
            <td colspan="3" style="text-align: center"><b>:::: SOLICITUD CERTIFICADO DE CALIDAD ::::</b></td>
        </tr>
        @if($datos['observaciones_certificado'])
            <tr style="background-color:#ffffff;color:#067603">
                <td colspan="3" style="text-align: center">"{{$datos['observaciones_certificado']}}"</td>
            </tr>
        @endif
    @endif
    <tr style="color:#ffffff">
        <td><b>Despacho # </b>  {{$datos['consecutivo']}}</td>
        <td><b>Fecha </b>  {{$datos['created_at']}}</td>
        <td><b>Conductor </b> {{$datos['placa']}} - {{$datos['conductor']}}</td>
    </tr>
    <tr style="color:#ffffff">
        <td><b>Total Kg </b>  {{$datos['total_kilos']}}</td>
        <td ><b>Vendedor </b>  {{$datos['nombre_vendedor']}}</td>
        <td ><b>Fecha aprox. cargue </b>  {{$datos['fecha_entrega']}}</td>
    </tr>
</table><br>
@foreach ($datos['info_pedidos']['pedidos_encabezado'] as $pedido)
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="700px" style="border:1px solid black;">
        <tr style="background-color:#F1D281">
            <td colspan="3">&nbsp;&nbsp;&nbsp;<b>CLIENTE&nbsp;&nbsp;</b>  {{ $pedido->nit }} - {{ $pedido->cliente }} &nbsp; ({{ $pedido->ciudad }}) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Total Kg </b> {{number_format($pedido->total_kilos, 0, ',','.')}} <b>&nbsp;&nbsp;&nbsp; Valor Total </b>{{number_format($pedido->valor_total , 0, ',','.')}}</td>
        </tr>
        @if($pedido->observaciones)
        <tr style="background-color:#F1D281">
            <td colspan="3">&nbsp;&nbsp;&nbsp;<b>Observaciones&nbsp;&nbsp;</b>{{$pedido->observaciones}}</td>
        </tr>
        @endif
        @foreach ($datos['info_pedidos']['pedidos_encabezado'][$loop->index]['pedidos_detalle'] as $pedido_detalle)
            <tr style="height:35px">
                <td width="80">&nbsp;&nbsp;&nbsp;(<b>{{$pedido_detalle->cantidad}}</b>) {{$pedido_detalle->unidad_medida}}&nbsp;&nbsp;&nbsp;</td>
                <td width="380">{{$pedido_detalle->cod_producto}} - {{$pedido_detalle->nom_producto}}</td>
                <td width="240" align="left"><b>$ Unitario&nbsp;&nbsp;</b>{{number_format($pedido_detalle->valor, 0, ',','.')}}&nbsp;&nbsp;&nbsp;<b>$ Total &nbsp;&nbsp;</b>{{number_format($pedido_detalle->valor * $pedido_detalle->cantidad, 0, ',','.')}}</td>
            </tr>
        @endforeach
    </table><br>
@endforeach
</body>
</html>
