<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
</body>
<table align="center" width="700px" style="border:1px solid black;padding: 20px;">
    <tr style="background-color:#ffffff;color:#067603">
        <td style="text-align: center"><b>GESTIÓN ENTREGA DE MERCANCÍA</b></td>
    </tr>
    <tr >
        <td>El conductor <b>{{$datos['conductor']}} ({{$datos['placa']}})</b> reportó {{$datos['novedad'] == 'si' ? 'CON novedad':'SIN novedad'}} el despacho No. <b style="color:#ff0000">{{$datos['consecutivo']}}</b> </td>
    </tr>
    <tr >
        <td><b>Fecha y hora de la gestión : </b> {{$datos['fecha_hora_novedad']}}</td>
    </tr>
    @if($datos['novedad'] == 'si')
    <tr>
        <td><b>Novedad registrada :</b></td>
    </tr>
    <tr>
        <td>{{$datos['observaciones_novedad']}}</td>
    </tr>
    @endif
</table>
</body>
</html>
