<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
</body>
<table align="center" width="600px">
    <tr >
        <td>
            <img src="https://drive.google.com/uc?export=view&id=1wJV5-yVJYwzXl-U8c2WX_VsaAepDAzyE" alt="Imagen cumple" width="600" height="450">
        </td>
    </tr>
    <tr align="center">
        <td style="font-size:16px"><b>¡Feliz cumpleaños {{$datos['nombre_cumple']}} le desea todo el equipo de trabajo en ICOHARINAS!</b><br></td>
    </tr>
    <tr align="justify">
        <td style="font-size:14px">
            En este día especial, queremos tomar un momento para celebrar no solo el crecimiento de un año más en sus vidas, sino también el crecimiento continuo que aportan a nuestra empresa día tras día. Cada uno de ustedes es una parte fundamental de nuestro éxito y nos sentimos afortunados de contar con un equipo tan dedicado y talentoso.<br><br>
            Esperamos que este día esté lleno de alegría, amor y momentos especiales con sus seres queridos. En ICOHARINAS, valoramos y apreciamos su arduo trabajo, su compromiso y su pasión por lo que hacen. ¡Nuestros logros son posibles gracias a su dedicación!<br><br>
            Que este nuevo año que se inicia en sus vidas esté lleno de salud, prosperidad y éxito tanto en el ámbito personal como profesional. Esperamos seguir compartiendo muchos más cumpleaños juntos mientras continuamos creciendo y alcanzando metas en equipo.<br><br>
            Disfruten de su día al máximo y no olviden tomar un momento para celebrar sus propios logros y éxitos.<br><br>
            ¡Feliz cumpleaños, compañero!<br><br>
            Con gratitud,<br><br>
            ICOHARINAS SAS
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://drive.google.com/uc?export=view&id=1EHCEU4nktPCKiIFdrxnbkFn4egnqLhX6" alt="Imagen cumple banner" width="600" height="283">
        </td>
    </tr>
</table>
</body>
</html>
