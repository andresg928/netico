<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
</body>
    <table align="left" width="800px" style="border:0px solid black;padding: 11px;background-color:#FFFFFF">
        <tr>
            <td>
                Estimados,<br><br>Se adjunta informe de clientes que no se reflejó movimiento de venta en el mes de <b>{{ strtolower($datos['mes_actual']) }}</b>, pero si en el mes de <b>{{strtolower($datos['mes_anterior'])}}</b>.<br><br>
                <b>Importante:</b> La información está filtrada a venta superior o igual a un millón de pesos (1.000.000).<br><br>
                <em>Por favor no responder este mensaje ya que es automático.</em>
            </td>
        </tr>
    </table>
</body>
</html>
