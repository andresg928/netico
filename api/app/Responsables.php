<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsables extends Model
{
    protected $primaryKey = 'id_registro';
    protected $keyType = 'string';
}
