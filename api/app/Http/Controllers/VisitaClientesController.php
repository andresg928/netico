<?php

namespace App\Http\Controllers;

use App\VisitaClientes;
use App\Pedido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;

class VisitaClientesController extends Controller
{
    public function registrarVisita(Request $request){

        $visita = new VisitaClientes();
        $visita->id = Uuid::generate()->string;
        $visita->fecha = $request->fecha_visita;
        $visita->cod_vendedor = JWTAuth::user()->vendedor;
        $visita->nom_vendedor = JWTAuth::user()->name;
        $visita->nit_cliente = $request->nit;
        $visita->nom_cliente = $request->cliente;
        $visita->presencial = $request->presencial;
        $visita->operacion = $request->operacion;
        $visita->comentarios = $request->comentarios;
        $visita->valor_cobro = $request->cobro;
        $visita->usu_registro = JWTAuth::user()->vendedor;
        $visita->fec_registro = date('Y-m-d H:i:s');
        $saved = $visita->save();

        if($saved){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Visita registrada exitosamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al registrar la visita'
            ],200);           
        }
    }

    public function isVendedor($codVen){

        $ctrl_auth = new AuthController();
        $info = $ctrl_auth->obtenerVendedores($codVen);
        if(count($info)>0){
            return true;
        }
        return false;
    }

    public function gestionarVisitaPedido($data){

        //Actualiza gestion de visita al registrar el pedido
        $nit_aux = $data['nit'];
        $nit_old = $data['nit_old'];
        $cliente = $data['nom_cliente'];
        $fec_aux = $data['fecha'];
        $modifica = $data['modifica'];

        if (stripos($nit_old, 'S') !== false) {
            $nit_aux = $this->obtenerIdAlternoSucursal($nit_aux);
        }

        $visita = VisitaClientes::select('*', DB::raw('id as id_visita'))->where('nit_cliente', $nit_old)->where('fecha', $fec_aux)->where('deleted', '0')->where('operacion', 'pedido')->first();

        if($visita){
            $visita->fec_modifica = $data['fec_modifica'];
            $visita->usu_modifica = $data['usu_modifica'];
            $visita->estado = 'gestionado';
            $visita->id_pedido = $data['id_pedido'];
            $visita->comentarios = 'Comentario automático: Se encontró el pedido y se cruzó para quedar en estado gestionado.';
            $visita->save();
        }
    }

    public function modificarVisita(Request $request){

        $visita = VisitaClientes::find($request->id_visita);
        $visita->usu_modifica = JWTAuth::user()->vendedor;
        $visita->nom_vendedor = JWTAuth::user()->name;
        $visita->fec_modifica = date('Y-m-d H:i:s');
        $respuesta = 'Visita actualizada exitosamente';
        if($request->eliminar == 'si'){
            $visita->deleted = '1';
            $respuesta = 'Visita eliminada exitosamente';
        }else{
            $visita->nit_cliente = $request->nit;
            $visita->nom_cliente = $request->cliente;
            $visita->presencial = $request->presencial;
            $visita->operacion = $request->operacion;
            $visita->comentarios = $request->comentarios;
            $visita->valor_cobro = $request->cobro;
        }

        $saved = $visita->save();

        if($saved){
            return response()->json([
                "estado" => "ok",
                "mensaje" => $respuesta
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al actualizar la visita'
            ],200);           
        }
    }

    public function gestionarVisita(Request $request){

        $visita = VisitaClientes::find($request->id_visita);
        $visita->usu_modifica = JWTAuth::user()->vendedor;
        $visita->fec_modifica = date('Y-m-d H:i:s');
        $visita->estado = $request->estado;
        $visita->comentarios = $request->comentarios;
        $visita->valor_cobro = $request->cobro;
        $saved = $visita->save();

        if($saved){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Gestionado correctamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al actualizar la visita'
            ],200);           
        }
    }

    public function obtenerVisitas(Request $request){

        $ctrl_coment = new ComentarioController();
        $info = array();
        $clientes = array();
        if($request->tipo_vista == 'vendedor'){
            $visitas = VisitaClientes::select('*', DB::raw('id as id_visita'))->where('deleted', '0')->where('cod_vendedor', JWTAuth::user()->vendedor)
                                    ->where('nom_cliente', 'like', '%'.$request->cliente.'%');
                                    if($request->estado!= null){
                                        $visitas->where('estado', $request->estado);
                                    }
                                    if($request->filtro_fecha == 'hoy'){
                                        $visitas->whereRaw("DATE_FORMAT(fecha,'%Y-%m-%d') BETWEEN '".date('Y-m-d')."' and '".date('Y-m-d')."'");
                                    }
                                    $visitas->orderBy('fecha', 'DESC');
                                    $visitas = $visitas->paginate(5);

            for ($i=0; $i < count($visitas); $i++) { 
                $visitas[$i]['fecha_texto'] = $this->showDate($visitas[$i]['fecha']);
            }

            return $visitas;

        } else if($request->tipo_vista == 'jefe_vendedores'){

            $fecha = $ctrl_coment->obtenerInicioFinMes($request->mes,$request->anio);
            $fec_ini=$fecha[0];
            $fec_fin=$fecha[1];

            $vendedor = $request->vendedor;
            //Obtener clientes por cada vendedor
            $ctrl_despacho = new DespachoController();
            if($vendedor){
                $clientes = $ctrl_despacho->obtenerClientesxVendedor($vendedor);
            }else{
                $clientes = $ctrl_despacho->obtenerClientesxVendedor('');
            }

            $visitas = VisitaClientes::select('*', DB::raw('id as id_visita'))->where('deleted', '0')
                                    ->orderBy('fecha', 'DESC');
                                    if($vendedor){
                                        $visitas->where('cod_vendedor', $vendedor);
                                    }
                                    $visitas->whereRaw("DATE_FORMAT(fecha,'%Y-%m-%d') BETWEEN '".$fec_ini."' and '".$fec_fin."'");

            $visitas = $visitas->get();

            // Nit con pedidos por fecha
            $ped = $this->obtenerPedidosFecha($fecha);

            for ($i=0; $i < count($visitas); $i++) {

                $compara = $visitas[$i]['nit_cliente'].'-'.$visitas[$i]['fecha'];
                if(in_array($compara, $ped) && $visitas[$i]['operacion'] == 'pedido' && $visitas[$i]['id_pedido'] == null){
                    $visitas[$i]['estado'] = $this->convertirCampoTexto('estado', 'gestionado');
                    $visitas[$i]['comentarios'] = 'Comentario automático: Se encontró el pedido y se cruzó para quedar en estado gestionado.';
                    $visitas[$i]['fec_modifica'] = '';
                }else{
                    $visitas[$i]['estado'] = $this->convertirCampoTexto('estado', $visitas[$i]['estado']);
                }

                $visitas[$i]['fecha_texto'] = $this->showDate($visitas[$i]['fecha']);
                $visitas[$i]['fecha_calendario'] = $this->fechaVueCalendar($visitas[$i]['fecha']);
                $visitas[$i]['presencial'] = $this->convertirCampoTexto('presencial', $visitas[$i]['presencial']);
                $visitas[$i]['operacion'] = $this->convertirCampoTexto('operacion', $visitas[$i]['operacion']);
                
            }
        }

        $info['visitas'] = $visitas;
        $info['clientes'] = $clientes;
        $info['anios'] = $this->obtenerAniosRegistro();
        return $info;
    }

    public function obtenerPedidosFecha($fecha){
        $pedidos = Pedido::select('nit', DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as fecha"))->where('deleted', '0')
                            ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '".$fecha[0]."' and '".$fecha[1]."'")
                            ->distinct()->get();

        // Crear un array de combinaciones 'nit-fecha' para $pedidos
        $ped_unif = $pedidos->map(function($item) {

            // Si es sucursal que obtenga el nit sin la S
            if (stripos($item->nit, 'S') !== false) {
                $item->nit = $this->obtenerIdAlternoSucursal($item->nit);
            }

            return $item->nit . '-' . $item->fecha;
        })->toArray();

        return $ped_unif;
    }

    public function obtenerIdAlternoSucursal($nit){

        $clientes = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Id_Alterno) AS Id_Alterno"))
                                    ->where('c.Cliente', $nit)
                                    ->where('c.Habilitado', 1)
                                    ->first();
        return $clientes->Id_Alterno;
    }

    public function obtenerAniosRegistro(){
        // Extraer los años únicos del campo created_at
        $visitas = VisitaClientes::all();
        $years = $visitas->map(function ($visita) {
            return Carbon::parse($visita->fecha)->year;
        })->unique()->values()->all();

        return $years;
    }
    public function fechaVueCalendar($fecha){
        //Esto para que en el calendario cuadre por temas de zona horaria se agregó un dia
        $date = Carbon::parse($fecha);
        $newDate = $date->addDay();
        $formattedDate = $newDate->format('Y-m-d');
        return $formattedDate;
    }

    public function obtenerVisitarPorEstado(Request $request){
        $visitas = VisitaClientes::where('estado', $request->estado)->where('deleted', '0')->where('cod_vendedor', JWTAuth::user()->vendedor)->count();
        return $visitas;
    }

    public function convertirCampoTexto($nombreCampo, $valor){
        if($nombreCampo == 'presencial'){
            if($valor == 'si'){
                return 'Presencial';
            }else{
                return 'No presencial';
            }
        }else if($nombreCampo == 'operacion'){
            return ucfirst($valor);
        }else if ($nombreCampo == 'estado'){
            return ucfirst($valor);
        }
    }

    public function showDate($date){

        $date = Carbon::parse($date);
        Carbon::setLocale('es');
        $formattedDate = $date->isoFormat('D MMM YYYY');
        return $formattedDate;
    }

}
