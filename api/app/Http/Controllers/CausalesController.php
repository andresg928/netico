<?php

namespace App\Http\Controllers;

use App\causales;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use  JWTAuth;

class CausalesController extends Controller
{
    public function registrar(Request $request){

        $causa = causales::where('codigo', strtoupper($request->codigo))->where('deleted', '0')->first();
        if($causa){
            return response()->json([
                "mensaje" => '¡Error! '.strtoupper($request->nombre).' ya existe en el sistema',
                'estado' => 'error',
            ],200);              
        }

        $causal = new causales();
        $causal->id = Uuid::generate()->string;
        $causal->id_area = $request->id_area;
        $causal->codigo = strtoupper($request->codigo);
        $causal->nombre = strtoupper($request->nombre);
        $causal->usu_registra = JWTAuth::user()->vendedor;
        $saved = $causal->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Se creó correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }  
    }

    public function actualizar(Request $request){

        // dd($request);
        $causal = causales::find($request->id_registro);
        $causal->codigo = strtoupper($request->codigo);
        $causal->nombre = strtoupper($request->nombre);
        $causal->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $causal->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! información actualizada correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar la operación, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }  
    }

    public function obtenerCausales(Request $request){
        $causales = causales::select('id as id_registro', 'deleted', 'id_area', 'codigo', 'nombre')->where('deleted', '0')
                                    ->where('id_area', $request->id_area)->orderBy('codigo', 'ASC')->get();
        return $causales;
    }

    public function obtenerCausalxCodigo($codigo){
        $area = causales::select('codigo', 'nombre', 'id_area')->where('codigo', $codigo)->where('deleted', '0')->first();
        return $area;
    }
}
