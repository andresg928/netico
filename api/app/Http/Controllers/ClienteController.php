<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::select( 
            'clientes.id as id_cliente','clientes.nit','clientes.nombre_comercial', 'clientes.razon_social', 'clientes.representante',
            'clientes.telefono', 'clientes.celular', 'clientes.direccion', 'clientes.correo', 'clientes.estado'
             ,'c.codigo as cod_ciu', 'c.nombre as nom_ciu','d.nombre as nom_dep'
        )
        ->join('ciudades as c', 'c.id', '=', 'clientes.id_ciudad')
        ->join('departamentos as d', 'd.codigo', '=', 'c.cod_dep')
        ->orderBy('clientes.nombre_comercial', 'ASC')->get();
        
        return $clientes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

            $cliente = new Cliente();
            $cliente->id = Uuid::generate()->string;
            $cliente->nit = $request->input('nit');
            $cliente->nombre_comercial = $request->input('nombre_comercial');
            $cliente->razon_social = $request->input('razon_social');
            $cliente->representante = $request->input('representante');
            $cliente->telefono = $request->input('telefono');
            $cliente->celular = $request->input('celular');
            $cliente->direccion = $request->input('direccion');
            $cliente->correo = $request->input('correo');
            $cliente->id_ciudad = $request->input('ciudad');
            $cliente->estado = 'activo';
            $saved = $cliente->save();

            if($saved === true){
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "id_cliente" => $cliente->nit,
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso"
                ],500);                
            }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
