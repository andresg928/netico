<?php

namespace App\Http\Controllers;

use App\Ficho;
use Illuminate\Http\Request;

class FichoController extends Controller
{
    public function obtenerFichos(Request $request){

        $fichos = Ficho::where('deleted', '0')->orderBy('id','ASC');

                if($request->filtro == 'industrial'){
                    $fichos->where('nombre','LIKE','B%');
                }else if($request->filtro == 'subproducto'){
                    $fichos->where('nombre','LIKE','S%');
                }else if($request->filtro == 'paqueteo'){
                    $fichos->where('nombre','LIKE','P%');
                }else{
                    $fichos->where('nombre','LIKE','C%');
                }

        $fichos = $fichos->get();

        return $fichos;
    }

    public function cambiarEstado($idFicho, $idFichoOld, $accion){

        //Asigna turno normalmente
        if($idFichoOld == '' && $accion == 'add'){
            $ficho = Ficho::find($idFicho);
            $ficho->disponible = 'no';
            $ficho->save();
        }

        // Reasignar turno, habilito el old y deshabilito el new
        if($idFichoOld !='' &&  $accion == 'edit'){
            $ficho = Ficho::find($idFicho);
            $ficho->disponible = 'no';
            $ficho->save();

            $ficho_ = Ficho::find($idFichoOld);
            $ficho_->disponible = 'si';
            $ficho_->save();             
        }

        if($accion == 'deleted'){
            $ficho = Ficho::find($idFicho);
            $ficho->disponible = 'si';
            $ficho->save();           
        }
    }

    public function liberarFichos(){
        // Consulta los fichos que no están disponibles y se actulizan ya sea automatico o por acción de un botón
        $fichos = Ficho::where('disponible', 'no')->where('deleted', '0')->update(['disponible' => 'si', 'updated_at' => date("Y-m-d H:i:s")]);
        if($fichos >= 0){
            return response()->json([
                "estado" => 'ok',
                "mensaje" => 'La operación se realizó correctamente, todos los turnos quedaron disponibles.'
            ],200);

        } else {
            return response()->json([
                "estado" => 'error',
                "mensaje" => 'Ha ocurrido un error, vuelva a intentarlo'
            ],200);            
        }
    }

}
