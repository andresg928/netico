<?php

namespace App\Http\Controllers;

use App\Responsables;
use App\ResponsablesAreas;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use  JWTAuth;

class ResponsablesController extends Controller
{

    public function registrar(Request $request){

        $responsable = Responsables::where('cedula', $request->cedula)->where('deleted', '0')->first();
        if($responsable){
            return response()->json([
                "mensaje" => '¡Error! '.$request->nombre.' ya existe como responsable',
                'estado' => 'error',
            ],200);              
        }

        $responsable = new Responsables();
        $responsable->id_registro = Uuid::generate()->string;
        $responsable->cedula = $request->cedula;
        $responsable->nombre = $request->nombre;
        $responsable->usu_registra = JWTAuth::user()->vendedor;
        $saved = $responsable->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Responsable registrado correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }        
    }

    public function obtenerResponsables(Request $request){
        $responsables = Responsables::select('id_registro','cedula', 'nombre')->where('deleted', '0')->where('nombre','LIKE','%'.$request->filtro.'%')->orderBy('nombre', 'ASC')->get();
        return $responsables;
    }

    public function inactivar(Request $request){

        $resp_area_aux = ResponsablesAreas::where('id_responsable', $request->id_registro)->where('deleted', '0')->get();

        if(count($resp_area_aux)>0){
            return response()->json([
                "mensaje" => "¡Error! No se puede eliminar, está asignado a un área",
                'estado' => 'error',
            ],200);               
        }

        $responsable = Responsables::find($request->id_registro);
        $responsable->deleted = '1';
        $responsable->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $responsable->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Eliminado correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo eliminar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }     
    }

}
