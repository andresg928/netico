<?php

namespace App\Http\Controllers;

use App\Cargue;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Vehiculo;
use  JWTAuth;
use Illuminate\Support\Facades\DB;

class CargueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $cargues = Cargue::select(
                                    'cargues.id as id_cargue', 'cargues.placa as placa', 'cargues.peso_origen', 'empresas.nombre as empresa',
                                     'cargues.nit_empresa','cargues.estado', 'cargues.observaciones'
                                  )
        ->leftjoin('empresas', 'cargues.nit_empresa', '=', 'empresas.nit')
        //->whereBetween('pesajes.fecha', [$request->input('fecha_inicial'), $request->input('fecha_final')])
        //->where('tipo', 'like', '%' . $request->input('tipo_producto') . '%')
        ->orderBy('cargues.estado', 'ASC')
        ->orderBy('cargues.created_at', 'DESC')->get();

        return $cargues;
    }

    public function obtenerInfoBasculaApiPowerBi(){

        $pesajes = DB::connection('mysql_bascula')
                        ->table('pesajes')
                            ->select('fecha','peso_neto', 'peso_bruto_erp', 'kilos_dif', 'precaucion', 'ajuste_diferencia', 'usu_anula_reg')
                            ->where('tipo', 'producto_terminado')
                            ->where('estado', 'peso_2')
                            ->get();


        for ($i=0; $i < count($pesajes) ; $i++) { 
            $pesajes[$i]->peso_bruto_erp = is_numeric($pesajes[$i]->peso_bruto_erp) ? $pesajes[$i]->peso_bruto_erp * 1 : 0;
            $pesajes[$i]->kilos_dif = is_numeric($pesajes[$i]->kilos_dif) ? $pesajes[$i]->kilos_dif * 1 : 0;
            $pesajes[$i]->ajuste_diferencia = is_numeric($pesajes[$i]->ajuste_diferencia) ? $pesajes[$i]->ajuste_diferencia * 1 : 0;
            $pesajes[$i]->anulado = $pesajes[$i]->usu_anula_reg ? $pesajes[$i]->anulado = 'si' : 'no';
        }

        return $pesajes;
    }

    public function obtnerInfoPesoxConductorApiPowerBi(){

        $datos = DB::connection('sqlsrv')
                                    ->table('v_Ico_PesoxDcto as v')
                                    ->select('fecha', 'placa',DB::raw("SUM(PESO_MAS_EMPQ * 1 ) as peso_mas_empaque"))
                                    ->groupBy('fecha', 'placa')->get();

        for ($i=0; $i < count($datos) ; $i++) { 
            $datos[$i]->peso_mas_empaque = is_numeric($datos[$i]->peso_mas_empaque) ? $datos[$i]->peso_mas_empaque * 1 : 0;
        }
        
        return $datos;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //REGISTRA PLACA SI NO EXISTE
        $placa = Vehiculo::where('placa', $request->input('placa'))->first();
        if(!$placa) {
            $vehiculo = new Vehiculo();
            $vehiculo->id = Uuid::generate()->string;
            $vehiculo->placa = $request->input('placa');
            $vehiculo->deleted = '0';
            $vehiculo->save();
        }
        $cargue = new Cargue();
        $cargue->id = Uuid::generate()->string;
        $cargue->placa = $request->input('placa');
        $cargue->nit_empresa = $request->input('nit_empresa');
        $cargue->peso_origen = $request->input('peso_origen');
        $cargue->estado = 'activo';
        $cargue->id_usuario = JWTAuth::user()->id;
        $cargue->observaciones = $request->input('observaciones');
        $saved = $cargue->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "registro_exitoso",
                "id_cliente" => $cargue->placa,
                "guardo" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function show(Cargue $cargue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function edit(Cargue $cargue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_cargue)
    {
        //REGISTRA PLACA SI NO EXISTE
        $placa = Vehiculo::where('placa', $request->input('placa'))->first();
        if(!$placa) {
            $vehiculo = new Vehiculo();
            $vehiculo->id = Uuid::generate()->string;
            $vehiculo->placa = $request->input('placa');
            $vehiculo->deleted = '0';
            $vehiculo->save();
        }
        $cargue = Cargue::find($id_cargue);
        $cargue->placa = $request->input('placa');
        $cargue->nit_empresa = $request->input('nit_empresa');
        $cargue->peso_origen = $request->input('peso_origen');
        //$cargue->estado = 'peso_2';
        $cargue->id_usuario_modifico = JWTAuth::user()->id;
        $cargue->observaciones = $request->input('observaciones');
        $saved = $cargue->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "registro_exitoso",
                "modifico" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }
    public function inactivarCargue(Request $request, $id_cargue){
        $cargue = Cargue::find($id_cargue);
        $cargue->estado = 'inactivo';
        $cargue->transaccion_pesaje = $request->input('num_doc');
        $cargue->id_usuario_modifico = JWTAuth::user()->id;
        $cargue->save();
    }
    public function obtenerInfoCargue($placa){

        $cargue = Cargue::select('cargues.id as id_cargue', 'cargues.placa as placa', 'cargues.peso_origen', 'empresas.nombre as empresa', 'cargues.nit_empresa')
        ->where('estado', '=', 'activo')
        ->where('placa', '=', $placa)
        ->leftjoin('empresas', 'cargues.nit_empresa', '=', 'empresas.nit')
        ->orderBy('cargues.created_at', 'DESC')->take(1)->get();

        return $cargue;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cargue $cargue)
    {
        //
    }
}
