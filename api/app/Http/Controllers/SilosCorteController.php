<?php

namespace App\Http\Controllers;

use App\silos_corte;
use Illuminate\Http\Request;
use App\Silo;
use App\silos_saldo;
use Webpatser\Uuid\Uuid;
use App\Mail\MailCumple;
use Mail;

class SilosCorteController extends Controller
{

    public function index()
    {
        //
    }

    public function generarCorteTrigo(){

        $saldo = silos_saldo::select('id_silo', 'saldo')->get();

        //Inactiva el corte del dia anterior de todos los silos
        silos_corte::where('deleted', '0')->update(['deleted' => '1']);

        //Genero registro corte de silos de trigo
        for ($i=0; $i < count($saldo) ; $i++) {

            $corte = new silos_corte();
            $corte->id = Uuid::generate()->string;
            $corte->id_silo = $saldo[$i]['id_silo'];
            $corte->saldo = $saldo[$i]['saldo'];
            $corte->save();
        }

    }
    //funcion que busca las o la persona que cumple años
    public function obtenerCumpleaneros(){

        //return false;
        $mes = date('m');
        $dia = date('d');

        //$mes = '09';
        //$dia = '28';

        $datos = \DB::connection('sqlsrv')
                                    ->table('MTEMPLEA')
                                        ->select('EMAIL', 'NOMBRE', 'NOMBRE2', 'APELLIDO', 'APELLIDO2')
                                        ->whereRaw("month(FECNAC) = '$mes' AND day(FECNAC) = '$dia'")
                                        ->where('ACTIVO', '0')
                                        ->get();

        $info = array();
        for ($i=0; $i < count($datos) ; $i++) {

            $full_name = ucfirst(strtolower(rtrim($datos[$i]->NOMBRE))).' '.ucfirst(strtolower(rtrim($datos[$i]->NOMBRE2)));
            $email = rtrim($datos[$i]->EMAIL);

            $info['nombre_cumple'] = $full_name;
            $info['email'] = $email;

            if($info['email']){
                Mail::to($info['email'])->send(new MailCumple($info));
            }

        }


    }

}
