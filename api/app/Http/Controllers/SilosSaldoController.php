<?php

namespace App\Http\Controllers;

use App\silos_saldo;
use Illuminate\Http\Request;

class SilosSaldoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\silos_saldo  $silos_saldo
     * @return \Illuminate\Http\Response
     */
    public function show(silos_saldo $silos_saldo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\silos_saldo  $silos_saldo
     * @return \Illuminate\Http\Response
     */
    public function edit(silos_saldo $silos_saldo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\silos_saldo  $silos_saldo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, silos_saldo $silos_saldo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\silos_saldo  $silos_saldo
     * @return \Illuminate\Http\Response
     */
    public function destroy(silos_saldo $silos_saldo)
    {
        //
    }
}
