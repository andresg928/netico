<?php

namespace App\Http\Controllers;

use App\MovimientosProduccion;
use App\MaestrosCodBarras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\DespachosTurnoController;
use App\Http\Controllers\DespachoController;

class MovimientosProduccionController extends Controller
{
    public function registrarMovimientoProduccion(Request $request){

        $hoy = Carbon::now();
        $info_maestro = MaestrosCodBarras::select('id as id_registro')->where('cod_barras', $request->cod_barras)->where('deleted', '0')->first();

        // Valida que ese mismo código de barra no sea ingresado doble en menos de 10 segundos por el mismo usuario
        $info_movi = MovimientosProduccion::where('id_maestro', $info_maestro['id_registro'])->where('deleted', '0')->where('usu_registra', JWTAuth::user()->vendedor)->orderBy('created_at', 'DESC')->first();
        if(count($info_movi)>0){
            $segundos = $hoy->diffInSeconds($info_movi['created_at']);
            if($segundos<=10){
                return response()->json([
                    'estado' => 'error',
                    'mensaje' => 'No se puede registrar el movimiento, debe esperar 10 seg.'
                ],200);
            }
        }

        if(count($info_maestro)==0){
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'El código de barras no se encuentra registrado en el sistema.'
            ],200); 
        }

        if($request->lote == ''){
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'Falta ingresar el lote.'
            ],200);   
        }

        $mov = new MovimientosProduccion();
        $mov->id = Uuid::generate()->string;
        $mov->id_maestro = $info_maestro['id_registro'];
        $mov->usu_registra = JWTAuth::user()->vendedor;
        $mov->fecha_produccion = $this->obtenerFechaSegunHoraCorte();
        $mov->lote = $request->lote;

        try {

            $save = $mov->save();
        
            if ($save) {
                return response()->json([
                    'estado' => 'ok',
                    'mensaje' => 'Producción ingresada correctamente.'
                ],200);
            } else {
                return response()->json([
                    'estado' => 'error',
                    'mensaje' => 'Error en el registro'
                ],200);
            }
        } catch (\Exception $e) {
            // Capturar y manejar cualquier excepción que se haya lanzado
            // Esto puede ser útil para detectar problemas inesperados
            return response()->json([
                'estado' => 'error',
                'mensaje' => "Ocurrió una excepción: " . $e->getMessage()
            ],200);
        }

    }

    public function listarMovimientosProduccion(Request $request){

        $ctrl_despacho = new DespachoController();
        $fechas = $ctrl_despacho->filtrosFecha($request->filtro_fecha);
        $fec_ini = $fechas[0];
        $fec_fin = $fechas[1];

        $t = 'movimientos_produccions.';
        $movi_prod = MovimientosProduccion::select($t.'id as id_registro', 'cb.cod_barras', 'cb.nom_producto', $t.'created_at', 'cb.cantidad', 'cb.unidad_medida', $t.'gestionado', $t.'lote')
                                        ->join('maestros_cod_barras as cb', 'cb.id','=',$t.'id_maestro')
                                        ->where($t.'deleted', '0')
                                        ->where($t.'usu_registra', JWTAuth::user()->vendedor)
                                        ->whereRaw("DATE_FORMAT(movimientos_produccions.fecha_produccion ,'%Y-%m-%d') BETWEEN '".$fec_ini."' and '".$fec_fin."'")
                                        ->orderByRaw("DATE_FORMAT(movimientos_produccions.created_at ,'%Y-%m-%d %H:%i:%s') DESC")->paginate(5);

        $ctl = new DespachosTurnoController();
        for ($i=0; $i < count($movi_prod) ; $i++) {
            $movi_prod[$i]['tiempo_transcurrido'] = $ctl->obtenerTiempoTranscurrido($movi_prod[$i]['created_at']);
        }

        return $movi_prod;
    }

    public function listadoMovProduccionxGesionar(Request $request){

        $mov_pro = MovimientosProduccion::select('movimientos_produccions.fecha_produccion as fecha','cb.nombre_consecutivo','cb.consecutivo','cb.unidad_medida',
                                                DB::raw('sum(cb.cantidad) as cantidad'), DB::raw('count(*) as num_registros'))
                                          ->join('maestros_cod_barras as cb', 'cb.id','=', 'movimientos_produccions.id_maestro')
                                          ->where('movimientos_produccions.deleted', '0')
                                          //->where('movimientos_produccions.gestionado', '0')
                                          ->whereBetween('movimientos_produccions.fecha_produccion', [$request->desde, $request->hasta])
                                          ->groupBy('movimientos_produccions.fecha_produccion','cb.nombre_consecutivo','cb.consecutivo','cb.unidad_medida')
                                          ->orderBy('movimientos_produccions.fecha_produccion', 'DESC')->get();

        for ($i=0; $i < count($mov_pro) ; $i++) {
           // Obtiene los registros pendientes por gestionar
           $reg_gest = count($this->obtenerIdsMovimientosParaProcesar($mov_pro[$i]['consecutivo'], $mov_pro[$i]['fecha'], 'si'));
           $reg_no_gest = count($this->obtenerIdsMovimientosParaProcesar($mov_pro[$i]['consecutivo'], $mov_pro[$i]['fecha'], 'no'));
           $total_reg = $reg_gest + $reg_no_gest;

           if($reg_gest == $total_reg){
                $mov_pro[$i]['gestionado'] = 'si';
           }else if($reg_gest < $total_reg){
                $mov_pro[$i]['gestionado'] = 'no';
                $mov_pro[$i]['num_registros'] = $total_reg - $reg_gest;
           }

        }

        return $mov_pro;

    }

    public function obtenerConsolidadoProductos(Request $request){

        $productos = MovimientosProduccion::select('cb.cod_barras','cb.cod_producto','cb.nom_producto','cb.unidad_medida',
                                                    DB::raw('sum(cb.cantidad) as cantidad'),DB::raw('count(*) as num_registros'), 'movimientos_produccions.gestionado','movimientos_produccions.nro_dcto_erp')
                                          ->join('maestros_cod_barras as cb', 'cb.id','=', 'movimientos_produccions.id_maestro')
                                          ->where('movimientos_produccions.deleted', '0')
                                          //->where('movimientos_produccions.gestionado', '0')
                                          ->where('cb.consecutivo', $request->consecutivo)
                                          ->where('movimientos_produccions.fecha_produccion', $request->fecha_produccion)
                                          ->groupBy('cb.cod_barras','cb.cod_producto','cb.nom_producto','cb.unidad_medida', 'movimientos_produccions.gestionado','movimientos_produccions.nro_dcto_erp')
                                          ->orderBy('movimientos_produccions.gestionado', 'DESC')
                                          ->orderBy('movimientos_produccions.nro_dcto_erp', 'DESC')
                                          ->get();

        return $productos;

    }

    // Funcion para determinar a que fecha pertence el registro de producción.
    public function obtenerFechaSegunHoraCorte(){

        $hora_corte='06:00';
        $hora_actual = date('H:i');
        $fecha_actual = date('Y-m-d'); 

        if(strtotime($hora_actual) < strtotime($hora_corte)){
            $fecha_actual = date('Y-m-d', strtotime($fecha_actual."- 1 days"));
        }

        return $fecha_actual;
    }

    public function obtenerIdsMovimientosParaProcesar($consecutivo, $fecha_produccion, $gestionado){

        $mvtos = MovimientosProduccion::select('movimientos_produccions.id as id_registro')
                                          ->join('maestros_cod_barras as cb', 'cb.id','=', 'movimientos_produccions.id_maestro')
                                          ->where('movimientos_produccions.deleted', '0')
                                          ->where('movimientos_produccions.gestionado', $gestionado == 'si' ? '1':'0')
                                          ->where('cb.consecutivo', $consecutivo)
                                          ->where('movimientos_produccions.fecha_produccion', $fecha_produccion)
                                          ->get();

        return $mvtos;
    }

    public function ingresarProduccionERP(Request $request){

        DB::beginTransaction();
        try{

                $date = Carbon::parse($request->fecha_produccion);
                $actual = Carbon::now();
                $fecha_mvto = $date->format('Ymd');
                $consecutivo = $request->consecutivo;
                $usuario = JWTAuth::user()->vendedor;

                // Se inserta en encabezado
                $trade = DB::connection('sqlsrv')->statement('exec ICO_SP_AddTrade ?,?,?', array($consecutivo, $fecha_mvto, $usuario));
                //$trade = true;

                if($trade === true) {

                    $nro_dcto = $this->obtenerConsecutivo($consecutivo);
                    $nro_dcto_full = $nro_dcto->TIPODCTO.$nro_dcto->CONSECUT;

                    // Se obtienen los IDS de los movimientos para gestionarlos
                    $mvtos = $this->obtenerIdsMovimientosParaProcesar($consecutivo, $request->fecha_produccion, 'no');
                    //dd($mvtos);
                    $cont_mvtos = 0;

                    for($i=0; $i<count($mvtos); $i++){

                        // Se buscar y se actualiza los registros a gestionado
                        $mvto = MovimientosProduccion::find($mvtos[$i]['id_registro']);
                        $mvto->gestionado = '1';
                        $mvto->usu_gestiona = $usuario;
                        $mvto->fec_hor_gestiona = $actual->toDateTimeString();
                        $mvto->nro_dcto_erp = $nro_dcto_full;
                        $save = $mvto->save();

                        if($save === true){
                            $cont_mvtos++;
                        }
                    }

                    // Se ingresa a mvtrade los movimientos consolidados
                    $prod = $request->productos;
                    $cont_prod = 0;

                    for($i=0; $i<count($prod); $i++){
                        // Se inserta el movimiento
                        $cod_pro = $prod[$i]['cod_producto'];
                        $cantidad = $prod[$i]['cantidad'];

                        $mvtrade = DB::connection('sqlsrv')->statement('exec ICO_SP_AddMvTrade ?,?,?,?,?', array($nro_dcto->TIPODCTO, $nro_dcto->CONSECUT, $fecha_mvto, $cod_pro, $cantidad));
                        if($mvtrade === true){
                            $cont_prod++;
                        }
                    }

                    // Verifica que todo quedo insertado
                    if(count($prod) == $cont_prod && count($mvtos) == $cont_mvtos){
                        DB::commit();
                        return response()->json([
                            "estado" => "ok",
                            "mensaje" => 'La producción se ingresó correctamente al sistema'
                        ],200);  
                    }else{
                        DB::commit();
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => 'Hubo un error, por favor intentar nuevamente'
                        ],200);   
                    }
                }
            }
            catch(\Exception $e){
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "estado" => 'error'
                ],200);               
            }

    }

    public function obtenerConsecutivo($codigo){

        $consecutivo = DB::connection('sqlsrv')
                        ->table('V_CONSECUTIVOS_ICO as c')
                            ->select('c.CONSECUT', 'c.TIPODCTO')
                            ->where('c.CODIGOCONS', '=', $codigo)
                            ->first();

        return $consecutivo;
    }

    public function eliminarRegistro(Request $request){

        $mov = MovimientosProduccion::find($request->id_registro);

        if($mov->gestionado == '0'){
            $mov->deleted = '1';
            $mov->usu_modifica = JWTAuth::user()->vendedor;
            $save = $mov->save();

            if($save === true){
                return response()->json([
                    'estado' => 'ok',
                    'mensaje' => 'Registro eliminado correctamente'
                ],200);
            } else {
                return response()->json([
                    'estado' => 'error',
                    'mensaje' => 'Error al eliminar el registro'
                ],200);
            }
        } else {
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'Error al eliminar, ya fue gestionado este registro.'
            ],200);            
        }
    }

}
