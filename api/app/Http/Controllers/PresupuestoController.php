<?php

namespace App\Http\Controllers;

use App\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;

class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function obtenerGrupoProductos(Request $request){

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        $grupos = DB::connection('sqlsrv')
                                            ->table('MTMERCIA as s')
                                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"), DB::raw("'0' AS enero"),
                                                         DB::raw("'0' AS febrero"),DB::raw("'0' AS marzo"),DB::raw("'0' AS abril"),DB::raw("'0' AS mayo"),
                                                         DB::raw("'0' AS junio"),DB::raw("'0' AS julio"),DB::raw("'0' AS agosto"),DB::raw("'0' AS septiembre"),
                                                         DB::raw("'0' AS octubre"),DB::raw("'0' AS noviembre"),DB::raw("'0' AS diciembre"))
                                                ->where('s.codigo', 'like', 'G0%')
                                                ->orderBy('s.codigo', 'ASC')->get();

        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
        $meses_aux = ['enero_total', 'febrero_total', 'marzo_total', 'abril_total', 'mayo_total', 'junio_total', 'julio_total', 'agosto_total', 'septiembre_total', 'octubre_total', 'noviembre_total', 'diciembre_total'];

        for($i=0; $i<count($grupos); $i++){

            // se valida que el presupuesto exista para poner los valores en el respectivo mes
            $presupuesto = Presupuesto::where('anio', $request->anio)->where('codigo_agrupa', $grupos[$i]->codigo)->first();

            if($presupuesto){

                for($m=0; $m<count($meses);$m++){
                    $mes = $meses[$m];
                    $mes_total = $meses_aux[$m];
                    $grupos[$i]->$mes = (int) $presupuesto->{$meses[$m]};
                    $grupos[$i]->$mes_total = round(($presupuesto->{$meses[$m]} * $grupos[$i]->peso) / $unidad_medida);
                }

            } else {
                for($m=0; $m<count($meses);$m++){
                    $mes = $meses[$m];
                    $mes_total = $meses_aux[$m];
                    $grupos[$i]->$mes = 0;
                    $grupos[$i]->$mes_total = 0;
                }                
            }
        }

        return $grupos;
    }

    public function obtenerFechaActual () {

        //$start = Carbon::now();
        $start = Carbon::now()->startOfMonth();
        $date = Carbon::now()->endOfMonth();

        return  response()->json([
            'fecha_inicio' => $start->format('Y-m-d'),
            //'fecha_inicio' => $date->format('Y-m-d'),
            'fecha_final'  => $date->format('Y-m-d'),
        ], 200);
    }

    public function obtenerUltimoDiaMes($fecha){

        $end = new Carbon($fecha);
        $date = $end->endOfMonth();
        return  $date->format('d');
    }

    public function obtenerVentasMes(Request $request){

        $anio = $request->anio;
        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        $data = array();
        
        // $request->carta == '' ? 0 : $request->carta;

        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

        for($m=0; $m<count($meses); $m++){

            $suma_tota_bultos = 0;
            $mes = $meses[$m];
            $fechas = $this->obtenerInicioFinMes($mes, $anio);

            $fec_ini = $fechas[0];
            $fec_fin = $fechas[1];

            $ventas_mes = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, '', 'total', $unidad_medida);

            for($i=0; $i<count($ventas_mes); $i++){

                $suma_tota_bultos = $suma_tota_bultos + $ventas_mes[$i]->cantidad_bultos;
            }

            $data[] = round($suma_tota_bultos);
        }

        return $data;
    }

    public function esBisiesto($year){

        return date('L',mktime(1,1,1,1,1,$year));

    }

    public function obtenerInicioFinMes($mes, $anio){

        $datos = array();

        switch ($mes) {

            case 'enero':
                $mes = '01';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'febrero':
                $mes = '02';
                $dia = $this->esBisiesto($anio) ==   0 ? '28' : '29';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.$dia;
            break;

            case 'marzo':
                $mes = '03';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'abril':
                $mes = '04';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'mayo':
                $mes = '05';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'junio':
                $mes = '06';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'julio':
                $mes = '07';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'agosto':
                $mes = '08';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'septiembre':
                $mes = '09';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'octubre':
                $mes = '10';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'noviembre':
                $mes = '11';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'diciembre':
                $mes = '12';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;
        }

        return $datos;
    }

    public function obtenerVentasVsPresupuestoMes(Request $request){

        Carbon::setLocale('es');
        $date1 = new Carbon($request->fecha_inicial);
        $date2 = new Carbon($request->fecha_final);

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;

        if($date1->monthName !== $date2->monthName){
            return response()->json([
                "estado" => "error",
                "mensaje" => 'El rango de fechas deben estar en el mismo mes'
            ],200); 
        }

        $mes  = $date1->monthName;
        $fec_ini = $request->fecha_inicial;
        $fec_fin = $request->fecha_final;
        $anio = $request->anio;
        
        $request->carta == '' ? 0 : $request->carta;

        $grupos = DB::connection('sqlsrv')
                                    ->table('MTMERCIA as s')
                                        ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                        ->where('s.codigo', 'like', 'G0%')
                                        ->whereRaw("s.codigo not in ('G017', 'G016', 'G018')")
                                        ->orderBy('s.codigo', 'ASC')->get();
        
        for($i=0; $i<count($grupos); $i++){

            $presup_grupo = Presupuesto::select($mes)->where('codigo_agrupa', $grupos[$i]->codigo)->where('anio', $anio)->first();

            $ventas_grupo = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, $grupos[$i]->codigo, 'detallada', $unidad_medida);


            if($presup_grupo && $ventas_grupo){

                // quita las grasas por que no se pueden convertir a bultos
                if($grupos[$i]->codigo != 'G015'){

                    // se consulta el grupo granel para sumarlo al grupo pasta
                    $ventas_past_granel = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, 'G018', 'detallada', $unidad_medida);

                    if($grupos[$i]->codigo == 'G012'){
                        $grupos[$i]->venta_cantidad =  $ventas_past_granel ? $ventas_grupo->cantidad + $ventas_past_granel->cantidad : $ventas_grupo->cantidad;
                        $grupos[$i]->venta_bultos = $ventas_past_granel ? round($ventas_grupo->cantidad_bultos + ((round($ventas_past_granel->cantidad * $grupos[$i]->peso)) / $unidad_medida)) : round($ventas_grupo->cantidad_bultos);
                    // le suma el valor de la carta    
                    } else if ($grupos[$i]->codigo == 'G001') {
                        $grupos[$i]->venta_cantidad =  $ventas_grupo->cantidad + $request->carta;
                        $grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos + $request->carta;
                    } else {
                        $grupos[$i]->venta_cantidad =  intval($ventas_grupo->cantidad);
                        $grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos * 1;
                    }

                    
                    $grupos[$i]->presupuesto_cantidad = intval($presup_grupo[$mes]);
                    $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
                    //$grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos;
    
                    if($presup_grupo[$mes]!=0) {

                        // si es carta tambien afecta el porcetaje del grupo 1
                        if($grupos[$i]->codigo == 'G001'){
                            $grupos[$i]->porcentaje =  round((($ventas_grupo->cantidad_bultos + $request->carta)  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),0);
                        }else{
                            if($grupos[$i]->codigo == 'G012'){
                                $grupos[$i]->porcentaje = $ventas_past_granel ?  round((($ventas_grupo->cantidad_bultos + ((round($ventas_past_granel->cantidad * $grupos[$i]->peso)) / $unidad_medida))  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),0) : round((($ventas_grupo->cantidad_bultos)  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),0);
                            }else{
                                $grupos[$i]->porcentaje =  round(($ventas_grupo->cantidad_bultos  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),0);
                            } 
                        }
                        
                    } else {
                        $grupos[$i]->porcentaje = 0;
                    }
                } else {

                    $grupos[$i]->presupuesto_cantidad = intval($presup_grupo[$mes]);
                    $grupos[$i]->presupuesto_bultos =  $presup_grupo[$mes] * 1;
                    $grupos[$i]->venta_cantidad = intval($ventas_grupo->cantidad);
                    $grupos[$i]->venta_bultos = $ventas_grupo->cantidad * 1;
    
                    if($presup_grupo[$mes]!=0) {
                        $grupos[$i]->porcentaje =  round(($ventas_grupo->cantidad  * 100) / (round(($presup_grupo[$mes]))),0);
                    } else {
                        $grupos[$i]->porcentaje = 0;
                    }
                }                
            } else {
                // para llenar datos grupos que no se ha reflejado ventas, o sea salga en 0
                $grupos[$i]->presupuesto_cantidad = intval($presup_grupo[$mes]);
                $grupos[$i]->venta_cantidad =  0;
                $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
                $grupos[$i]->venta_bultos = 0;
                $grupos[$i]->porcentaje = 0;
            }
        }
   
        return $grupos;
    }

    public function obtenerAniosFacturacion(Request $rquest){

        $data = \DB::connection('sqlsrv')
                                    ->table('v_Ico_InformeVentasPorFecha as i')
                                        ->select(DB::raw("MIN(YEAR(i.FECHA)) as min_anio"), DB::raw("MAX(YEAR(i.FECHA)) as max_anio"))
                                        ->first();

        $yearMin =  $data->min_anio;
        $yearMax =  $data->max_anio;

        $anios = array();

        for ($i=$yearMin; $i <= $yearMax ; $i++) { 
            $anios[] = $i;
        }

        return array_reverse($anios);
    }

    public function ventasPorGrupoFecha($fecha_ini, $fecha_fin, $cod_agrupa, $tipo_consulta = 'detallada', $unidad_medida = ''){

        $fecha_ini = str_replace("-","",$fecha_ini);
        $fecha_fin = str_replace("-","",$fecha_fin);

        switch ($tipo_consulta) {

            case 'detallada':

                    $data = \DB::connection('sqlsrv')
                                        ->table('v_Ico_InformeVentasPorFecha as i')
                                            ->select(DB::raw("RTRIM(i.codigo_grupo) AS codigo_grupo"), DB::raw("RTRIM(i.grupo) AS grupo"), DB::raw("case when codigo_grupo = 'G018' then CAST(sum(cantidad)/12 AS NUMERIC(17,2)) else sum(cantidad) end as cantidad"), DB::raw("CAST(sum((peso_neto) / $unidad_medida) AS NUMERIC(17,2)) as cantidad_bultos"))
                                            ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                            ->whereRaw("(descripcio not like '%mogolla%' or descripcio  not like '%salvado%' or descripcio  not like '%polipr%'  or descripcio  not like '%flete%'  or descripcio  not like '%polvillo%' and descripcio not like '%trigo%')")
                                            ->whereRaw("i.PRODUCTO not in ('001', '42104', '004', '008')")
                                            //->whereRaw("i.producto not like '5%'")
                                            ->where('i.codigo_grupo', $cod_agrupa)
                                            //->whereRaw("i.codigo_grupo not in ('G018', 'G017', 'G016')")
                                            //->whereRaw("i.vendedor not in ('0')")
                                            ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                            //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                            ->groupBy('i.codigo_grupo')
                                            ->groupBy('i.grupo')
                                            ->orderBy('i.grupo', 'ASC')
                                            ->first();

            break;      
 
            case 'total':

                    $data = \DB::connection('sqlsrv')
                                        ->table('v_Ico_InformeVentasPorFecha as i')
                                            ->select(DB::raw("case when codigo_grupo <> 'G018' then CAST(sum((peso_neto) / $unidad_medida) AS NUMERIC(17,2)) else CAST(((sum(cantidad)/12)*12) / $unidad_medida AS NUMERIC(17,2)) end as cantidad_bultos"))
                                            ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                            ->whereRaw("(descripcio not like '%mogolla%' or descripcio  not like '%salvado%' or descripcio  not like '%polipr%'  or descripcio  not like '%flete%'  or descripcio  not like '%polvillo%' and descripcio not like '%trigo%')")
                                            ->whereRaw("i.PRODUCTO not in ('001', '42104', '004', '008')")
                                            //->whereRaw("i.producto not like '5%'")
                                            ->whereRaw("i.codigo_grupo not in ('G014', 'G015', 'G016', 'G017', '0')")
                                            //->whereRaw("i.vendedor not in ('0')")
                                            ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                            //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                            ->groupBy('i.codigo_grupo')
                                            ->get();
            break;
        }

        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ingresarPresupuesto(Request $request){

        $val_presupuesto = Presupuesto::select('id as id_pre')->where('codigo_agrupa', $request->cod_agrupa)->where('anio', $request->anio)->first();

        if($val_presupuesto){
            $presupuesto = Presupuesto::find($val_presupuesto['id_pre']);
            $presupuesto->{$request->campo} = $request->valor;
            $presupuesto->usu_modifica = JWTAuth::user()->vendedor;
            $saved = $presupuesto->save();
        } else {
            $presupuesto = new Presupuesto();
            $presupuesto->id = Uuid::generate()->string;
            $presupuesto->codigo_agrupa = $request->cod_agrupa;
            $presupuesto->anio = $request->anio;
            $presupuesto->{$request->campo} = $request->valor;
            $presupuesto->usu_registra = JWTAuth::user()->vendedor;
            $saved = $presupuesto->save();
        }

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'La información de guardó de manera correcta'
            ],200); 
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al guardar la información'
            ],200); 
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function show(Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit(Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presupuesto $presupuesto)
    {
        //
    }
}
