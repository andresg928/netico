<?php

namespace App\Http\Controllers;

use App\compras_trigo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Illuminate\Support\Facades\Storage;

class ComprasTrigoController extends Controller
{

    public function ObtenerProveedoresTrigo(){

        $proveedores = DB::connection('sqlsrv')
                                    ->table('MTPROCLI as p')
                                    ->select(DB::raw("RTRIM(p.nit) as nit"), DB::raw("RTRIM(p.nombre) as nombre"))
                                    ->where('p.comentario', 'like', '%trigo%')
                                    ->orderBy('p.nombre', 'DESC')->get();
        return $proveedores;
    }

    public function obtenerMotonaveActiva(){

        $motonave = compras_trigo::select('motonave')->where('inactivo', 'no')->get();

        count($motonave) == 1 ? $motonave_activa = $motonave[0]['motonave'] : $motonave_activa = null;

        return $motonave_activa;
    }

    public function obtenerUltimasMotonaves(){
        $motonaves = compras_trigo::select('motonave')->where('inactivo', 'no')->where('deleted', '0')->orderBy('fecha_embarque1', 'desc')->get();
        return $motonaves;
    }

    public function registrarCompra(Request $request){

        $id_generado = '';
        $documento_anterior = '';

        if($request->id_registro !=''){
            $tipo_movimiento = 'modificacion';
            $compra =  compras_trigo::find($request->id_registro);
            $id_generado = $request->id_registro;
            $documento_anterior =  $compra->adjunto;
            $compra->usu_modifica = JWTAuth::user()->vendedor;
        } else {
            $compra = new compras_trigo();
            $tipo_movimiento = 'insertar';
            $compra->id_registro = $id_generado = Uuid::generate()->string;
            $compra->usu_registra = JWTAuth::user()->vendedor;
        }

        $compra->fecha_embarque1 = $request->fecha_embarque1;
        $compra->fecha_embarque2 = $request->fecha_embarque2;
        $compra->nit_proveedor = $request->nit_proveedor;
        $compra->nombre_proveedor = $request->nombre_proveedor;
        $compra->origen_trigo = strtoupper($request->origen);
        $compra->motonave = strtoupper($request->motonave);
        $compra->tipo_trigo1 = strtoupper($request->tipo1);
        $compra->proteina1 = $request->proteina1;

        $nombre = '';
        if($request->file){
            $file = $request->file;
            $nombre = $id_generado.'.'.$file->getClientOriginalExtension();
            $compra->adjunto = $nombre;
        }

        $compra->cantidad1 = $request->cantidad1;
        $compra->precio_base1 = $request->precio1;
        $compra->precio_final1 = $request->precio_final1;
        $compra->tipo_trigo2 = strtoupper($request->tipo2);
        $compra->proteina2 = empty($request->proteina2) ? '' : $request->proteina2;
        $compra->cantidad2 =  empty($request->cantidad2) ? 0 : $request->cantidad2;
        $compra->precio_base2 = empty($request->precio2) ? '' : $request->precio2;
        $compra->precio_final2 = empty($request->precio_final2) ? 0 : $request->precio_final2;
        $compra->tipo_trigo3 = strtoupper($request->tipo3);
        $compra->proteina3 = empty($request->proteina3) ? '' : $request->proteina3;
        $compra->cantidad3 = empty($request->cantidad3) ? 0 : $request->cantidad3;
        $compra->precio_base3 = empty($request->precio3) ? '' : $request->precio3;
        $compra->precio_final3 = empty($request->precio_final3) ? 0 : $request->precio_final3;
        $saved = $compra->save();

        if($saved === true){

            if($request->file){
                $this->subirArchivo($request->file, $id_generado, $tipo_movimiento, $documento_anterior);
            }

            return response()->json([
                "estado" => "ok",
                "mensaje" => 'La compra se ha registrado exitosamente'
            ],200); 
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error, por favor vuelva a intentar'
            ],200); 
        }
    }

    public function subirArchivo($file, $Idregistro, $tipoMoviminento, $adjuntoAnterior){

        $nombre = $Idregistro.'.'.$file->getClientOriginalExtension();

       //si no existe el directorio se crea
       if(!Storage::disk('public')->exists('contrato_compras')){
            Storage::makeDirectory('public/contrato_compras');
        }

        if($tipoMoviminento == 'modificacion'){
            //Elimina el archivo si existe
            Storage::disk('public')->delete('contrato_compras/'.$adjuntoAnterior);
        }

        //filesystem.php esta definido el driver contrato_compras
        Storage::disk('contrato_compras')->put($nombre,  \File::get($file));
    }

    public function downloadFile($archivo){
        return response()->download(storage_path('app/public/contrato_compras/'.$archivo));
    }

    public function obtenerCompras(Request $request){

        $compras = compras_trigo::select('id_registro as id_reg', 'deleted', 'fecha_embarque1', 'fecha_embarque2', 'nit_proveedor', 'nombre_proveedor', 'origen_trigo', 'motonave', 'adjunto',
                                          'tipo_trigo1', 'proteina1', 'cantidad1', 'precio_base1', 'precio_final1', 'tipo_trigo2', 'proteina2', 'cantidad2', 'precio_base2',
                                          'precio_final2', 'tipo_trigo3', 'proteina3', 'cantidad3', 'precio_base3', 'precio_final3', 'usu_registra', 'usu_modifica', 'usu_elimina',
                                           'created_at', 'updated_at', 'inactivo'
                                        )
                                    ->where('deleted', '0')
                                    ->orderBy('fecha_embarque1', 'DESC');

        if($request->fecha_ini !='' && $request->fecha_fin !=''){
            $compras->whereBetween('fecha_embarque1', [$request->fecha_ini, $request->fecha_fin]);
        }
        if($request->filtro_motonave == 'si'){
            $compras->where('motonave', '');
        }
        $compras = $compras->get();

        return $compras;
    }

    public function cambiarEstadoCompra(Request $request){

        $compra =  compras_trigo::find($request->id_registro);
        $compra->inactivo = $request->estado;
        $saved = $compra->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Gestionado correctamente'
            ],200); 
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error, por favor vuelva a intentar'
            ],200); 
        }
    }

}
