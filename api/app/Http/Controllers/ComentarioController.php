<?php

namespace App\Http\Controllers;

use App\Comentario;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;

class ComentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listarComentarios(){

        $comentarios = Comentario::select('id as id_comentario', 'fecha_inicio', 'fecha_final', 'tipo', 'mensaje')->where('deleted', '0')->orderBy('fecha_inicio', 'DESC')->get();

        return $comentarios;
    }

    public function esBisiesto($year){

        return date('L',mktime(1,1,1,1,1,$year));

    }

    public function obtenerInicioFinMes($mes, $anio){

        $datos = array();
        $mes = strtolower($mes);

        switch ($mes) {

            case 'enero':
                $mes = '01';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'febrero':
                $mes = '02';
                $dia = $this->esBisiesto($anio) ==   0 ? '28' : '29';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.$dia;
            break;

            case 'marzo':
                $mes = '03';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'abril':
                $mes = '04';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'30';
            break;

            case 'mayo':
                $mes = '05';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'junio':
                $mes = '06';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'30';
            break;

            case 'julio':
                $mes = '07';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'agosto':
                $mes = '08';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'septiembre':
                $mes = '09';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'30';
            break;

            case 'octubre':
                $mes = '10';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;

            case 'noviembre':
                $mes = '11';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'30';
            break;

            case 'diciembre':
                $mes = '12';
                $datos[0] = $anio.'-'.$mes.'-'.'01';
                $datos[1] = $anio.'-'.$mes.'-'.'31';
            break;
        }

        return $datos;
    }

    public function obtenerComentario(Request $request){

        $request->fecha_inicial == '' ? $request->fecha_inicial = date('Y-m-d') : $request->fecha_inicial;
        switch ($request->tipo) {
            case 'Diario':
                $comentario = Comentario::select('mensaje', 'tipo')->where('deleted', '0')->where('fecha_inicio', $request->fecha_inicial)->first();
            break;
        }

        return $comentario;

    }
    public function obtenerComentariosFecha(Request $request){

        Carbon::setLocale('es');
        // $request->fecha_inicial = '2021-05-01';
        if($request->formulario === 'Informes'){
            $date1 = new Carbon($request->fecha_inicial);
        }

        if($request->formulario === 'Presupuestos'){
            $fechas = $this->obtenerInicioFinMes($request->mes, $request->anio);
            $date1 = new Carbon($fechas[0]);
        }

        $mes  = $date1->monthName;
        $anio = $date1->year;

        $fechas = $this->obtenerInicioFinMes($mes, $anio);

        $fec_ini = $fechas[0];
        $fec_fin = $fechas[1];

        $data = array();
        $data_mensual = array();
        $data_diario = array();
        $data_encabezado_mensual = array();
        $data_encabezado_mensual['mes'] = ucfirst($mes);
        $data_encabezado_mensual['anio'] = $anio;

        $comentarios = Comentario::select('id as id_comentario', 'fecha_inicio', 'tipo', 'mensaje')
                        ->where('deleted', '0')
                        ->whereBetween('fecha_inicio', [$fec_ini, $fec_fin])
                        ->orderBy('fecha_inicio', 'DESC')
                        ->get();
        $d=0;
        $m=0;
        for ($i=0; $i < count($comentarios) ; $i++) {

            $tipo_comentario = $comentarios[$i]['tipo'];
            $mensaje = $comentarios[$i]['mensaje'];
            $fecha = $comentarios[$i]['fecha_inicio'];
            $fecha_aux = new Carbon($fecha);

            if($tipo_comentario === 'Diario') {
                $data_diario[$d]['tipo'] = $tipo_comentario;
                $data_diario[$d]['fecha_inicio'] = ucfirst($fecha_aux->monthName) .' '.$fecha_aux->day.', '.$fecha_aux->year;
                $data_diario[$d]['mensaje'] = $mensaje;
                $d++;
            }

            if($tipo_comentario === 'Mensual') {
                $data_mensual[$m]['tipo'] = $tipo_comentario;
                $data_mensual[$m]['fecha_inicio'] = $fecha;
                $data_mensual[$m]['mensaje'] = $mensaje;
                $data_mensual[$m]['mes'] = ucfirst($mes);
                $data_mensual[$m]['anio'] = $anio;
                $m++;
            }
        }

        $data['comentarios_diarios'] = $data_diario;
        $data['comentarios_mensuales'] = $data_mensual;
        $data['encabezado'] = $data_encabezado_mensual;

        return $data;
    }

    public function store(Request $request){

        // modifica
        if ($request->id_comentario){

            if($request->accion == 'editar') {

                $comentario = Comentario::find($request->id_comentario);
                $comentario->usu_modifica = JWTAuth::user()->vendedor;
            }

            if($request->accion == 'eliminar') {

                $comentario = Comentario::find($request->id_comentario);
                $comentario->usu_modifica = JWTAuth::user()->vendedor;
                $comentario->deleted = '1';
                $saved = $comentario->save();

                if($saved === true){    
                    return response()->json([
                        "estado" => "ok",
                        "mensaje" => 'Comentario eliminado correctamente'
                    ],200); 
                } else {
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'Error al eliminar el comentario'
                    ],200); 
                }
            }

        } else {
            // insert
            $comentario = new Comentario();
            $comentario->id = Uuid::generate()->string;
            $comentario->usu_registro = JWTAuth::user()->vendedor;
        }

        $comentario->tipo = $request->tipo;
        $comentario->mensaje = $request->mensaje;
        $comentario->fecha_inicio = $request->fecha_inicio;
        $comentario->fecha_final = $request->fecha_final;
        $saved = $comentario->save();

        if($saved === true){    
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'La información se ha guardado correctamente'
            ],200); 
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al guardar el comentario'
            ],200); 
        }
    }

}
