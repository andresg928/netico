<?php

namespace App\Http\Controllers;

use App\despachos_trigo;
use App\silos_saldo;
use Illuminate\Http\Request;
use Excel;
use App\Imports\DespachosTrigoImport;
use App\Imports\DespachosTrigoDirectoImport;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
// use Ralego\Parser\Traits\XmlParser;
class DespachosTrigoController extends Controller
{

    // use XmlParser;
    public function obtenerDetalleTransportadora($campo, $valor){

        $detalle_empresa = DB::connection('mysql_bascula')->table('empresas')->where($campo, $valor)->first();
        return $detalle_empresa;

    }

    public function importarDatos(Request $request){

        try {
                if(!$request->file('documento_despacho')){
                    throw new \Excepcion('Archivo no existe');
                }
                $file = $request->documento_despacho;
                $data = array();
                $data['motonave'] = $request->motonave;
                $data['silo'] = $request->silo;
                $data['bl'] = $request->bl;
                $data['trigo_directo'] =  strtolower($request->trigo_directo) == 'true' ? true : false;
                $res = Excel::toArray([],$file);
                $datos_excel = $res[0];

                $data_new = array();
                $total_kilos = 0;


                if($data['trigo_directo'] == false) {

                    for ($i=1; $i <count($datos_excel);$i++) {

                        $fecha_aux = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datos_excel[$i][1]));
                        $fecha = $fecha_aux->format('Y-m-d');
    
                        $registro = despachos_trigo::select('id_registro')->where('placa', $datos_excel[$i][2])->where('fecha', $fecha)->where('deleted', '0')->first();
                        //Si no existe que sume el total de kilos
                        if(!$registro){
                            $total_kilos += ($datos_excel[$i][7] - $datos_excel[$i][6]);
                        }
                    }

                    //Obtener el saldo del silo para saber si se puede sacar trigo
                    $info_saldo = silos_saldo::select('saldo')->where('id_silo', $request->silo)->first();
    
                    if($info_saldo['saldo'] < ($total_kilos/1000)){
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => '¡Error! El saldo de la bodega es menor al total de kilos que contiene el archivo a importar'
                        ],200); 
                    }
                    Excel::import(new DespachosTrigoImport($data), $file);
                } else {
                    Excel::import(new DespachosTrigoDirectoImport($data), $file);
                }



                return response()->json([
                    "estado" => "ok",
                    "mensaje" => '¡Muy bien! El archivo de despachos se subió correctamente'
                ],200); 

        } catch (\Maatwebsite\Excel\ValidatorsExcepcion $e) {

            foreach ($failures as $failure) {
                $failure->row();
                $failure->attribute();
                $failure->errors();
                $failure->values();
            }

        }

    }
    
    public function obtenerListadoDespachosTrigo(Request $request){

        $t='despachos_trigo.';

        $despachos_directos = despachos_trigo::select($t.'id_registro', $t.'deleted', $t.'motonave', $t.'bl', $t.'estado', $t.'fecha as fecha', $t.'placa', $t.'tiquete' ,
                                               $t.'nombre_transportadora', DB::raw("'directo' as nombre"), $t.'destino', $t.'peso_entrada', $t.'peso_salida', $t.'peso_neto', $t.'acumulado', $t.'saldo', $t.'precintos',
                                               $t.'usu_registra', $t.'usu_modifica', $t.'created_at',$t.'updated_at', DB::raw("'directo' as silo"))
                                      ->where($t.'deleted', '0')
                                      ->where($t.'id_silo', 'DIRECTO')
                                      ->where($t.'motonave', 'like', '%' . $request->filtro_motonave . '%')
                                      ->whereBetween($t.'fecha', [$request->fecini, $request->fecfin])
                                      ->whereIn($t.'estado', ['directo', 'descargada']);

        $registros = despachos_trigo::select($t.'id_registro', $t.'deleted', $t.'motonave', $t.'bl', $t.'estado', $t.'fecha as fecha', $t.'placa', $t.'tiquete' ,
                                             $t.'nombre_transportadora', 's.nombre', $t.'destino', $t.'peso_entrada', $t.'peso_salida', $t.'peso_neto', $t.'acumulado', $t.'saldo', $t.'precintos',
                                             $t.'usu_registra', $t.'usu_modifica', $t.'created_at',$t.'updated_at', 's.nombre as silo')
                                      ->join('silos as s', 's.id', '=', $t.'id_silo')
                                      ->where($t.'deleted', '0')
                                      ->where($t.'motonave', 'like', '%' . $request->filtro_motonave . '%')
                                      ->whereBetween($t.'fecha', [$request->fecini, $request->fecfin])
                                      ->where('s.nombre', 'like', '%' . $request->filtro_bodega . '%')
                                      ->orderBy('fecha', 'DESC');
                
                                      if($request->filtro_estado_despacho != 'directo' && $request->filtro_estado_despacho != 'todos'){
                                        $registros->where($t.'estado','like', '%' . $request->filtro_estado_despacho . '%');
                                      }

                                      if($request->filtro_estado_despacho == 'directo'){
                                        $registros->union($despachos_directos);
                                        $registros->whereNotIn($t.'estado', ['descargada', 'en_transito']);
                                      }

                                      if($request->filtro_estado_despacho == 'todos'){
                                        $registros->whereIn($t.'estado', ['descargada', 'en_transito']);
                                        if(!$request->filtro_bodega){
                                            $registros->union($despachos_directos);
                                        }
                                      }

                                      $registros = $registros->get();
        return $registros;

    }

    public function downloadFile(){
        return response()->download(storage_path('app/public/formatos_importacion/FORMATO_DESPACHOS_TRIGO.xlsx'));
    }

    public function obtenerTrigoEnTransito(){

        $info = despachos_trigo::select('peso_neto')->where('deleted', '0')->whereIn('estado', ['en_transito','directo'])->get();
        $data = array();
        $tota_kilos = 0;

        for ($i=0; $i <count($info) ; $i++) {
            $tota_kilos += $info[$i]['peso_neto'];
        }

        $data['total_kilos'] = $tota_kilos;
        $data['total_registros'] = count($info);

        return $data;
    }

    public function obtenerTrigoTransitoDetallado(){

        $info = despachos_trigo::select(DB::raw("sum(peso_neto) as peso_neto"), 'motonave','estado', DB::raw("count(*) as numero_mulas"))
                                ->where('deleted', '0')
                                ->whereIn('estado', ['en_transito', 'directo'])
                                ->groupBy(['motonave', 'estado'])
                                ->orderBy('estado')
                                ->get();

        return $info;
    }

    /*public function readXML(){

        $arrFiles = glob(storage_path('/app/public/xml/*.xml'));

        dd($arrFiles[0]);

        //$xmlString = file_get_contents(storage_path('app/public/xml/FFE10958306230000000275.xml'));

        $xml = file_get_contents(storage_path('app/public/xml/ad0800143512008230000018d.xml'));
        //$object = $this->xmlToObject($xml);
        $array = $this->xmlToArray($xml);
        //$json = $this->xmlToJson($xml);
        dd($this->xmlToArray($array['AttachedDocument']['Attachment']['ExternalReference']['Description']));
    }*/
}
