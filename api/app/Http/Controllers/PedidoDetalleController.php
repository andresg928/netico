<?php

namespace App\Http\Controllers;

use App\pedido_detalle;
use Illuminate\Http\Request;

class PedidoDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pedido_detalle  $pedido_detalle
     * @return \Illuminate\Http\Response
     */
    public function show(pedido_detalle $pedido_detalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pedido_detalle  $pedido_detalle
     * @return \Illuminate\Http\Response
     */
    public function edit(pedido_detalle $pedido_detalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pedido_detalle  $pedido_detalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pedido_detalle $pedido_detalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pedido_detalle  $pedido_detalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(pedido_detalle $pedido_detalle)
    {
        //
    }
}
