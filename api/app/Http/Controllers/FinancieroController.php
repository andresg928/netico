<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinancieroController extends Controller
{

    private $fechaInicio;
    private $fechaFinal;

    public function obtenerValorPorCuenta($cuenta = '', $logitud_cuenta = 0, $tipo_consulta = '', $multiples_cuentas = 'no', $obtener_cuentas = ''){

        if($tipo_consulta == 'cuenta_empieza'){

                $valor = DB::connection('sqlsrv')
                                        ->table('MVTONIIF as m')
                                        ->select(
                                            DB::raw("sum(CREDITO) as credito"), 
                                            DB::raw("sum(DEBITO) as debito"),
                                            DB::raw("SUBSTRING (CODIGOCTA, 0, ".$logitud_cuenta.") as CUENTA"),
                                            DB::raw("(sum(CREDITO)  - sum(DEBITO)) as diferencia")
                                        )
                                        ->whereRaw("(CODIGOCTA like '".$cuenta."%' and FECHAMVTO between '".$this->fechaInicio."' and '".$this->fechaFinal."')")
                                        ->whereRaw("CODCOMPROB <> 'CI'")
                                        ->groupBy(DB::raw("SUBSTRING (CODIGOCTA,0,".$logitud_cuenta.")"))
                                        ->first();
                if($valor){
                    $diferencia =  $valor->diferencia;
                    $diferencia <= 0 ? $valor->diferencia = $valor->diferencia * -1 : $valor->diferencia = $diferencia;
                    $valor->diferencia_label =  $this->number_format_short($valor->diferencia, 'con_letra');                   
                }
        }
        else if ($tipo_consulta == 'cuenta_igual'){

                if($multiples_cuentas == 'si') {
                    $cuenta = $this->armarCondicionMultipleCuenta($obtener_cuentas);
                }

                $valor = DB::connection('sqlsrv')
                                        ->table('MVTONIIF AS m')
                                        ->select(
                                            DB::raw("sum(m.CREDITO) AS credito"), 
                                            DB::raw("sum(m.DEBITO) AS debito"),
                                            DB::raw("RTRIM(c.cuenta) AS CUENTA"),
                                            DB::raw("RTRIM(c.Nombre_Cuentas) AS NOMBRE_CUENTA"),
                                            DB::raw("(sum(m.CREDITO)  - sum(m.DEBITO)) AS diferencia")
                                        )
                                        //->whereRaw("INNER JOIN vReporteCuentasnif c ON (c.cuenta = m.CODIGOCTA)")
                                        ->join('vReporteCuentasnif as c', 'c.cuenta', '=', 'm.CODIGOCTA')
                                        ->whereRaw("(m.CODIGOCTA IN (".$cuenta.") AND m.FECHAMVTO BETWEEN '".$this->fechaInicio."' and '".$this->fechaFinal."')")
                                        ->whereRaw("m.CODCOMPROB <> 'CI'")
                                        ->groupBy(DB::raw("c.cuenta,c.Nombre_Cuentas")) 
                                        ->get();

                for ($i=0; $i < count($valor) ; $i++) { 

                    $diferencia =  $valor[$i]->diferencia * 1;
                    $valor[$i]->diferencia = $diferencia;
                    // para lo ingresos debe mantaner el negativo
                    if($obtener_cuentas == 'costos'){
                        $diferencia <= 0 ? $valor[$i]->diferencia = $valor[$i]->diferencia * -1 : $valor[$i]->diferencia = $diferencia;
                    }

                    $valor[$i]->diferencia_label =  $this->number_format_short($valor[$i]->diferencia, 'con_letra');
                }
        }

        return $valor;
    }

    public function obtenerAniosContables(){

        $anios = DB::connection('sqlsrv')
                                    ->table('MVTONIIF AS m')
                                    ->select(DB::raw("distinct year(FECHAMVTO) as anio"))
                                    ->get();

        $data = array();

        for ($i=0; $i < count($anios) ; $i++) { 
            $data[] = $anios[$i]->anio;
        }

        return $data;
    }

    public function obtenerMesesContables(Request $request){

        $meses = DB::connection('sqlsrv')
                                    ->table('MVTONIIF AS m')
                                    ->select(DB::raw("distinct month(FECHAMVTO) as mes"))
                                    ->whereRaw("year(FECHAMVTO) = ".$request->anio."")
                                    ->whereRaw("CODIGOCTA like '60%'")
                                    ->get();

        $data = array();

        for ($i=0; $i < count($meses) ; $i++) {

            $mes = $meses[$i]->mes;

            if($mes <= 9){
                $mes = '0'.$meses[$i]->mes;
            }

            $data[] = $mes;
        }

        return $data;
    }

    public function obtenerMesesContablesAux($anio){

        $meses = DB::connection('sqlsrv')
                                    ->table('MVTONIIF AS m')
                                    ->select(DB::raw("distinct month(FECHAMVTO) as mes"))
                                    ->whereRaw("year(FECHAMVTO) = ".$anio."")
                                    ->whereRaw("CODIGOCTA like '60%'")
                                    ->get();

        $data = array();

        for ($i=0; $i < count($meses) ; $i++) {

            $mes = $meses[$i]->mes;

            if($mes <= 9){
                $mes = '0'.$meses[$i]->mes;
            }

            $data[] = $mes;
        }

        return $data;
    }

    public function armarCondicionMultipleCuenta($tipo_cuentas){

        $respuesta = '';

        if($tipo_cuentas == 'costos'){
            // cuentas pertenecientes a costos, todas estas deben sumar el total de costos de las dash
            $cuentas = ['6061200601', '6061201201', '606120140101', '6061202001', '6061352001', '6061352201'];
        }

        if($tipo_cuentas == 'ingresos_harina'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['404120060101', '404120060103', '404175010101'];
        }

        if($tipo_cuentas == 'ingresos_pasta'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['404120120101', '404120125001', '404175010601'];
        }

        if($tipo_cuentas == 'ingresos_subproducto'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['404120060102', '404175010102'];
        }

        if($tipo_cuentas == 'ingresos_agropecuarios'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['4041352201', '404175012004'];
        }

        if($tipo_cuentas == 'ingresos_no_fabricados'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['4041352005', '404175012005'];
        }

        if($tipo_cuentas == 'ingresos_mezclas_secas'){
            // cuentas pertenecientes a ingresos para comparar
            $cuentas = ['404120140101', '404175010103'];
        }

        for ($i=0; $i<count($cuentas); $i++) {
            $respuesta .= "'".$cuentas[$i]."',";
        }

        return substr($respuesta, 0, -1);
    }

    public function obtenerInformacionPeridos(Request $request){

        $anio = $request->anio;

        $meses = $this->obtenerMesesContablesAux($anio);

        $data = array();

        // $data['ingresos']['tipos'] = ['ing_ordinarios'];
        $data['ingresos']['tipos'] = ['ing_ordinarios', 'otros_ingresos', 'ing_financieros'];

        for ($i=0; $i < count($data['ingresos']['tipos']) ; $i++) {

            $sumador = 0;

            for ($j=0; $j < count($meses) ; $j++) {

                $fechas = $this->obtenerInicioFinMes($meses[$j], $anio);
                $this->fechaInicio = $fechas[0];
                $this->fechaFinal = $fechas[1];

                $data['ingresos']['total'][$data['ingresos']['tipos'][$i]][$meses[$j]] = $this->obtenerIngresosPorTipo($data['ingresos']['tipos'][$i]);
                // $sumador += $this->obtenerIngresosPorTipo($data['ingresos']['tipos'][$i]);

            }

            //$data['ingresos']['total'][$data['ingresos']['tipos'][$i]] = $sumador;
        }

        return $data;

    }

    public function obtenerIngresosPorTipo($tipo){

        // $valor = 0;

        switch ($tipo) {

            case 'ing_ordinarios':
                $valor = $this->obtenerValorPorCuenta('4041', 5, 'cuenta_empieza', 'no');
            break;

            case 'otros_ingresos':
                $valor = $this->obtenerValorPorCuenta('4042', 5, 'cuenta_empieza', 'no');
            break;

            case 'ing_financieros':
                $valor = $this->obtenerValorPorCuenta('404215', 7, 'cuenta_empieza', 'no');
            break;

            case 'ing_harinas':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_harina');
                return $valor;
            break;

            case 'ing_pastas':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_pasta');
                return $valor;
            break;

            case 'ing_subproductos':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_subproducto');
                return $valor;
            break;

            case 'ing_agropecuarios':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_agropecuarios');
                return $valor;
            break;

            case 'ing_no_fabricados':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_no_fabricados');
                return $valor;
            break;

            case 'ing_mezclas':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'ingresos_mezclas_secas');
                return $valor;
            break;

            case 'costos':
                $valor = $this->obtenerValorPorCuenta('6', 1, 'cuenta_empieza', 'no');
            break;

            case 'costos_detallados':
                $valor = $this->obtenerValorPorCuenta('', 1, 'cuenta_igual', 'si', 'costos');
                return $valor;
            break;

            case 'gastos':
                $valor = $this->obtenerValorPorCuenta('5', 1, 'cuenta_empieza', 'no');
            break;

            case 'gastos_detallados':

                $gastos_admon = $this->obtenerValorPorCuenta('5051', 5, 'cuenta_empieza', 'no');
                $gastos_distribucion = $this->obtenerValorPorCuenta('5052', 5, 'cuenta_empieza', 'no');
                $gastos_finan_no_opera = $this->obtenerValorPorCuenta('5053', 5, 'cuenta_empieza', 'no');
                $gastos_imp_resta = $this->obtenerValorPorCuenta('5054', 5, 'cuenta_empieza', 'no');
                $gastos_opera_conjunta = $this->obtenerValorPorCuenta('5055', 5, 'cuenta_empieza', 'no');

                $valor['gastos_admon'] = $gastos_admon == '' ? $gastos_admon = 0 : $gastos_admon;
                $valor['gastos_distribucion'] = $gastos_distribucion == '' ? $gastos_distribucion = 0 : $gastos_distribucion;
                $valor['gastos_finan_no_opera'] = $gastos_finan_no_opera == '' ? $gastos_finan_no_opera = 0 : $gastos_finan_no_opera;
                $valor['gastos_imp_resta'] = $gastos_imp_resta == '' ? $gastos_imp_resta = 0 : $gastos_imp_resta;
                $valor['gastos_opera_conjunta'] = $gastos_opera_conjunta == '' ? $gastos_opera_conjunta = 0 : $gastos_opera_conjunta;

                return $valor;

            break;

        }

        if(!$valor){
            return 0;
        }
        if($valor->diferencia <=0){
            $valor->diferencia = $valor->diferencia * -1;
        }
        // se multiplica * 1 para volverlo numero
        return $valor->diferencia * 1;
    }

    function number_format_short( $n, $tipo_visualiza = '' ) {

        if($n<=0){
            return 0;
        }

        if ($n > 0 && $n < 1000) {
            // 1 - 999
            $n_format = floor($n);
            $suffix = '';
        } else if ($n >= 1000 && $n < 1000000) {
            // 1k-999k
            $n_format = floor($n / 1000);
            $suffix = ' Mil';
        } else if ($n >= 1000000 && $n < 1000000000) {
            // 1m-999m
            $n_format = floor($n / 1000000);
            $suffix = ' Mill';
        } else if ($n >= 1000000000 && $n < 1000000000000) {
            // 1b-999b
            //$n_format = floor($n / 1000000000);
            $n_format = floor($n / 1000000);
            $suffix = ' Mill';
        } else if ($n >= 1000000000000) {
            // 1t+
            $n_format = floor($n / 1000000000000);
            $suffix = 'T+';
        }
    
        if($tipo_visualiza == 'sin_letra'){
            $suffix = '';
        }

        return !empty($n_format . $suffix) ? $n_format . $suffix : 0;
    }

    public function convertirString($string){

        if($string == 'Primer trimestre'){
            return 'primer_trimestre';
        } else if($string == 'Segundo trimestre'){
            return 'segundo_trimestre';
        } else if($string == 'Tercer trimestre'){
            return 'tercer_trimestre';
        } else if ($string == 'Cuarto trimestre'){
            return 'cuarto_trimestre';
        } else if ($string == 'Primer semestre'){
            return 'primer_semestre';
        } else if ($string == 'Segundo semestre'){
            return 'segundo_semestre';
        }

    }

    public function ejecutarFuncionFinanciera(Request $request){

        $data = array();

        if($request->accion == 'individual'){

            if($request->trimestre){
                $request->mes = $this->convertirString($request->trimestre);
            }
    
            $fechas = $this->obtenerInicioFinMes($request->mes, $request->anio);
    
            $this->fechaInicio = $fechas[0];
            $this->fechaFinal = $fechas[1];
    
            $data['ing_ordinarios'] = round($this->obtenerIngresosPorTipo('ing_ordinarios'));
            $data['ing_ordinarios_label'] = $this->number_format_short(round($this->obtenerIngresosPorTipo('ing_ordinarios')), 'con_letra');
            $data['ing_ordinarios_text'] = 'Ingresos Ordinarios';
    
            $data['otros_ingresos'] = round($this->obtenerIngresosPorTipo('otros_ingresos') - $this->obtenerIngresosPorTipo('ing_financieros'));
            $data['otros_ingresos_label'] = $this->number_format_short(round($this->obtenerIngresosPorTipo('otros_ingresos') - $this->obtenerIngresosPorTipo('ing_financieros')), 'con_letra');
            $data['otros_ingresos_text'] = 'Otros Ingresos';
    
            $data['ing_financieros'] = round($this->obtenerIngresosPorTipo('ing_financieros'));
            $data['ing_financieros_label'] = $this->number_format_short(round($this->obtenerIngresosPorTipo('ing_financieros')), 'con_letra');
            $data['ing_financieros_text'] = 'Ingresos Financieros';
    
            //ingresos por harina
            $data['ing_harinas'] = $this->obtenerIngresosPorTipo('ing_harinas');
    
            //ingresos por pasta
            $data['ing_pastas'] = $this->obtenerIngresosPorTipo('ing_pastas');
    
            //ingresos por subproductos
            $data['ing_subproductos'] = $this->obtenerIngresosPorTipo('ing_subproductos');
    
            //ingresos por subproductos
            $data['ing_agropecuarios'] = $this->obtenerIngresosPorTipo('ing_agropecuarios');
    
            // ingresos no fabricados
            $data['ing_no_fabricados'] = $this->obtenerIngresosPorTipo('ing_no_fabricados');
    
            // ingresos mezclas secas
            $data['ing_mezclas'] = $this->obtenerIngresosPorTipo('ing_mezclas');
    
            $data['total_ingresos'] = round($data['ing_ordinarios'] + $data['otros_ingresos'] + $data['ing_financieros']);
            $data['total_ingresos_label'] = $this->number_format_short(round($data['ing_ordinarios'] + $data['otros_ingresos'] + $data['ing_financieros']), 'con_letra');
    
            $data['costos'] = round($this->obtenerIngresosPorTipo('costos'));
            $data['costos_label'] = $this->number_format_short(round($this->obtenerIngresosPorTipo('costos')), 'con_letra');
            $data['costos_detallados'] = $this->obtenerIngresosPorTipo('costos_detallados');
    
            $data['gastos'] = round($this->obtenerIngresosPorTipo('gastos'));
            $data['gastos_label'] = $this->number_format_short(round($this->obtenerIngresosPorTipo('gastos')), 'con_letra');
            $data['gastos_detallados'] = $this->obtenerIngresosPorTipo('gastos_detallados');
    
            $data['utilidad'] = round($data['total_ingresos'] - $data['costos'] - $data['gastos']);
            // si da negativo se vuelve positivo
            $utilidad = $data['utilidad'];
            $data['utilidad_label'] = $this->number_format_short(round($utilidad), 'con_letra');
    
            $this->fechaInicio = '';
            $this->fechaFinal = '';
        
        } else if ($request->accion == 'multiple'){

            $meses = $this->obtenerMesesContablesAux($request->anio);

            for ($i=0; $i < count($meses) ; $i++) {

                $fechas = $this->obtenerInicioFinMes($meses[$i], $request->anio);

                $this->fechaInicio = $fechas[0];
                $this->fechaFinal = $fechas[1];

                $ing_ordinarios = $this->obtenerIngresosPorTipo('ing_ordinarios');
                $ing_financieros = $this->obtenerIngresosPorTipo('ing_financieros');
                $ing_otros = $this->obtenerIngresosPorTipo('otros_ingresos') - $this->obtenerIngresosPorTipo('ing_financieros');
                $total_ingresos = $ing_ordinarios + $ing_financieros + $ing_otros;

                $costos = $this->obtenerIngresosPorTipo('costos');
                $gastos = $this->obtenerIngresosPorTipo('gastos');

                $mes = $this->convertirNumeroMes($meses[$i]);
                $data['utilidad'][$i][$mes] = $this->number_format_short(round($total_ingresos - $costos - $gastos), 'sin_letra');
                $data['utilidad_aux'][$i] = ($total_ingresos - $costos - $gastos);
                $data['costos'][$i][$mes] = $this->number_format_short(round($costos), 'sin_letra');
            }

            $data['utilidad_hoy'] = $this->number_format_short(array_sum($data['utilidad_aux']));
        }

        return $data;
    }

    public function esBisiesto($year){

        return date('L',mktime(1,1,1,1,1,$year));

    }

    public function convertirNumeroMes($mesNumero){

        switch ($mesNumero) {

            case '01':
                return 'Ene';
            break;
            case '02':
                return 'Feb';
            break;
            case '03':
                return 'Mar';
            break; 
            case '04':
                return 'Abr';
            break;
            case '05':
                return 'May';
            break;
            case '06':
                return 'Jun';
            break;
            case '07':
                return 'Jul';
            break;
            case '08':
                return 'Ago';
            break;
            case '09':
                return 'Sep';
            break;
            case '10':
                return 'Oct';
            break;
            case '11':
                return 'Nov';
            break;
            case '12':
                return 'Dic';
            break;
            default:
                # code...
            break;
        }
    }
    public function obtenerInicioFinMes($mes, $anio){

        $datos = array();

        switch ($mes) {

            case '01':
                $mes = '01';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '02':
                $mes = '02';
                $dia = $this->esBisiesto($anio) ==   0 ? '28' : '29';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.$dia;
            break;

            case '03':
                $mes = '03';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '04':
                $mes = '04';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case '05':
                $mes = '05';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '06':
                $mes = '06';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case '07':
                $mes = '07';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '08':
                $mes = '08';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '09':
                $mes = '09';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case '10':
                $mes = '10';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case '11':
                $mes = '11';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case '12':
                $mes = '12';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            // semestre
            case 'primer_semestre':
                $datos[0] = $anio.'01'.'01';
                $datos[1] = $anio.'06'.'30';
            break;

            case 'segundo_semestre':
                $datos[0] = $anio.'07'.'01';
                $datos[1] = $anio.'12'.'31';
            break;

            // por trimestre
            case 'primer_trimestre':
                $datos[0] = $anio.'01'.'01';
                $datos[1] = $anio.'03'.'31';
            break;

            case 'segundo_trimestre':
                $datos[0] = $anio.'04'.'01';
                $datos[1] = $anio.'06'.'30';
            break;

            case 'tercer_trimestre':
                $datos[0] = $anio.'07'.'01';
                $datos[1] = $anio.'09'.'30';
            break;

            case 'cuarto_trimestre':
                $datos[0] = $anio.'10'.'01';
                $datos[1] = $anio.'12'.'31';
            break;
        }

        return $datos;
    }
}
