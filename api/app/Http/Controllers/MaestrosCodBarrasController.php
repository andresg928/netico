<?php

namespace App\Http\Controllers;

use App\MaestrosCodBarras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;

class MaestrosCodBarrasController extends Controller
{

    public function listarMaestros(Request $request){

        $maestros = MaestrosCodBarras::select('id as id_registro', 'deleted', 'cod_barras', 'consecutivo', 'nombre_consecutivo', 'cod_producto', 'nom_producto',
                                            'unidad_medida', 'cantidad','tipo_lote', 'usu_registra', 'usu_modifica', 'created_at', 'updated_at')
                                        ->where('deleted', '0')
                                        ->orderBy('cod_producto', 'DESC')->get();
    
        for ($i=0; $i<count($maestros); $i++){

        }

        return $maestros;
    }

    public function registrarMaestro(Request $request){

        $datos = $request->info;

        // Valida que el cod de barras no se repita
        $info_maestro = MaestrosCodBarras::where('cod_barras', $datos['codigo_barra'])->where('deleted', '0')->first();
        if(count($info_maestro)>0){
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'El código de barras ya se encuentra registrado'
            ],200);  
        }

        // Valida que no exista otro registro con el mismo consecutivo , cod de producto y cantidad
        $info_maestro = MaestrosCodBarras::where('consecutivo', $datos['consecutivo']['CODIGOCONS'])
                                          ->where('cod_producto', $datos['producto']['codigo'])
                                          ->where('cantidad', $datos['cantidad'])
                                          ->where('deleted', '0')->first();
        if(count($info_maestro)>0){
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'Maestro existente con la configuración -> consecutivo: '.$info_maestro['consecutivo'].', producto: '.$info_maestro['cod_producto'].' y cantidad: '.$info_maestro['cantidad']
            ],200);  
        }

        $maestro = new MaestrosCodBarras();
        $maestro->id = Uuid::generate()->string;
        $maestro->cod_barras = $datos['codigo_barra'];
        $maestro->consecutivo = $datos['consecutivo']['CODIGOCONS'];
        $maestro->nombre_consecutivo = $datos['consecutivo']['DESCRIPCIO'];
        $maestro->cod_producto = $datos['producto']['codigo'];
        $maestro->nom_producto = $datos['producto']['descripcion'];
        $maestro->unidad_medida = $datos['uni_medida'];
        $maestro->cantidad = $datos['cantidad'];
        $maestro->tipo_lote = $datos['tipo_lote'];
        $maestro->usu_registra = JWTAuth::user()->vendedor;
        $save = $maestro->save();

        if($save === true){
            return response()->json([
                'estado' => 'ok',
                'mensaje' => 'El registro se ha guardado correctamente.'
            ],200);
        } else {
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'No se pudo guardar el registro'
            ],200);
        }
        
    }

    public function actualizarMaestro(Request $request){

        $datos = $request->info;

        $maestro = MaestrosCodBarras::find($datos['id_registro']);

        // Valida que el cod de barras no se repita
        if($maestro->cod_barras !== $datos['codigo_barra']){
            $info_maestro = MaestrosCodBarras::where('cod_barras', $datos['codigo_barra'])->where('deleted', '0')->first();
            if(count($info_maestro)>0){
                return response()->json([
                    'estado' => 'error',
                    'mensaje' => 'El código de barras ya se encuentra registrado'
                ],200);  
            }
        }


        // Valida que no exista otro registro con el mismo consecutivo , cod de producto y cantidad
        if($maestro->consecutivo !== $datos['consecutivo']['CODIGOCONS'] && $maestro->cod_producto !== $datos['producto']['codigo'] && $maestro->cantidad !== $datos['cantidad']){

            $info_maestro = MaestrosCodBarras::where('consecutivo', $datos['consecutivo']['CODIGOCONS'])
                                            ->where('cod_producto', $datos['producto']['codigo'])
                                            ->where('cantidad', $datos['cantidad'])
                                            ->where('deleted', '0')->first();
            if(count($info_maestro)>0){
                return response()->json([
                    'estado' => 'error',
                    'mensaje' => 'Maestro existente con la configuración -> consecutivo: '.$info_maestro['consecutivo'].', producto: '.$info_maestro['cod_producto'].' y cantidad: '.$info_maestro['cantidad']
                ],200);  
            }

        }

        $maestro->cod_barras = $datos['codigo_barra'];
        $maestro->consecutivo = $datos['consecutivo']['CODIGOCONS'];
        $maestro->nombre_consecutivo = $datos['consecutivo']['DESCRIPCIO'];
        $maestro->cod_producto = $datos['producto']['codigo'];
        $maestro->nom_producto = $datos['producto']['descripcion'];
        $maestro->unidad_medida = $datos['uni_medida'];
        $maestro->cantidad = $datos['cantidad'];
        $maestro->tipo_lote = $datos['tipo_lote'];
        $maestro->usu_modifica = JWTAuth::user()->vendedor;
        $save = $maestro->save();

        if($save === true){
            return response()->json([
                'estado' => 'ok',
                'mensaje' => 'El registro se modificó correctamente.'
            ],200);
        } else {
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'No se pudo modificar el registro'
            ],200);
        }
        
    }

    public function eliminarMaestro(Request $request){

        $datos = $request->info;

        $maestro = MaestrosCodBarras::find($datos['id_registro']);
        $maestro->deleted = '1';
        $maestro->usu_modifica = JWTAuth::user()->vendedor;
        $save = $maestro->save();

        if($save === true){
            return response()->json([
                'estado' => 'ok',
                'mensaje' => 'Registro eliminado correctamente'
            ],200);
        } else {
            return response()->json([
                'estado' => 'error',
                'mensaje' => 'No se pudo eliminar el registro'
            ],200);
        }
        
    }
}
