<?php

namespace App\Http\Controllers;

use App\facturacion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FacturacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function obtenerInfOfima($placa = ''){

        //$request->fecha = '20200304';
        $date = Carbon::now();
        $fecha_actual = $date->format('Ymd');
        // $fecha_actual = '20210327';

        $datos = \DB::connection('sqlsrv')
                                    ->table('MTPEDIDO_REMISION as p')
                                    ->select('mv.FECHA', 'p.placa', 'p.NRODCTO', 'p.TIPODCTO',
                                            DB::raw("CONVERT(decimal(10,2),round(pro.MEAPSVR * mv.CANTIDAD, 2)) as peso_bruto"),'pro.CODIGO as cod_pro')
                                    ->join('MVTRADE as mv', DB::raw("replace(mv.nrodcto,' ', '')"), '=', DB::raw("replace(p.nrodcto,' ', '')"))
                                    ->where(DB::raw("CONVERT(varchar(10), p.tipodcto)"), '=', DB::raw("CONVERT(varchar(10), mv.tipodcto)"))
                                    ->join('MTMERCIA as pro', 'pro.codigo', '=', DB::raw("replace(mv.producto,' ', '')"))
                                    ->where('mv.fecha', '=', $fecha_actual)
                                    ->where('p.placa','LIKE','%'.$placa.'%')
                                    ->whereIn('mv.tipodcto', ['RF','F1','F2','MO','PA','RP','RH','RS','N3','N4','N5','D1','NC'])
                                    ->orderBy('p.placa', 'DESC')->get();
        return $datos;

    }
    //ESTA FUNCTION DEBE IR EN TAREAS PROGRAMADAS, CADA X TIEMPO
    public function insertarDatosOfimaFacturacion(){

        $datos_ofima = $this->obtenerInfOfima();
        
        if($datos_ofima){
            //recorro el objeto
            for($i=0;$i<count($datos_ofima); $i++){
                //se verifica si el registro ya esxiste en la tabla para no volver a insertarlo
                $nro_dcto = $datos_ofima[$i]->NRODCTO;
                $tipo_dcto = $datos_ofima[$i]->TIPODCTO;
                $placa = $datos_ofima[$i]->placa;
                $fecha = substr($datos_ofima[$i]->FECHA, 0, 10);
                $peso_bruto = $datos_ofima[$i]->peso_bruto;
                $cod_pro = $datos_ofima[$i]->cod_pro;

                $facturacion = Facturacion::where('nro_dcto', $nro_dcto)->where('tipo_dcto', $tipo_dcto)->where('placa', $placa)->where('cod_pro', $cod_pro)->first();

                if(!$facturacion){
                    $fact = new Facturacion();
                    $fact->fecha = $fecha;
                    $fact->nro_dcto = $nro_dcto;
                    $fact->tipo_dcto = $tipo_dcto;
                    $fact->placa = $placa;
                    $fact->peso_bruto = $peso_bruto;
                    $fact->cod_pro = $cod_pro;
                    $fact->save();
                }
            }
            
        }

        // return $datos_ofima;

    }

    public function obtenerPesoPorPlaca(Request $request){

        // $request->placa = 'VLG728';
        $request->fecha = '2020-02-12';

        $facturacion = Facturacion::where('placa', $request->placa)->where('fecha', $request->fecha)->where('gestionado', 'no')->get();
        $total_kilos = 0;

        for ($i=0; $i<count($facturacion);$i++){
            $total_kilos = $total_kilos + $facturacion[$i]['peso_bruto'];
        }

        if($total_kilos){
            return response()->json([
                "respuesta" => round($total_kilos, 2),
            ],200);
        } else {
            return response()->json([
                "respuesta" => "error_respuesta",
            ],200);
        }

    }

    public function actualizarFacturaGestionada($placa){

        $date = Carbon::now();
        $fecha_actual = $date->format('Y-m-d');
        $fecha_actual = '2020-02-12';

        $facturacion = Facturacion::where('placa', $placa)->where('fecha', $fecha_actual )->where('gestionado', 'no')->get();

        for ($i=0; $i<count($facturacion);$i++){

            $fact = Facturacion::find($facturacion[$i]['id']);
            $fact->gestionado = 'si';
            $fact->save();

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function show(facturacion $facturacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function edit(facturacion $facturacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, facturacion $facturacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(facturacion $facturacion)
    {
        //
    }
}
