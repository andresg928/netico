<?php

namespace App\Http\Controllers;

use App\Despacho;
use App\Pedido;
use App\User;
use App\Despachos_turno;
use App\pedido_detalle;
use App\Jobs\ProcesarColaTrabajos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Mail;
use App\Mail\MailPedidos;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\NovedadSistemaController;
use App\Http\Controllers\FacturasPagasController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\DespachosTurnoController;
use App\Events\Notificaciones;

class DespachoController extends Controller
{

    public function obtenerDespachosEnBodega($idDespacho){

        $info_despacho = Despacho::select('despachos.id as id_despacho')
                                    ->where('despachos.id', $idDespacho)
                                    ->where('bodega', 'si')
                                    ->join('pedidos as p', 'p.id_despacho', '=', 'despachos.id')
                                    ->first();
        return $info_despacho;

    }

    public function obtenerDespachosConCertificado($data){


        $despachos = Despacho::select('despachos.id as id_despacho')
                                    ->where('p.deleted', '0')
                                    ->join('pedidos as p', 'p.id_despacho', '=', 'despachos.id')
                                    ->where('p.certificado_calidad', 'si')
                                    ->where('despachos.estado', 'cerrado');

                                    if($data['fecha_inicial']!=='' && $data['fecha_final']!==''){
                                        $despachos->whereRaw("DATE_FORMAT(despachos.created_at ,'%Y-%m-%d') BETWEEN '".$data['fecha_inicial']."' and '".$data['fecha_final']."'");
                                    }else{
                                        $despachos->where('despachos.id', $data['id_despacho']);
                                    }
                                    
                                    $despachos->where('despachos.deleted', '0');
                                    $despachos = $despachos->get()->toArray();

        $despachos_aux = array();

        foreach($despachos  as $val) {
            $despachos_aux[] = $val['id_despacho'];
        }
        return $despachos_aux;

    }

    public function listadoDespachos(Request $request){
		// 5 min
        set_time_limit(300);

        $roles = JWTAuth::user()->getRoleNames();
        $rol = $roles[0];

        $despachos = Despacho::select('despachos.consecutivo', 'despachos.id as id_despacho', 'despachos.fecha_entrega', 'u.name as vendedor', 
                                      'despachos.vendedor as codigo_vend', 'despachos.placa', 'despachos.conductor', 'despachos.nit_conductor', 
                                      'despachos.capacidad', 'despachos.estado', 'despachos.created_at as fecha','vp.num_pend', 'vg.num_gestionado', 'despachos.cargado',
                                      'despachos.autorizado')
                             ->where('despachos.deleted', '0')
                             ->where('despachos.estado', 'cerrado')
                             ->where('despachos.documento_vendedor', '<>', '')
                             ->whereRaw("DATE_FORMAT(despachos.created_at ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicial."' and '".$request->fecha_final."'")
                             ->join('users as u', 'u.documento_identidad', '=', 'despachos.documento_vendedor')
                             ->leftjoin('v_despachos_pendientes as vp', 'vp.despacho', '=', 'despachos.id')
                             ->leftjoin('v_despachos_gestionado as vg', 'vg.despacho', '=', 'despachos.id')
                             ->orderBy('despachos.consecutivo', 'DESC');

                             if($rol == 'RECEPCION') {
                                $despachos->where('autorizado', 'si');
                             }

                             $despachos = $despachos->get();

        $turno_ctrl = new DespachosTurnoController();
        $turnos_reportados = $turno_ctrl->obtenerTurnosPorEstado('reportado');

        //Obtiene los ids de despacho que tienen certificados de calidad
        $data = array();
        $data['fecha_inicial'] = $request->fecha_inicial;
        $data['fecha_final'] = $request->fecha_final;
        $data['id_despacho'] = '';
        $despachos_certificado = $this->obtenerDespachosConCertificado($data);

        for($i=0; $i<count($despachos); $i++) {

            // Valida si el despacho ya está reportado para que no se puede volver asignar ficho desde esta consulta
            if(in_array($despachos[$i]['id_despacho'], $turnos_reportados)){
                $despachos[$i]['reportado'] = 'si';
            }else{
                $despachos[$i]['reportado'] = 'no';
            }

            // Valida si el despacho requiere certificado de calidad
            if(in_array($despachos[$i]['id_despacho'], $despachos_certificado)){
                $despachos[$i]['cert_calidad'] = 'si';
            }else{
                $despachos[$i]['cert_calidad'] = 'no';
            }

            $despachos[$i]['rol'] = $rol;

            $info_pedido = Pedido::select(DB::raw('SUM(valor_total) AS valor_total'),  DB::raw('SUM(total_kilos) AS total_kilos'))
                                    ->where('id_despacho', $despachos[$i]['id_despacho'])->where('deleted', '0')
                                    ->first();

            if($despachos[$i]['num_pend'] == null){
                $despachos[$i]['estado_aux'] = 'completo';
                //elimino del array la fila para no mostrar en el listado de despachos
                // unset($despachos[$i]);
            } else {
                $despachos[$i]['estado_aux'] = 'incompleto';
            }

            // controla que si hay un pedido detalle gestionado no se deje modificar info del despacho
            if($despachos[$i]['num_pend'] > 0 && $despachos[$i]['num_gestionado']>0){
                $despachos[$i]['parcial'] = 'si';
            } else {
                $despachos[$i]['parcial'] = 'no';
            }

            $despachos[$i]['valor_total'] = $info_pedido['valor_total'];
            $despachos[$i]['total_kilos'] = $info_pedido['total_kilos'];
            $despachos[$i]['fecha_registro'] = substr($despachos[$i]['fecha'], 0, -9); 

            //Saber si el despacho contiene pedidos que son para enviar a bodega
            //$desp_bodega = $this->obtenerDespachosEnBodega($despachos[$i]['id_despacho']);

            $despachos[$i]['bodega'] = 'no';
            /*if($desp_bodega){
                $despachos[$i]['bodega'] = 'si';
            }*/

            /*if($request->estado === 'false'){
                if ($despachos[$i]['estado_aux'] == 'incompleto'){
                    //array_splice($despachos, 1, 1, true);
                    array_splice($despachos, $i, 1);
                } 
            }*/
        }

        return $despachos;
    }

    public function reprocesarPedido($idPedido){
        // Cuando se modifica una linea debe actualizar las cantidades y valor del pedido
        $detalle = pedido_detalle::select('valor', 'peso', 'cantidad')->where('id_pedido', $idPedido)->where('deleted', '0')->get();
        $valor_total = 0;
        $total_kikos = 0;

        for ($i=0; $i <count($detalle) ; $i++) { 
            $valor_total = $valor_total + ($detalle[$i]['cantidad'] * $detalle[$i]['valor']);
            $total_kikos = $total_kikos + ($detalle[$i]['peso'] * $detalle[$i]['cantidad']);
        }

        Pedido::where('id', $idPedido)->update(['valor_total' => $valor_total, 'total_kilos' => $total_kikos]);
        
    }

    public function editarDetallePedido(Request $request){

        DB::beginTransaction();
        try{
                $detalle = pedido_detalle::find($request->id_detalle);
                $detalle->cantidad_old = $detalle->cantidad;
                $detalle->cantidad = $request->cantidad;
                $detalle->usu_bodega = JWTAuth::user()->vendedor;
                $detalle->fec_reg_bodega = date('Y-m-d H:i:s');
                $saved = $detalle->save();
        
                if($saved === true){
                    $this->reprocesarPedido($detalle->id_pedido);
                    DB::commit();
                    return response()->json([
                        "estado" => "ok",
                        "mensaje" => 'Cantidad modificada correctamente.'
                    ],200);
                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'Ocurrió un error, vuelta a intentarlo'
                    ],200);
                }
        } catch(\Exception $e){

            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }

    public function consultaDespacho(Request $request){
		// 5 min
        set_time_limit(300);

        if($request->tipo_consulta == 'placa'){
            $condicion = "despachos.placa = '$request->placa'";
        } else {
            $condicion = "despachos.consecutivo = '$request->placa'";
        }

        //Valida si la consulta visualiza solo los autorizados o no
        if($request->solo_autorizado == 'si'){
            $condicion = $condicion ." and despachos.autorizado = 'si'";
        }

        $despachos = Despacho::select('despachos.consecutivo', 'despachos.id as id_despacho', 'despachos.fecha_entrega', 'u.name as vendedor', 
                                      'despachos.vendedor as codigo_vend', 'despachos.placa', 'despachos.conductor', 'despachos.nit_conductor', 
                                      'despachos.capacidad', 'despachos.estado', 'despachos.created_at as fecha', 'despachos.cargado',
                                      'despachos.usu_reg_cargado', 'despachos.fec_reg_cargado', 'dp.celular')
                             ->where('despachos.deleted', '0')
                             ->where('despachos.estado', 'cerrado')
                             ->where('despachos.documento_vendedor', '<>', '')
                             ->whereRaw($condicion)
                             ->join('users as u', 'u.documento_identidad', '=', 'despachos.documento_vendedor')
                             ->leftjoin('despachos_turnos as dp', 'dp.id_despacho', '=', 'despachos.id' )
                             ->orderBy('despachos.consecutivo', 'DESC')->take(1)->get();
        $now = Carbon::now();
        for($i=0; $i<count($despachos); $i++) {

            $info_pedido = Pedido::select(DB::raw('SUM(total_kilos) AS total_kilos'))
                                    ->where('id_despacho', $despachos[$i]['id_despacho'])->where('deleted', '0')
                                    ->first();

            $date = Carbon::parse($despachos[$i]['fecha']);
            $diferencia_en_dias = $date->diffInDays($now);

            if($despachos[$i]['celular']){
                $despachos[$i]['conductor'] = $despachos[$i]['conductor'].' ('.$despachos[$i]['celular'].')';
            }
            
            
            $despachos[$i]['alerta_fecha'] = '';
            if($diferencia_en_dias > 3){
                $despachos[$i]['alerta_fecha'] = 'Atencion: Despacho con mas de '.$diferencia_en_dias.' dias, verifique si este es el cargue vigente.';
            }

            $despachos[$i]['total_kilos'] = $info_pedido['total_kilos'];
            $despachos[$i]['fecha_registro'] = substr($despachos[$i]['fecha'], 0, -9);

            // Obtiene el detalle de los pedidos
            $despachos[$i]['info_pedidos'] = $this->consultaPedidoxDespacho($despachos[$i]['id_despacho'], $request->vista);
            $despachos[$i]['pedidos'] = $this->obtenerPedidosxDespacho($despachos[$i]['id_despacho']);

        }

        return $despachos;
    }

    public function consultaPedidoxDespacho($idDespacho, $vista){

        if( $vista == 'agrupada'){

            $info_pedido = Despacho::select('pd.cod_producto', 'pd.nom_producto', DB::raw('SUM(pd.cantidad) as cantidad'), 'pd.unidad_medida')
                                    ->join('pedidos as p', 'p.id_despacho', '=', 'despachos.id')
                                    ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'p.id')
                                    ->where('despachos.id', $idDespacho)
                                    ->where('p.deleted', '0')
                                    ->groupBy('pd.cod_producto','pd.nom_producto','pd.unidad_medida')
                                    ->orderBy('pd.cod_producto', 'ASC')
                                    ->get();

        } else {
            $info_pedido = Despacho::select('pd.id as id_pedido_detalle', 'p.cliente', 'pd.cod_producto', 'pd.nom_producto', 'pd.cantidad', 'pd.unidad_medida')
                                    ->join('pedidos as p', 'p.id_despacho', '=', 'despachos.id')
                                    ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'p.id')
                                    ->where('despachos.id', $idDespacho)
                                    ->orderBy('p.cliente', 'DESC')
                                    ->get();
        }

        return $info_pedido;
    }

    public function obtenerProductosPorDespacho(Request $request){

        $productos = Despacho::select('pd.id as id_pedido_detalle', 'p.cliente', 'pd.cod_producto', 'pd.nom_producto', 'pd.cantidad', 'pd.unidad_medida')
                                ->join('pedidos as p', 'p.id_despacho', '=', 'despachos.id')
                                ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'p.id')
                                ->where('despachos.id', $request->id_despacho)
                                ->where('pd.cod_producto', $request->cod_producto)
                                ->orderBy('pd.cantidad', 'DESC')
                                ->get();
        return $productos;
    }

    public function obtenerProductosDespachoPorFecha(Request $request){

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        $pedido_ctrl = new PedidoController();

        if($request->tipo_consulta == ''){
            $fecha_ini = $request->fecha_inicial;
            $fecha_fin = $request->fecha_final;
        }else{
            $fechas = $pedido_ctrl->obtenerFeciniFecfinMysql($request->tipo_consulta);
            $fecha_ini = $fechas['fecha_inicial'];
            $fecha_fin = $fechas['fecha_final'];            
        }

        $despachos = Despacho::select('pedidos.cliente', DB::raw("SUM(pd.cantidad * pd.peso)/$unidad_medida as peso"))
                                ->join('pedidos as pedidos', 'pedidos.id_despacho', '=', 'despachos.id')
                                ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'pedidos.id')
                                ->whereRaw("pd.cod_producto not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(pd.nom_producto not like '%mogolla%' and pd.nom_producto not like '%salvado%' and pd.nom_producto not like '%retal%' and pd.nom_producto not like '%polipr%' and pd.nom_producto not like '%flete%' and pd.nom_producto not like '%polvil%' and pd.nom_producto not like '%trigo%')")
                                ->whereRaw("pd.cod_producto not like '5%'")
                                ->where('pd.deleted', '0')
                                ->where('pedidos.deleted', '0')
                                ->whereBetween('despachos.fecha_entrega', [$fecha_ini, $fecha_fin])
                                ->where('despachos.deleted', '0')
                                ->where('despachos.estado', 'cerrado')
                                ->groupBy('pedidos.cliente')
                                ->orderBy('peso', 'DESC')
                                ->get();

        return $despachos;
    }

    public function obtenerItemPedidoPorDespacho1($id_despacho, $accion){
		
		$pedidos = Pedido::select('id  as id_ped')->where('id_despacho', $id_despacho)->where('deleted', '0')->get();
		
		$items_pedidos = 0;
		
		for($i=0; $i<count($pedidos); $i++) {
			
			$ped_det = pedido_detalle::where('id_pedido', $pedidos[$i]['id_ped'])->whereRaw('nro_pedido_ofima is not null')->count();
			$items_pedidos = $items_pedidos + $ped_det; 
		}
		
        return $items_pedidos;
    }


    public function obtenerItemPedidoPorDespacho2($id_despacho, $accion){
				
		$pedidos = Pedido::select('id  as id_ped')->where('id_despacho', $id_despacho)->where('deleted', '0')->get();
		
		$items_pedidos = 0;
		
		for($i=0; $i<count($pedidos); $i++) {
			
			$ped_det = pedido_detalle::where('id_pedido', $pedidos[$i]['id_ped'])->count();
			$items_pedidos = $items_pedidos + $ped_det; 
		}
		
        return $items_pedidos;
    }

    public function obtenerItemPedidoPorDespacho($id_despacho, $accion){

        // $accion === '' ? 'pd.nro_pedido_ofima is null' : 'pd.nro_pedido_ofima is not null';

        $items_pedidos = Pedido::whereRaw($accion === 'nulos' ? 'pd.nro_pedido_ofima is not null' : 'pedidos.consecutivo > 0 ')
                                ->where('pedidos.id_despacho', $id_despacho)
                                ->where('pedidos.deleted', '0')
                                ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'pedidos.id')
                                ->count();
        return $items_pedidos;
    }

    public function obtenerInfoPlacaConductoresOfima(Request $request){

        $datos = \DB::connection('sqlsrv')
                                    ->table('v_Asin_VehiculosConductores as c')
                                    ->select('c.PLACA as PLACA', 'c.CAPACIDAD', DB::raw("RTRIM(c.NOMBRE_PROVEEDOR) AS NOMBRE_PROVEEDOR"), DB::raw("RTRIM(c.NOMBRE_CONDUCTOR) AS NOMBRE_CONDUCTOR"), DB::raw("RTRIM(c.NIT_CONDUCTOR) AS NIT_CONDUCTOR") )
                                    ->where('c.PLACA', 'like', '%'.$request->placa.'%')
                                    ->where('c.NOMBRE_CONDUCTOR', 'like', '%'.$request->conductor.'%')
                                    ->orderBy('c.PLACA', 'DESC')->get();
        return $datos;

    }


    public function establecerVendedor($cod_vendedor, $funcion){

        $roles = JWTAuth::user()->getRoleNames();
        $rol = $roles[0];
        // Para que le aparezca habilitado todos los clientes y puedan pasar pedidos
        if($rol === 'AUXILIAR_VENTAS' || $rol === 'JEFE_CARTERA'){
            return '';
        }

        // 008 luis enrrique que podra visualizar todos los clientes (orden de LJ el dia 04-10-2022)
        if($cod_vendedor === 'ADMIN' || $cod_vendedor === 'APINZON' || $cod_vendedor === '008' || $cod_vendedor === 'LVEGA') {
            return '';
        } else if ($cod_vendedor === '004' && ($funcion == 'obtenerClientesOfima' || $funcion == 'obtenerListadoClientes' || $funcion == 'obtenerCarteraFecha' || $funcion == 'obtenerDeudaCliente')){
            return '';
        }else {
            return $cod_vendedor;
        }
    }
    public function obtenerClientesOfima(Request $request){

        $vendedor = $this->establecerVendedor(JWTAuth::user()->vendedor, 'obtenerClientesOfima');

        $clientes = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Cliente) AS NIT"), DB::raw("RTRIM(c.Nombre_Cliente) AS NOMBRE"), DB::raw("RTRIM(c.Nombre_Ciudad) AS CIUDAD"), 'Cupo_Credito', 'Id_Alterno')
                                    ->where('c.Nombre_Cliente', 'like', '%'.$request->nombre.'%')
                                    ->where('c.Vendedor', 'like', $vendedor.'%');
                                    $clientes->where('c.Habilitado', 1);
                                    if($request->sucursal == 'no'){
                                        $clientes->where('c.Cliente', 'not like', '%S%');
                                    }
                                    //->where('c.NIT', 'not like', '%S%')
                                    $clientes->orderBy('c.Nombre_Cliente', 'ASC');
                                    $clientes = $clientes->get();
        return $clientes;
    }

    public function obtenerClientesxVendedor($codVendedor){

        $clientes = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Cliente) AS NIT"), DB::raw("RTRIM(c.Nombre_Cliente) AS NOMBRE"), DB::raw("RTRIM(c.Nombre_Ciudad) AS CIUDAD"), 'Cupo_Credito', DB::raw("RTRIM(c.Vendedor) AS Vendedor"))
                                    ->where('c.Vendedor', 'like', $codVendedor.'%')
                                    ->where('c.Vendedor', '!=', '0')
                                    ->where('c.Habilitado', 1)
                                    ->where('c.Cliente', 'not like', '%S%')
                                    ->orderBy('c.Nombre_Cliente', 'ASC')->get();
        return $clientes;
    }

    public function obtenerListadoClientes(Request $request){

        $vendedor = $this->establecerVendedor(JWTAuth::user()->vendedor, 'obtenerListadoClientes');

        $clientes = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Cliente) AS NIT"), DB::raw("RTRIM(c.Nombre_Cliente) AS NOMBRE"), DB::raw("RTRIM(c.Nombre_Ciudad) AS CIUDAD"),DB::raw("RTRIM(c.E_Mail) AS EMAIL"),
                                             'Cupo_Credito', DB::raw("RTRIM(c.Direccion) AS DIRECCION"), DB::raw("RTRIM(c.Telefono_1) AS Tel1"),DB::raw("RTRIM(c.Telefono_2) AS Tel2"))
                                    ->where('c.Vendedor', 'like', $vendedor.'%')
                                    ->where('c.Nombre_Cliente', 'like', '%'.$request->nombre.'%')
                                    ->where('c.Habilitado', 1)
                                    ->orderBy('c.Nombre_Cliente', 'ASC')
                                    ->paginate(5);
        return $clientes;
    }

    public function obtenerPrecioUltimosProductos(Request $request){

        $data = DB::connection('sqlsrv')
                                    ->table('v_Ico_InformeVentasPorFecha as i')
                                    ->select('i.fecha', 'i.PRODUCTO as CODPRO', DB::raw("RTRIM(i.DESCRIPCIO) AS DESCRIPCIO"), 'i.valor_unitario')
                                    ->whereRaw("i.TIPODCTO in ('f1','f2', 'rp', 'rf')")
                                    ->where('i.nit', $request->nit)
                                    ->orderBy('i.fecha', 'DESC')
                                    ->take(10)
                                    ->get();
        return $data;
    }

    public function obtenerCupoCliente($nit){

        $cliente = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Cliente) AS NIT"), DB::raw("RTRIM(c.Nombre_Cliente) AS NOMBRE"), DB::raw("RTRIM(c.Nombre_Ciudad) AS CIUDAD"), 'Cupo_Credito', 'Lista_Precio', 'Plazo')
                                    ->where('c.Cliente', '=', $nit)
                                    //->where('c.ESCLIENTE', '=', 'S')
                                    //->where('c.NIT', 'not like', '%S%')
                                    ->orderBy('c.Nombre_Cliente', 'DESC')
                                    ->first();
        return $cliente;
    }

    public function obtenerDeudaCliente(Request $request){

        $hoy = date("Ymd");

        $saldo = \DB::connection('sqlsrv')
                                            ->table("dbo.fnvOF_ReporteCartera('".$hoy."','".$hoy."')")
                                            ->select(DB::raw("RTRIM(Nombre_Cliente) AS Nombre_Cliente"), DB::raw("SUM(Saldo) AS saldo"))
                                            ->where('saldo', '<>', '0')
                                            ->where('Nombre_Cliente', '=', $request->filtro)
                                            ->groupBy('Nombre_Cliente')
                                            ->get();
        return $saldo;
    }

    public function obtenerCarteraFecha (Request $request) {

        $vendedor = $this->establecerVendedor(JWTAuth::user()->vendedor, 'obtenerCarteraFecha');

        $anio = date("Y");
        $mes = date("m");
        $dia = date("d");
        $hoy = $anio.$mes.$dia;

        $filtro = $this->eliminarAcentos($request->filtro);

        $cartera = \DB::connection('sqlsrv')
                                            ->table("dbo.fnvOF_ReporteCartera('".$hoy."','".$hoy."')")
                                            ->select(   DB::raw("RTRIM(Cliente) AS Cliente"),DB::raw("RTRIM(Nombre_Cliente) AS Nombre_Cliente"),'T_Dcto', DB::raw("RTRIM(Documento) AS Documento"), DB::raw('CONVERT(VARCHAR(10), F_Expedic, 103) as F_Expedic'), 
                                                        DB::raw('CONVERT(VARCHAR(10), F_Vencim, 103) as F_Vencim'),'DiasVc','Deuda','Pagado','Saldo','Mt_Cupo_Credito',
                                                        'Nombre_Ciudad'
                                                    )
                                            ->where('saldo', '<>', '0')
                                            ->where('Vendedor', 'like', $vendedor.'%')
                                            ->where('Nombre_Cliente', 'like', '%'.$filtro.'%')
                                            ->where('Documento', 'like', '%'.$request->factura.'%')
                                            ->orderBy('F_Inicial', 'DESC')
                                            ->get();

        $deuda = $this->obtenerCupoDisponibleNombre($filtro);

        $ctl_fact_desc = new FacturasPagasController();
        $fact = $ctl_fact_desc->obtenerFacturasDescargadas();

        for ($i=0; $i<count($cartera); $i++){

            $factura = $cartera[$i]->T_Dcto.$cartera[$i]->Documento;

            if(in_array($factura, $fact)){
                $cartera[$i]->factura_paga = 'si';
            }else{
                $cartera[$i]->factura_paga = 'no';
            }

            foreach ($deuda as $clave=>$value) {
                if($cartera[$i]->Cliente == $clave){
                    $cartera[$i]->deuda_total = $cartera[$i]->Mt_Cupo_Credito - $value;
                }
            } 
        }

        return $cartera;
    }

    public function obtenerCupoDisponibleNombre ($filtro) {

        $anio = date("Y");
        $mes = date("m");
        $dia = date("d");
        $hoy = $anio.$mes.$dia;
        $cupo_aux = array();
        

        $cupo = \DB::connection('sqlsrv')
                                            ->table("dbo.fnvOF_ReporteCartera('".$hoy."','".$hoy."')")
                                            ->select(DB::raw("SUM(Saldo) as saldo"), DB::raw("RTRIM(Cliente) AS Cliente"), DB::raw("RTRIM(Nombre_Cliente) AS Nombre_Cliente"))
                                            ->where('saldo', '<>', '0')
                                            ->where('Nombre_Cliente', 'like', '%'.$filtro.'%')
                                            ->groupBy('Cliente', 'Nombre_Cliente')
                                            ->get();
        for ($i=0; $i<count($cupo); $i++){

            $cupo_aux[$cupo[$i]->Cliente] = $cupo[$i]->saldo;
        }

        return $cupo_aux;
    }

    public function eliminarAcentos($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
		array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
		array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
		$cadena
		);

		//Reemplazamos la E y e
		$cadena = str_replace(
		array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
		array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
		$cadena );

		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );

		//Reemplazamos la O y o
		$cadena = str_replace(
		array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
		array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
		$cadena );

		//Reemplazamos la U y u
		$cadena = str_replace(
		array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
		array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
		$cadena );

		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
		array('Ñ', 'ñ', 'Ç', 'ç'),
		array('N', 'n', 'C', 'c'),
		$cadena
		);
		
		return $cadena;
	}

    public function enviarNotificacionEmail($datos = array()){

        $datos['nombre'] = 'Prueba';

        switch ($datos['accion']) {

            case 'cerrar_despacho':
                $ctl_auth = new AuthController();
                $info_usuario = $ctl_auth->obtenerInfoUsuarioxId($datos['vendedor']);

                $datos['cod_vendedor'] = $info_usuario['vendedor'];
                $datos['email_cartera'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_cartera');
                $notificar = $datos['email_cartera'];
            break;
            
            case 'autorizar':
                $ctl_auth = new AuthController();
                $info_usuario = $ctl_auth->obtenerInfoUsuarioxId($datos['vendedor']);

                $datos['nombre_vendedor'] = $info_usuario['name'];
                $datos['email_vendedor'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : $info_usuario['email'];
                $datos['email_facturacion'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_facturacion');
                $datos['cod_vendedor'] = $info_usuario['vendedor'];
                $notificar = $datos['email_facturacion'];
            break;

            case 'terminar_cargue':
                $datos['cod_vendedor'] = JWTAuth::user()->vendedor;
                $datos['email_facturacion'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_facturacion');
                $notificar = $datos['email_facturacion'];
            break;

            case 'notificar_vendedor_despacho_gestionado':
                $datos['email_vendedor'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : $datos['email_vendedor'];
                $notificar = $datos['email_vendedor'];
            break;

            case 'notifica_certificado_calidad':
                $datos['cod_vendedor'] = JWTAuth::user()->vendedor;
                $datos['email_calidad'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_calidad');
                $datos['copia_email_calidad'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.copia_email_calidad');
                $notificar = $datos['email_calidad'];
            break;

            case 'notifica_clientes_sin_venta':
                $datos['email_gerencia_ventas'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_gerencia_ventas');
                $datos['email_lider_ventas'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_lider_ventas');
                $datos['email_cartera'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_cartera');
                Carbon::setLocale('es');
                $dosMesesAntes = Carbon::now()->subMonth(2);
                $primerDiaMesAnt = $dosMesesAntes->firstOfMonth();

                $mesVigente = Carbon::now()->subMonth();
                $primerDiaMesAct = $mesVigente->firstOfMonth();
                $datos['mes_anterior'] = ucfirst($primerDiaMesAnt->monthName);
                $datos['mes_actual'] = ucfirst($primerDiaMesAct->monthName);
                // Envía el correo electrónico con el archivo adjunto
                $notificar = $datos['email_gerencia_ventas'];
            break;

            case 'gestionar_entrega':
                $datos['nombre_vendedor'] ='';
                $datos['emails_notif_entrega'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : config('global.email_notif_entrega');
                $notificar = $datos['emails_notif_entrega'];
            break;
        }

        Mail::to($notificar)->send(new MailPedidos($datos));

        if($datos['accion']=='cerrar_despacho'){
            $enviado = Mail::failures();
            if(!$enviado){
                $despacho_ = Despacho::find($datos['despacho_id']);
                $despacho_->correo_enviado = 'si';
                $despacho_->save();
            }
        }
    }
    public function obtenerProductosOfima (Request $request) {

        $productos = \DB::connection('sqlsrv')
                                    ->table('vReporteTopVentaProductos_ICO as p')
                                    //->join('MTMERCIA as pro', 'pro.codigo', '=', DB::raw("RTRIM(p.producto)"))
                                    ->select(
                                        DB::raw("RTRIM(p.codigo) AS codigo"), 
                                        DB::raw("RTRIM(p.nombre) AS nombre"), 
                                        'p.peso as peso',
                                        DB::raw("RTRIM(p.grupo) AS grupo"),
                                        DB::raw("RTRIM(p.unidad) AS unidad"),
                                        DB::raw("CONCAT(RTRIM(p.codigo), '-', RTRIM(p.nombre)) AS nombre_select")
                                    )
                                    //->where('p.Nombre_Producto', 'like', '%'.$request->producto.'%')
                                    //->orWhere('p.producto', 'like', '%'.$request->producto.'%')
                                    //->where('pro.esproducto', '1')
                                    //->where('pro.habilitado', '1')
                                    ->orderBy(DB::raw("RTRIM(p.TOTAL_VENTA)"), 'ASC')
                                    //->distinct()
                                    ->get();

        for ($i=0; $i < count($productos) ; $i++) {
            // obtener precio segun lista de precio por cliente
            $productos_paq = config('global.productos_convertir_paquetes');
            if(in_array($productos[$i]->codigo, $productos_paq)){
                $productos[$i]->unidad = 'PAQ';
                $productos[$i]->peso = 6;
            }
            $precio = $this->obtenerPrecioProducto($productos[$i]->codigo, $request->lista_precio);

            if($precio){
                $productos[$i]->precio = intval($precio->precio);
            } else {
                $productos[$i]->precio = 0;
            }

        }

        return $productos;

    }

    public function obtenerPrecioProducto($codPro, $CodlistaPrecio){

        $precio = \DB::connection('sqlsrv')
                                    ->table('MVPRECIO as p')
                                    ->select('precio')
                                    ->where('CODPRECIO', $CodlistaPrecio)
                                    ->where('CODPRODUC', $codPro)
                                    ->first();
        return $precio;
    }

    public function filtrosFecha($tipo) {

        $date = Carbon::now();
        $data = array();

        switch ($tipo) {
            case 'Hoy':
                $fecha_inicial = $date->format('Y-m-d');
                $fecha_final = $date->format('Y-m-d');
            break;
            case 'Ayer':
                $fecha_inicial = $date->yesterday()->format('Y-m-d');
                $fecha_final = $date->yesterday()->format('Y-m-d');
            break;
            case 'Esta semana':
                $fecha_inicial = $date->startOfWeek()->format('Y-m-d');
                $fecha_final = $date->endOfWeek()->format('Y-m-d');
            break;
            case 'Este mes':
                $fecha_inicial = $date->startOfMonth()->format('Y-m-d'); 
                $fecha_final = $date->endOfMonth()->format('Y-m-d'); 
            break;
            case 'Mes anterior':
                $fecha_inicial = $date->startOfMonth()->subMonth()->format('Y-m-d');// 2021-08-01 00:00:00
                $fecha_final = $date->endOfMonth()->format('Y-m-d');// 2021-08-31 23:59:59
            break;
            case 'Este año':
                $anio_actual = date('Y');
                $fecha_inicial = $anio_actual.'-01-01';
                $fecha_final =   $anio_actual.'-12-31';
            break;

            default:
                $fecha_inicial = '2001-01-01';
                $fecha_final = '2100-12-31';
        }

        $data[0] = $fecha_inicial;
        $data[1] = $fecha_final;

        return $data;
    }

    public function obtenerInformacionDetallada(Request $request){

        $ctl_auth = new AuthController();

        $vendedor = $this->establecerVendedor(JWTAuth::user()->vendedor, 'obtenerInformacionDetallada');

        $roles = JWTAuth::user()->getRoleNames();
        $rol = $roles[0];

        if($rol === 'AUXILIAR_VENTAS' || $rol === 'JEFE_CARTERA'){
            $vendedor = JWTAuth::user()->vendedor;
        }

        $filtro_fecha = $this->filtrosFecha($request->filtro_fecha);
        $fecha_inicial = $filtro_fecha[0];
        $fecha_final = $filtro_fecha[1];
        $filtro = $request->filtro_general;

        $despachos_compartidos = Despacho::select('id as id_despacho', 'vendedor', 'consecutivo', 'placa', 'fecha_entrega', 'conductor', 'created_at as fecha_reg', 
                                                    'capacidad', 'estado','compartido', 'compartido_cod_vendedor', 'compartido_nom_vendedor', 'autorizado', 'cargado')
                                ->where('compartido_cod_vendedor', 'like', JWTAuth::user()->vendedor.'%')
                                ->where('deleted', '0')
                                ->whereRaw("DATE_FORMAT(created_at ,'%Y-%m-%d') BETWEEN '".$fecha_inicial."' and '".$fecha_final."'")
                                ->where('estado', '<>', 'temp');
                                if($filtro){
                                    $despachos_compartidos->where(function($query) use ($filtro) {
                                        $query->where('placa','like', '%'.$filtro.'%')
                                                    ->orWhere('estado','like', '%'.$filtro.'%')
                                                    ->orWhere('conductor','like', '%'.$filtro.'%')
                                                    ->orWhere('consecutivo','like', '%'.$filtro.'%');
                                    });
                                }

        $despachos =            Despacho::select('id as id_despacho', 'vendedor', 'consecutivo', 'placa','fecha_entrega', 'conductor', 'created_at as fecha_reg', 
                                                'capacidad', 'estado','compartido', 'compartido_cod_vendedor', 'compartido_nom_vendedor', 'autorizado', 'cargado')
                                ->where('vendedor', 'like', $vendedor.'%')
                                ->union($despachos_compartidos)
                                ->where('deleted', '0')
                                ->whereRaw("DATE_FORMAT(created_at ,'%Y-%m-%d') BETWEEN '".$fecha_inicial."' and '".$fecha_final."'")
                                ->where('estado', '<>', 'temp')
                                ->orderBy('consecutivo', 'DESC');
                                //Aplica filtro si hay
                                if($filtro){
                                    $despachos->where(function($query) use ($filtro) {
                                        $query->where('placa','like', '%'.$filtro.'%')
                                                    ->orWhere('estado','like', '%'.$filtro.'%')
                                                    ->orWhere('conductor','like', '%'.$filtro.'%')
                                                    ->orWhere('consecutivo','like', '%'.$filtro.'%');
                                    });
                                }
                                $despachos = $despachos->paginate(100);
                                

        for($i=0; $i<count($despachos); $i++) {

            $pedidos = Pedido::where('id_despacho', $despachos[$i]['id_despacho'])->where('deleted', '0')->count();
            $despachos[$i]['nro_pedidos'] = $pedidos;
            $despachos[$i]['capacidad'] = $despachos[$i]['capacidad'];

            if($despachos[$i]['compartido'] == 'si'){
                if(JWTAuth::user()->vendedor == $despachos[$i]['compartido_cod_vendedor']){
                    $nom_vend = $ctl_auth->obtenerVendedores($despachos[$i]['vendedor']);
                    if(count($nom_vend)>0){
                        $despachos[$i]['compartido_nom_vendedor'] = $nom_vend[0]->nombre;
                    } else {
                        $despachos[$i]['compartido_nom_vendedor'] = $despachos[$i]['vendedor'];
                    }

                }
            }

            $info_pedido = Pedido::select(DB::raw('SUM(valor_total) AS valor_total'),  DB::raw('SUM(total_kilos) AS total_kilos'))
                                    ->where('id_despacho', $despachos[$i]['id_despacho'])->where('deleted', '0')
                                    ->first();

            $despachos[$i]['valor_total'] = $info_pedido['valor_total'];
            $despachos[$i]['total_kilos'] = $info_pedido['total_kilos'];
            $despachos[$i]['fecha_registro'] = $this->showDate(substr($despachos[$i]['fecha_reg'], 0, -9));

            $desp_bodega = $this->obtenerDespachosEnBodega($despachos[$i]['id_despacho']);
            $despachos[$i]['bodega'] = 'no';
            if($desp_bodega){
                $despachos[$i]['bodega'] = 'si';
            }
        }

        return $despachos;
    }

    public function showDate($date){
        $date = Carbon::parse($date);
        Carbon::setLocale('es');
        $formattedDate = $date->isoFormat('D MMM YY');
        return $formattedDate;
    }

    public function obtenerNumeroDespachosxEstado(Request $request){

        $vendedor = $this->establecerVendedor(JWTAuth::user()->vendedor, 'obtenerNumeroDespachosxEstado');
        $roles = JWTAuth::user()->getRoleNames();
        $rol = $roles[0];
        $data = array();
        if($rol == 'AUXILIAR_VENTAS' || $rol == 'JEFE_CARTERA'){
            $vendedor = JWTAuth::user()->vendedor;
        }

        $despachos = Despacho::where('vendedor', 'like', $vendedor.'%')
                                ->where('deleted', '0')
                                ->where('estado', '=', $request->estado)
                                ->count();

        $despachos_compartido = Despacho::where('compartido_cod_vendedor', 'like', $vendedor.'%')
                                ->where('deleted', '0')
                                ->where('estado', '=', $request->estado)
                                ->count();

        $data['por_cerrar'] = $despachos;
        $data['por_cerrar_compartido'] = $despachos_compartido;
        return $data;
    }
    public function obtenerTotalesPedidosxDespacho($idDespacho){

        $info_pedido = Pedido::select(DB::raw('SUM(valor_total) AS valor_total'),  DB::raw('SUM(total_kilos) AS total_kilos'))
                                ->where('id_despacho', $idDespacho)->where('deleted', '0')
                                ->first();
        return $info_pedido;
    }

    public function modoDesarrollo(){

        if (config('global.test') === true){
            return 'desarrollo';
        } else {
            return 'produccion';
        }
    }

    public function enviarNotificacionVendedorDespachoGestionado($data){

        $info_pedidos = $this->obtenerPedidosxDespacho($data['id_despacho']);
        $despacho['info_pedidos'] = $info_pedidos;

        $vendedor = User::select('id as id_usuario', 'name', 'email')->where('vendedor', $data['vendedor'])->where('deleted', '0')->first();
        $despacho['nombre_vendedor'] = $vendedor['name'];
        $despacho['cod_vendedor'] = $vendedor['vendedor'];
        $despacho['email_vendedor'] = $vendedor['email'];
        $despacho['despacho_id'] = $data['id_despacho'];
        $totales_despacho = $this->obtenerTotalesPedidosxDespacho($data['id_despacho']);
        $despacho['total_kilos'] = number_format($totales_despacho['total_kilos'], 0, ',','.');
        $despacho['accion'] = 'notificar_vendedor_despacho_gestionado';
        $despacho['fecha_entrega'] = $data['fecha_entrega'];
        $despacho['placa'] = $data['placa'];
        $despacho['conductor'] = $data['conductor'];
        $despacho['consecutivo'] = $data['consecutivo'];
        $despacho['created_at'] = $data['created_at'];
        ProcesarColaTrabajos::dispatch($despacho);
    }

	public function enviarProcesoColaTrabajo($data){
        $NotifiJob = (new ProcesarColaTrabajos($data))->delay(Carbon::now()->addSeconds(1));
        dispatch($NotifiJob);
    }

    public function enviarInformeClientesSinVenta(){
        $data['accion'] ='notifica_clientes_sin_venta';
        $data['placa'] ='';
        $data['conductor'] ='';
        $data['nombre_vendedor'] ='';
        $NotifiJob = (new ProcesarColaTrabajos($data))->delay(Carbon::now()->addSeconds(1));
        dispatch($NotifiJob);
    }

    public function checkPuerto($dominio, $puerto){
        $starttime = microtime(true);
        $file      = @fsockopen ($dominio, $puerto, $errno, $errstr, 10);
        $stoptime  = microtime(true);
        $status    = 0;
      
        if (!$file){    
            $status = -1;  // Sitio caído
        } else {
            fclose($file);
            $status = ($stoptime - $starttime) * 1000;
            $status = floor($status);
        }
         
        if ($status <> -1) {
            return true;
        } else {
            return false;
        }
    }

    public function cerrarDespacho(Request $request){

        $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));

        DB::beginTransaction();
        try{
                $ctl_novedad = new NovedadSistemaController();
                $novedad = $ctl_novedad->obtenerNovedad();
        
                if($novedad){
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => $novedad['mensaje']
                    ],200);             
                }

                $despacho = Despacho::find($request->id_despacho);

                if($request->accion == 'cerrar_despacho') {
                    if($despacho->estado == 'cerrado'){
                        DB::commit();
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => 'El despacho ya se encuentra cerrado'
                        ],200);
                    }
                    //Se cierra el despacho
                    $despacho->estado = 'cerrado';
                    $despacho->fec_cerrado = date('Y-m-d H:i:s');
                }

                if($request->accion == 'autorizar') {
                    if($despacho->estado == 'cerrado' && $despacho->autorizado == 'si'){
                        DB::commit();
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => 'El despacho ya se encuentra autorizado'
                        ],200);
                    }
                    //Se autoriza el despacho
                    $despacho->autorizado = 'si';
                    $despacho->usu_autorizado = JWTAuth::user()->vendedor;
                    $despacho->fec_autorizado = date('Y-m-d H:i:s');
                }

                $saved = $despacho->save();

                if($saved === true){

                    $info_pedidos = $this->obtenerPedidosxDespacho($request->id_despacho);
                    $despacho['info_pedidos'] = $info_pedidos;

                    $vendedor = JWTAuth::user()->name;
                    $despacho['nombre_vendedor'] = $vendedor;
                    $despacho['despacho_id'] = $request->id_despacho;
                    $totales_despacho = $this->obtenerTotalesPedidosxDespacho($request->id_despacho);
                    $despacho['total_kilos'] = number_format($totales_despacho['total_kilos'], 0, ',','.');
                    $despacho['accion'] = $request->accion;
                    $despacho['fecha_entrega'] = $despacho->fecha_entrega;
                    //ProcesarColaTrabajos::dispatch($despacho);
                    $this->enviarProcesoColaTrabajo($despacho);

                    // Dispara el evento de notificación
                    if($request->accion == 'cerrar_despacho' || $request->accion == 'autorizar'){
                        $data['accion'] = $request->accion;
                        $data['placa'] = '';
                        $data['modulo_notifica'] = 'listado_despachos';
                        $data['mensaje'] = $request->accion == 'cerrar_despacho' ? '¡Atención! Nuevo pedido por autorizar' : '¡Atención! Despacho '.$despacho->consecutivo. ' AUTORIZADO';

                        if($rpta == true) {
                            event(new Notificaciones($data));
                        }

                    }

                    DB::commit();
                    return response()->json([
                        "estado" => "ok",
                        "mensaje" => $request->accion == 'autorizar' ? 'Despacho autorizado exitosamente' : 'El despacho se cerró exitosamente'
                    ],200);
                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'Error al cerrar el despacho'
                    ],200);            
                }
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }

    public function terminarDespachoCargue(Request $request){


        DB::beginTransaction();
        try{
    
        $despacho = Despacho::find($request->id_despacho);

        if($despacho->cargado == 'si'){
            DB::commit();
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Este cargue ya se ha dado por finalizado, por favor recargue la página'
            ],200);            
        }

        $despacho->cargado = 'si';
        $despacho->fec_reg_cargado = date('Y-m-d H:i:s');
        $despacho->usu_reg_cargado = JWTAuth::user()->vendedor;
        $saved = $despacho->save();

        if($saved === true){
            // Finaliza la gestión del turno
            $turno = Despachos_turno::select('id as id_registro')->where('id_despacho', $request->id_despacho)->where('deleted', '0')->where('estado', 'en_cargue')->first();
            if($turno){
                $objTurno =  Despachos_turno::find($turno['id_registro']);
                $objTurno->deleted = '1';
                $objTurno->estado = 'cargado';
                $objTurno->usuario_finaliza = JWTAuth::user()->vendedor;
                $objTurno->fecha_hora_finaliza = date('Y-m-d H:i:s');
                $objTurno->save();
            }else{
                DB::commit();
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'Error, vuelva a intentarlo'
                ],200);  
            }
            $despacho['accion'] = 'terminar_cargue';
            ProcesarColaTrabajos::dispatch($despacho);

            $despacho['accion'] = 'notifica_vendedor_despacho_gestionado';
            $despacho['id_despacho'] = $request->id_despacho;
            $despacho['fecha_entrega'] = $despacho->fecha_entrega;
            $despacho['vendedor'] = $despacho->vendedor;
            $despacho['placa'] = $despacho->placa;
            $despacho['conductor'] = $despacho->conductor;
            $this->enviarNotificacionVendedorDespachoGestionado($despacho);

            $data['accion'] = 'terminar_cargue';
            $data['placa'] = $despacho->placa;
            $data['modulo_notifica'] = 'listado_despachos';
            $data['mensaje'] = '¡Atención! Facturar vehículo '.$data['placa']. ' notificado por: '.JWTAuth::user()->vendedor;
            $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));
            if($rpta === true){
                event(new Notificaciones($data));
            }


            DB::commit();
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Cargue terminado correctamente'
            ],200);
        } else {
            DB::commit();
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error, vuelva a intentarlo'
            ],200);            
        }
    }
    catch(\Exception $e){
        DB::rollback();
        return response()->json([
                "mensaje" => $e->getMessage(),
                "estado" => 'error'
        ],200);               
    }
    }

    public function obtenerPedidosxDespacho($idDespacho) {

        $pedidos['pedidos_encabezado'] = Pedido::select('id as id_pedido', 'nit', 'cliente', 'estado', 'valor_total', 'total_kilos', 'created_at as tiempo_transcurrido', 'observaciones', 'ciudad')
                            ->where('deleted', '0')
                            ->where('id_despacho', $idDespacho)
                            ->orderBy('consecutivo', 'DESC')->get();

        for($i=0; $i<count($pedidos['pedidos_encabezado']);$i++){

            $pedidos_detalle = pedido_detalle::where('id_pedido', $pedidos['pedidos_encabezado'][$i]['id_pedido'])->get();

            $pedidos['pedidos_encabezado'][$i]['pedidos_detalle'] = $pedidos_detalle;

        }

        return $pedidos;
    }

    public function store(Request $request){

        // Verifica que el usuario este activo
        $user = User::where('deleted', '1')->where('vendedor', JWTAuth::user()->vendedor)->get();

        if(count($user)>0){
            return response()->json([
                "estado" => "error",
                "id_despacho" => 'Usuario se encuentra inactivo'
            ],200);              
        }

        $ctl_novedad = new NovedadSistemaController();
        $novedad = $ctl_novedad->obtenerNovedad();

        if($novedad){
            return response()->json([
                "estado" => "error",
                "id_despacho" => $novedad['mensaje']
            ],200);             
        }

        if(!$request->nit_conductor){
            return response()->json([
                "estado" => "error",
                "id_despacho" => 'Error al crear el despacho, no se obtiene información del conductor'
            ],200);            
        }
        $despacho = New Despacho();
        $despacho->id = Uuid::generate()->string;
        $despacho->vendedor = JWTAuth::user()->vendedor;
        $despacho->documento_vendedor = JWTAuth::user()->documento_identidad;
        $despacho->placa = $request->placa;
        $despacho->fecha_entrega = $request->fecha_despacho;
        $despacho->capacidad = $request->capacidad;
        $despacho->conductor = $request->conductor;
        $despacho->nit_conductor = $request->nit_conductor;
        $despacho->estado = 'temp';
        $despacho->correo_enviado = 'no';
        $saved = $despacho->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "id_despacho" => $despacho->id
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "id_despacho" => ''
            ],200);            
        }

    }

    public function actualizaDespacho(Request $request){

        $despacho = Despacho::find($request->id_despacho);

        // Valida que no se elimine ni se desautorice cuando el despacho está REPORTADO
        $turno = Despachos_turno::where('id_despacho', $request->id_despacho)->where('deleted', '0')->first();

        if($request->accion == 'mod_full') {
            $despacho->placa = $request->placa;
            $despacho->conductor = $request->conductor;
            $despacho->nit_conductor = $request->nit_conductor;
            $despacho->capacidad = $request->capacidad;
        }
        
        if($request->accion == 'mod_fecha') {
            $despacho->fecha_entrega = $request->fecha_entrega;
        }

        if($request->accion == 'mod_estado') {

            if($despacho->autorizado == 'si'){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'No se puede abrir, el despacho se encuentra AUTORIZADO'
                ],200);                 
            }

            $despacho->estado = 'abierto';
        }

        if($request->accion == 'mod_estado_eliminar') {
            if($turno){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'No se puede eliminar el despacho ya que se encuentra en estado: ('.$turno['estado'].')'
                ],200);
            }
            $despacho->estado = 'eliminado';
            Pedido::where('id_despacho', $request->id_despacho)->update(['deleted'=>'1', 'updated_at'=>date('Y-m-d H:i:s')]);
        }

        if($request->accion == 'eliminar_despacho') {
            if($despacho->autorizado == 'si'){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'No se puede eliminar, el despacho se encuentra AUTORIZADO'
                ],200);                 
            }
            $despacho->estado = 'eliminado';
            $despacho->deleted = '1';
            Pedido::where('id_despacho', $request->id_despacho)->update(['deleted'=>'1', 'updated_at'=>date('Y-m-d H:i:s')]);
        }

        if($request->accion == 'desautorizar') {

            if($turno){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'No se puede DESAUTORIZAR el despacho ya que se encuentra en estado: ('.$turno['estado'].')'
                ],200);
            }

            if($despacho->estado == 'cerrado' && $despacho->autorizado == 'no'){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => 'No se puede desautorizar, el despacho se encuentra DESAUTORIZADO'
                ],200);                 
            }

            $despacho->autorizado = 'no';
            $despacho->usu_autorizado = JWTAuth::user()->vendedor;
            $despacho->fec_autorizado = date('Y-m-d H:i:s');
        }

        $despacho->usu_mod = JWTAuth::user()->vendedor;
        $saved = $despacho->save();

        if($saved === true){

            if($request->accion == 'desautorizar'){
                $data['accion'] = $request->accion;
                $data['modulo_notifica'] = 'listado_despachos';
                $data['mensaje'] = '¡Atención! Despacho desautorizado';
                $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));
                if($rpta === true){
                    event(new Notificaciones($data));
                }
            }

            // notifica si la placa se cambia ya cuando está cerrado el pedido, asegura que lo hace alguien de ventas
            if($request->accion == 'mod_full' && $despacho->estado = 'cerrado' && $despacho->autorizado = 'si'){
                $data['accion'] = $request->accion;
                $data['modulo_notifica'] = 'listado_despachos';
                $data['mensaje'] = '¡Atención! Cambio de conductor o de placa';
                $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));
                if($rpta === true){
                    event(new Notificaciones($data));
                }
            }

            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Despacho modificado exitosamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al modificar el despacho'
            ],200);            
        }
    }

    public function actualizaDespachoFechaFacturacion(Request $request){

        $despacho = Despacho::find($request->id_despacho);
        // esta fecha_entrega es la fecha estimada de facturacion
        $despacho->fecha_entrega = $request->fecha_facturacion;
        $despacho->usu_mod = JWTAuth::user()->vendedor;
        $saved = $despacho->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Fecha ingresada exitosamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al ingresar la fecha'
            ],200);            
        }
    }

    public function obtenerInfoDespachoxConsecutivo(Request $request){

        $despacho = Despacho::select('id as id_despacho', 'vendedor', 'placa', 'capacidad', 'conductor', 'estado')
                            ->where('consecutivo', $request->consecutivo)
                            ->first();

        $info_pedido = Pedido::select(DB::raw('SUM(valor_total) AS valor_total'),  DB::raw('SUM(total_kilos) AS total_kilos'))
                                ->where('id_despacho', $despacho['id_despacho'])->where('deleted', '0')
                                ->first();

        $despacho['valor_total'] = $info_pedido['valor_total'];
        $despacho['total_kilos'] = $info_pedido['total_kilos'];

        return $despacho;
    }

    public function compartirDespacho(Request $request){

        $cadena = explode('-', $request->vendedor);
        $despacho = Despacho::find($request->id_despacho);
        $despacho->compartido = 'si';
        $despacho->compartido_cod_vendedor = $cadena[0];
        $despacho->compartido_nom_vendedor = $cadena[1];
        $saved = $despacho->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'Despacho compartido con '.$despacho->compartido_nom_vendedor
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al compartir el despacho'
            ],200);            
        }
    }
}
