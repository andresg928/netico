<?php

namespace App\Http\Controllers;

use App\NovedadSistema;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;

class NovedadSistemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ejecutarNovedad(Request $request){

        if($request->novedad){

            $novedad = NovedadSistema::find($request->novedad['id_novedad']);
            $novedad->deleted = '1';
            $novedad->usu_modifica = JWTAuth::user()->vendedor;
            $save = $novedad->save();
            if($save === true){

                return response()->json([
                    "estado" => 'ok',
                    "mensaje" => 'La novedad se ha ingresado correctamente'
                ],200);
    
            } else {
    
                return response()->json([
                    "estado" => 'ok',
                    "mensaje" => 'Error al ingresar la novedad'
                ],200);
            
            }
        }

        $novedad = new NovedadSistema();
        $novedad->id = Uuid::generate()->string;
        $novedad->tipo = $request->tipo;
        $novedad->mensaje = $request->mensaje;
        $novedad->usu_registro = JWTAuth::user()->vendedor;
        $save = $novedad->save();

        if($save === true){
            return response()->json([
                "estado" => 'ok',
                "mensaje" => 'La novedad se ha ingresado correctamente'
            ],200);

        } else {

            return response()->json([
                "estado" => 'ok',
                "mensaje" => 'Error al ingresar la novedad'
            ],200);
        
        }

    }

    public function obtenerNovedad(){

        $novedad = NovedadSistema::select('id as id_novedad', 'deleted', 'mensaje')->where('deleted', '0')->where('tipo', 'cambia_precios')->first();
        return $novedad;

    }

}
