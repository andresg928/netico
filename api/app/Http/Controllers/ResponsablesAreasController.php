<?php

namespace App\Http\Controllers;

use App\ResponsablesAreas;
use Illuminate\Http\Request;
use  JWTAuth;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class ResponsablesAreasController extends Controller
{
    public function registrar(Request $request){

        $resp_area_aux = ResponsablesAreas::where('id_area', $request->id_area)->where('id_responsable', $request->id_responsable)->where('deleted', '0')->get();

        if(count($resp_area_aux)>0){
            return response()->json([
                "mensaje" => "¡Atención! responsable ya asignado a esta área",
                'estado' => 'error',
            ],200);            
        }

        $resp_area = new ResponsablesAreas();
        $resp_area->id = Uuid::generate()->string;
        $resp_area->id_area = $request->id_area;
        $resp_area->id_responsable = $request->id_responsable;
        $resp_area->usu_registra = JWTAuth::user()->vendedor;
        $saved = $resp_area->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! se asignó correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }  
    }

    public function eliminar(Request $request){

        $resp_area = ResponsablesAreas::find($request->id_registro);
        $resp_area->deleted = '1';
        $resp_area->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $resp_area->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! eliminado correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }          
    }
}
