<?php

namespace App\Http\Controllers;

use App\Mercado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use  JWTAuth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Webpatser\Uuid\Uuid;
use DateTime;
use Carbon\Carbon;

class MercadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function obtenerListaGlobal(Request $request){

        switch ($request->tipo_lista) {
            case 'competencia':
                $datos = Config::get('global.competencia');
            break;
            case 'marcas':
                $datos = Config::get('global.marcas');
            break;
            case 'fabricantes':
                $datos = Config::get('global.fabricantes');
            break;
            case 'modo-mantenimiento':
                $datos = Config::get('global.modo_mantenimiento');
                if($datos === true) {return 'si';} else {return 'no';}
            break;
            case 'version':
                $datos = Config::get('global.version');
            break;
        }
        
        return $datos;
    }

    public function obtenerZonasVendedores(){

        $filtro = "";

        if (JWTAuth::user()->vendedor !== 'ADMIN') {
            $filtro = "z.vendedor = '".JWTAuth::user()->vendedor."'";
        } else {
            $filtro = "z.codigo <> ''";
        }

        $zonas = \DB::connection('sqlsrv')
                                    ->table('v_Zonas_Vendedor as z')
                                    ->select(
                                        DB::raw("RTRIM(z.codigo) AS codigo"), 
                                        DB::raw("RTRIM(z.nombre) AS nombre")
                                    )
                                    ->whereRaw($filtro)
                                    ->orderBy(DB::raw("RTRIM(z.NOMBRE)"), 'ASC')
                                    ->get();

        return $this->elementosUnicos($zonas);  
    }

    function elementosUnicos($array){
        $arraySinDuplicados = [];
        foreach($array as $elemento) {
            if (!in_array($elemento, $arraySinDuplicados)) {
                $arraySinDuplicados[] = $elemento;
            }
        }
        return $arraySinDuplicados;
    }

    public function obtenerProductosOfima () {

        $productos = \DB::connection('sqlsrv')
                                    ->table('vReporteTopVentaProductos_ICO as p')
                                    //->join('MTMERCIA as pro', 'pro.codigo', '=', DB::raw("RTRIM(p.producto)"))
                                    ->select(
                                        DB::raw("RTRIM(p.codigo) AS codigo"), 
                                        DB::raw("RTRIM(p.nombre) AS nombre"), 
                                        'p.peso as peso', 
                                        DB::raw("RTRIM(p.unidad) AS unidad"),
                                        DB::raw("CONCAT(RTRIM(p.codigo), '-', RTRIM(p.nombre)) AS nombre_select")
                                    )
                                    ->whereIn('p.codigo', ['2103','2034','2113','2044','2123','1001','1053','1030','1031','1050','2133','2064','2064','2009'])
                                    ->orderBy(DB::raw("RTRIM(p.codigo)"), 'ASC')
                                    ->get();
        return $productos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registrar(Request $request){

        $id_registro = '';

        if($request->id_registro){
            $mercado = Mercado::find($request->id_registro);
            $id_registro = $request->id_registro;
        } else {
            $mercado = New Mercado();
            $mercado->id = $id_registro = Uuid::generate()->string;
        }

        if($request->file){
            $evidencia = $this->subirEvidencia($request->file, $id_registro);
            $mercado->adjunto = $evidencia['nombre_archivo'];
            if($evidencia['guardo'] == 'no'){
                return response()->json([
                    "estado" => "error",
                    "mensaje" => $evidencia['mensaje']
                ],200);
            }
        }


        $request->competencia = json_decode($request->competencia);
        $request->zona = json_decode($request->zona);
        $request->producto = json_decode($request->producto);
        $request->fabricante = json_decode($request->fabricante);
        $mercado->nit = $request->competencia->nit;
        $mercado->nombre = $request->competencia->nombre;

        if($request->marca){
            $mercado->marca = $request->marca;
        }

        if($request->fabricante){
            $mercado->nit_fabricante = $request->fabricante->nit;
            $mercado->fabricante = $request->fabricante->nombre;
        }

        $mercado->fecha = $request->fecha;
        $mercado->cod_zona = $request->zona->codigo;
        $mercado->nom_zona = $request->zona->nombre;
        $mercado->cod_producto = $request->producto->codigo;
        $mercado->nom_producto = $request->producto->nombre;
        $mercado->precio = $request->precio;
        $mercado->usu_registro = JWTAuth::user()->vendedor;
        $saved = $mercado->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'La información se ha registrado correctamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al registrar la información, vuelva a intentarlo'
            ],200);            
        }
    }

    public function subirEvidencia($file, $id_registro){

        $data = array();
        $data['guardo'] = 'no';
        $data['mensaje'] = '';
        $data['nombre_archivo'] = '-';
       //obtenemos el campo file definido en el formulario del front
       // $file = $request->file;

       //$meta_imagen = exif_read_data($file);
	
		 try {
			$meta_imagen = exif_read_data($file);
		 }
		 catch (Exception $exp) {
			$meta_imagen = false;
		 }
		 if ($meta_imagen){
			   if(isset($meta_imagen['DateTimeOriginal'])){

					$date = new Carbon($meta_imagen['DateTimeOriginal']);
					$currentDate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
					$shippingDate = Carbon::createFromFormat('Y-m-d', $date->format('Y-m-d'));
					$diferencia_en_dias = $currentDate->diffInDays($shippingDate);

					// si la foto tiene mas de 2 dias no la deja tomar como evidencia
					if($diferencia_en_dias > 20){
						$data['mensaje'] = 'La foto tiene '.$diferencia_en_dias.' dias de tomada no sirve como evidencia';
						return $data;
					}
			   } else {
						$data['mensaje'] = 'La foto no tiene fecha de captura y no sirve como evidencia';
						return $data;
			   }
		 }	
	


       //obtenemos el nombre del archivo
       $nombre = $id_registro.'.'.$file->getClientOriginalExtension();
 
       //si no existe el directorio se crea
       if(!Storage::disk('public')->exists('evidencias')){
            Storage::makeDirectory('public/evidencias');
       }

       //Verifico si el adjunto es imagen, para redimensionarla
       if($file->getClientOriginalExtension() !== 'pdf'){
            $photo = Image::make($file)
                            ->resize(800, null, function ($constraint) { 
                            $constraint->aspectRatio(); 
                    })->encode('jpg',100);

            Storage::disk('evidencias')->put($nombre, $photo);
       } else {
            //filesystem.php esta definido el driver vehiculos
            Storage::disk('evidencias')->put($nombre,  \File::get($file));
       }

       //verifico si guardó el archivo
       if(Storage::disk('public')->exists('evidencias/'.$nombre)){

           /*$mercado = Mercado::find($id_registro);
           if($mercado){
                //Elimina el archivo si existe
                Storage::disk('public')->delete('evidencias/'.$mercado->adjunto);
                $mercado->adjunto = $nombre;
                $mercado->save();
           }*/

           $data['guardo'] = 'si';
           $data['nombre_archivo'] = $nombre;
           $data['mensaje'] = '';

        }else{
            //Storage::disk('public')->delete('vehiculos/'.$nombre);
            $data['guardo'] = 'no';
            $data['mensaje'] = 'No se pudo subir el archivo, por favor vuelva intentarlo';
        }

        return $data;
    }

    public function obtenerRegistros(Request $request){

        $registros = Mercado::select('id as id_registro', 'deleted', 'fecha', 'nit', 'nombre', 'marca', 'nit_fabricante', 'fabricante', 'cod_zona', 'nom_zona', 'cod_producto', 'nom_producto', 'precio', 'adjunto', 'usu_registro', 'created_at', 'updated_at')
                            ->where('usu_registro', JWTAuth::user()->vendedor)
                            ->where('deleted', '0')
                            ->orderBy('created_at', 'DESC')
                            ->paginate(10);

        for ($i=0; $i < count($registros); $i++) { 

            $registros[$i]['existe_adjunto'] = 'no';

            if(Storage::disk('public')->exists('evidencias/'.$registros[$i]['adjunto'])){
                $registros[$i]['existe_adjunto'] = 'si';
            }
        }

        return $registros;
    }

    public function downloadFile($archivo){
        return response()->download(storage_path('app/public/evidencias/'.$archivo));
    }

    public function eliminarRegistro(Request $request){

        $registro = Mercado::find($request->id_registro);
        $registro->deleted = '1';
        $saved = $registro->save();

        if($saved === true){
            Storage::disk('public')->delete('evidencias/'.$registro->adjunto);
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'El registro se ha eliminado correctamente.'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al eliminar el registro, vuelva a intentarlo'
            ],200);            
        }
    }
    public function obtenerDiaInicioFinMes () {
        //Primer dia del mes
        //$start = new Carbon('first day of this month');
        //$start->startOfMonth();

        $date = Carbon::now();
        $date2 = Carbon::now();
        //$date = $carbon->now();

        //Ultimo dia del mes
        $end = new Carbon('last day of this month');
        $end->endOfMonth();
        //$anio_actual = date('Y');

        return  response()->json([
            // 'fecha_inicio' => $start->format('Y-m-d'),
            'fecha_inicio' => $date2->format('Y-m-'.'01'),
            'fecha_final'  => $end->format('Y-m-d'),
        ], 200);
    }
    public function getMax($array){
       $array = array_filter($array);
       $arr_aux = array_values($array);
       $n = count($arr_aux);
       $max = $arr_aux[0];
       for ($i = 1; $i < $n; $i++)
           if ($max < $arr_aux[$i] && $arr_aux[$i] !== null)
               $max = $arr_aux[$i];
        return $max;      
    }
     
    // Returns maximum in array
    public function getMin($array){
       $array = array_filter($array);
       // reindexar
       $arr_aux = array_values($array);
       $n = count($arr_aux);
       $min = $arr_aux[0];
       for ($i = 1; $i < $n; $i++)
           if ($min > $arr_aux[$i] && $arr_aux[$i] !== null)
               $min = $arr_aux[$i];
        return $min;      
    }

    public function obtenerInfoGraficas(Request $request){

        $request->tipo_grafica = 'rango_fechas';
        $fecha_ini = str_replace("-","",$request->fec_ini);
        $fecha_fin = str_replace("-","",$request->fec_fin);

        $nit = '';
        $producto = '';
        $zona = '';

        if($request->nit){
            $nit = $request->nit;
        }

        if($request->producto){
            $producto = $request->producto;
        }

        if($request->zona){
            $zona = $request->zona;
        }
        switch ($request->tipo_grafica) {

            case 'rango_fechas':

                $array_aux = array();

                $datos = DB::connection('sqlsrv')
                                    ->table('MVTRADE AS mv')
                                        ->select(DB::raw("DAY(mv.FECHA) as dia"), DB::raw("ROUND(max(mv.VALORUNIT), 0) AS valor_max"), DB::raw("DATENAME (MONTH, DATEADD(MONTH, MONTH(mv.Fecha) - 1, '1900-01-01')) Mes"))
                                        ->join('trade as t', 't.nrodcto', '=', 'mv.NRODCTO')
                                        ->join('MTPROCLI as c', 'c.NIT', '=', 't.NIT')
                                        ->whereRaw("mv.TIPODCTO in ('rf', 'rp', 'f1', 'f2')")
                                        ->where('mv.producto', $producto)
                                        ->whereBetween('mv.fecha', [$fecha_ini, $fecha_fin])
                                        ->groupBy('mv.FECHA')
                                        ->orderBy('mv.FECHA', 'ASC');
                                        if(isset($request->zona)){
                                            $datos->where('c.CIUDAD', $zona);
                                        }
                                        $datos = $datos->get();


                // MERCADO
                $datos_mercado = Mercado::select(DB::raw("day(fecha) as dia"), DB::raw("max(precio) as valor_max"), DB::raw("ELT(MONTH(fecha), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mes"))
                                        ->where('cod_producto', $producto)
                                        ->whereBetween('fecha', [$request->fec_ini, $request->fec_fin])
                                        ->whereRaw("nit like '%$nit%'")
                                        ->groupBy('fecha')
                                        ->orderBy('fecha', 'ASC');
                                        if(isset($request->zona)){
                                            $datos_mercado->where('cod_zona', $zona);
                                        }
                                        if(isset($request->marca)){
                                            $datos_mercado->where('marca', $request->marca);
                                        }
                                        if(isset($request->fabricante)){
                                            $datos_mercado->where('nit_fabricante', $request->fabricante);
                                        }                                        
                                        $datos_mercado = $datos_mercado->get();

                 // Calcula numero de dias entre las fechas seleccionadas
                 $fecha1= new DateTime($request->fec_ini);
                 $fecha2= new DateTime($request->fec_fin);
                 $diff = $fecha1->diff($fecha2);
                 $num_dias = $diff->days + 1;

                 $n = 0;
                 $m = 0;

                 $date = new Carbon($request->fec_ini);
                 $date2 = new Carbon($request->fec_fin);
                 $start = (int) $date->format('d');
                 $end = (int) $date2->format('d');

                 for ($k= $start; $k <= $end ; $k++) {

                    // Datos icoharinas
                    if(isset($datos[$n]->dia)) {

                        if($k == $datos[$n]->dia){
                            $array_aux['valores_ico'][] = (int) $datos[$n]->valor_max;
                            $n++;
                         } else {
                            if($n>0){
                                $array_aux['valores_ico'][] = (int) $datos[$n-1]->valor_max;
                            } else {
                                $array_aux['valores_ico'][] = '';
                            }
                            
                         }
                         //$n++;
                    } else {
                        $array_aux['valores_ico'][] = '';
                    }

                    // Datos mercado
                    if(isset($datos_mercado[$m]['dia'])) {

                        if($k == $datos_mercado[$m]['dia']){
                            $array_aux['valores_mercado'][] = (int) $datos_mercado[$m]['valor_max'];
                            $m++;
                         } else {
                             if($m>0){
                                $array_aux['valores_mercado'][] = (int) $datos_mercado[$m-1]['valor_max'];
                             } else {
                                $array_aux['valores_mercado'][] = '';
                             }
                            
                         }
                    } else {
                        $array_aux['valores_mercado'][] = '';
                    }

                    // lleno para que grafique todos los dias con respecto al rango de fechas seleccionadas
                    $array_aux['label_dias'][] = $k;
                 }

                 $array_aux['datos_unidos'] = array_merge($array_aux['valores_mercado'], $array_aux['valores_ico']); 

                 if(count($datos)>0 && count($datos_mercado)>0){
                    $array_aux['valor_min'] = $this->getMin($array_aux['datos_unidos']);
                    $array_aux['valor_max'] = $this->getMax($array_aux['datos_unidos']);
                 }


                return $array_aux;

            break;

            case 'mensual':
                # code...
            break;

            default:
                # code...
            break;
        }
    }
}
