<?php

namespace App\Http\Controllers;

use App\Pedido;
use App\Despacho;
use App\pedido_detalle;
use App\VisitaClientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\DespachoController;
use App\Presupuesto;
use App\Http\Controllers\NovedadSistemaController;
use App\Http\Controllers\CausalesController;
use App\Http\Controllers\AreaController;

class PedidoController extends Controller
{
    public function store(Request $request){
        DB::beginTransaction();
        try{
                $ctl_novedad = new NovedadSistemaController();
                $novedad = $ctl_novedad->obtenerNovedad();

                if($novedad){
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => $novedad['mensaje']
                    ],200);             
                }

                if($request->id_despacho){
                    $despacho_id = Despacho::select('id as id_despacho')->where('consecutivo', $request->id_despacho)->first();
                    //actualizo el estado del despacho
                    $despacho_ = Despacho::find($despacho_id['id_despacho']);
                    $despacho_->estado = 'abierto';
                    $despacho_->save();
                }

                $nit_old = $request->nit;
                if ($request->modifica === true){
                    $pedido = Pedido::find($request->id_pedido);
                    $id_pedido = $pedido->id;
                    $nit_old = $pedido->nit;
                } else {
                    $pedido = new Pedido();
                    $pedido->id = $id_pedido = Uuid::generate()->string;
                }
                // verifica que en el despacho no quede repetido pedidos del mismo cliente
                /*$pedido_cliente = Pedido::where('id_despacho', $despacho['id_despacho'])->where('deleted', '0')->where('nit', $request->nit)->first();

                if($pedido_cliente){
                    if($request->modifica === false){
                        DB::commit();
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => 'El cliente ya tiene pedido en el despacho'
                        ],200);                       
                    }
                }*/

                $cliente = $this->obtenerClientesOfimaxNit($request->nit);

                $pedido->nit = $request->nit;
                $pedido->cliente = $request->cliente;
                $pedido->id_despacho = $despacho_id['id_despacho'];
                $pedido->valor_total = $request->valor_total;
                $pedido->total_kilos = $request->total_kilos;
                $pedido->ciudad = $cliente->CIUDAD;
                $pedido->bodega = $request->bodega;
                $pedido->observaciones = $request->observaciones;
                if ($request->cerrar_pedido === true) {
                    $pedido->estado = 'cerrado';
                }
                $pedido->certificado_calidad = $request->certificado;
                $observaciones_certificado_old = $pedido->observaciones_certificado;
                $pedido->observaciones_certificado = $request->observaciones_certificado;
                $save = $pedido->save();
                if ($request->modifica === true){
                    $pedido->id = $pedido->consecutivo;
                }

                // Datos para certificado de calidad
                $data = array();
                $data['id_despacho'] = $despacho_id['id_despacho'];
                $data['asunto'] = 'Solicitud de certificado de calidad';
                $data['observaciones_certificado'] = $request->observaciones_certificado;
                // -------------------------------------
                //dd($pedido->observaciones_certificado .'-->'.$observaciones_certificado_old);
                //Valida si requiere volver a notificar el certificado de calidad
                if($request->certificado == 'si' && $request->modifica === true){
                    if($observaciones_certificado_old != $pedido->observaciones_certificado){
                        $this->notificaCertificadoCalidad($data);
                    }
                }

                if($save === true){

                    $visita['nit'] = $request->nit;
                    $visita['nom_cliente'] = $request->cliente;
                    $visita['nit_old'] = $nit_old;
                    $visita['fecha'] = substr($pedido->created_at, 0, 10);
                    $visita['fec_modifica'] = $pedido->created_at;
                    $visita['usu_modifica'] = JWTAuth::user()->vendedor;
                    $visita['id_pedido'] = $id_pedido;
                    $visita['modifica'] = $request->modifica;
                    $ctrl_visita = new VisitaClientesController();
                    $ctrl_visita->gestionarVisitaPedido($visita);

                    $this->registraDetallePedido($pedido->id, $request->productos, $request->modifica);

                    if($request->certificado == 'si' && $request->modifica === false){
                        $this->notificaCertificadoCalidad($data);
                    }

                    DB::commit();
                    return response()->json([
                        "estado" => "ok",
                        "mensaje" => $request->modifica === false ? 'El pedido al cliente '.$pedido->cliente.' se ha creado correctamente.' : 'El pedido al cliente '.$pedido->cliente.' se ha modificado correctamente.'
                    ],200);

                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'No se pudo registrar el pedido, intente nuevamente'
                    ],200);
                
                }
            }
            catch(\Exception $e){
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "estado" => 'error'
                ],200);               
            }
    }

    public function notificaCertificadoCalidad($data = array()){
        $ctl_despacho = new DespachoController();
        // Datos
        $despacho = Despacho::find($data['id_despacho'] );
        $info_pedidos = $ctl_despacho->obtenerPedidosxDespacho($data['id_despacho'] );
        $despacho['info_pedidos'] = $info_pedidos;

        $ctl_auth = new AuthController();
        $info_usuario = $ctl_auth->obtenerInfoUsuarioxId($despacho['vendedor']);

        $vendedor = $info_usuario['name'];
        $despacho['nombre_vendedor'] = $vendedor;
        $despacho['despacho_id'] = $data['id_despacho'] ;
        $totales_despacho = $ctl_despacho->obtenerTotalesPedidosxDespacho($data['id_despacho'] );
        $despacho['total_kilos'] = number_format($totales_despacho['total_kilos'], 0, ',','.');
        $despacho['accion'] = 'notifica_certificado_calidad';
        $despacho['fecha_entrega'] = '--';
        $despacho['observaciones_certificado'] = $data['observaciones_certificado'];
        $despacho['asunto'] = $data['asunto'];

        $ctl_despacho->enviarProcesoColaTrabajo($despacho);
    }

    public function obtenerZonas(Request $request){


        $zonas = \DB::connection('sqlsrv')
                                    ->table('v_Asin_Zonas as z')
                                    ->select(DB::raw("RTRIM(z.ID_ZONA) AS ID_ZONA"), DB::raw("CONCAT(RTRIM(z.ID_ZONA), ' - ', RTRIM(z.NOMBRE)) AS NOMBRE"))
                                    ->orderBy('z.ID_ZONA', 'ASC')->get();
        return $zonas;
    }

    public function getDiasHabiles($fechainicio, $fechafin, $diasferiados = array()) {
        // Convirtiendo en timestamp las fechas
        $fechainicio = strtotime($fechainicio);
        $fechafin = strtotime($fechafin);
       
        // Incremento en 1 dia
        $diainc = 24*60*60;
       
        // Arreglo de dias habiles, inicianlizacion
        $diashabiles = array();
       
        // Se recorre desde la fecha de inicio a la fecha fin, incrementando en 1 dia
        for ($midia = $fechainicio; $midia <= $fechafin; $midia += $diainc) {
                // Si el dia indicado, no es sabado o domingo es habil
                if (!in_array(date('N', $midia), array(7))) { // DOC: http://www.php.net/manual/es/function.date.php
                        // Si no es un dia feriado entonces es habil
                        if (!in_array(date('Y-m-d', $midia), $diasferiados)) {
                                array_push($diashabiles, date('Y-m-d', $midia));
                        }
                }
        }

        return $diashabiles;
    }

    public function ejecutarDiasHabiles(Request $request){
        Carbon::setLocale('es');
        $hoy = Carbon::now();
        $unidad_medida = $request->unidad_medida == 'kilos' ? 1:50; // 1 es kilos, 50 es bultos
        $date1 = new Carbon($hoy->startOfMonth()->format('Y-m-d'));
        $date2 = new Carbon($hoy->endOfMonth()->format('Y-m-d'));
        $mes  = $date1->monthName;
        $anio = $date1->format('Y');

        $grupos = \DB::connection('sqlsrv')
                        ->table('MTMERCIA as s')
                            ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                            ->where('s.codigo', 'like', 'G0%')
                            ->whereRaw("s.codigo not in ('G017', 'G016', 'G018', 'G014')")
                            ->orderBy('s.codigo', 'ASC')->get();

        $presup_total = 0;
        $total_venta_mensual = 0;
        $total_venta_dia_actual = 0;

        for($i=0; $i<count($grupos); $i++){

            if($grupos[$i]->codigo != 'G015'){
                $presup_grupo = Presupuesto::select($mes)->where('codigo_agrupa', $grupos[$i]->codigo)->where('anio', $anio)->first();
                // $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / 50);
                $presup_total = $presup_total + round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
            }

        }

        // Si hay presupuesto (>0) se realiza todas las operaciones
        $data = array();
        if($presup_total>0){

            $dias = array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');

            $dias_habiles = $this->getDiasHabiles($date1, $date2, config('global.festivos'));
    
            $data['exist_data'] = 'si';
            $data['nro_dias_habiles'] = count($this->getDiasHabiles($date1, $date2, config('global.festivos')));
            $data['presupuesto_mensual'] = $presup_total;
            $data['presupuesto_diario'] = $presup_total / $data['nro_dias_habiles'];
            $data['mes_actual'] = $mes;
    
            $dias_faltantes = $data['nro_dias_habiles'];

            for ($i=0; $i < count($dias_habiles) ; $i++) {
    
                $j=$i-1;
                $fecha = str_replace ('-', '', $dias_habiles[$i]);
                $dia_semana = $dias[(date('N', strtotime($dias_habiles[$i]))) - 1];
    
                //Venta diaria, por fecha
                $total_kilos = \DB::connection('sqlsrv')
                                        ->table('v_Ico_InformeVentasPorFecha as i')
                                        ->select(DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos "))
                                        ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                        ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                        ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                        ->whereRaw("i.producto not like '5%'")
                                        ->where('i.fecha', $fecha)
                                        ->first();
    
                if(!$total_kilos->cantidad_bultos){
                    $total_kilos->cantidad_bultos = 0;
                }

                if($fecha == date('Ymd')){
                    $total_venta_dia_actual = $total_kilos->cantidad_bultos;
                }

                $total_venta_mensual = round($total_venta_mensual + $total_kilos->cantidad_bultos, 2);

                $data['detalle_fact'][$i]['fecha'] = $dias_habiles[$i];
                $data['detalle_fact'][$i]['dia_semana'] = $dia_semana;
                $data['detalle_fact'][$i]['venta'] = $total_kilos->cantidad_bultos;
    
                if($i==0){
                    $data['detalle_fact'][$i]['diferencia'] = $data['presupuesto_diario'] - $total_kilos->cantidad_bultos;
                    $data['detalle_fact'][$i]['new_presupuesto_diario'] = round($data['presupuesto_diario']);
                }else{
                    // Si la diferencia (dia anterior) es positiva, quiere decir que no se cumplio el presupuesto diario y se le debe sumar al nuevo presu diario
                    $data['detalle_fact'][$i]['diferencia'] = $data['detalle_fact'][$j]['diferencia'];
    
                    //if($data['detalle_fact'][$i]['diferencia']>0){
                        $data['detalle_fact'][$i]['new_presupuesto_diario'] = round($data['detalle_fact'][$j]['new_presupuesto_diario'] + ($data['detalle_fact'][$i]['diferencia']/$dias_faltantes));
                        $data['detalle_fact'][$i]['diferencia']=$data['detalle_fact'][$i]['new_presupuesto_diario'] - $total_kilos->cantidad_bultos;
                   //} else { // Si la diferencia es negativa (significa que la venta fue positiva), se le resta al presupuesto diaria
                        //$data['detalle_fact'][$i]['new_presupuesto_diario'] = round($data['detalle_fact'][$j]['new_presupuesto_diario'] - ($data['detalle_fact'][$i]['diferencia']/$dias_faltantes));
                        //$data['detalle_fact'][$i]['diferencia']=$data['detalle_fact'][$i]['new_presupuesto_diario'] - $total_kilos->cantidad_bultos;
                   //}
    
                }

                $dias_faltantes--;
    
                // Se acaba el ciclo cuando la fecha sea igual a la de hoy
                if($dias_habiles[$i] == date('Y-m-d')){
                    break;
                }

            }

            if(($presup_total - $total_venta_mensual)<=0){
                $data['presupuesto_diario_sueno'] = 0;
            }else{
                $data['presupuesto_diario_sueno'] = ($presup_total - ($total_venta_mensual - $total_venta_dia_actual)) / ($dias_faltantes + 1);
            }

            $data['dias_falta_factu'] = $dias_faltantes + 1;
        }else{
            $data['exist_data'] = 'no';
        }

        return $data;
    }

    public function obtenerClientesNoVenta(){

        $mesVigente = Carbon::now()->subMonth();
        $dosMesesAntes = Carbon::now()->subMonth(2);

        $start1 = $mesVigente->firstOfMonth()->format('Ym01');
        $end1 = $mesVigente->lastOfMonth()->format('Ymd');

        $start2 = $dosMesesAntes->firstOfMonth()->format('Ym01');
        $end2 = $dosMesesAntes->lastOfMonth()->format('Ymd');

        $venta_mayor = 1000000;

        $sql = "
                SELECT venta_mes_ant.*  FROM 
                        (SELECT DISTINCT RTRIM(c.nit) AS nit,RTRIM(c.NOMBRE) AS NOMBRE,RTRIM(v.NOMBRE) AS vendedor,RTRIM(c.CONTACTO) AS contacto, RTRIM(CONCAT(c.TEL1,' ',c.TEL2)) AS telefono,
                        RTRIM(c.DIRECCION) AS direccion,RTRIM(c.COMENTARIO) AS comentario,max(t.FECHA) AS ult_compra  FROM MTPROCLI c
                        INNER JOIN trade AS t ON(t.NIT=c.NIT)
                        INNER JOIN VENDEN AS v ON (v.CODVEN=t.CODVEN)
                        WHERE t.TIPODCTO IN ('f1','f2','rp','rf')
                        AND v.HABILITADO=1
                        AND c.HABILITADO='S'
                        AND (t.BRUTO+t.IVABRUTO)>=$venta_mayor
                        AND t.FECHA BETWEEN '$start2' AND '$end2'
                        GROUP BY c.nit,c.NOMBRE,v.NOMBRE,c.HABILITADO,c.CONTACTO,c.TEL1,c.TEL2,c.DIRECCION,c.COMENTARIO
                        ) AS venta_mes_ant
                LEFT JOIN  
                        (SELECT DISTINCT RTRIM(c.nit) AS nit,RTRIM(c.NOMBRE) AS NOMBRE,RTRIM(v.NOMBRE) AS vendedor,RTRIM(c.CONTACTO) AS contacto, RTRIM(CONCAT(c.TEL1,' ',c.TEL2)) AS telefono,
                        RTRIM(c.DIRECCION) AS direccion,RTRIM(c.COMENTARIO) AS comentario,max(t.FECHA) AS ult_compra  FROM MTPROCLI c
                        INNER JOIN trade AS t ON(t.NIT=c.NIT)
                        INNER JOIN VENDEN AS v ON (v.CODVEN=t.CODVEN)
                        WHERE t.TIPODCTO IN ('f1','f2','rp','rf')
                        AND v.HABILITADO=1
                        AND c.HABILITADO='S'
                        AND (t.BRUTO+t.IVABRUTO)>=$venta_mayor
                        AND t.FECHA BETWEEN '$start1' AND '$end1'
                        GROUP BY c.nit,c.NOMBRE,v.NOMBRE,c.HABILITADO,c.CONTACTO,c.TEL1,c.TEL2,c.DIRECCION,c.COMENTARIO
                        ) AS venta_mes_actual
                ON venta_mes_ant.nit = venta_mes_actual.nit
                WHERE venta_mes_actual.nit IS NULL
                ORDER BY venta_mes_ant.NOMBRE
        ";

        $datos = \DB::connection('sqlsrv')->select($sql);

        return $datos;

    }

    public function obtenerInfoNotasCredito(Request $request){

        $ctl_causal = new CausalesController();
        $ctl_area = new AreaController();

        if($request->tipo_consulta == 'por_mes_mes'){

            $sql="select remi, Enero, Febrero,Marzo,Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre
                    from
                    (
                        select substring(remision,1,5) AS remi, DATENAME(mm, Fecha) AS Fecha, count(*) as total
                        from trade
                        where TIPODCTO in ('n3','n5')
                        and LEN(remision)>0 
                        group by substring(remision,1,5),DATENAME(mm, Fecha)
                    ) as trad
                    pivot
                    (
                    sum(trad.total)
                    for trad.Fecha in (Enero, Febrero, Marzo,Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre)
            ) piv order by remi";

            $remisiones['datos'] = \DB::connection('sqlsrv')->select($sql);

            $meses = ['Enero', 'Febrero', 'Marzo','Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

            for ($i=0; $i < count($remisiones['datos']) ; $i++) {
                $area = $ctl_area->obtenerAreaxCodigo($remisiones['datos'][$i]->remi);
                $remisiones['datos'][$i]->area_label = $area['nombre'];

                for ($m=0; $m < count($meses) ; $m++) {

                    if($remisiones['datos'][$i]->{$meses[$m]} == null){

                        $remisiones['datos'][$i]->{$meses[$m]} = 0;

                    }else{

                        $remisiones['datos'][$i]->{$meses[$m]} = (int) $remisiones['datos'][$i]->{$meses[$m]};

                    }
                }
            }
       
        }

        if($request->tipo_consulta == 'por_area_agrupada'){

            $remisiones['datos'] = \DB::connection('sqlsrv')
                                                    ->table('TRADE as t')
                                                    ->select(DB::raw('SUBSTRING(remision,1,5) AS remision'), DB::raw("count(*) as total"))
                                                    ->whereRaw("t.TIPODCTO in ('n3','n5')")
                                                    ->whereRaw("LEN(t.remision)>0")
                                                    ->groupBy(DB::raw('SUBSTRING(remision,1,5)'))
                                                    ->orderBy('total', 'ASC')
                                                    //->whereBetween('t.fecha', [$fecha_ini, $fecha_fin])
                                                    ->get();

                $suma_registros = 0;

                for ($i=0; $i < count($remisiones['datos']) ; $i++) {

                    $suma_registros += $remisiones['datos'][$i]->total;
                    $area = $ctl_area->obtenerAreaxCodigo($remisiones['datos'][$i]->remision);
                    $remisiones['datos'][$i]->nom_causal = $area['nombre'];
                    $remisiones['label_areas'][] = $area['nombre'];
                    $remisiones['valor_areas'][] = $remisiones['datos'][$i]->total;

                }

                $remisiones['total'] = $suma_registros;
        }

        if($request->tipo_consulta == 'por_causal_agrupada'){

            $remisiones['datos'] = \DB::connection('sqlsrv')
                                                    ->table('TRADE as t')
                                                    ->select(DB::raw("RTRIM(t.remision) AS remision"), DB::raw("count(*) as total"))
                                                    ->whereRaw("t.TIPODCTO in ('n3','n5')")
                                                    ->whereRaw("LEN(t.remision)>0")
                                                    ->groupBy("remision")
                                                    ->orderBy('total', 'ASC')
                                                    //->whereBetween('t.fecha', [$fecha_ini, $fecha_fin])
                                                    ->get();


                $ctl_causal = new CausalesController();
                $suma_registros = 0;

                for ($i=0; $i < count($remisiones['datos']) ; $i++) {

                    $suma_registros += $remisiones['datos'][$i]->total;
                    $causal = $ctl_causal->obtenerCausalxCodigo($remisiones['datos'][$i]->remision);
                    $remisiones['datos'][$i]->nom_causal = $causal['nombre'];
                    $remisiones['label_causales'][] = $causal['nombre'];
                    $remisiones['valor_causales'][] = $remisiones['datos'][$i]->total;
                    // $remisiones['datos'][$i]->graficas->causal_agrupadas_nombre =  $causal['nombre'];
                    //$remisiones['datos']['graficas']['causal_agrupadas_valor'] = $remisiones['datos'][$i]->total;
                }

                $remisiones['total'] = $suma_registros;
        }



        return $remisiones;
        

    }

    public function obtenerDataInformes(Request $request){

        $fecha_ini = str_replace("-","",$request->fecha_inicial);
        $fecha_fin = str_replace("-","",$request->fecha_final);
        // si la unidad de medida es kilos divido en 1 para que no afecte, de lo contrario lo divido en 50 kilos que equivale a un bulto
        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        //dd($fecha_fin);

        switch ($request->accion) {

            case 'grafico_ventas_fecha':

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.clasificacion) AS name"), DB::raw("sum((i.peso_neto) / $unidad_medida) as value"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0','1')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                ->groupBy('i.clasificacion')
                                ->orderBy('i.clasificacion', 'ASC')
                                ->get();

                /*for($i=0; $i<count($data);$i++){
                    //$data[$i]->cantidad = number_format($data[$i]->cantidad, 2, ',', '.');
                    //$data[$i]->value = (int)$data[$i]->value;
                    //$data[$i]->cantidad = number_format($data[$i]->cantidad, 2, ',', '.');
                }*/

            break;

            case 'detalle':

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.clasificacion) AS clasificacion"), DB::raw("sum(i.cantidad) as cantidad"), DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                ->groupBy('i.clasificacion')
                                ->orderBy('i.clasificacion', 'ASC')
                                ->get();

                for($i=0; $i<count($data);$i++){
                    //$data[$i]->cantidad = number_format($data[$i]->cantidad, 2, ',', '.');
                    // se multiplica por 1 para que se vuelva numero y se pueda ordenar en la qtable
                    //$data[$i]->cantidad_bultos = number_format($data[$i]->cantidad_bultos, 2, ',', '.');
                    $data[$i]->cantidad = $data[$i]->cantidad * 1;
                    $data[$i]->cantidad_bultos = $data[$i]->cantidad_bultos * 1;
                }

            break;

            case 'detalle_especial':

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.descripcio) AS descripcio"), DB::raw("sum(i.cantidad) as cantidad"), DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("(descripcio like '%especial%' or descripcio like '%tercera%' or i.producto = '1005')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                ->groupBy('i.DESCRIPCIO')
                                ->orderBy('i.DESCRIPCIO', 'ASC')
                                ->get();

                for($i=0; $i<count($data);$i++){
                    $data[$i]->cantidad = $data[$i]->cantidad * 1;
                    $data[$i]->cantidad_bultos = $data[$i]->cantidad_bultos * 1;
                }

            break;

            case 'detalle_especial_otros':

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.clasificacion) AS clasificacion"), DB::raw("sum(i.cantidad) as cantidad_"), DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("(descripcio like '%mogolla%' or descripcio  like '%salvado%' or descripcio  like '%polipr%'  or descripcio  like '%polvil%' or clasificacion  like '%grasa%' or descripcio  like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                ->groupBy('i.clasificacion')
                                ->orderBy('i.clasificacion', 'ASC')
                                ->get();

                for($i=0; $i<count($data);$i++){
                    $data[$i]->cantidad = $data[$i]->cantidad * 1;
                }

            break;

            case 'total_bultos':

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos "))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                ->first();

                                return response()->json([
                                    "bultos" => number_format($data->cantidad_bultos, 2, ',', '.'),
                                    "total_bultos" => $data->cantidad_bultos
                                ],200);   
            break;

            case 'total_bultos_linea':

                //$start = new Carbon('first day of this month');

                $date1 = new Carbon($request->fecha_inicial);
                $date2 = new Carbon($request->fecha_final);
 
                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos "))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$date1->startOfMonth()->format('Ymd'), $date2->endOfMonth()->format('Ymd')])
                                ->first();

                                return response()->json([
                                    "total_bultos" => round($data->cantidad_bultos,2)
                                ],200);
            break;

            case 'total_bultos_presupuesto':

                Carbon::setLocale('es');
                $hoy = new Carbon($request->fecha_inicial);

                $date1 = new Carbon($request->tipo_consulta == 'presup_general_mensual' ? $hoy->startOfMonth()->format('Y-m-d') : $request->fecha_inicial);
                $date2 = new Carbon($request->tipo_consulta == 'presup_general_mensual' ? date('Y-m-d') : $request->fecha_final);
                $mes  = $date1->monthName;
                $date_anio = new Carbon($request->tipo_consulta == 'presup_general_mensual' ? $hoy->startOfMonth()->format('Y-m-d') : $request->fecha_inicial);
                $anio = $date_anio->format('Y');

                //$inicio_mes = date('Y-m'.'-01');
                $start = new Carbon($request->tipo_consulta == 'presup_general_mensual' ? $hoy->startOfMonth()->format('Y-m-d') : $request->fecha_inicial);
                $diasDiferencia = $date2->diffInDays($start->startOfMonth()->format('Y-m-d')) + 1;

                if($diasDiferencia > 30){
                    $diasDiferencia = 31;
                }
                //dd($diasDiferencia);


                $grupos = \DB::connection('sqlsrv')
                                ->table('MTMERCIA as s')
                                    ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                    ->where('s.codigo', 'like', 'G0%')
                                    ->whereRaw("s.codigo not in ('G017', 'G016', 'G018', 'G014')")
                                    ->orderBy('s.codigo', 'ASC')->get();

                $presup_total_bultos = 0;

                for($i=0; $i<count($grupos); $i++){

                    if($grupos[$i]->codigo != 'G015'){
                        $presup_grupo = Presupuesto::select($mes)->where('codigo_agrupa', $grupos[$i]->codigo)->where('anio', $anio)->first();
                        // $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / 50);
                        $presup_total_bultos = $presup_total_bultos + round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
                    }

                }

                $f1 = new Carbon($hoy->startOfMonth()->format('Y-m-d'));
                $f2 = new Carbon($hoy->endOfMonth()->format('Y-m-d'));

                // Dias trancurridos hábiles
                $f1_trans = new Carbon($hoy->startOfMonth()->format('Y-m-d'));


                return response()->json([
                    "bultos" => $presup_total_bultos,
                    "dias_transcurridos" => count($this->getDiasHabiles($f1_trans, $date2, config('global.festivos'))),
                    //"dias_mes" => $this->obtenerUltimoDiaMes($request->tipo_consulta == 'presup_general_mensual' ? $hoy->startOfMonth()->format('Y-m-d') : $request->fecha_inicial) * 1
                    "dias_mes" => count($this->getDiasHabiles($f1, $f2, config('global.festivos')))
                ],200);

            break;

            case 'top-clientes':

                $start = Carbon::now()->startOfMonth();
                $fecha_ini = $start->format('Ymd');

                $end = Carbon::now()->endOfMonth();
                $fecha_fin = $end->format('Ymd');

                $data = \DB::connection('sqlsrv')
                                    ->table('v_Ico_InformeVentasPorFecha as i')
                                        ->select(DB::raw("sum(i.VLR_BRUTO) as total_ventas"), DB::raw("RTRIM(i.cliente) as cliente"))
                                        ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                        ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                        ->groupBy('i.cliente')
                                        ->orderBy('total_ventas', 'DESC')
                                        ->take(10)
                                        ->get();
            break;

            case 'top-clasificacion':

                $start = Carbon::now()->startOfMonth();
                $fecha_ini = $start->format('Ymd');

                $end = Carbon::now()->endOfMonth();
                $fecha_fin = $end->format('Ymd');

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.grupo) AS grupo"), DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%retal%' and descripcio not like '%flete%' and descripcio not like '%polvil%')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                ->groupBy('i.grupo')
                                ->orderBy('cantidad_bultos', 'DESC')
                                ->take(10)
                                ->get();

                for($i=0; $i<count($data);$i++){
                    $data[$i]->cantidad_bultos = $data[$i]->cantidad_bultos * 1;
                }

            break;

            case 'top-vendedores':

                $start = Carbon::now()->startOfMonth();
                $fecha_ini = $start->format('Ymd');

                $end = Carbon::now()->endOfMonth();
                $fecha_fin = $end->format('Ymd');

                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("RTRIM(i.NOM_VENDEDOR) AS NOM_VENDEDOR"), DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos"))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                ->groupBy('i.NOM_VENDEDOR')
                                ->orderBy('cantidad_bultos', 'DESC')
                                ->take(5)
                                ->get();

                for($i=0; $i<count($data);$i++){
                    $data[$i]->cantidad_bultos = $data[$i]->cantidad_bultos * 1;
                }

            break;

            case 'total_ventas':

                // Para la versión mobile esta consulta
                $hoy = Carbon::now();
                $fechas = $this->obtenerFeciniFecfin($request->tipo_consulta);
                $fecha_ini = $fechas['fecha_inicial'];
                $fecha_fin = $fechas['fecha_final'];

                if ($request->tipo_consulta == 'general_mensual'){
                    $fecha_ini = $hoy->startOfMonth()->format('Ymd');
                    $fecha_fin = $hoy->endOfMonth()->format('Ymd');
                }

 
                $data = \DB::connection('sqlsrv')
                                ->table('v_Ico_InformeVentasPorFecha as i')
                                ->select(DB::raw("sum((i.peso_neto) / $unidad_medida) as cantidad_bultos "))
                                ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                ->whereRaw("i.PRODUCTO not in ('001', '002', '42104', '004', '008')")
                                ->whereRaw("(descripcio not like '%mogolla%' and descripcio not like '%salvado%' and descripcio not like '%retal%' and descripcio not like '%polipr%' and descripcio not like '%flete%' and descripcio not like '%polvil%' and descripcio not like '%trigo%')")
                                //->whereRaw("i.vendedor not in ('0')")
                                ->whereRaw("i.producto not like '5%'")
                                ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                ->first();

                                return response()->json([
                                    "total_bultos" => round($data->cantidad_bultos,2)
                                ],200);   
            break;
        }

        return $data;
    }
    public function obtenerFeciniFecfinMysql($accion){

        $date = Carbon::now();
        $datos = array();

        switch ($accion) {

            case 'hoy':
                $fecha_inicial = $date->format('Y-m-d');
                $fecha_final = $date->format('Y-m-d');
            break;
            case 'ayer':
                $fecha_inicial = $date->yesterday()->format('Y-m-d');
                $fecha_final = $date->yesterday()->format('Y-m-d');
            break;
            case 'semana':
                $fecha_inicial = $date->startOfWeek()->format('Y-m-d');
                $fecha_final = $date->endOfWeek()->format('Y-m-d');
            break;
            case 'mensual':
                $fecha_inicial = $date->startOfMonth()->format('Y-m-d'); 
                $fecha_final = $date->endOfMonth()->format('Y-m-d'); 
            break;
            case 'Este año':
                $anio_actual = date('Y');
                $fecha_inicial = $anio_actual.'-01-01';
                $fecha_final =   $anio_actual.'-12-31';
            break;
            default:
                $fecha_inicial = '2001-01-01';
                $fecha_final = '2100-12-31';
        }

        $datos['fecha_inicial'] = $fecha_inicial;
        $datos['fecha_final'] = $fecha_final;

        return $datos;
    }

    public function obtenerFeciniFecfin($accion){

        $date = Carbon::now();
        $datos = array();

        switch ($accion) {

            case 'hoy':
                $fecha_inicial = $date->format('Ymd');
                $fecha_final = $date->format('Ymd');
            break;
            case 'ayer':
                $fecha_inicial = $date->yesterday()->format('Ymd');
                $fecha_final = $date->yesterday()->format('Ymd');
            break;
            case 'semana':
                $fecha_inicial = $date->startOfWeek()->format('Ymd');
                $fecha_final = $date->endOfWeek()->format('Ymd');
            break;
            case 'mensual':
                $fecha_inicial = $date->startOfMonth()->format('Ymd'); 
                $fecha_final = $date->endOfMonth()->format('Ymd'); 
            break;
            case 'Este año':
                $anio_actual = date('Y');
                $fecha_inicial = $anio_actual.'-01-01';
                $fecha_final =   $anio_actual.'-12-31';
            break;
            default:
                $fecha_inicial = '2001-01-01';
                $fecha_final = '2100-12-31';
        }

        $datos['fecha_inicial'] = $fecha_inicial;
        $datos['fecha_final'] = $fecha_final;

        return $datos;
    }
    public function obtenerUltimoDiaMes($fecha){

        $end = new Carbon($fecha);
        $date = $end->endOfMonth();
        return  $date->format('d');
    }

    public function obtenerCiudadxZona(Request $request) {

        $ciudad = \DB::connection('sqlsrv')
                                    ->table('MTZONAS_RUTAS as r')
                                    ->select(DB::raw("RTRIM(r.ID_MUNICIPIO) AS ID_MUNICIPIO"), DB::raw("CONCAT(RTRIM(r.ID_MUNICIPIO), ' - ', RTRIM(r.MUNICIPIO)) AS MUNICIPIO"))
                                    ->where('r.ID_ZONA', $request->id_zona)
                                    ->orderBy('r.MUNICIPIO', 'ASC')->get();
        return $ciudad;
    }

    public function obtenerClientesOfimaxNit($nit){

        $cliente = \DB::connection('sqlsrv')
                                    ->table('vReporteClienteICO as C')
                                    ->select(DB::raw("RTRIM(c.Cliente) AS NIT"), DB::raw("RTRIM(c.Nombre_Cliente) AS NOMBRE"), DB::raw("RTRIM(c.Nombre_Ciudad) AS CIUDAD"))
                                    ->where('c.Cliente', '=', $nit)
                                    //->where('c.ESCLIENTE', '=', 'S')
                                    //->where('c.NIT', 'not like', '%S%')
                                    ->orderBy('c.Nombre_Cliente', 'DESC')->first();
        return $cliente;
    }

    public function obtenerConsecutivo($codigo){

        $consecutivo = \DB::connection('sqlsrv')
                        ->table('V_CONSECUTIVOS_ICO as c')
                            ->select('c.CONSECUT')
                            ->where('c.CODIGOCONS', '=', $codigo)
                            ->first();

        return $consecutivo;
    }

    public function obtenerConsecutivosxOrigen(Request $request){

        $consecutivo = \DB::connection('sqlsrv')
                        ->table('CONSECUT as c')
                            ->select(DB::raw("RTRIM(CODIGOCONS) as CODIGOCONS"),DB::raw("CONCAT(RTRIM(CODIGOCONS),' -> ',tipodcto,' - ', RTRIM(DESCRIPCIO)) as DESCRIPCIO"))
                            ->where('c.ORIGEN', '=', $request->origen)
                            ->get();

        return $consecutivo;
    }

    public function obtenerAllProductosOfima(){

        $productos = \DB::connection('sqlsrv')
                        ->table('MTMERCIA')
                            ->select(DB::raw("RTRIM(codigo) AS codigo"), DB::raw("CONCAT(RTRIM(codigo), '-', RTRIM(DESCRIPCIO)) AS descripcion"))
                            ->where('ESPRODUCTO', '1')
                            ->where('HABILITADO', '1')
                            ->orderBy('CODIGO', 'ASC')->get();
        return $productos;
    }

    public function obtenerPedidosEnBodega(Request $request){
        $pedidos_en_bodega = Pedido::select('pedidos.created_at as fecha_despacho', 'pedidos.cliente','pd.cod_producto', 'pd.nom_producto', 'pd.cantidad as cantidad_original', 'pd.unidad_medida', 'pd.peso', 'pd.cantidad_despachada as despachado')
                                    ->join('pedido_detalles as pd', 'pd.id_pedido', '=', 'pedidos.id')
                                    ->where('pedidos.deleted', '0')
                                    ->where('pedidos.bodega', 'si')
                                    ->orderBy('pedidos.created_at','DESC');

                                    if($request->filtro){
                                        $pedidos_en_bodega->whereRaw("pedidos.cliente like '%$request->filtro%' OR pd.nom_producto like '%$request->filtro%'");
                                    }
                                    if($request->paginacion == 'si'){
                                        $pedidos_en_bodega = $pedidos_en_bodega->paginate(15);
                                    } else {
                                        $pedidos_en_bodega = $pedidos_en_bodega->get();
                                    }                   

        for ($i=0; $i <count($pedidos_en_bodega) ; $i++) {
            $result = $pedidos_en_bodega[$i]['cantidad_original'] - $pedidos_en_bodega[$i]['despachado'];
            $result == 0 ? $pedidos_en_bodega[$i]['estado'] = 'facturado' : $pedidos_en_bodega[$i]['estado'] = 'pendiente';
        }

        return $pedidos_en_bodega;
    }

    public function obtenerTotalPesoEnBodega(Request $request){

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;

        if($request->tipo_consulta == ''){
            $fecha_ini = $request->fecha_inicial;
            $fecha_fin = $request->fecha_final;
        }else{
            $fechas = $this->obtenerFeciniFecfinMysql($request->tipo_consulta);
            $fecha_ini = $fechas['fecha_inicial'];
            $fecha_fin = $fechas['fecha_final'];            
        }

        $total = Pedido::select(DB::raw("SUM(total_kilos/$unidad_medida) as total_kilos"))->where('bodega', 'si')
                           ->where('deleted', '0')
                           ->whereRaw("DATE_FORMAT(created_at ,'%Y-%m-%d') BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                           ->first();

        return $total;
    }

    public function ingresaPedidoOfima(Request $request){

        DB::beginTransaction();
        try{

                if(count($request->productos) <= 0){
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'No se enviaron item en el pedido'
                    ],200);                        
                }

                $date = Carbon::now();
                $fecha_actual = $date->format('Ymd');

                $tipodcto = 'FA005';
                $nit = $request->nit;
                $fechadcto = $fecha_actual;
                $usuario = JWTAuth::user()->vendedor;

                //$trade = \DB::connection('sqlsrv')->statement("EXEC [dbo].[OF_SP_INGPedidoTrade] '$tipodcto', '$nit', '$fechadcto'");
                $trade = \DB::connection('sqlsrv')->statement('exec OF_SP_INGPedidoTrade_ICO ?,?,?,?', array($tipodcto, $nit, $fechadcto, $usuario));
                //$res_trade = response()->json($trade);
                //dd($trade);
                // valida si guardó en trade
                if($trade === true) {

                    $consecutivo = $this->obtenerConsecutivo('FA005');

                    $detalle_pedido = $this->obtenerDetallePedidoAux($request->id_pedido, $request->productos);
                    $cont_mvtrade = 0;

                    for($i=0; $i<count($detalle_pedido); $i++){
            
                        $codigo = $detalle_pedido[$i]['codigo'];
                        $cantidad = $detalle_pedido[$i]['cantidad'];
                        $precio = $detalle_pedido[$i]['valor'];
                        $unidad = $detalle_pedido[$i]['unidad'];
                        $id_ped_det = $detalle_pedido[$i]['id_ped_det'];
                        $nota = $detalle_pedido[$i]['nota'];

                        //actualizo el item que ya se haya generado pedido en ofima
                        $ped_det = pedido_detalle::find($id_ped_det);

                        //verifica si el pedido pertenece a una bodega
                        $info_pedido = Pedido::select('id as id_pedido', 'bodega')->where('id', $ped_det->id_pedido)->first();

                        if($info_pedido['bodega'] == 'si'){
                            $ped_det->cantidad_despachada = $cantidad;
                        }

                        $ped_det->nro_pedido_ofima = 'P1'.$consecutivo->CONSECUT;
                        $ped_det->save();

                        // obtiene info bodega y cent cost
                        $dat_bod_cc = $this->obtenerBodegaCentCostProducto($codigo);
            
                        $mvtrade = \DB::connection('sqlsrv')->statement('exec OF_SP_INGPedidoMVTrade_ICO ?,?,?,?,?,?,?,?,?,?', array('P1', $consecutivo->CONSECUT, $fechadcto, $codigo, $cantidad, $precio, $unidad, $dat_bod_cc->BODEGA, $dat_bod_cc->CODCC, $nota ));

                        if($mvtrade === true) {
                            $cont_mvtrade = $cont_mvtrade + 1;
                        }
                    }

                    if(count($detalle_pedido) === $cont_mvtrade && $trade === true) {
                        //obtengo información para la tabla MTPEDIDO_REMISION
                        $info_conductor = $this->obtenerInfoConductorPlaca($request->placa, $request->nit_conductor);

                        $data['nit_conductor'] = $info_conductor->NIT_CONDUCTOR;
                        $data['nombre'] = $info_conductor->NOMBRE;
                        $data['direccion'] = $info_conductor->DIRECCION;
                        $data['placa'] = $request->placa;
                        $data['nrodcto'] = $consecutivo->CONSECUT;
                        $data['tipodcto'] = 'P1';
                        $data['idzona'] = $request->id_zona;
                        $data['idciudad'] = $request->id_ciudad;
                        $data['barrio'] = $request->barrio;

                        $this->crearPedidoRemision($data);

                        DB::commit();
                        return response()->json([
                            "estado" => "ok",
                            "mensaje" => 'El pedido se creó exitosamente'
                        ],200);                
                    } else {
                        DB::commit();
                        return response()->json([
                            "estado" => "error",
                            "mensaje" => 'Error al crear el pedido, por favor intentar nuevamente'
                        ],200);                  
                    }
                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => "error",
                        "mensaje" => 'No se registró el detalle del pedido'
                    ],200);              
                }
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }

    public function crearPedidoRemision($data = array()){

        $mt_pedido_remi = \DB::connection('sqlsrv')->statement('exec sp_SetDatosPedidoRemision ?,?,?,?,?,?,?,?,?', array($data['nrodcto'], $data['tipodcto'], 'FAC', $data['idzona'], $data['idciudad'], $data['barrio'], $data['nit_conductor'], $data['placa'], $data['nombre']));
    }
    public function obtenerBodegaCentCostProducto($codPro){

        $datos = \DB::connection('sqlsrv')
                        ->table('V_PRODUCTOS_BODEGA_CENTCOS_ICO as p')
                            ->select('p.BODEGA', 'p.CODCC', 'p.PRODUCTO')
                            ->where('p.PRODUCTO', '=', $codPro)
                            ->first();

        return $datos;
    }

    public function obtenerInfoConductorPlaca($placa, $nit_conductor){

        $conductor = \DB::connection('sqlsrv')
                                    ->table('V_conductores_info_ICO as c')
                                    ->select('c.ID as NIT_CONDUCTOR', DB::raw("RTRIM(c.NOMBRE) AS NOMBRE"), DB::raw("RTRIM(c.DIRECCION) AS DIRECCION"), DB::raw("RTRIM(c.TELEFONO) AS TELEFONO") )
                                    ->where('c.PLACA', $placa)
                                    ->where('c.ID', $nit_conductor)
                                    ->orderBy('c.ID', 'DESC')->first();
        return $conductor;

    }

    public function registraDetallePedido($id_pedido, $productos = array(), $modifica ){

        // obtiene el id del pedido con respecto al consecutivo
        if ($id_pedido){
            $pedido = Pedido::select('id as id_pedido')->where('consecutivo', $id_pedido)->first();
        }

        if ($modifica === true){
            pedido_detalle::where('id_pedido', $pedido['id_pedido'])->delete();
        }

        $total_kilos = 0;

        for($i=0; $i<count($productos); $i++){

            $pedido_detalle = new pedido_detalle();
            $pedido_detalle->id = Uuid::generate()->string;
            $pedido_detalle->id_pedido = $pedido['id_pedido'];
            $pedido_detalle->cod_grupo = '';
            $pedido_detalle->cod_producto = $productos[$i]['codigo'];
            $pedido_detalle->nom_producto = $productos[$i]['nombre'];
            $pedido_detalle->cantidad = $productos[$i]['cantidad'];
            $pedido_detalle->peso = $productos[$i]['peso'];
            $pedido_detalle->valor = $productos[$i]['valor'];
            $pedido_detalle->unidad_medida = $productos[$i]['unidad'];
            $pedido_detalle->save();

            $total_kilos = $total_kilos + ($productos[$i]['cantidad'] * $productos[$i]['peso']);
        }

        // Recalcula el total de kilos en la tabla pedidos
        $ped = Pedido::find($pedido['id_pedido']);
        $ped->total_kilos = round($total_kilos);
        $ped->save();

        // Proceso reprocesar pedido para evitar el error de los kilos que no coincide
        // $ctl_despacho = new DespachoController();
        // $ctl_despacho->reprocesarPedido($pedido_detalle->id_pedido);

    }

    public function obtenerProductosOfima ($cod_producto) {

        $producto = \DB::connection('sqlsrv')
                                    ->table('vReporteTopVentaProductos_ICO as p')
                                    //->join('MTMERCIA as pro', 'pro.codigo', '=', DB::raw("RTRIM(p.producto)"))
                                    ->select(
                                        DB::raw("RTRIM(p.codigo) AS codigo"), 
                                        DB::raw("RTRIM(p.nombre) AS nombre"), 
                                        'p.peso as peso',
                                        DB::raw("RTRIM(p.grupo) AS grupo"),
                                        DB::raw("RTRIM(p.unidad) AS unidad"),
                                        DB::raw("CONCAT(RTRIM(p.codigo), '-', RTRIM(p.nombre)) AS nombre_select")
                                    )
                                    ->where('p.codigo', $cod_producto)
                                    //->orWhere('p.producto', 'like', '%'.$request->producto.'%')
                                    //->where('pro.esproducto', '1')
                                    //->where('pro.habilitado', '1')
                                    //->orderBy(DB::raw("RTRIM(p.TOTAL_VENTA)"), 'ASC')
                                    //->distinct()
                                    ->first();
        
        return $producto;

    }

    public function obtenerTotalesPorDespacho($despacho){

        $info_despacho = Despacho::select('id as id_despacho')->where('consecutivo', $despacho)->where('deleted', '0')->first();

        $info_pedido = Pedido::select(DB::raw('SUM(valor_total) AS valor_total'),  DB::raw('SUM(total_kilos) AS total_kilos'))
                                ->where('id_despacho', $info_despacho['id_despacho'])->where('deleted', '0')
                                ->first();
        return $info_pedido;
    }

    public function modificaItemPedidoDetalle(Request $request){

        $ctl_despacho = new DespachoController();
        $info_producto = $this->obtenerProductosOfima($request->codigo);
        $productos_paq = config('global.productos_convertir_paquetes');

        if(in_array($info_producto->codigo, $productos_paq)){
            $info_producto->unidad = 'PAQ';
            $info_producto->peso = 6;
        }

        $pedido_detalle = pedido_detalle::find($request->id_ped_det);
        $pedido_detalle->cod_producto = $info_producto->codigo;
        $pedido_detalle->nom_producto = $info_producto->nombre;
        $pedido_detalle->cantidad_old = $pedido_detalle->cantidad;
        $pedido_detalle->cantidad = $request->cantidad;
        $pedido_detalle->valor = $request->valor;
        $pedido_detalle->nota = $request->nota;
        $pedido_detalle->unidad_medida = $info_producto->unidad;
        $pedido_detalle->peso = $info_producto->peso;
        $pedido_detalle->usu_mod = JWTAuth::user()->vendedor;
        $saved = $pedido_detalle->save();

        if($saved === true){
            $ctl_despacho->reprocesarPedido($pedido_detalle->id_pedido);
            //Obtener total kilos y valor total de despacho por que el metodo reprocesarPedido los actualizó
            $total = $this->obtenerTotalesPorDespacho($request->despacho);

            return response()->json([
                "estado" => "ok",
                "totales" => $total,
                "mensaje" => 'Producto modificado exitosamente'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al modificar el producto'
            ],200);           
        }

    }

    public function eliminarPedido(Request $request){

        $pedido = Pedido::find($request->id_pedido);
        $pedido->deleted = '1';
        $saved = $pedido->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'El pedido se ha eliminado exitosamente'
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al eliminar el pedido'
            ],200);           
        }
    }

    public function obtenerPreciosProducto(Request $request){

        $precios = \DB::connection('sqlsrv')
                                    ->table('MVPRECIO as p')
                                    ->select('p.precio')
                                    ->where('p.codproduc', $request->cod_producto)
                                    ->where('p.codprecio', 'like', '0%')
                                    ->where('p.precio', '>', 0)
                                    ->distinct()
                                    ->get();

        for ($i=0; $i<count($precios); $i++){
            $precios[$i]->precio = (int)$precios[$i]->precio;
        }
        
        return $precios;

    }

    public function obtenerPedidosxDespacho(Request $request) {

        $ctl_despacho = new DespachoController();

        $despacho = Despacho::select('id as id_despacho')->where('consecutivo', $request->consecutivo)->first();

        $pedidos = Pedido::select('id as id_pedido', 'nit', 'cliente', 'estado', 'valor_total', 'total_kilos', 'created_at as tiempo_transcurrido', 'observaciones', 'ciudad', 'bodega', 'certificado_calidad', 'observaciones_certificado')
                            ->where('deleted', '0')
                            ->where('id_despacho', $despacho['id_despacho'])
                            ->orderBy('consecutivo', 'DESC')->get();

        for ($i=0; $i<count($pedidos); $i++){

            $cupo = $ctl_despacho->obtenerCupoCliente($pedidos[$i]['nit']);
            //$deuda = $ctl_despacho->obtenerCupoDisponibleNombre($pedidos[$i]['cliente']);

            $pedidos[$i]['plazo'] = $cupo->Plazo === '0' ? 'Contado' : 'Crédito';
            $pedidos[$i]['cupo_asignado'] = $cupo->Cupo_Credito;
            $pedidos[$i]['lista_precio'] = $cupo->Lista_Precio;

            //obtiene los item por pedido
            $pedido_detalle = pedido_detalle::where('id_pedido', $pedidos[$i]['id_pedido'])->where('deleted', '0')->where('cantidad','<>', '0')->count();
            //Obtiene si el pedido ha sido modificado por bodega
            $pedido_detalle_modif = pedido_detalle::where('id_pedido', $pedidos[$i]['id_pedido'])->where('deleted', '0')->whereNotNull('usu_bodega')->count();
            $pedidos[$i]['pedido_mod'] = 'no';
            if($pedido_detalle_modif>0){
                $pedidos[$i]['pedido_mod'] = 'si';
            }
            //$pedidos[$i]['nro_items'] = $pedido_detalle;

            // item que ya se creo un pedido en ofima
            $pedido_detalle_gestionado = pedido_detalle::where('id_pedido', $pedidos[$i]['id_pedido'])->where('deleted', '0')->where('cantidad','<>', '0')->where('nro_pedido_ofima', '<>', '')->count();
            //$pedidos[$i]['nro_items_pedido_ofima'] = $pedido_detalle_gestionado;
            if ($pedido_detalle == $pedido_detalle_gestionado){
                $pedidos[$i]['estado_pedido'] = 'completo';
            } else {
                $pedidos[$i]['estado_pedido'] = 'incompleto';
            }
            /*foreach ($deuda as $clave=>$value) {
                    if($pedidos[$i]['nit'] == $clave){
                        $pedidos[$i]['deuda_total'] = $cupo->Cupo_Credito - $value;
                    }
            } */

            //envio cupo a los que no aparecen con cartera
            if(!$pedidos[$i]['deuda_total']){
                $pedidos[$i]['deuda_total'] = $cupo->Cupo_Credito;
            }

            $pedidos[$i]['bodega'] == 'si' ? $pedidos[$i]['bodega'] = true : $pedidos[$i]['bodega'] = false;

        }
        return $pedidos;
    }

    public function obtenerDiaInicioFinMes () {
        //Primer dia del mes
        //$start = new Carbon('first day of this month');
        //$start->startOfMonth();

        $date = Carbon::now();
        $date2 = Carbon::now();
        //$date = $carbon->now();

        //Ultimo dia del mes
        //$end = new Carbon('last day of this month');
        //$end->endOfMonth();
        //$anio_actual = date('Y');

        return  response()->json([
            // 'fecha_inicio' => $start->format('Y-m-d'),
            'fecha_inicio' => $date2->subDays(7)->format('Y-m-d'),
            'fecha_final'  => $date->format('Y-m-d'),
        ], 200);
    }

    public function obtenerFechaActual () {

        $date = Carbon::now();
        $date2 = Carbon::now();

        return  response()->json([
            // 'fecha_inicio' => $start->format('Y-m-d'),
            'fecha_inicio' => $date->format('Y-m-d'),
            'fecha_final'  => $date->format('Y-m-d'),
        ], 200);
    }

    public function obtenerHistoricoPedidos(Request $request){

        $pedidos = Pedido::select('pedidos.id as id_pedido', 'pedidos.consecutivo', 'pedidos.nit', 'pedidos.cliente', 'pedidos.estado', 'pedidos.valor_total', 'bodega','certificado_calidad', 'observaciones_certificado',
                                  'pedidos.total_kilos', 'pedidos.created_at as tiempo_transcurrido', 'pedidos.observaciones', 'des.consecutivo as conse_desp', 'des.placa', 'des.conductor', 'pedidos.ciudad')
                            ->join('despachos as des', 'des.id', '=', 'pedidos.id_despacho')
                            ->where('des.vendedor', JWTAuth::user()->vendedor)
                            ->where('pedidos.deleted', '0')
                            ->where('des.deleted', '0')
                            ->whereRaw("pedidos.cliente like '%$request->cliente%'")
                            ->orderBy('pedidos.consecutivo', 'DESC')
                            ->paginate(10);

        /*for ($i=0 ; $i<count($pedidos);$i++){

            $fecha_registro = $pedidos[$i]['created_at'];
            $pedidos[$i]['tiempo_transcurrido'] = $this->obtenerTiempoTrancurrido($fecha_registro);
        }*/
        return $pedidos;
    }

    public function obtenerDetallePedido(Request $request) {

        $detalle_pedido = pedido_detalle::select('cod_producto as codigo', 'nom_producto as nombre', 'cantidad', 'unidad_medida as unidad', 'valor', 'peso', 'nro_pedido_ofima', 'id as ped_det', 'nota', 'cantidad_old', 'usu_bodega')
                                        ->where('deleted', '0')
                                        ->where('id_pedido', $request->id_pedido)
                                        ->orderBy('cod_producto', 'ASC')
                                        ->get();

        return $detalle_pedido;
    }

    public function obtenerDetallePedidoxId($id_pedido) {

        $detalle_pedido = pedido_detalle::select('cod_producto as codigo', 'nom_producto as nombre', 'cantidad', 'unidad_medida as unidad', 'valor', 'peso', 'nro_pedido_ofima', 'id as ped_det', 'nota')
                                        ->where('deleted', '0')
                                        ->where('id_pedido', $id_pedido)
                                        ->orderBy('cod_producto', 'ASC')
                                        ->get();

        return $detalle_pedido;
    }

    public function obtenerDetallePedidoAux($id_pedido, $productos = array()) {

        $items = '';

        
        for ($i=0; $i<count($productos); $i++){
            $items .= "'".$productos[$i]."'".',';
        }
        $items = substr($items, 0, -1);
        //dd($not_ids[0]);
        //dd($items);
        $detalle_pedido = pedido_detalle::select('cod_producto as codigo', 'nom_producto as nombre', 'cantidad', 'unidad_medida as unidad', 'valor', 'peso', 'id as id_ped_det', 'nota')
                                        ->where('deleted', '0')
                                        ->where('id_pedido', $id_pedido)
                                        //->whereIn('cod_producto', [$items])
                                        ->whereRaw("cod_producto in ($items)")
                                        ->get();

        return $detalle_pedido;
    }

    public function obtenerEncabezadoPedido(Request $request) {

        $ctl_despacho = new DespachoController();

        $pedido = Pedido::select('nit', 'cliente', 'estado', 'ciudad', 'observaciones', 'bodega','certificado_calidad', 'observaciones_certificado')
                                        ->where('deleted', '0')
                                        ->where('id', $request->id_pedido)
                                        ->first();
                                
        

        $cupo = $ctl_despacho->obtenerCupoCliente($pedido['nit']);
        $cupo_disp = $ctl_despacho->obtenerCupoDisponibleNombre($pedido['cliente']);
        $pedido['lista_precio'] = $cupo->Lista_Precio;

        if (isset($cupo_disp[$pedido['nit']])) {
            $pedido['deuda_total'] = $cupo->Cupo_Credito - $cupo_disp[$pedido['nit']];           
        } else {
            $pedido['deuda_total'] = $cupo->Cupo_Credito;
        }
        
        $pedido['bodega'] == 'si' ? $pedido['bodega'] = true : $pedido['bodega'] = false;

        return $pedido;
    }

    public function obtenerCupoDisponiblexCliente(Request $request){

        $ctl_despacho = new DespachoController();
        $cupo = $ctl_despacho->obtenerCupoCliente($request->nit);
        $cupo_disp = $ctl_despacho->obtenerCupoDisponibleNombre($request->nombre_cliente);

        //obtiene informacion adicional
        $pedido['lista_precio'] = $cupo->Lista_Precio;

        if (isset($cupo_disp[$request->nit])) {
            $pedido['deuda_total'] = $cupo->Cupo_Credito - $cupo_disp[$request->nit];           
        } else {
            $pedido['deuda_total'] = $cupo->Cupo_Credito;
        }

        return $pedido;
    }
    public function obtenerTiempoTrancurrido($fecha){

        $created = new Carbon($fecha);
        $now = Carbon::now();

        $transcurrido = $created->diffInMinutes($now);

        if ($transcurrido >=0 && $transcurrido <=3){
            $mensaje = 'Hace un momento';
        } else if($transcurrido > 3 && $transcurrido <= 60){
            $mensaje = 'Hace '.$transcurrido. ' min';
        } else if ($transcurrido > 60 && $transcurrido <= 1440){
            $mensaje = 'Hace '.round($transcurrido/60). ' hor';
            if (round($transcurrido/60)==1){
                $mensaje = 'Hace '.round($transcurrido/60). ' hor';
            }
        } else if ($transcurrido > 1440 && $transcurrido <= 11520) {
            $mensaje = 'Hace '.round(($transcurrido/60)/24). ' dias';
        } else if ($transcurrido > 11520 && $transcurrido <= 46080 ) {
            $mensaje = 'Hace '.round((($transcurrido/60)/24)/8). ' sem';
            if (round((($transcurrido/60)/24)/8) == 1){
                $mensaje = 'Hace '.round((($transcurrido/60)/24)/8). ' sem';
            }
        } else if ($transcurrido > 46080) {
            $mensaje = 'Hace más de '.round(((($transcurrido/60)/24)/8)/4). ' meses';
        } else {
            $mensaje = $fecha;
        }

        return $mensaje;
    }

    public function listadoPedidos(Request $request){

        $pedidos = Pedido::select('pedidos.id as id_pedido', 'pedidos.consecutivo', 'pedidos.nit', 'pedidos.cliente', 'pedidos.estado', 'pedidos.valor_total',
                                  'pedidos.total_kilos', 'pedidos.created_at as fecha', 'pedidos.observaciones', 'pedidos.ciudad', 'usu.name as vendedor')
                            ->where('pedidos.deleted', '0')
                            ->join('despachos as des', 'des.id', '=', 'pedidos.id_despacho')
                            ->join('users as usu', 'usu.vendedor', '=', 'des.vendedor')
                            ->whereRaw("DATE_FORMAT(pedidos.created_at ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicial."' and '".$request->fecha_final."'")
                            ->orderBy('pedidos.consecutivo', 'DESC')
                            ->get();
        
        for ($i=0; $i <count($pedidos) ; $i++) { 
            $pedidos[$i]['items_pedido'] = $this->obtenerDetallePedidoxId($pedidos[$i]['id_pedido']);
        }
 
        return $pedidos;
    }
}
