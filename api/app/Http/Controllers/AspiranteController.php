<?php

namespace App\Http\Controllers;

use App\Aspirante;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Jobs\ProcesarColaTrabajosAspirantes;
use Mail;
use App\Mail\MailAspirante;

class AspiranteController extends Controller
{

    // Este metodo se registra y se actualiza el aspirante
    public function regitrarAspirante(Request $request)
    {

        try{

                //dd($request->aspirante);
                $aspirante_obj = json_decode($request->aspirante, true);

                $id_generado = '';
                $tipo_movimiento = 'insertar';
                $documento_anterior = '';
                //Edit Aspirante
                if($aspirante_obj['id']){
                    $tipo_movimiento = 'modificacion';
                    $aspirante = Aspirante::find($aspirante_obj['id']);
                    $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
                    $id_generado = $aspirante_obj['id'];

                    if($aspirante->adjunto != 'no'){
                        $documento_anterior =  $aspirante->adjunto;
                    }

                }else{
                    //New Aspirante
                    $proceso = $this->verificarProcesoAbierto(trim($aspirante_obj['documento_identidad']));

                    if(count($proceso) > 0){
                        return response()->json([
                            "estado" => 'proceso_abierto',
                            "mensaje" => 'Atención: El aspirante tiene un proceso vigente.',
                            'proceso' => $proceso
                        ],200);                    
                    }

                    $aspirante = new Aspirante();
                    $aspirante->id = $id_generado = Uuid::generate()->string;
                    $aspirante->usuario_registra = JWTAuth::user()->vendedor;
                }

                $nombre = '';
                if($request->file){
                    $file = $request->file;
                    $nombre = $id_generado.'.'.$file->getClientOriginalExtension();
                    $aspirante->adjunto = $nombre;
                }

                $aspirante->documento_identidad = trim($aspirante_obj['documento_identidad']);
                $aspirante->fecha_expedicion = $aspirante_obj['fecha_expedicion'];
                $aspirante->apellido1 = trim(strtoupper($aspirante_obj['apellido1']));
                $aspirante->apellido2 = trim(strtoupper($aspirante_obj['apellido2']));
                $aspirante->nombre1 = trim(strtoupper($aspirante_obj['nombre1']));
                $aspirante->nombre2 = trim(strtoupper($aspirante_obj['nombre2']));
                $aspirante->nombre_completo = $aspirante->apellido1.' '.$aspirante->apellido2.' '.$aspirante->nombre1.' '.$aspirante->nombre2;
                $aspirante->celular = trim($aspirante_obj['celular']);
                $aspirante->correo = trim($aspirante_obj['correo']);
                $aspirante->cargo = trim(strtoupper($aspirante_obj['cargo']));
                $saved = $aspirante->save();

                if($saved === true){
                    if($request->file){
                        $this->subirArchivo($request->file, $id_generado, $tipo_movimiento, $documento_anterior);
                    }
                    return response()->json([
                        "estado" => 'ok',
                        "mensaje" => $aspirante_obj['id'] ? 'Los cambios se guardaron exitosamente' : 'El aspirante '.$aspirante->nombre_completo.' se registró correctamente.',
                    ],200);
                }
         }
         catch(\Exception $e){
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => $e->getMessage()
                ],500);  
         }

    }

    public function subirArchivo($file, $Idregistro, $tipoMoviminento, $adjuntoAnterior){

        $nombre = $Idregistro.'.'.$file->getClientOriginalExtension();

       //si no existe el directorio se crea
       if(!Storage::disk('public')->exists('hojas_vida')){
            Storage::makeDirectory('public/hv');
        }

        if($tipoMoviminento == 'modificacion'){
            //Elimina el archivo si existe
            Storage::disk('public')->delete('hv/'.$adjuntoAnterior);
        }

        //filesystem.php esta definido el driver
        Storage::disk('hojas_vida')->put($nombre,  \File::get($file));
    }

    public function verificarProcesoAbierto($documentoIdentidad){
        $info_aspirante = Aspirante::select('id as id_aspirante','preseleccion','entrevista','pruebas','examen','contratacion', 'created_at as fecha_proceso')
                                    ->where('documento_identidad', $documentoIdentidad)
                                    ->where('proceso_finalizado', 'no')
                                    ->get();
        return $info_aspirante;
    }

    public function obtenerInfoAspirante(Request $request){
        $info_aspirante = Aspirante::where('documento_identidad', $request->documento_identidad)
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
        return $info_aspirante;
    }

    public function finalizarProceso(Request $request){

        $aspirante = Aspirante::find($request->id_aspirante);
        $aspirante->proceso_finalizado = 'si';
        $aspirante->estado_proceso = 'proceso_terminado';
        $saved = $aspirante->save();

        if($saved === true){
            return response()->json([
                "estado" => 'ok',
                "mensaje" => 'Proceso finalizado correctamente',
            ],200);
        }else{
            return response()->json([
                "estado" => 'error',
                "mensaje" => 'Ocurrió un error al finalizar el proceso',
            ],500);
        }
    }

    public function obtenerListadoAspirantes(Request $request){

        $aspirantes = Aspirante::select('id as id_aspirante', 'deleted', 'documento_identidad', 'fecha_expedicion', 'apellido1', 'apellido2', 'nombre1', 'nombre2',
                                        'nombre_completo', 'celular', 'cargo','correo',
                                        'preseleccion',
                                        'entrevista', 'entrevista_fecha_cita', 'entrevista_hora_cita', 'entrevista_entrevistador','entrevista_observacion', 'entrevista_aprobo',
                                        'pruebas', 'pruebas_fecha', 'pruebas_hora', 'pruebas_lugar', 'pruebas_aprobo','pruebas_observacion',
                                        'examen', 'examen_fecha', 'examen_hora', 'examen_lugar', 'examen_aprobo','examen_observacion',
                                        'contratacion', 'contratacion_fecha','contratacion_hora', 'contratacion_observaciones','contratacion_fecha_fin_periodo_prueba',
                                        'proceso_finalizado', 'created_at as fecha_reg', 'updated_at as fecha_mod', 'estado_proceso', 'adjunto',
                                        DB::raw('(CASE 
                                                      WHEN estado_proceso = "sin_gestionar" THEN "Sin Gestionar" 
                                                      WHEN estado_proceso = "preseleccionado" THEN "Preseleccionado" 
                                                      WHEN estado_proceso = "entrevista" THEN "Entrevista"
                                                      WHEN estado_proceso = "pruebas" THEN "Pruebas"
                                                      WHEN estado_proceso = "examen_medico" THEN "Examen Médico"
                                                      WHEN estado_proceso = "examen_medico_aprobo" THEN "Exam. Médico Aprob."
                                                      WHEN estado_proceso = "contratado" THEN "Contratado"
                                                      WHEN estado_proceso = "entrevista_aprobada" THEN "Entrevista Aprobada"
                                                      WHEN estado_proceso = "pruebas_aprobada" THEN "Prueba Aprobada"
                                                      WHEN estado_proceso = "proceso_terminado" THEN "Proc. Finalizado"
                                                      ELSE "sin_gestionar" END) AS estado_label'))
                                ->where('deleted', $request->deleted)
                                ->orderBy('created_at', 'ASC');

                                if($request->filtro != 'no_aplica') {

                                    if($request->filtro != 'todos' && $request->fecha_corte == ''){
                                        if($request->filtro == 'en_proceso') {
                                            $aspirantes->whereIn('estado_proceso', ['preseleccionado', 'entrevista', 'entrevista_aprobada', 'pruebas', 'pruebas_aprobada', 'examen_medico', 'examen_medico_aprobo']);
                                        }else{
                                            $aspirantes->where('estado_proceso', '=', $request->filtro);
                                        }
                                    }
    
                                    if($request->fecha_corte !='' && $request->filtro == 'contratado'){
                                        $aspirantes->where('estado_proceso', '=', $request->filtro);
                                        $aspirantes->where('contratacion_fecha_fin_periodo_prueba', '<=', $request->fecha_corte);
                                    }

                                }

        $aspirantes = $aspirantes->get();

        $hoy = Carbon::now();
        for ($i=0; $i < count($aspirantes); $i++) {
            $aspirantes[$i]['indice'] = $i+1;
            $aspirantes[$i]['direccion_pruebas'] = config('global.pruebas.direccion');
            $aspirantes[$i]['lugar_pruebas'] = config('global.pruebas.lugar');
            $aspirantes[$i]['direccion_examen'] = config('global.examen_medico.direccion');
            $aspirantes[$i]['lugar_examen'] = config('global.examen_medico.lugar');

            $aspirantes[$i]['fecha_notifica_periodo'] = '';
            $aspirantes[$i]['label_notifica_fin_per_prueba'] = '';

            if($aspirantes[$i]['contratacion_fecha_fin_periodo_prueba']){

                $fecha_fin_prueba = Carbon::parse($aspirantes[$i]['contratacion_fecha_fin_periodo_prueba']);
                $fecha_resultado = $fecha_fin_prueba->subDays(config('global.dias_notifica'))->format('Y-m-d');
                $aspirantes[$i]['fecha_notifica_periodo'] = $fecha_resultado;
                $fecha_actual = $hoy->format('Y-m-d');

                if($fecha_actual >= $fecha_resultado && $fecha_actual <= $aspirantes[$i]['contratacion_fecha_fin_periodo_prueba']){
                    $aspirantes[$i]['label_notifica_fin_per_prueba'] = 'Próximo vencer';
                } else if ($fecha_actual > $aspirantes[$i]['contratacion_fecha_fin_periodo_prueba']) {
                    $aspirantes[$i]['label_notifica_fin_per_prueba'] = 'Periodo de prueba vencido';
                }
            }
            
        }

        return $aspirantes;
                                
    }

    public function downloadFile($archivo){
        return response()->download(storage_path('app/public/hv/'.$archivo));
    }

    public function cambiarEstadoProceso(Request $request){

        $actual = Carbon::now();
        $aspirante = Aspirante::find($request->parametros['id_aspirante']);
        $mensaje_alerta = '';

        if($request->parametros['accion'] == 'preseleccionado'){
            $aspirante->preseleccion = 'si';
            $aspirante->preseleccion_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->preseleccion_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = $request->parametros['accion'];
            $mensaje_alerta = 'El aspirante ha sido preselecionado correctamente';
        }

        if($request->parametros['accion'] == 'eliminar_aspirante'){
            $aspirante->deleted = '1';
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'proceso_terminado';
            $aspirante->proceso_finalizado = 'si';
            $mensaje_alerta = 'El registro ha sido eliminado del sistema';
            //Elimina la HV adjunta
            if($aspirante->adjunto !== 'no'){
                Storage::disk('public')->delete('hv/'.$aspirante->adjunto);
            }
        }

        // ENTREVISTA -------------------------------------------------------------------------------------------------

        if($request->parametros['accion'] == 'entrevista' || $request->parametros['accion'] == 'reprogramar'){
            $aspirante->entrevista = 'si';
            $aspirante->entrevista_entrevistador = strtoupper($request->parametros['entrevista']['entrevistador']);
            $fecha_hora = explode(" ", $request->parametros['entrevista']['fecha']);
            $aspirante->entrevista_fecha_cita = $fecha_hora[0];
            $aspirante->entrevista_hora_cita = $fecha_hora[1];
            $aspirante->entrevista_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->entrevista_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'entrevista';
            $mensaje_alerta = '¡Muy bien! entrevista programada exitosamente ...';
        }

        if($request->parametros['accion'] == 'aprobar_entrevista'){
            $aspirante->entrevista_aprobo = 'si';
            $aspirante->entrevista_observacion = $request->parametros['entrevista']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'entrevista_aprobada';
            $mensaje_alerta = '¡Muy bien! la entrevista se aprobó exitosamente ...';
        }

        if($request->parametros['accion'] == 'no_aprobar_entrevista'){
            $aspirante->entrevista_aprobo = 'no';
            $aspirante->entrevista_observacion = $request->parametros['entrevista']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'proceso_terminado';
            $aspirante->proceso_finalizado = 'si';
            $mensaje_alerta = 'El aspirante no aprobó la entrevista, el proceso terminó correctamente.';
        }

        if($request->parametros['accion'] == 'anular_aprobar_entrevista'){
            $aspirante->entrevista_aprobo = 'no';
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'entrevista';
            $mensaje_alerta = 'Se anuló la aprobación de la entrevista';
        }
        //------------------------- END ENTREVISTA -----------------------------------------------------------

        // PRUEBAS -------------------------------------------------------------------------------------------

        if($request->parametros['accion'] == 'pruebas' || $request->parametros['accion'] == 'reprogramar_pruebas'){
            $aspirante->pruebas = 'si';
            $fecha_hora = explode(" ", $request->parametros['pruebas']['fecha']);
            $aspirante->pruebas_fecha = $fecha_hora[0];
            $aspirante->pruebas_hora = $fecha_hora[1];
            $aspirante->pruebas_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->pruebas_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->pruebas_lugar = config('global.pruebas.lugar').' '.config('global.pruebas.direccion');
            $aspirante->estado_proceso = 'pruebas';
            $mensaje_alerta = '¡Muy bien! el aspirante ha sido programado a pruebas';
        }

        if($request->parametros['accion'] == 'aprobar_pruebas'){
            $aspirante->pruebas_aprobo = 'si';
            $aspirante->pruebas_observacion = $request->parametros['pruebas']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'pruebas_aprobada';
            $mensaje_alerta = '¡Muy bien! la prueba se aprobó exitosamente';
        }

        if($request->parametros['accion'] == 'anular_aprobar_pruebas'){
            $aspirante->pruebas_aprobo = 'no';
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'pruebas';
            $mensaje_alerta = 'Se anuló la aprobación de las pruebas';
        }

        if($request->parametros['accion'] == 'no_aprobar_pruebas'){
            $aspirante->pruebas_aprobo = 'no';
            $aspirante->pruebas_observacion = $request->parametros['pruebas']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'proceso_terminado';
            $aspirante->proceso_finalizado = 'si';
            $mensaje_alerta = 'El aspirante no pasó las pruebas, el proceso terminó correctamente.';
        }

        //------------------------- END PRUEBAS -----------------------------------------------------------

        // EXAMEN MEDICO -------------------------------------------------------------------------------------------

        if($request->parametros['accion'] == 'examen' || $request->parametros['accion'] == 'reprogramar_examen'){
            $aspirante->examen = 'si';
            $fecha_hora = explode(" ", $request->parametros['examen']['fecha']);
            $aspirante->examen_fecha = $fecha_hora[0];
            $aspirante->examen_hora = $fecha_hora[1];
            $aspirante->examen_lugar = config('global.pruebas.lugar').' '.config('global.pruebas.direccion');
            $aspirante->examen_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->examen_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'examen_medico';
            $mensaje_alerta = '¡Muy bien! el aspirante ha sido enviado a examen médico';
        }

        if($request->parametros['accion'] == 'aprobar_examen'){
            $aspirante->examen_aprobo = 'si';
            $aspirante->examen_observacion = $request->parametros['examen']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'examen_medico_aprobo';
            $mensaje_alerta = '¡Muy bien! el aspirante pasó el examen médico';
        }

        if($request->parametros['accion'] == 'no_aprobar_examen'){
            $aspirante->examen_aprobo = 'no';
            $aspirante->examen_observacion = $request->parametros['examen']['observacion'];
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'proceso_terminado';
            $aspirante->proceso_finalizado = 'si';
            $mensaje_alerta = 'El aspirante NO pasó los exámenes médicos, el proceso terminó correctamente';
        }

        if($request->parametros['accion'] == 'anular_aprobar_examen'){
            $aspirante->examen_aprobo = 'no';
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->estado_proceso = 'examen_medico';
            $mensaje_alerta = 'Se anuló la aprobación del examen médico';
        }

        //------------------------- END EXAMEN MEDICO --------------------------------------------------------------

        // CONTRATACION
        if($request->parametros['accion'] == 'contratacion' || $request->parametros['accion'] == 'reprogramar_contratacion'){
            $aspirante->contratacion = 'si';
            $fecha_hora = explode(" ", $request->parametros['contratacion']['fecha']);
            $aspirante->contratacion_fecha = $fecha_hora[0];
            $aspirante->contratacion_hora = $fecha_hora[1];
            $aspirante->contratacion_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->contratacion_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->contratacion_observaciones = $request->parametros['contratacion']['observacion'];
            $fecha_vence_periodo_prueba = Carbon::parse($aspirante->contratacion_fecha)->addMonths(config('global.periodo_prueba'));
            $aspirante->contratacion_fecha_fin_periodo_prueba = $fecha_vence_periodo_prueba;
            $aspirante->estado_proceso = 'contratado';
            $aspirante->proceso_finalizado = 'si';
            if($request->parametros['accion'] == 'reprogramar_contratacion'){
                $mensaje_alerta = 'Se ha reprogramado la fecha y hora de ingreso del aspirante';
            }else{
                $mensaje_alerta = '¡Muy bien! el aspirante se ha contratado exitosamente';
            }
        }
        if($request->parametros['accion'] == 'contratacion_anular'){
            $aspirante->contratacion = 'no';
            $aspirante->contratacion_fecha_usu_reg = $actual->toDateTimeString();
            $aspirante->contratacion_usu_reg = JWTAuth::user()->vendedor;
            $aspirante->usuario_modifica = JWTAuth::user()->vendedor;
            $aspirante->contratacion_observaciones = $request->parametros['contratacion']['observacion'];
            $aspirante->estado_proceso = 'proceso_terminado';
            $aspirante->proceso_finalizado = 'si';
            $mensaje_alerta = 'Se ha cancelado el proceso de contratación del aspirante';
        }
        
        //--------------------------END CONTRATACION ---------------------------------------------------------------

        $saved = $aspirante->save();

        if($saved === true){

            $aspirante['accion'] = $request->parametros['accion'];

            if(config('global.notifica_aspirante') == true && ($aspirante['accion'] == 'entrevista' || $aspirante['accion'] == 'reprogramar')){
                ProcesarColaTrabajosAspirantes::dispatch($aspirante);
            }

            return response()->json([
                "estado" => 'ok',
                "mensaje" => $mensaje_alerta,
            ],200);
        }else{
            return response()->json([
                "estado" => 'error',
                "mensaje" => 'Ocurrió un error al gestionar el aspirante',
            ],500);
        }

    }

    public function modoDesarrollo(){

        if (config('global.test') === true){
            return 'desarrollo';
        } else {
            return 'produccion';
        }
    }

    public function enviarNotificacionEmail($datos = array()){

        switch ($datos['accion']) {

            case 'entrevista':

                $datos['email_aspirante'] = $this->modoDesarrollo() === 'desarrollo' ? 'sistemas@icoharinas.com.co' : $datos['correo'];
                $datos['direccion_pruebas'] = config('global.entrevista.direccion');
                $datos['lugar_pruebas'] = config('global.entrevista.lugar');
                Mail::to($datos['email_aspirante'], $datos['nombre_completo'])->send(new MailAspirante($datos));

            break;
        }
    }
}
