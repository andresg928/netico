<?php

namespace App\Http\Controllers;

use App\facturas_pagas;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class FacturasPagasController extends Controller
{
    public function obtenerFacturasDescargadas(){

        $facturas = facturas_pagas::select('factura')->where('deleted', '0')->get();

        $fa = array();

        foreach($facturas  as $val) {
            $fa[] = $val->factura;
        }
        return $fa;
    }

    public function registroFacturaPaga(Request $request){

        $facturas_ = facturas_pagas::select('id as id_registro', 'factura')->where('factura', $request->factura)->where('deleted', '0')->get();

        // Si existe la elimina
        if(count($facturas_) > 0){
            $facturas = facturas_pagas::find($facturas_[0]['id_registro']);
            $facturas->deleted = '1';
            $saved = $facturas->save();

            if($saved === true){
                return response()->json([
                    "estado" => 'ok',
                    "mensaje" => 'Registro exitoso ...'
                ],200);
            } else {
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => 'Vuelva a intentarlo'
                ],200);            
            }
        }

        $factura = new facturas_pagas();
        $factura->id = Uuid::generate()->string;
        $factura->factura = $request->factura;
        $saved = $factura->save();

        if($saved === true){
            return response()->json([
                "estado" => 'ok',
                "mensaje" => 'Registro exitoso ...'
            ],200);
        } else {
            return response()->json([
                "estado" => 'error',
                "mensaje" => 'Vuelva a intentarlo'
            ],200);            
        }
    }
}
