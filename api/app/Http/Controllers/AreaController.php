<?php

namespace App\Http\Controllers;

use App\Area;
use App\causales;
use App\ResponsablesAreas;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use  JWTAuth;

class AreaController extends Controller
{
    public function registrar(Request $request){

        $area = new Area();
        $area->id = Uuid::generate()->string;
        $area->codigo = strtoupper($request->codigo);
        $area->nombre = strtoupper($request->nombre);
        $area->usu_registra = JWTAuth::user()->vendedor;
        $saved = $area->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Área registrada correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }  
    }

    public function actualizar(Request $request){

        $area = Area::find($request->id_registro);
        $area->codigo = strtoupper($request->codigo);
        $area->nombre = strtoupper($request->nombre);
        $area->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $area->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Área actualizada correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo realizar la operación, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }  
    }

    public function obtenerResponsablesxArea($idArea){
        $t = 'responsables_areas.';
        $respxarea = ResponsablesAreas::select('res.nombre as nom_responsable', $t.'id as id_resp_area')
                                        ->where($t.'deleted', '0')
                                        ->join('responsables as res', 'res.id_registro', '=', $t.'id_responsable')
                                        ->where('res.deleted', '0')
                                        ->where($t.'id_area', $idArea)
                                        ->get();
        return $respxarea;
    }

    public function obtenerAreaxCodigo($codigo){
        $area = Area::select('codigo', 'nombre')->where('codigo', $codigo)->where('deleted', '0')->first();
        return $area;
    }

    public function obtenerCausalxArea($idArea){
        $causales = causales::select('nombre', 'id as id_causal')->where('id_area', $idArea)->where('deleted','0')->orderBy('codigo', 'ASC')->get();
        return $causales;
    }
    public function obtenerAreas(Request $request){
        $areas = Area::select('id as id_registro','codigo', 'nombre')->where('deleted', '0')->where('nombre','LIKE','%'.$request->filtro.'%')->orderBy('nombre', 'ASC')->get();

        for ($i=0; $i < count($areas); $i++) {

            $areas[$i]['responsables'] = $this->obtenerResponsablesxArea($areas[$i]['id_registro']);
            $areas[$i]['causales'] = $this->obtenerCausalxArea($areas[$i]['id_registro']);
        }

        return $areas;
    }

    public function inactivar(Request $request){

        $resp_area_aux = ResponsablesAreas::where('id_area', $request->id_registro)->where('deleted', '0')->get();

        if(count($resp_area_aux)>0){
            return response()->json([
                "mensaje" => "¡Error! No se puede eliminar, tiene asignado un responsable",
                'estado' => 'error',
            ],200);               
        }

        causales::where('id_area', $request->id_registro)->where('deleted', '0')->update(['deleted' => '1']);

        $area = Area::find($request->id_registro);
        $area->deleted = '1';
        $area->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $area->save();

        if($saved == true){
            return response()->json([
                "mensaje" => "¡Muy bien! Eliminado correctamente",
                'estado' => 'ok',
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "¡Error! No se pudo eliminar el registro, vuelta a intentarlo",
                'estado' => 'error',
            ],200);                
        }     
    }
}
