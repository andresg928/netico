<?php

namespace App\Http\Controllers;

use App\PresupuestoVendedores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\AuthController; 

class PresupuestoVendedoresController extends Controller
{

    public function obtenerPresupuestosApiPowerBi(){

        $presupuesto = PresupuestoVendedores::select('vendedor', 'anio','codigo_agrupa','enero as 01','febrero as 02', 'marzo as 03', 'abril as 04','mayo as 05', 'junio as 06', 'julio as 07',
                                                    'agosto as 08', 'septiembre  as 09', 'octubre as 10', 'noviembre as 11', 'diciembre as 12')
                                            ->where('deleted', '0')->get();
        return $presupuesto;
    }

    public function obtenerGrupoProductosVendedor(Request $request){

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;

        $grupos = DB::connection('sqlsrv')
                                            ->table('MTMERCIA as s')
                                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"), DB::raw("'0' AS enero"),
                                                         DB::raw("'0' AS febrero"),DB::raw("'0' AS marzo"),DB::raw("'0' AS abril"),DB::raw("'0' AS mayo"),
                                                         DB::raw("'0' AS junio"),DB::raw("'0' AS julio"),DB::raw("'0' AS agosto"),DB::raw("'0' AS septiembre"),
                                                         DB::raw("'0' AS octubre"),DB::raw("'0' AS noviembre"),DB::raw("'0' AS diciembre"))
                                                ->where('s.codigo', 'like', 'G0%')
                                                ->orderBy('s.codigo', 'ASC')->get();

        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
        $meses_aux = ['enero_total', 'febrero_total', 'marzo_total', 'abril_total', 'mayo_total', 'junio_total', 'julio_total', 'agosto_total', 'septiembre_total', 'octubre_total', 'noviembre_total', 'diciembre_total'];

        for($i=0; $i<count($grupos); $i++){

            // se valida que el presupuesto exista para poner los valores en el respectivo mes
            $presupuesto = PresupuestoVendedores::where('anio', $request->anio)->where('codigo_agrupa', $grupos[$i]->codigo)->where('vendedor', $request->vendedor)->first();

            if($presupuesto){

                for($m=0; $m<count($meses);$m++){
                    $mes = $meses[$m];
                    $mes_total = $meses_aux[$m];
                    $grupos[$i]->$mes = (int) $presupuesto->{$meses[$m]};
                    $grupos[$i]->$mes_total = ($presupuesto->{$meses[$m]} * $grupos[$i]->peso) / $unidad_medida;
                }

            } else {
                for($m=0; $m<count($meses);$m++){
                    $mes = $meses[$m];
                    $mes_total = $meses_aux[$m];
                    $grupos[$i]->$mes = 0;
                    $grupos[$i]->$mes_total = 0;
                }                
            }
        }

        return $grupos;
    }

    public function obtenerUltimoDiaMes($fecha){
        $end = new Carbon($fecha);
        $date = $end->endOfMonth();
        return  $date->format('d');
    }

    public function obtenerPresupuestoConsolidadoPorVendedor(Request $request){

        //$request->fecha_inicial = '2021-04-01';
        //$request->fecha_final = '2021-04-30';
        //$request->anio = '2021';
        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        Carbon::setLocale('es');

        if($request->tipo_consulta === 'full_vendedores') {
            $date1 = new Carbon($request->fecha_inicial);
            $date2 = new Carbon($request->fecha_final);
            $anio = $request->anio;
            $mes  = $date1->monthName;    
        } else {
            $date1 = new Carbon(date('Y-m-d'));
            $date2 = new Carbon(date('Y-m-d'));
            $anio = $date1->year;
            $mes  = $date1->monthName;
            $fechas = $this->obtenerInicioFinMes($mes, $anio);
            $request->fecha_inicial = $fechas[0];
            $request->fecha_final = $fechas[1];
        }

        if($date1->monthName !== $date2->monthName){
            return response()->json([
                "estado" => "error",
                "mensaje" => 'El rango de fechas deben estar en el mismo mes'
            ],200); 
        }

        if($request->opcion === 'true'){

            $date2 = new Carbon(date('Y-m-d'));
            $start = new Carbon($request->fecha_inicial);
            $diasTranscurridos= $date2->diffInDays($start->startOfMonth()->format('Y-m-d')) + 1;

            if($diasTranscurridos > 30){
                $diasTranscurridos = 30;
            }

            $dias_mes = $this->obtenerUltimoDiaMes($request->fecha_inicial);

        } else {
            $diasTranscurridos = 1;
            $dias_mes = 1;
        }

        $auth_ctrl = new AuthController();

        if($request->tipo_consulta == 'full_vendedores') {
            $vendedores = $auth_ctrl->obtenerVendedores();
        } else {
            $vendedores = $auth_ctrl->obtenerVendedores(JWTAuth::user()->vendedor);
        }
        
        $data = array();

        for($i=0; $i<count($vendedores); $i++){

            $presup_vendedores = PresupuestoVendedores::select('codigo_agrupa', $mes)->whereRaw("codigo_agrupa not in ('G014', 'G015', 'G016', 'G017', 'G018')")->where('anio', $anio)->where('vendedor', $vendedores[$i]->codigo)->get();
            //$presup_vendedores = PresupuestoVendedores::select('codigo_agrupa', $mes)->whereRaw("codigo_agrupa  in ('G018')")->where('anio', $anio)->where('vendedor', $vendedores[$i]->codigo)->get();
            //dd($presup_vendedores);
            $ventas_vendedores = $this->ventasPorGrupoFecha($request->fecha_inicial, $request->fecha_final, '',$vendedores[$i]->codigo, 'fn_sql', $unidad_medida);

            $sum_pre_bultos = 0;
            $sum_venta_bultos = 0;
            $sum_venta_pasta_granel = 0;
            $suma_venta_total_bultos = 0;
            $cumplimiento = 0;

            if(count($ventas_vendedores)>0){
                $sum_venta_bultos = $sum_venta_bultos + $ventas_vendedores[0]->cantidad_bultos;
            }

            for($p=0; $p<count($presup_vendedores); $p++){

                $presup_vendedor = ($presup_vendedores[$p][$mes] / $dias_mes) *  $diasTranscurridos;
                // se obtiene el peso del grupo
                $grupo = $this->obtenerGruposProductos('individual', $presup_vendedores[$p]['codigo_agrupa']);
                // $ventas_vendedores = $this->ventasPorGrupoFecha($request->fecha_inicial, $request->fecha_final, $presup_vendedores[$p]['codigo_agrupa'], $vendedores[$i]->codigo);

                if($presup_vendedores){
                    if($presup_vendedores[$p][$mes]>0){
                        $sum_pre_bultos = $sum_pre_bultos + ($presup_vendedor * $grupo->peso) / $unidad_medida;
                    }
                }
            }

            $cumplimiento = $sum_pre_bultos > 0 ? (($sum_venta_bultos + $sum_venta_pasta_granel) * 100) / $sum_pre_bultos : 0;
            $data[$i]['estado_cumplimiento'] = $this->obtenerCumplimientoVentas($cumplimiento, 1);
            $data[$i]['cumplimiento'] = $cumplimiento;
            $data[$i]['vendedor'] = $vendedores[$i]->nombre;
            $data[$i]['presupuesto_bultos'] = round($sum_pre_bultos,2);
            $data[$i]['ventas_bultos'] = round(($sum_venta_bultos + $sum_venta_pasta_granel),2);
        }

        $suma_venta_total_bultos = 0;

        for($d=0; $d<count($data); $d++){

            $suma_venta_total_bultos = $suma_venta_total_bultos + $data[$d]['ventas_bultos'];

        }

        for($d=0; $d<count($data); $d++){

            $data[$d]['participacion'] = $suma_venta_total_bultos > 0 ? round(($data[$d]['ventas_bultos'] * 100) / $suma_venta_total_bultos, 2) : 0;

        }

        return $data;
    }

    public function obtenerCumplimientoVentas($porc_cumplimiento, $tipo){

        if($tipo == 1){
            if($porc_cumplimiento < 60){
                return 'muy_bajo';
            } else if ($porc_cumplimiento >= 60 && $porc_cumplimiento < 70){
                return 'bajo';
            } else if ($porc_cumplimiento >= 70 && $porc_cumplimiento < 80){
                return 'medio_bajo';
            } else if ($porc_cumplimiento >= 80 && $porc_cumplimiento < 95){
                return 'medio_alto';
            } else if ($porc_cumplimiento >= 95){
                return 'alto';
            }
        } else if ($tipo == 2){
            if($porc_cumplimiento < 40){
                return 'muy_bajo';
            } else if ($porc_cumplimiento >= 40 && $porc_cumplimiento < 90){
                return 'bajo';
            } else if ($porc_cumplimiento >= 90 && $porc_cumplimiento < 100){
                return 'medio_alto';
            } else if ($porc_cumplimiento >=100){
                return 'alto';
            }           
        }

    }

    public function generarPresupuestosCero($anio, $vendedor, $mes){

        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

        $grupos = $this->obtenerGruposProductos('full_grupos', '');

        for($i=0; $i<count($grupos); $i++){

            for($m=0; $m<count($meses);$m++){

                $mes = $meses[$m];

                $val_presup = PresupuestoVendedores::select($mes)
                                                    ->where('codigo_agrupa', $grupos[$i]->codigo)->where('anio', $anio)->where('vendedor', $vendedor)->first();
                
                if(!$val_presup){

                    $presupuesto = new PresupuestoVendedores();
                    $presupuesto->id = Uuid::generate()->string;
                    $presupuesto->codigo_agrupa = $grupos[$i]->codigo;
                    $presupuesto->vendedor = $vendedor;
                    $presupuesto->anio = $anio;
                    $presupuesto->{$mes} = 0;
                    $presupuesto->usu_registra = JWTAuth::user()->vendedor;
                    $presupuesto->save();
                }
            }
        }

    }

    public function ingresarPresupuestoVendedor(Request $request){

        $val_presupuesto = PresupuestoVendedores::select('id as id_pre')
                                                ->where('codigo_agrupa', $request->cod_agrupa)
                                                ->where('anio', $request->anio)
                                                ->where('vendedor', $request->vendedor)
                                                ->first();

        if($val_presupuesto){
            $presupuesto = PresupuestoVendedores::find($val_presupuesto['id_pre']);
            $presupuesto->{$request->campo} = $request->valor;
            $presupuesto->usu_modifica = JWTAuth::user()->vendedor;
            $saved = $presupuesto->save();
        } else {
            $presupuesto = new PresupuestoVendedores();
            $presupuesto->id = Uuid::generate()->string;
            $presupuesto->codigo_agrupa = $request->cod_agrupa;
            $presupuesto->vendedor = $request->vendedor;
            $presupuesto->anio = $request->anio;
            $presupuesto->{$request->campo} = $request->valor;
            $presupuesto->usu_registra = JWTAuth::user()->vendedor;
            $saved = $presupuesto->save();
        }

        if($saved === true){

            // se genera presupuesto en 0 a los meses que faltan de todos los grupos de productos
            $this->generarPresupuestosCero($request->anio, $request->vendedor, $request->campo);
    
            return response()->json([
                "estado" => "ok",
                "mensaje" => 'La información de guardó de manera correcta'
            ],200); 
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Error al guardar la información'
            ],200); 
        }

    }

    public function obtenerGruposProductos($tipo_visualiza, $filtro){

        if($tipo_visualiza === 'full_grupos'){

            $grupos = DB::connection('sqlsrv')
                            ->table('MTMERCIA as s')
                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                ->where('s.codigo', 'like', 'G0%')
                                //->whereRaw("s.codigo not in ('G017', 'G016', 'G018')")
                                ->orderBy('s.codigo', 'ASC')->get();
        }

        if($tipo_visualiza === 'grupos'){

            $grupos = DB::connection('sqlsrv')
                            ->table('MTMERCIA as s')
                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                ->where('s.codigo', 'like', 'G0%')
                                ->whereRaw("s.codigo not in ('G017', 'G016', 'G018')")
                                ->orderBy('s.codigo', 'ASC')->get();
        }

        if($tipo_visualiza === 'individual'){

            $grupos = DB::connection('sqlsrv')
                            ->table('MTMERCIA as s')
                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                ->where('s.codigo', 'like', 'G0%')
                                //->whereRaw("s.codigo not in ('G017', 'G016', 'G018')")
                                ->where('s.codigo', $filtro)
                                ->orderBy('s.codigo', 'ASC')->first();            
        }

        if($tipo_visualiza === 'vendedor') {

            $grupos = DB::connection('sqlsrv')
                            ->table('MTMERCIA as s')
                                ->select(DB::raw("RTRIM(s.codigo) AS codigo"), DB::raw("RTRIM(s.DESCRIPCIO) AS descripcion"), DB::raw("RTRIM(s.MEAPBVR) AS peso"))
                                ->where('s.codigo', 'like', 'G0%')
                                ->whereRaw("s.codigo not in ('G017', 'G016', 'G018', 'G014', 'G015')")
                                ->orderBy('s.codigo', 'ASC')->get();
        }

        return $grupos;
    }

    public function obtenerVentasVendedoresFecha(Request $request){

        $auth_ctrl = new AuthController();
        Carbon::setLocale('es');

        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;

        $fec_ini = $request->fecha_inicial;
        $fec_fin = $request->fecha_final;

        $fec_ini_aux = $request->fecha_inicial;
        $date1_aux = new Carbon($fec_ini_aux);
        $fec1 = $this->obtenerInicioFinMes($date1_aux->monthName, $date1_aux->year);

        $date1 = new Carbon($request->fecha_inicial);
        $date2 = new Carbon($request->fecha_final);
        $mes  = $date1->monthName;
        $anio = $date1->year;
        $diasTranscurridos = 1;
        $dias_mes = 1;
        
        $vendedores = $auth_ctrl->obtenerVendedores();

        for($v=0; $v<count($vendedores); $v++){

            $sum_bultos = 0;
            $sum_bultos_mes = 0;
            $sum_pre_bultos = 0;
            $vendedor = $vendedores[$v]->codigo;

            // consulta el presupesto
            $presup_vendedores = PresupuestoVendedores::select('codigo_agrupa', $mes)->whereRaw("codigo_agrupa not in ('G014', 'G015', 'G016', 'G017', 'G018')")->where('anio', $anio)->where('vendedor', $vendedor)->get();

            for($p=0; $p<count($presup_vendedores); $p++){

                $presup_vendedor = ($presup_vendedores[$p][$mes] / $dias_mes) *  $diasTranscurridos;
                // se obtiene el peso del grupo
                $grupo = $this->obtenerGruposProductos('individual', $presup_vendedores[$p]['codigo_agrupa']);

                if($presup_vendedores){
                    if($presup_vendedores[$p][$mes]>0){
                        $sum_pre_bultos = $sum_pre_bultos + ($presup_vendedor * $grupo->peso) / $unidad_medida;
                    }
                }
            }

            $ventas_vendedor = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, '', $vendedor, 'total', $unidad_medida);
            $ventas_vendedor_mes = $this->ventasPorGrupoFecha($fec1[0], $fec1[1], '', $vendedor, 'total', $unidad_medida);

            for($i=0; $i<count($ventas_vendedor); $i++){
                $sum_bultos = $sum_bultos + $ventas_vendedor[$i]->cantidad_bultos;
            }

            for($i=0; $i<count($ventas_vendedor_mes); $i++){
                $sum_bultos_mes = $sum_bultos_mes + $ventas_vendedor_mes[$i]->cantidad_bultos;
            }

            $cumplimiento = $sum_pre_bultos > 0 ? (($sum_bultos_mes) * 100) / $sum_pre_bultos : 0;
            $vendedores[$v]->cumplimiento = $cumplimiento;
            $vendedores[$v]->venta_bultos = $sum_bultos;
            $vendedores[$v]->estado_cumplimiento= $this->obtenerCumplimientoVentas($cumplimiento, 2);
        }

        return $vendedores;       
    }

    public function esBisiesto($year){

        return date('L',mktime(1,1,1,1,1,$year));

    }

    public function obtenerInicioFinMes($mes, $anio){

        $datos = array();

        switch ($mes) {

            case 'enero':
                $mes = '01';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'febrero':
                $mes = '02';
                $dia = $this->esBisiesto($anio) ==   0 ? '28' : '29';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.$dia;
            break;

            case 'marzo':
                $mes = '03';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'abril':
                $mes = '04';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'mayo':
                $mes = '05';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'junio':
                $mes = '06';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'julio':
                $mes = '07';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'agosto':
                $mes = '08';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'septiembre':
                $mes = '09';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'octubre':
                $mes = '10';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;

            case 'noviembre':
                $mes = '11';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'30';
            break;

            case 'diciembre':
                $mes = '12';
                $datos[0] = $anio.$mes.'01';
                $datos[1] = $anio.$mes.'31';
            break;
        }

        return $datos;
    }

    public function obtenerVentasVsPresupuestoMes(Request $request){

        Carbon::setLocale('es');
        $unidad_medida = $request->unidad_medida == 'kilos' ? 1 : 50;
        $vendedor = $request->vendedor;

        if ($request->origen_modulo !== 'presupuestos'){
            $fechas = $this->obtenerInicioFinMes(strtolower($request->mes), $request->anio);
            $request->fecha_inicial = $fechas[0];
            $request->fecha_final = $fechas[1];
            $vendedor = JWTAuth::user()->vendedor;
            //$vendedor = '012';
        }

        $date1 = new Carbon($request->fecha_inicial);
        $date2 = new Carbon($request->fecha_final);

        if($date1->monthName !== $date2->monthName){
            return response()->json([
                "estado" => "error",
                "mensaje" => 'El rango de fechas deben estar en el mismo mes'
            ],200); 
        }

        $mes  = $date1->monthName;
        $fec_ini = $request->fecha_inicial;
        $fec_fin = $request->fecha_final;
        $anio = $request->anio;
        
        $request->carta == '' ? 0 : $request->carta;

        $grupos = $this->obtenerGruposProductos('grupos', '');
        
        for($i=0; $i<count($grupos); $i++){

            $presup_grupo = PresupuestoVendedores::select($mes)->where('codigo_agrupa', $grupos[$i]->codigo)->where('anio', $anio)->where('vendedor', $vendedor)->first();

            $ventas_grupo = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, $grupos[$i]->codigo, $vendedor, 'detallada', $unidad_medida);


            if($presup_grupo && $ventas_grupo){

                // quita las grasas por que no se pueden convertir a bultos
                if($grupos[$i]->codigo != 'G015'){

                    // se consulta el grupo granel para sumarlo al grupo pasta
                    $ventas_past_granel = $this->ventasPorGrupoFecha($fec_ini, $fec_fin, 'G018', $vendedor, 'detallada', $unidad_medida);

                    if($grupos[$i]->codigo == 'G012'){
                        $grupos[$i]->venta_cantidad = $ventas_past_granel ? $ventas_grupo->cantidad + $ventas_past_granel->cantidad : $ventas_grupo->cantidad * 1;
                        $grupos[$i]->venta_bultos = $ventas_past_granel ? round($ventas_grupo->cantidad_bultos + ((round($ventas_past_granel->cantidad * $grupos[$i]->peso)) / $unidad_medida)) : round($ventas_grupo->cantidad_bultos);
                    // le suma el valor de la carta    
                    } else if ($grupos[$i]->codigo == 'G001') {
                        $grupos[$i]->venta_cantidad =  $ventas_grupo->cantidad + $request->carta;
                        $grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos + $request->carta;
                    } else {
                        $grupos[$i]->venta_cantidad =  $ventas_grupo->cantidad * 1;
                        $grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos * 1;
                    }

                    
                    $grupos[$i]->presupuesto_cantidad = $presup_grupo[$mes] * 1;
                    $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
                    //$grupos[$i]->venta_bultos = $ventas_grupo->cantidad_bultos;
    
                    if($presup_grupo[$mes]!=0) {

                        // si es carta tambien afecta el porcetaje del grupo 1
                        if($grupos[$i]->codigo == 'G001'){
                            $grupos[$i]->porcentaje =  round((($ventas_grupo->cantidad_bultos + $request->carta)  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),1);
                        }else{
                            if($grupos[$i]->codigo == 'G012'){
                                $grupos[$i]->porcentaje = $ventas_past_granel ?  round((($ventas_grupo->cantidad_bultos + ((round($ventas_past_granel->cantidad * $grupos[$i]->peso)) / $unidad_medida))  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),1) : round((($ventas_grupo->cantidad_bultos)  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),1);
                            }else{
                                $grupos[$i]->porcentaje =  round(($ventas_grupo->cantidad_bultos  * 100) / (round(($presup_grupo[$mes] * $grupos[$i]->peso)) / $unidad_medida),1);
                            } 
                        }
                        
                    } else {
                        $grupos[$i]->porcentaje = 0;
                    }
                } else {

                    $grupos[$i]->presupuesto_cantidad = $presup_grupo[$mes] * 1;
                    $grupos[$i]->presupuesto_bultos =  round($presup_grupo[$mes]) * 1;
                    $grupos[$i]->venta_cantidad = $ventas_grupo->cantidad * 1;
                    $grupos[$i]->venta_bultos = $ventas_grupo->cantidad * 1;
    
                    if($presup_grupo[$mes]!=0) {
                        $grupos[$i]->porcentaje =  round(($ventas_grupo->cantidad  * 100) / (round(($presup_grupo[$mes]))),1);
                    } else {
                        $grupos[$i]->porcentaje = 0;
                    }
                }                
            } else {
                // para llenar datos grupos que no se ha reflejado ventas, o sea salga en 0
                $grupos[$i]->presupuesto_cantidad = $presup_grupo[$mes] * 1;
                $grupos[$i]->venta_cantidad =  0;
                $grupos[$i]->presupuesto_bultos =  round(($presup_grupo[$mes] * $grupos[$i]->peso) / $unidad_medida);
                $grupos[$i]->venta_bultos = 0;
                $grupos[$i]->porcentaje = 0;
            }
        }
   
        return $grupos;
    }

    public function ventasPorGrupoFecha($fecha_ini, $fecha_fin, $cod_agrupa, $vendedor, $tipo_consulta = 'detallada', $unidad_medida = ''){

        $fecha_ini = str_replace("-","",$fecha_ini);
        $fecha_fin = str_replace("-","",$fecha_fin);

        switch ($tipo_consulta) {

            case 'detallada':

                    $data = \DB::connection('sqlsrv')
                                        ->table('v_Ico_InformeVentasPorFecha as i')
                                            ->select(DB::raw("RTRIM(i.codigo_grupo) AS codigo_grupo"), DB::raw("RTRIM(i.grupo) AS grupo"), DB::raw("case when codigo_grupo = 'G018' then CAST(sum(cantidad)/12 AS NUMERIC(17,2)) else sum(cantidad) end as cantidad"), DB::raw("CAST(sum((peso_neto)/$unidad_medida) AS NUMERIC(17,2)) as cantidad_bultos"))
                                            ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                            ->whereRaw("(descripcio not like '%mogolla%' or descripcio  not like '%salvado%' or descripcio  not like '%polipr%'  or descripcio  not like '%flete%'  or descripcio  not like '%polvillo%' and descripcio not like '%trigo%')")
                                            ->whereRaw("i.PRODUCTO not in ('001', '42104', '004', '008')")
                                            //->whereRaw("i.producto not like '5%'")
                                            ->where('i.codigo_grupo', $cod_agrupa)
                                            ->where('i.vendedor', $vendedor)
                                            //->whereRaw("i.codigo_grupo not in ('G018', 'G017', 'G016')")
                                            //->whereRaw("i.vendedor not in ('0')")
                                            ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                            //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                            ->groupBy('i.codigo_grupo')
                                            ->groupBy('i.grupo')
                                            ->orderBy('i.grupo', 'ASC')
                                            ->first();
            break;      
 
            case 'total':

                    $data = \DB::connection('sqlsrv')
                                        ->table('v_Ico_InformeVentasPorFecha as i')
                                            ->select(DB::raw("case when codigo_grupo <> 'G018' then CAST(sum((peso_neto) / $unidad_medida) AS NUMERIC(17,2)) else CAST(((sum(cantidad)/12)*12) / $unidad_medida AS NUMERIC(17,2)) end as cantidad_bultos"))
                                            ->whereRaw("i.TIPODCTO not in ('f4','f5', 'n5', 'fg')")
                                            ->whereRaw("(descripcio not like '%mogolla%' or descripcio  not like '%salvado%' or descripcio  not like '%polipr%'  or descripcio  not like '%flete%'  or descripcio  not like '%polvillo%' and descripcio not like '%trigo%')")
                                            ->whereRaw("i.PRODUCTO not in ('001', '42104', '004', '008')")
                                            //->whereRaw("i.producto not like '5%'")
                                            ->whereRaw("i.codigo_grupo not in ('G014', 'G015', 'G016', 'G017', '0')")
                                            //->whereRaw("i.vendedor not in ('0')")
                                            ->whereBetween('i.fecha', [$fecha_ini, $fecha_fin])
                                            ->where('i.vendedor', $vendedor)
                                            //->whereRaw("BETWEEN '".$fecha_ini."' and '".$fecha_fin."'")
                                            ->groupBy('i.codigo_grupo')
                                            ->get();
            break;

            case 'fn_sql':

                        $data = \DB::connection('sqlsrv')
                                                ->select("select * from Fn_Ico_InformeVentasPorVendedor(?, ?, ?, ?)", [$fecha_ini, $fecha_fin, $vendedor, $unidad_medida]);
                                                //->select("select * from Fn_Ico_InformeVentasPorVendedor(?, ?, ?)", [$fecha_ini, $fecha_fin, $vendedor]);

            break;
        }

        return $data;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PresupuestoVendedores  $presupuestoVendedores
     * @return \Illuminate\Http\Response
     */
    public function edit(PresupuestoVendedores $presupuestoVendedores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PresupuestoVendedores  $presupuestoVendedores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PresupuestoVendedores $presupuestoVendedores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PresupuestoVendedores  $presupuestoVendedores
     * @return \Illuminate\Http\Response
     */
    public function destroy(PresupuestoVendedores $presupuestoVendedores)
    {
        //
    }
}
