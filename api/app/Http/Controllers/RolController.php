<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use  JWTAuth;

class RolController extends Controller
{
    public function store(Request $request)
    {
        $tipo_ingreso = $request->input('tipo_ingreso');

        if($tipo_ingreso == 'Rol'){

            $valida_rol = Role::where('name', strtoupper($request->input('nombre')))->first();
            if(!$valida_rol){
                $rol = new Role();
                $rol->name =  strtoupper($request->input('nombre'));
                $rol->guard_name = 'web';
                $saved = $rol->save();
        
                if($saved === true){
                    return response()->json([
                        "mensaje" => "registro_exitoso",
                        "transaccion" => $rol->name,
                        "guardo" => $saved
                    ],200);
                }
                else{
                    return response()->json([
                        "mensaje" => "registro_no_exitoso"
                    ],500);                
                }
            }else{
                return response()->json([
                    "mensaje" => "doble_ingreso"
                ],200);               
            }
        }
        else{
            $valida_per = Permission::where('name', strtoupper($request->input('nombre')))->first();
            if(!$valida_per){
                $permiso = new Permission();
                $permiso->name =  strtoupper($request->input('nombre'));
                $permiso->guard_name = 'web';
                $saved = $permiso->save();
        
                if($saved === true){
                    return response()->json([
                        "mensaje" => "registro_exitoso",
                        "transaccion" => $permiso->name,
                        "guardo" => $saved
                    ],200);
                }
                else{
                    return response()->json([
                        "mensaje" => "registro_no_exitoso"
                    ],500);                
                }
            }else{
                return response()->json([
                    "mensaje" => "doble_ingreso"
                ],200);               
            }
        }


    }
    public function index($tipo = ''){
        if($tipo == 'Rol'){
            $datos = Role::select( 
                'id as id_','name as nombre'
            )
            ->orderBy('nombre', 'ASC')->get();
        }
        else{
            $datos = Permission::select( 
                'id as id_','name as nombre'
            )
            ->orderBy('nombre', 'ASC')->get();
        }
        return $datos;
    }

    public function listarPermisos(Request $request){
        $datos = Permission::select( 'id as id_','name as nombre')->orderBy('nombre', 'ASC')->get();
        $rol = Role::find($request->input('id_reg'));
        $permisos_rol = $rol->permissions()->get();

        for($i=0;$i<count($datos);$i++){
            $datos[$i]['asignado'] = false;
            for($j=0;$j<count($permisos_rol);$j++){
                if($datos[$i]['nombre']==$permisos_rol[$j]['name']){
                    $datos[$i]['asignado'] = true;
                    break;
                }
            }           
        }
        return $datos;
    }

    public function update(Request $request, $id_registro){
        $tipo_ingreso = $request->input('tipo_ingreso');
        if($tipo_ingreso == 'Rol'){

            $valida_rol = Role::where('name', strtoupper($request->input('nombre')))->first();

            if(!$valida_rol){

                $rol = Role::find($id_registro);
                $rol->name = strtoupper($request->input('nombre'));
                $saved = $rol->save();
        
                if($saved === true){
                    return response()->json([
                        "mensaje" => "registro_exitoso",
                        "transaccion" => $rol->name,
                        "guardo" => $saved
                    ],200);
                }
                else{
                    return response()->json([
                        "mensaje" => "registro_no_exitoso"
                    ],500);                
                }
            }else{
                return response()->json([
                    "mensaje" => "doble_ingreso"
                ],200);               
            }
        }
        else{
            $valida_per = Permission::where('name', strtoupper($request->input('nombre')))->first();

            if(!$valida_per){
                $permiso = Permission::find($id_registro);
                $permiso->name = strtoupper($request->input('nombre'));
                $saved = $permiso->save();
        
                if($saved === true){
                    return response()->json([
                        "mensaje" => "registro_exitoso",
                        "transaccion" => $permiso->name,
                        "guardo" => $saved
                    ],200);
                }
                else{
                    return response()->json([
                        "mensaje" => "registro_no_exitoso"
                    ],500);                
                }
            }else{
                return response()->json([
                    "mensaje" => "doble_ingreso"
                ],200);               
            }            
        }
    }
    public function asignarPermisosRol(Request $request){
        //consulta el rol
        if($request->input('id_reg')){
            $rol = Role::find($request->input('id_reg'));
            // $permissions = $rol->permissions()->get();
            // $rol = new Role();
            $permisos = $request->input('permisos');
            $rol->syncPermissions($permisos);
            //for($i=0;$i<count($permisos);$i++){
                //$rol->revokePermissionTo($permisos[$i]);
                //$rol->syncPermissions($permisos);
                //$rol->givePermissionTo($permisos[$i]);
            //}
            return response()->json([
                "mensaje" => "permisos_asignados"
            ],200);              
        }
        else{
            return response()->json([
                "mensaje" => "falta_rol"
            ],200);              
        }

    }
    public function verificaPermiso(Request $request){
        $tiene_acceso = JWTAuth::user()->hasPermissionTo($request->input('permiso'));
        return response()->json([
            "acceso" => $tiene_acceso
        ],200); 
    }
}
