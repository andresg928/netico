<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($placa='')
    {
        //dd($placa);
        if($placa){
            $vehiculos = Vehiculo::select('id as id_veh', 'placa', 'descripcion', 'peso')->where('placa', 'like', '%' . $placa . '%')->orderBy('placa', 'ASC')->get();
        }
        else{
            $vehiculos = Vehiculo::select('id as id_veh', 'placa', 'descripcion', 'peso')->orderBy('placa', 'ASC')->get();
        }
        
        return $vehiculos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //verifica si existe la placa
        $vehiculo_ = Vehiculo::where('placa', $request->input('placa'))->first();
        if (!$vehiculo_) {
            $vehiculo = new Vehiculo();
            $vehiculo->id = Uuid::generate()->string;
            $vehiculo->placa = $request->input('placa');
            $vehiculo->descripcion = $request->input('descripcion');
            $vehiculo->peso = $request->input('peso');
            $saved = $vehiculo->save();
    
            if($saved === true){
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "id_cliente" => $vehiculo->placa,
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso"
                ],500);                
            }
        }
        else{
            return response()->json([
                "mensaje" => "existe_vehiculo"
            ],200);              
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function show(Vehiculo $vehiculo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehiculo $vehiculo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehiculo = Vehiculo::find($id);
        $vehiculo->placa = $request->input('placa');
        $vehiculo->descripcion = $request->input('descripcion');
        $vehiculo->peso = $request->input('peso');
        $saved = $vehiculo->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "modificacion_exitosa",
                "id_vehiculo" => $vehiculo->id,
                "modifico" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "modificacion_no_exitosa"
            ],500);                
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehiculo $vehiculo)
    {
        //
    }
}
