<?php

namespace App\Http\Controllers;

use App\Despachos_turno;
use App\Ficho;
use App\Despacho;
use App\Pedido;
use App\despachos_novedades;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use App\Http\Controllers\FichoController;
use App\Http\Controllers\DespachoController;
use App\Http\Controllers\PedidoController;
use Illuminate\Support\Facades\DB;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Carbon\Carbon;
use App\Events\Notificaciones;
use Illuminate\Database\QueryException;

class DespachosTurnoController extends Controller
{
    public function registrarTurno(Request $request){

        DB::beginTransaction();
        try{

                $id_ficho_old = '';
                $accion = 'add';
                $mensaje = 'Ficho asignado correctamente';
                //Verifica si el ficho está ocupado, para no dejar pasar ya que no puede suceder dicha situación
                $ficho = Ficho::where('id', $request->id_ficho)->where('disponible', 'no')->first();
                //Verifica si hay un registro activo con el ficho asignado
                $turno_ = Despachos_turno::where('id_ficho', $request->id_ficho)->where('deleted', '0')->first();

                if(count($ficho)>0 || count($turno_)>0){
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => 'El ficho no se puede asignar, ya se encuentra asignado a un despacho'
                    ],200);                
                }
                //Valido si es registro nuevo o actualización
                if(!$request->id_reportado){
                    $turno = new Despachos_turno();
                    $turno->id = Uuid::generate()->string;
                    $turno->id_despacho = $request->id_despacho;
                    $turno->fecha_hora_reporta = date("Y-m-d H:i:s");
                }else{
                    $turno = Despachos_turno::find($request->id_reportado);
                    $id_ficho_old = $turno->id_ficho;
                    $mensaje = 'Ficho reasignado correctamente';
                    $accion = 'edit';

                    // Verifico que se modifique solo cuando el turno esté en estado reportado
                    if($turno->estado !== 'reportado'){
                        DB::commit();
                        return response()->json([
                            "estado" => 'error',
                            "mensaje" => 'No puede gestionar el turno por que se encuentra en estado ('.$turno->estado.')'
                        ],200);      
                    }
                }

                $turno->id_ficho = $request->id_ficho;
                $turno->celular = $request->celular;
                $turno->ciudad = $request->ciudad;
                $turno->usuario_reporta = JWTAuth::user()->vendedor;
                $saved = $turno->save();

                if($saved === true){
                    // Cambia a estado no disponible el ficho asignado a este turno
                    $ctl_ficho = new FichoController();
                    $ctl_ficho->cambiarEstado($request->id_ficho, $id_ficho_old, $accion);

                    // Consulta el despacho para obtener la placa
                    $despacho = Despacho::select('placa')->where('id', $turno->id_despacho)->first();

                    //Consulta info del ficho
                    $ficho_ = Ficho::select('nombre')->where('id', $request->id_ficho)->first();
                    $data = array();
                    $data['fecha_hora'] = $turno->fecha_hora_reporta;
                    $data['ficho'] = $ficho_['nombre'];
                    $data['placa'] = $despacho['placa'];
                    $this->imprimirTicket($data);

                    // Dispara el evento de notificación
                    $data['accion'] = $accion;
                    $data['placa'] = $despacho['placa'];
                    $data['modulo_notifica'] = 'gestion_turnos';
                    $data['mensaje'] = '¡Atención! vehículo con placa No. '.$data['placa'].' reportado';
                    $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));
                    if($rpta === true){
                        event(new Notificaciones($data));
                    }

                    // Notifica a calidad que se requiere certificado, esto sucede al reportar ingreso del vehículo
                    if($request->cert_calidad == 'si' && $accion == 'add'){
                        $ctl_ped = new PedidoController();
                        $data_ = array();
                        $data_['id_despacho'] = $turno->id_despacho;
                        $inf_ped = Pedido::select('observaciones_certificado')->where('id_despacho', $turno->id_despacho)->where('deleted', '0')->where('certificado_calidad', 'si')->first();
                        $data_['observaciones_certificado'] = $inf_ped['observaciones_certificado'];
                        $data_['asunto'] = '¡Recordatorio! Solicitud de certificado de calidad';
                        $ctl_ped->notificaCertificadoCalidad($data_);
                    }

                    DB::commit();
                    return response()->json([
                        "estado" => 'ok',
                        "mensaje" => $mensaje
                    ],200);

                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => 'Ha ocurrido un error al asingar el ficho, vuelva a intentarlo'
                    ],200);            
                }
        } catch(\Exception $e){

            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }

    public function checkDatabaseConnection($connectionName = null) {
        try {
            if ($connectionName) {
                DB::connection($connectionName)->getPdo();
                return true;
            } else {
                DB::connection()->getPdo();
                return true;
            }
        } catch (QueryException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function obtenerInfoBascula($placa, $fechaReportado){

        // Validar conexión secundaria
        if ($this->checkDatabaseConnection('mysql_bascula')) {

            // Primeros 10 caracteres para que tome solo la fecha y NO la hora
            $fecha_inicial = substr($fechaReportado, 0, 10);
            $fecha_final = Carbon::parse($fecha_inicial)->addDay()->toDateString();
            $pesaje = DB::connection('mysql_bascula')
                            ->table('pesajes')
                                ->select('estado', 'created_at as fec_pes_ini', 'updated_at as fec_pes_fin')
                                ->where('tipo', 'producto_terminado')
                                ->where('placa', $placa)
                                ->where('deleted', '0')
                                ->whereBetween('fecha', [$fecha_inicial, $fecha_final])
                                ->oldest('created_at')
                                ->first();

            return $pesaje;

        } else {
            return 'error_conexion';
        }

    }

    public function anularTurno(Request $request){

        DB::beginTransaction();

        try{

            $turno = Despachos_turno::find($request->id_reportado);

            if($turno->estado !== 'reportado'){
                DB::commit();
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => 'No se puede anular el turno ya que se encuentra en estado : '.$turno->estado
                ],200);     
            }

            $turno->deleted = '1';
            $turno->fecha_hora_anula = date("Y-m-d H:i:s");
            $turno->usuario_anula = JWTAuth::user()->vendedor;
            $saved = $turno->save();

            if($saved === true){
                // Cambia a estado no disponible el ficho asignado a este turno
                $ctl_ficho = new FichoController();
                $ctl_ficho->cambiarEstado($request->id_ficho, '', 'deleted');

                DB::commit();
                return response()->json([
                    "estado" => 'ok',
                    "mensaje" => 'El turno se ha anulado correctamente'
                ],200);

            } else {
                DB::commit();
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => 'Ha ocurrido un error al anular el turno, vuelva a intentarlo'
                ],200);            
            }
        }
        catch(\Exception $e){

            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }

    public function obtenerTurnosPorEstado($estado){

        $turnos = Despachos_turno::where('deleted', '0')->get()->toArray();

        $turnos_aux = array();

        foreach($turnos  as $val) {
            $turnos_aux[] = $val['id_despacho'];
        }
        return $turnos_aux;

    }

    public function convertirEstado($estado){
        switch ($estado) {
            case 'reportado':
                return 'Reportado';
            break;
            case 'en_espera':
                return 'En espera';
            break;
            case 'en_cargue':
                return 'En Cargue';
            break;
            case 'cargado':
                return 'Cargado';
            case 'finalizado':
                return 'Finalizado';
            break;
        }
    }

    public function reimprimirTicket(Request $request){

        $data['ficho'] = $request->ficho;
        $data['placa'] = $request->placa;
        $data['fecha_hora'] = $request->fecha_hora_reporta;
        $this->imprimirTicket($data);

    }

    public function imprimirTicket($data = array()){
        // $nombreImpresora = "POS-80C";
        //$connector = new WindowsPrintConnector(config('global.nombre_impresora_turnos'));
        if(config('global.imprimir_ficho') == true){
            $connector = new NetworkPrintConnector("192.168.0.14", 9100);
            $printer = new Printer($connector);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("ICOHARINAS SAS"."\n\n\n\n");
            $printer->setEmphasis(false);
            $printer->setTextSize(3, 3);
            $printer->text("Turno: ".$data['ficho']."\n\n");
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->setTextSize(2, 2);
            $printer->text('Placa: '.$data['placa']."\n\n\n");
            $printer->setEmphasis(false);
            $printer->setTextSize(1, 1);
            $printer->text($data['fecha_hora']."\n\n");
    
            $printer->setEmphasis(true);
    
            $printer->feed();
            $printer->cut();
            $printer->close();
        }
    }

    public function obtenerReportados(Request $request){

        $t='despachos_turnos.';

        // Turnos cargados - finalizados
        $turnos_cargados = Despachos_turno::select($t.'id AS id_registro', $t.'estado','f.nombre AS ficho','f.id AS id_ficho',$t.'id_despacho',$t.'ciudad',
                                                        'd.placa', 'd.conductor','d.consecutivo','d.created_at AS fecha','d.nit_conductor','d.vendedor','d.capacidad','d.autorizado',
                                                        $t.'celular', $t.'fecha_hora_reporta', $t.'fecha_hora_cargue',$t.'usuario_cargue',$t.'fecha_hora_en_espera',$t.'fecha_hora_salida',
                                                        $t.'usuario_autoriza_entrada',$t.'fecha_hora_autoriza_entrada','d.cargado'
                                                        )
                                                ->where($t.'deleted', '1')
                                                ->whereIn($t.'estado' ,['cargado','finalizado'])
                                                ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                                ->join('despachos AS d','d.id','=', $t.'id_despacho');

                                                if($request->fecha_inicial !== '2000-01-01' && $request->fecha_final !=='2000-01-01'){
                                                    $turnos_cargados->whereRaw("DATE_FORMAT(despachos_turnos.fecha_hora_reporta ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicial."' and '".$request->fecha_final."'");
                                                }

                                                if($request->tipo_ficho == 'industrial'){
                                                    $turnos_cargados->where('f.nombre', 'like', 'B%');
                                               }else if($request->tipo_ficho == 'subproducto'){
                                                    $turnos_cargados->where('f.nombre', 'like', 'S%');
                                               }else if($request->tipo_ficho == 'paqueteo'){
                                                    $turnos_cargados->where('f.nombre', 'like', 'P%');
                                               }else if($request->tipo_ficho == 'contado'){
                                                    $turnos_cargados->where('f.nombre', 'like', 'C%');
                                               }
        // -----------

        $reportados = Despachos_turno::select($t.'id AS id_registro', $t.'estado','f.nombre AS ficho','f.id AS id_ficho',$t.'id_despacho',$t.'ciudad',
                                                'd.placa', 'd.conductor','d.consecutivo','d.created_at AS fecha','d.nit_conductor','d.vendedor','d.capacidad','d.autorizado',
                                                $t.'celular', $t.'fecha_hora_reporta', $t.'fecha_hora_cargue',$t.'usuario_cargue',$t.'fecha_hora_en_espera',$t.'fecha_hora_salida',
                                                $t.'usuario_autoriza_entrada',$t.'fecha_hora_autoriza_entrada','d.cargado'
                                              )
                                       ->where($t.'deleted', '0')
                                       //->where($t.'estado', $request->estado)
                                       ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                       ->join('despachos AS d','d.id','=', $t.'id_despacho');

                                       if($request->fecha_inicial !== '2000-01-01' && $request->fecha_final !=='2000-01-01'){
                                            $reportados->union($turnos_cargados);
                                            $reportados->whereRaw("DATE_FORMAT(despachos_turnos.fecha_hora_reporta ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicial."' and '".$request->fecha_final."'");
                                        }

                                       // Organizar turnos según el estado
                                       if($request->estado){

                                            $reportados->where($t.'estado', $request->estado);
                                            if($request->estado == 'reportado'){
                                                $reportados->orderBy('fecha_hora_reporta');
                                            }else if($request->estado == 'en_espera'){
                                                $reportados->orderBy('fecha_hora_en_espera');
                                            }else if($request->estado == 'en_cargue'){
                                                $reportados->orderBy('fecha_hora_cargue');
                                            }

                                       }else{
                                            $reportados->orderByRaw("DATE_FORMAT(fecha_hora_reporta ,'%Y-%m-%d %H:%i:%s') DESC");
                                       }

                                       if($request->tipo_ficho == 'industrial'){
                                            $reportados->where('f.nombre', 'like', 'B%');
                                       }else if($request->tipo_ficho == 'subproducto'){
                                            $reportados->where('f.nombre', 'like', 'S%');
                                       }else if($request->tipo_ficho == 'paqueteo'){
                                            $reportados->where('f.nombre', 'like', 'P%');
                                       }else if($request->tipo_ficho == 'contado'){
                                            $reportados->where('f.nombre', 'like', 'C%');
                                       }

        $reportados = $reportados->get();

        $cont_insutrial = 0;
        $cont_paqueteo = 0;
        $cont_subproducto = 0;
        $cont_contado = 0;

        //Obtiene los ids de despacho que tienen certificados de calidad
        $data = array();
        $data['fecha_inicial'] = $request->fecha_inicial;
        $data['fecha_final'] = $request->fecha_final;
        $data['id_despacho'] = '';
        $ctl_desp = new DespachoController();
        $despachos_certificado = $ctl_desp->obtenerDespachosConCertificado($data);

        for ($i=0; $i <count($reportados) ; $i++) {

            // Valida si el despacho requiere certificado de calidad
            if(in_array($reportados[$i]['id_despacho'], $despachos_certificado)){
                $reportados[$i]['cert_calidad'] = 'si';
            }else{
                $reportados[$i]['cert_calidad'] = 'no';
            }

            //Obtener info de bascula para saber en que estado de pesaje esta (peso_inicial  o repesado)
            $reportados[$i]['estado_bascula'] = '';
            $inf_bascula = $this->obtenerInfoBascula($reportados[$i]['placa'],$reportados[$i]['fecha_hora_reporta']);
            if($inf_bascula != 'error_conexion'){
                if(count($inf_bascula)>0){
                    $inf_bascula->estado == 'peso_1' ? $reportados[$i]['estado_bascula'] = 'PI' : $reportados[$i]['estado_bascula'] = 'PF';
                }
            }


            $reportados[$i]['alerta'] = false;

            //Se extrae la primera letra para saber que tipo de turno es
            $turno = substr($reportados[$i]['ficho'], 0, 1);
            // Para obtener la posición por tipo de turno
            if($turno == 'B'){
                $cont_insutrial++;
                $reportados[$i]['posicion'] = $cont_insutrial;
            } else if($turno == 'S'){
                $cont_subproducto++;
                $reportados[$i]['posicion'] = $cont_subproducto;
            } else if($turno == 'P'){
                $cont_paqueteo++;
                $reportados[$i]['posicion'] = $cont_paqueteo;
            } else if($turno == 'C'){
                $cont_contado++;
                $reportados[$i]['posicion'] = $cont_contado;
            }

            if($reportados[$i]['estado'] == 'reportado'){

                $reportados[$i]['tiempo'] = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_reporta']);
                $tiempo = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_reporta'], 'numero');
                if($tiempo >= config('global.no_minutos_reportado')){
                    $reportados[$i]['alerta'] = true;
                }

            }else if($reportados[$i]['estado'] == 'en_espera'){
                $reportados[$i]['tiempo'] = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_en_espera']);
                $tiempo = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_en_espera'], 'numero');
                if($tiempo >= config('global.no_minutos_en_espera')){
                    $reportados[$i]['alerta'] = true;
                }
            }else if ($reportados[$i]['estado'] == 'en_cargue'){
                $reportados[$i]['tiempo'] = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_cargue']);
                $tiempo = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_cargue'], 'numero');
                if($tiempo >= config('global.no_minutos_cargue')){
                    $reportados[$i]['alerta'] = true;
                }
            }

            $reportados[$i]['estado_label'] = $this->convertirEstado($reportados[$i]['estado']);

            // Consulta que el está autorizado el vehículo para ingreso
            $reportados[$i]['autorizado_ingreso'] = 'no';
            if($reportados[$i]['usuario_autoriza_entrada']){
                $reportados[$i]['autorizado_ingreso'] = 'si';
            }

            // Tiempo desde que se reportó
            $reportados[$i]['tiempo_reporto'] = $this->obtenerTiempoTranscurrido($reportados[$i]['fecha_hora_reporta']);
        }

        return $reportados;
    }

    public function obtenerVehiculosCargados(Request $request){

        $t='despachos_turnos.';

        // Turnos cargados - finalizados
        $turnos_cargados = Despachos_turno::select($t.'id AS id_registro', $t.'estado','f.nombre AS ficho','f.id AS id_ficho',$t.'id_despacho',$t.'ciudad',
                                                        'd.placa', 'd.conductor','d.consecutivo','d.created_at AS fecha','d.nit_conductor','d.vendedor','d.capacidad','d.autorizado',
                                                        $t.'celular', $t.'fecha_hora_reporta', $t.'fecha_hora_cargue',$t.'usuario_cargue',$t.'fecha_hora_en_espera',$t.'fecha_hora_salida'
                                                        )
                                                ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                                ->join('despachos AS d','d.id','=', $t.'id_despacho')
                                                ->where($t.'deleted', '1')
                                                ->whereIn($t.'estado' ,['cargado','finalizado'])
                                                ->whereRaw("DATE_FORMAT(d.fec_reg_cargado ,'%Y-%m-%d') BETWEEN '".date('Y-m-d')."' and '".date('Y-m-d')."'")
                                                ->orderByRaw("DATE_FORMAT(d.fec_reg_cargado ,'%Y-%m-%d %H:%i:%s') DESC")
                                                ->get();

        return $turnos_cargados;
    }

    public function obtenerTiempoTranscurrido($fecHor, $tipoVista = ''){

        $updated = new Carbon($fecHor);
        $now = Carbon::now();
        $transcurrido = $updated->diffInMinutes($now);
       
        //Retorna solo el numero de minutos
        if($tipoVista == 'numero'){
            return  $transcurrido;
        }

        if ($transcurrido >=0 && $transcurrido <=3){
            $mensaje = 'Hace un momento';
        } else if($transcurrido > 3 && $transcurrido <= 60){
            $mensaje = 'Hace '.$transcurrido. ' minutos';
        } else if ($transcurrido > 60 && $transcurrido <= 1440){
            $mensaje = 'Hace '.round($transcurrido/60). ' horas';
            if (round($transcurrido/60)==1){
                $mensaje = 'Hace '.round($transcurrido/60). ' hora';
            }
        } else if ($transcurrido > 1440 && $transcurrido <= 11520) {
            $mensaje = 'Hace '.round(($transcurrido/60)/24). ' dias';
        } else if ($transcurrido > 11520 && $transcurrido <= 46080 ) {
            $mensaje = 'Hace '.round((($transcurrido/60)/24)/8). ' semanas';
            if (round((($transcurrido/60)/24)/8) == 1){
                $mensaje = 'Hace '.round((($transcurrido/60)/24)/8). ' semana';
            }
        } else if ($transcurrido > 46080) {
            $mensaje = 'Hace más de '.round(((($transcurrido/60)/24)/8)/4). ' meses';
        } else {
            $mensaje = $fecha;
        }

        return $mensaje;

    }

    public function obtenerCiudades(){
        $ciudades = config('global.ciudades');
        return $ciudades;
    }

    public function checkPuerto($dominio, $puerto){
        $starttime = microtime(true);
        $file      = @fsockopen ($dominio, $puerto, $errno, $errstr, 10);
        $stoptime  = microtime(true);
        $status    = 0;
      
        if (!$file){    
            $status = -1;  // Sitio caído
        } else {
            fclose($file);
            $status = ($stoptime - $starttime) * 1000;
            $status = floor($status);
        }
         
        if ($status <> -1) {
            return true;
        } else {
            return false;
        }
    }

    public function actualizarEstadoTurno(Request $request){

        DB::beginTransaction();

        try{
                $turno = Despachos_turno::find($request->id_reportado);
                $estado_old = $turno->estado;
                // No se cambie el estado cuando el registro está gestionado o eliminado
                if($turno->deleted == 1 && $request->estado !== 'finalizado'){
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => 'La gestión del turno se encuentra en estado ('.$estado_old.'), por ende no se puede gestionar.'
                    ],200);
                }
                // Cuando se autorice el ingreso no cambia el estado del turno, solo llena campos informativos para notificar a recepción
                if($request->estado !== 'autorizar_ingreso'){
                    $turno->estado = $request->estado;
                }

                // Darle salida al vehiculo lo haran varias personas, se valida que el turno esté en estado 'cargado' para realizar la gestión
                if($estado_old == 'finalizado'){
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => 'La gestión del turno se encuentra en estado ('.$estado_old.')'
                    ],200);
                }

                /*if($request->estado == 'reportado'){
                    $turno->usuario_reporta = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_reporta = date("Y-m-d h:i:s");
        }else */if($request->estado == 'en_cargue'){
                    $turno->usuario_cargue = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_cargue = date("Y-m-d H:i:s");
                }else if($request->estado == 'en_espera'){
                    $turno->usuario_en_espera = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_en_espera = date("Y-m-d H:i:s");            
                }else if($request->estado == 'eliminar'){
                    $turno->usuario_elimina = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_elimina = date("Y-m-d H:i:s");
                    $turno->deleted = '1';
                }else if($request->estado == 'finalizado'){
                    $turno->usuario_salida = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_salida = date("Y-m-d H:i:s");
                }else if($request->estado == 'autorizar_ingreso'){
                    $turno->usuario_autoriza_entrada = JWTAuth::user()->vendedor;
                    $turno->fecha_hora_autoriza_entrada = date("Y-m-d H:i:s");   
                }

                $saved = $turno->save();

                if($saved === true){

                    // Dispara el evento de notificación
                    $data['accion'] = 'actualiza_estado_turno';
                    $data['placa'] = '';
                    $data['modulo_notifica'] = 'gestion_turnos';

                    if($request->estado == 'autorizar_ingreso'){
                        $data['accion'] = 'autorizar_ingreso';
                        $data['mensaje'] = 'Vehículo AUTORIZADO para ingresar. ('.$request->placa.')';
                    }else{
                        $data['mensaje'] = 'Información actualizada ...';
                    }

                    $rpta = $this->checkPuerto(config('global.ip_websocket'), config('global.port_websocket'));
                    // Notifica siempre, menos cuando recepción le de salida al vehículo
                    if($rpta === true && $request->estado != 'finalizado'){
                        event(new Notificaciones($data));
                    }
                    if($request->estado == 'eliminar'){
                        // Cambia a estado no disponible el ficho asignado a este turno
                        $ctl_ficho = new FichoController();
                        $ctl_ficho->cambiarEstado($turno->id_ficho, '', 'deleted');
                    }

                    DB::commit();
                    return response()->json([
                        "estado" => 'ok',
                        "mensaje" => 'La operación se realizó correctamente'
                    ],200);
                } else {
                    DB::commit();
                    return response()->json([
                        "estado" => 'error',
                        "mensaje" => 'Ha ocurrido un error al procesar la solicitud, por favor vuelva a intentarlo'
                    ],200);            
                }
            }
            catch(\Exception $e){
    
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "estado" => 'error'
                ],200);               
            }
    }

    public function consultaTurnoVistaConductores(Request $request){

        $t='despachos_turnos.';
        $data = Despachos_turno::select($t.'id as id_turno',$t.'id_despacho','f.nombre', 'd.placa', $t.'estado',$t.'fecha_hora_reporta', $t.'ciudad', 'd.conductor')
                                ->where('d.nit_conductor', $request->documento_identidad)
                                ->where('d.placa', $request->placa)
                                ->where('celular', $request->celular)
                                ->whereIn($t.'estado', ['cargado','finalizado'])
                                ->whereRaw("despachos_turnos.id NOT IN (SELECT id_turno FROM despachos_novedades WHERE deleted='0')")
                                ->join('despachos AS d','d.id','=', $t.'id_despacho')
                                ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                ->latest($t.'created_at')
                                ->first();

        if(count($data)>0){
            $data['estado_label'] = $this->convertirEstado($data['estado']);
        }

        return $data;
    }

    public function obtenerTurnosPendientes(Request $request){

        $t='despachos_turnos.';

        $turno = substr($request->filtro_ficho, 0, 1);

        $reportados = Despachos_turno::select($t.'id AS id_registro', $t.'estado','f.nombre AS ficho', $t.'fecha_hora_reporta', 
                                              $t.'fecha_hora_cargue',$t.'usuario_cargue',$t.'fecha_hora_en_espera',$t.'fecha_hora_salida')
                                       ->where($t.'deleted', '0')
                                       //->whereNotIn('f.nombre', [$request->filtro_ficho])
                                       ->where('f.nombre', 'like', $turno.'%')
                                       ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                       ->join('despachos AS d','d.id','=', $t.'id_despacho');
                                       $reportados->orderBy('fecha_hora_reporta');

        $reportados = $reportados->get();

        $cont_insutrial = 0;
        $cont_paqueteo = 0;
        $cont_subproducto = 0;
        $cont_contado = 0;

        for ($i=0; $i <count($reportados) ; $i++) {

            $reportados[$i]['me'] = false;
            if($reportados[$i]['ficho'] == $request->filtro_ficho){
                $reportados[$i]['me'] = true;
            }

            //Se extrae la primera letra para saber que tipo de turno es
            $turno = substr($reportados[$i]['ficho'], 0, 1);
            // Para obtener la posición por tipo de turno
            if($turno == 'B'){
                $cont_insutrial++;
                $reportados[$i]['posicion'] = $cont_insutrial;
            } else if($turno == 'S'){
                $cont_subproducto++;
                $reportados[$i]['posicion'] = $cont_subproducto;
            } else if($turno == 'P'){
                $cont_paqueteo++;
                $reportados[$i]['posicion'] = $cont_paqueteo;
            } else if($turno == 'C'){
                $cont_contado++;
                $reportados[$i]['posicion'] = $cont_contado;
            }

            $reportados[$i]['estado_label'] = $this->convertirEstado($reportados[$i]['estado']);
        }

        return $reportados;
    }

    public function obtenerNombreFicho($ficho) {

        $primerCaracter = substr($ficho, 0, 1);

        if ($primerCaracter == 'C') {
          return 'contado';
        } else if ($primerCaracter == 'B') {
          return 'industrial';
        } else if ($primerCaracter == 'S') {
          return 'subproducto';
        } else if ($primerCaracter == 'P') {
          return 'paqueteo';
        }
      }

    public function obtenerTrazaDespacho(Request $request){

        $t="despachos.";
        $datos = Despacho::select($t.'fec_cerrado as Pedido_cerrado', $t.'fec_autorizado as Autorizado','t.fecha_hora_reporta as Reportado','t.fecha_hora_autoriza_entrada as Ingreso',
                                't.fecha_hora_finaliza as Cargado', 't.fecha_hora_salida as Salida','n.fecha_novedad as Entregado', $t.'placa')
                           ->leftJoin('despachos_turnos as t', function($join) {
                                $join->on('t.id_despacho' , '=', 'despachos.id')
                                     ->whereNull('t.usuario_anula')
                                     ->whereRaw("(t.estado not in ('eliminar') or t.estado is null)");
                            })
                           ->leftJoin('despachos_novedades as n', function($join) {
                               $join->on('n.id_turno' , '=', 't.id')
                                    ->where('n.deleted', '=', '0');
                           })
                          ->where([
                                [$t.'estado', '=', 'cerrado'],
                                [$t.'deleted', '=', '0'],
                                [$t.'id', '=', $request->id_despacho]
                            ])
                          ->get();

        $transformedData = [];

        foreach ($datos as $info) {

            $peso_inicial = null;
            $peso_final = null;
            // para poder tener info cercana de bascula, debe haber reportado el carro
            if($info->Reportado){
                // Informacion de basula
                //Obtener info de bascula para saber en que estado de pesaje esta (peso_inicial  o repesado)
                $inf_bascula = $this->obtenerInfoBascula($info->placa, $info->Reportado);
                if($inf_bascula != 'error_conexion'){
                    if(count($inf_bascula)>0){
                        if($inf_bascula->estado == 'peso_1'){
                            $peso_inicial = $inf_bascula->fec_pes_ini;
                        }else{
                            $peso_inicial = $inf_bascula->fec_pes_ini;
                            $peso_final = $inf_bascula->fec_pes_fin;
                        }
                    }
                }
            }

            $transformedData[] = ['label' => 'Pedido cerrado', 'registro' => $info->Pedido_cerrado];
            $transformedData[] = ['label' => 'Autorizado', 'registro' => $info->Autorizado];
            $transformedData[] = ['label' => 'Reportado (turno)', 'registro' => $info->Reportado];
            $transformedData[] = ['label' => 'Ingreso', 'registro' => $info->Ingreso];
            $transformedData[] = ['label' => 'Peso inicial', 'registro' => $peso_inicial];
            $transformedData[] = ['label' => 'Cargado', 'registro' => $info->Cargado];
            $transformedData[] = ['label' => 'Peso final', 'registro' => $peso_final];
            $transformedData[] = ['label' => 'Salida', 'registro' => $info->Salida];
            $transformedData[] = ['label' => 'Entregado', 'registro' => $info->Entregado];
        }

        $despacho = [];
        $turno = [];
        $novedad = [];
        // Turno
        $t="despachos_turnos.";
        $turno = Despachos_turno::select($t.'id as id_registro', $t.'ciudad','f.nombre AS ficho', $t.'estado', $t.'celular')
                                       ->whereNull($t.'usuario_anula')
                                       ->where($t.'id_despacho', $request->id_despacho)
                                       ->join('fichos AS f','f.id','=', $t.'id_ficho')
                                       ->latest($t.'created_at')
                                       ->first();

        count($turno)>0 ? $turno['nombre_ficho'] = $this->obtenerNombreFicho($turno['ficho']) : $turno = [];

        //Novedades entrega de mercancía
        if(isset($turno['id_registro'])){
            $novedad = despachos_novedades::where('id_turno', $turno['id_registro'])->where('deleted','0')->first();
        }

        $despacho = Despacho::select('despachos.*', 'u.name as usuario', 'u.vendedor as codigo')->where('despachos.id', $request->id_despacho)->join('users as u', 'u.documento_identidad', '=', 'despachos.documento_vendedor')->first();

        //Obtiene los ids de despacho que tienen certificados de calidad
        $dat = array();
        $dat['fecha_inicial'] = '';
        $dat['fecha_final'] = '';
        $dat['id_despacho'] = $request->id_despacho;
        $ctl_desp = new DespachoController();
        $despachos_certificado = $ctl_desp->obtenerDespachosConCertificado($dat);
        count($despachos_certificado)>0 ? $despacho['certificado'] = 'si' :  $despacho['certificado'] = 'no';

        $data['trazabilidades'] = $transformedData;
        $data['despacho'] = $despacho;
        $data['turno'] = $turno;
        $data['novedad'] = $novedad;
        return $data;
    }
}
