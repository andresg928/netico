<?php
namespace  App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use  JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class  AuthController extends  Controller {
    
	public  $loginAfterSignUp = true;

	public function listadoUsuarios(){

		$usuarios = User::select('users.id as id_usu','users.deleted', 'users.name', 'users.email', 'model_has_roles.role_id','roles.name as nom_rol', 'users.vendedor', 'users.documento_identidad')
						->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
						->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
						->orderBy('users.deleted', 'ASC')
						->orderBy('name', 'ASC')->get();

		return $usuarios;
	}

	public  function  register(Request  $request) {

		$user = new  User();
		$usuario_ = User::where('email', $request->email)->where('deleted', '0')->first();

        if (!$usuario_) {
			$user->name = $request->name;
			$user->documento_identidad = $request->documento_identidad;
			$user->vendedor = $request->vendedor;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$saved = $user->save();

            if($saved === true){
				$user->assignRole($request->rol);
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso"
                ],500);                
            }
			
			/*if ($this->loginAfterSignUp) {
				return  $this->login($request);
			}*/
	
			return  response()->json([
				'status' => 'ok',
				'data' => $user
			], 200);
		} else {
            return response()->json([
                "mensaje" => "existe_usuario"
            ],200);  
		}

	}

	public function actualizaUsuario(Request $request, $id){

		$usuario = User::find($id);
		$usuario->name = $request->name;
		$usuario->email = $request->email;
		$usuario->documento_identidad = $request->documento_identidad;
		$usuario->vendedor = $request->vendedor;
		// $usuario->password = bcrypt($request->password);

		if($request->desactivar === true){
			$usuario->deleted = '1';
			// Invalidar el token JWT
			if ($usuario->remember_token) {
				JWTAuth::setToken($usuario->remember_token)->invalidate();
				$usuario->remember_token = null;
			}
		}
		else {
			$usuario->deleted = '0';
		}

		$saved = $usuario->save();
		if($saved === true){
			$usuario->syncRoles([]);
			$usuario->assignRole($request->rol);
			return response()->json([
				"mensaje" => "actualizo_exitoso",
				"actualizo" => $saved
			],200);
		}
		else{
			return response()->json([
				"mensaje" => "modificacion_no_exitosa"
			],500);                
		}

	}

	public  function  login(Request  $request) {
		$input = $request->only('documento_identidad', 'password');
		$jwt_token = null;
		if (!$jwt_token = JWTAuth::attempt($input)) {
			return  response()->json([
				'status' => 'invalid_credentials',
				'message' => 'Correo o contraseña no válidos.',
			], 200);
		}

		return  response()->json([
			'status' => 'ok',
			'token' => $jwt_token,
		]);
	}

	public  function  inicioSesion(Request  $request) {
		sleep(2);
		$usuario = User::where('documento_identidad', $request->documento_identidad)->where('deleted', '0')->first();

		if($usuario){
			//$input = $request->only('email', 'password');
			$input = $request->only('documento_identidad', 'password');
			$jwt_token = null;
			
			if (!$jwt_token = JWTAuth::attempt($input)) {
				return  response()->json([
					'status' => 'error',
					'message' => 'Usuario NO autorizado para ingresar a netico, por favor revisar las credenciales de acceso.',
				], 200);
			}

			$permisos = $this->obtenerRolUsuario($request->documento_identidad);

			// Guardar el token en la base de datos
			$user = JWTAuth::user();
			$user->remember_token = $jwt_token;
			$user->save();

			return  response()->json([
				'status' => 'ok',
				'usuario' => $usuario,
				'permisos' => $permisos,
				'token' => $jwt_token,
			]);
		} else {
			return  response()->json([
				'status' => 'error',
				'usuario' => '',
				'message' => 'El usuario está inactivo o no existe, por favor contacte al soporte de la aplicación',
			], 200);			
		}
	}

	public function obtenerRolUsuario($documento){

		$usuario = User::select('users.id as id_usu', 'users.name', 'users.email', 'model_has_roles.role_id','roles.name as nom_rol', 'users.vendedor', 'users.documento_identidad')
						->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
						->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
						->where('users.documento_identidad', $documento)
						->orderBy('name', 'ASC')->first();

		return $usuario;
	}

	public  function  logout(Request  $request) {
		$this->validate($request, [
			'token' => 'required'
		]);

		try {
			JWTAuth::invalidate($request->token);
			return  response()->json([
				'status' => 'ok',
				'message' => 'Cierre de sesión exitoso.'
			]);
		} catch (JWTException  $exception) {
			return  response()->json([
				'status' => 'unknown_error',
				'message' => 'Al usuario no se le pudo cerrar la sesión.'
			], 500);
		}
	}

	public function obtenerUsuariosPorRol(Request  $request){

		$otros = User::select('users.vendedor AS codigo', 'users.name AS nombre')
						->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
						->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
						->where('users.vendedor', 'LVEGA')
						->where('users.deleted', '0');

		$usuarios = User::select('users.vendedor AS codigo', 'users.name AS nombre')
						->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
						->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
						->where('roles.name', $request->rol)
						->where('users.deleted', '0')
						->union($otros)
						->orderBy('nombre', 'ASC')->get();

		return $usuarios;
	}

	public function obtenerVendedores($codVendedor = ''){

		$vendedores = \DB::connection('sqlsrv')
								->table('VENDEN as v')
								->select(DB::raw("RTRIM(v.CODVEN) AS codigo"), DB::raw("RTRIM(v.NOMBRE) AS nombre"))
								->whereRaw("v.HABILITADO = '1'")
								->whereRaw("v.CODVEN not in ('008')")
								->whereRaw("v.CODVEN like '$codVendedor%'")
								//->whereRaw("v.CODVEN in ('001')")
								->orderBy('v.nombre', 'ASC')
								->get();
		return $vendedores;
	}

	public function obtenerInfoUsuarioxId($CodVend) {
		$usuario = User::select('id as id_usuario', 'name', 'email', 'vendedor')->where('deleted', '0')->where('vendedor', $CodVend)->first();
		return $usuario;
	}

	public  function  getAuthUser(Request  $request) {
		$this->validate($request, [
			'token' => 'required'
		]);

        $roles = JWTAuth::user()->getRoleNames();
        $rol = $roles[0];

		$user = JWTAuth::authenticate($request->token);
		$permisos = JWTAuth::user()->getAllPermissions();

		// Verifica si tiene foto o no
		$user->foto = 'no';
		$cedulas_vendedores = config('global.cedulas_vendedores');
        if(in_array($user->documento_identidad, $cedulas_vendedores)){
			$user->foto = $user->documento_identidad.'.png';
        }

		return  response()->json(['user' => $user, 'permisos' => $permisos, 'rol'=>$rol]);
	}

	public function validatePermissionID($idPer){

        $permisos = JWTAuth::user()->getAllPermissions();
        $existe = false;
        foreach ( $permisos as $permiso) {
            if( $permiso->id == $idPer) {
                $existe = true;
                continue;
            }
        }
		return $existe;
	}

	public function actualizarClave(Request $request){

		if(JWTAuth::user()->vendedor){

			$usuario = User::select('id as id_usuario', 'password')->where('documento_identidad', JWTAuth::user()->documento_identidad)->where('deleted', '0')->first();

			$clave_verificada = 0;
			if(Hash::check( $request->password_old, $usuario['password'] ) ){
				$clave_verificada = 1;
			}

			if($clave_verificada == 0){
				return  response()->json([
					'status' => 'error',
					'message' => 'La contraseña anterior no coincide',
				], 200);				
			}

			$user = User::find($usuario['id_usuario']);
			$user->password = bcrypt($request->password);
			$saved = $user->save();

			if($saved === true){
				return  response()->json([
					'status' => 'ok',
					'message' => 'La contraseña se ha cambiado exitosamente',
				], 200);
			} else {
				return  response()->json([
					'status' => 'error',
					'message' => 'Hubo un error al cambiar la contraseña',
				], 200);
			}			
		} else {
			return  response()->json([
				'status' => 'error',
				'message' => 'Falta información para actualizar la información',
			], 200);			
		}
	}
    public function obtenerPersonalActivoOfima(Request $request){

        $personal = \DB::connection('sqlsrv')
                                    ->table('MTEMPLEA')
                                        ->select('CEDULA',DB::raw("RTRIM(CARGO) as CARGO"),DB::raw("CONCAT( RTRIM(APELLIDO),' ',RTRIM(APELLIDO2),' ',RTRIM(NOMBRE),' ',RTRIM(NOMBRE2)) as NOMBRE_FULL"))
                                        ->where('ACTIVO', '0')
										->whereRaw("CONCAT( RTRIM(APELLIDO),' ',RTRIM(APELLIDO2),' ',RTRIM(NOMBRE),' ',RTRIM(NOMBRE2)) like '%$request->filtro%'")
										->orderBy('NOMBRE_FULL')
                                        ->get();
		return $personal;
    }
}
