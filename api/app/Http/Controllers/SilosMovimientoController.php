<?php

namespace App\Http\Controllers;

use App\silos_movimiento;
use App\Silo;
use App\silos_saldo;
use App\silos_corte;
use App\recepcion_trigo;
use App\despachos_trigo;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use  JWTAuth;

class SilosMovimientoController extends Controller
{
    public function obtenerMovimientos (Request $request) {

        $movimientos_directo = silos_movimiento::select('silos_movimientos.id as id_movimiento', DB::raw("'directo' as nom_silo"), 'silos_movimientos.tipo_movimiento',
                                                'silos_movimientos.valor', 'silos_movimientos.observaciones', 'silos_movimientos.created_at as registro', 'silos_movimientos.usu_registro',
                                                'despachos_trigo.motonave', 'despachos_trigo.estado',DB::raw("'-' as placa"), 'silos_movimientos.id_recepcion_trigo')
                        ->leftjoin('despachos_trigo', 'despachos_trigo.id_registro', '=', 'silos_movimientos.id_recepcion_trigo')
                        ->where('silos_movimientos.deleted', '0')
                        ->where('silos_movimientos.id_silo', 'like', '%' . $request->filtro_silo . '%')
                        ->where('silos_movimientos.id_silo', 'DIRECTO');

                        if($request->tipo_filtro == 'entre-fechas'){
                            $movimientos_directo->whereRaw("DATE_FORMAT(silos_movimientos.created_at ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicio."' and '".$request->fecha_final."'");
                        }
        
                        if($request->select_tipo_movimiento){
                            $movimientos_directo->where('silos_movimientos.tipo_movimiento', $request->select_tipo_movimiento);
                        }

                        if($request->filtro_motonave){
                            $movimientos_directo->where('despachos_trigo.motonave','LIKE','%'.$request->filtro_motonave.'%');
                        }

        //$movimientos_directo = $movimientos_directo->get();

        $movimientos = silos_movimiento::select('silos_movimientos.id as id_movimiento', 's.nombre as nom_silo', 'silos_movimientos.tipo_movimiento',
                                                'silos_movimientos.valor', 'silos_movimientos.observaciones', 'silos_movimientos.created_at as registro', 'silos_movimientos.usu_registro',
                                                'despachos_trigo.motonave', 'despachos_trigo.estado', 'recepcion_trigo.placa', 'silos_movimientos.id_recepcion_trigo')
                        ->join('silos as s', 's.id', '=', 'silos_movimientos.id_silo')
                        ->leftjoin('despachos_trigo', 'despachos_trigo.id_registro', '=', 'silos_movimientos.id_recepcion_trigo')
                        ->leftjoin('recepcion_trigo', 'recepcion_trigo.id_registro', '=', 'silos_movimientos.id_recepcion_trigo')
                        ->where('silos_movimientos.deleted', '0')
                        ->union($movimientos_directo)
                        // Para que quite las mulas de recepcion de trigo
                        ->where('recepcion_trigo.placa', '=', null)
                        ->where('silos_movimientos.id_silo', 'like', '%' . $request->filtro_silo . '%')
                        ->orderBy('registro', 'DESC');

                        if($request->tipo_filtro == 'entre-fechas'){
                            $movimientos->whereRaw("DATE_FORMAT(silos_movimientos.created_at ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicio."' and '".$request->fecha_final."'");
                        }

                        if($request->filtro_transito == 'true'){
                            $movimientos->where('despachos_trigo.estado', 'en_transito');
                        }

                        if($request->select_tipo_movimiento){
                            $movimientos->where('silos_movimientos.tipo_movimiento', $request->select_tipo_movimiento);
                        }

                        if($request->filtro_motonave){
                            $movimientos->where('despachos_trigo.motonave','LIKE','%'.$request->filtro_motonave.'%');
                        }

        $movimientos = $movimientos->get();

        for ($i=0; $i < count($movimientos) ; $i++) { 
            $movimientos[$i]['observaciones'] = $movimientos[$i]['observaciones'] .' (Registro por: '.$movimientos[$i]['usu_registro'].')';

            //Para saber si el trigo directo fue descargado
            $movimientos[$i]['directo_descargado']='no';
            if($movimientos[$i]['tipo_movimiento'] == 'directo'){
                $movi = silos_movimiento::where('tipo_movimiento', 'descargue')->where('id_recepcion_trigo', $movimientos[$i]['id_recepcion_trigo'])->get();
                if(count($movi)>0){
                    $movimientos[$i]['directo_descargado'] = 'si';
                    $movimientos[$i]['tipo_movimiento'] = $movimientos[$i]['tipo_movimiento'].' - Descargada';
                }
            }
        }

        return $movimientos;
    }

    public function registrarMovimientoSinRequest($id_silo, $tipo_novedad, $cantidad,$cantidad_ton, $observaciones, $id_recepcion_trigo){

                // consulta la info de silo
                $info_silo = Silo::select('id as id_silo')->where('id', $id_silo)->where('deleted', '0')->first();

                if(!$info_silo){
                    return null;
                }

                $info_saldo = silos_saldo::where('id_silo', $info_silo['id_silo'])->first();
                // (-1) Significa que no existe el registro en la tabla de saldos
                $saldo = -1;
                if($info_saldo){
                    $saldo = $info_saldo['saldo'];
                }

                if(!$info_saldo && ($tipo_novedad == 'moje' || $tipo_novedad == 'salida' )){
                    return null;
                }

                //valida que si es moje no vaya a extraer del silo una cantidad mayor a la actual que tiene de trigo
                if($cantidad_ton > $info_saldo['saldo'] && ($tipo_novedad == 'moje' || $tipo_novedad == 'salida')){
                    return null;             
                }

                $cantidad = ($tipo_novedad == 'moje' || $tipo_novedad == 'salida') ? -$cantidad_ton : $cantidad_ton;
                $mov_silo = new silos_movimiento();
                $mov_silo->id = Uuid::generate()->string;
                $mov_silo->id_silo = $info_silo['id_silo'];
                $mov_silo->tipo_movimiento = $tipo_novedad;
                $mov_silo->observaciones = $observaciones;
                $mov_silo->valor = $cantidad_ton;
                $mov_silo->id_recepcion_trigo = $id_recepcion_trigo;
                $mov_silo->usu_registro = JWTAuth::user()->vendedor;
                $mov_silo->usu_modifica = JWTAuth::user()->vendedor;
                $saved = $mov_silo->save();

                if($saved === true){

                    $data = array();
                    $data['tipo_novedad'] = $tipo_novedad;
                    $data['cantidad'] = $cantidad_ton;
                    $data['saldo_silo'] = $saldo;
                    $data['id_silo'] = $info_silo['id_silo'];
                    $this->actualizaSaldoSilo($data);
                }
                else{
                    return null;              
                }
    }

    public function registrarMovimientoTrigoDirecto($id_silo, $tipo_novedad, $cantidad,$cantidad_ton, $observaciones, $id_recepcion_trigo){

        $mov_silo = new silos_movimiento();
        $mov_silo->id = Uuid::generate()->string;
        $mov_silo->id_silo = $id_silo;
        $mov_silo->tipo_movimiento = $tipo_novedad;
        $mov_silo->observaciones = $observaciones;
        $mov_silo->valor = $cantidad_ton;
        $mov_silo->id_recepcion_trigo = $id_recepcion_trigo;
        $mov_silo->usu_registro = JWTAuth::user()->vendedor;
        $mov_silo->usu_modifica = JWTAuth::user()->vendedor;
        $saved = $mov_silo->save();

        if($saved === false){
            return null; 
        }
}

    public function registrarMovimiento(Request $request){

        DB::beginTransaction();
        try{
                // consulta la info de silo
                $info_silo = Silo::select('id as id_silo')->where('nombre', $request->silo)->where('deleted', '0')->first();

                if(!$info_silo){
                    DB::commit();
                    return response()->json([
                        "status" => "error",
                        "mensaje" => "Silo no disponible para ingresar la novedad de ".$request->tipo_novedad
                    ],200);  
                }

                $info_saldo = silos_saldo::where('id_silo', $info_silo['id_silo'])->first();
                // (-1) Significa que no existe el registro en la tabla de saldos
                $saldo = -1;
                if($info_saldo){
                    $saldo = $info_saldo['saldo'];
                }


                if(!$info_saldo && $request->tipo_novedad == 'moje'){
                    DB::commit();
                    return response()->json([
                        "status" => "error",
                        "mensaje" => "El silo ".$request->silo." no tiene saldo actualmente, por favor revisar nuevamente."
                    ],200);  
                }

                //valida que si es moje no vaya a extraer del silo una cantidad mayor a la actual que tiene de trigo
                if($request->cantidad > $info_saldo['saldo'] && $request->tipo_novedad == 'moje'){
                    DB::commit();
                    return response()->json([
                        "status" => "error",
                        "mensaje" => "¡Atención! La cantidad es mayor a lo que tiene el silo actualmente."
                    ],200);                
                }

                $cantidad = $request->tipo_novedad == 'moje' ? -$request->cantidad : $request->cantidad;
                $mov_silo = new silos_movimiento();
                $mov_silo->id = Uuid::generate()->string;
                $mov_silo->id_silo = $info_silo['id_silo'];
                $mov_silo->tipo_movimiento = $request->tipo_novedad;
                $mov_silo->observaciones = $request->observaciones;
                $mov_silo->valor = $cantidad;
                $mov_silo->usu_registro = JWTAuth::user()->vendedor;
                $mov_silo->usu_modifica = JWTAuth::user()->vendedor;
                $saved = $mov_silo->save();

                if($saved === true){

                    $data = array();
                    $data['tipo_novedad'] = $request->tipo_novedad;
                    $data['cantidad'] = $request->cantidad;
                    $data['saldo_silo'] = $saldo;
                    $data['id_silo'] = $info_silo['id_silo'];
                    $this->actualizaSaldoSilo($data);

                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Muy bien! Novedad ingresada correctamente",
                        'status' => 'ok',
                    ],200);
                }
                else{
                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Error! No se ingreso la novedad, vuelta a intentarlo",
                        'status' => 'error',
                    ],200);                
                }
            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "status" => 'error'
                ],200);               
            }
    }

    public function ActualizaCorteMoje($silo, $cantidad){

        $hora_actual = (int)date('H');
        $hora_restringe = 9;

        if($hora_actual <= $hora_restringe){

            $saldo_corte = silos_corte::where('id_silo', $silo)->where('deleted', '0')->first();
            $nuevo_saldo = $saldo_corte['saldo'] - $cantidad;

            silos_corte::where('id_silo', $silo)->where('deleted', '0')->update(['saldo' => $nuevo_saldo, 'updated_at' => date('Y-m-d H:i:s')]);
        }

    }

    public function actualizaSaldoSilo($datos){

        if($datos['tipo_novedad'] == 'moje' || $datos['tipo_novedad'] == 'salida'){

            // si es null significa que nunca ha tenido movimiento
            if($datos['saldo_silo'] == -1){
                DB::table('silos_saldos')->insert(['id' => Uuid::generate()->string, 'id_silo' => $datos['id_silo'], 'saldo' => $datos['cantidad'], 'usu_registro' => JWTAuth::user()->vendedor, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            } else {
                $saldo = $datos['saldo_silo'] - $datos['cantidad'];
                silos_saldo::where('id_silo', $datos['id_silo'])->update(['saldo' => $saldo, 'usu_registro' => JWTAuth::user()->vendedor]);
                $this->ActualizaCorteMoje($datos['id_silo'], $datos['cantidad']);
            }

        } else if ($datos['tipo_novedad'] == 'descargue') {
            if($datos['saldo_silo'] == -1){
                DB::table('silos_saldos')->insert(['id' => Uuid::generate()->string, 'id_silo' => $datos['id_silo'], 'saldo' => $datos['cantidad'], 'usu_registro' => JWTAuth::user()->vendedor, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            } else {
                $saldo = $datos['saldo_silo'] + $datos['cantidad'];
                silos_saldo::where('id_silo', $datos['id_silo'])->update(['saldo' => $saldo, 'usu_registro' => JWTAuth::user()->vendedor]);                 
            }

        }
    }

    public function anularMovimiento(Request $request){

        DB::beginTransaction();
        try{
                $movi = silos_movimiento::find($request->id_movimiento);
                $movi->deleted = '1';
                $movi->usu_modifica = JWTAuth::user()->vendedor;
                $saved = $movi->save();

                if($saved === true){

                    //Anula el registro en despachos de trigo que alimentan en santa marta, este registro se anuló desde la dasboard principal de silos en gereneral
                    if($movi->id_recepcion_trigo){
                        $desp_trigo = despachos_trigo::find($movi->id_recepcion_trigo);
                        $desp_trigo->deleted = '1';
                        $desp_trigo->usu_modifica = JWTAuth::user()->vendedor;
                        $desp_trigo->save();

                        //Inactiva el cargue en báscula
                        DB::connection('mysql_bascula')->table('cargues')
                                                            ->where('placa', $desp_trigo->placa)
                                                            ->where('estado', 'activo')
                                                            ->update(['estado' => 'inactivo']);
                    }

                    $saldo = silos_saldo::select('saldo')->where('id_silo', $movi->id_silo)->first();

                    if($movi->tipo_movimiento == 'moje'){
                        $saldo = $saldo['saldo'] + abs($movi->valor);
                    }else{
                        $saldo = $saldo['saldo'] - $movi->valor;
                    }

                    silos_saldo::where('id_silo', $movi->id_silo)->update(['saldo' => $saldo, 'usu_registro' => JWTAuth::user()->vendedor, 'updated_at' => date('Y-m-d H:i:s')]); 

                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Muy bien! Movimiento anulado correctamente",
                        'status' => 'ok',
                    ],200);
                }
                else{
                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Error! Vuelva a intentarlo",
                        'status' => 'error',
                    ],200);                
                }
            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "status" => 'error'
                ],200);               
            }
    }

    public function anularMovimientoMultiple(Request $request){

        DB::beginTransaction();
        try{
                $datos = $request->registros;
                $cont_reg = 0;

                if($request->visualiza == 'envio'){
                    $filter_info = $datos;
                    $filter_info = array_filter($datos,
                        function ($item) {
                            return ($item['estado'] == 'en_transito' || $item['estado'] == 'directo');
                        }
                    );
                    $datos = array_values($filter_info);
                }

                for($i=0; $i<count($datos);$i++){

                    //Se inactiva registro importado desde el excel
                    if($request->visualiza == 'recepcion'){
                        $recepcion = recepcion_trigo::find($datos[$i]['id_registro']);
                        $recepcion->deleted = '1';
                        $recepcion->save();
                        //NO existe en la tabla, pero la creo acá para que no intefiera en actualizar el saldo
                        $datos[$i]['estado'] = 'recepcion';
                    }

                    if($request->visualiza == 'envio'){
                        $despacho = despachos_trigo::find($datos[$i]['id_registro']);
                        $despacho->deleted = '1';
                        $despacho->save();

                        //Inactiva el cargue en bascula
                        DB::connection('mysql_bascula')->table('cargues')
                                                            ->where('placa', $datos[$i]['placa'])
                                                            ->where('estado', 'activo')
                                                            ->update(['estado' => 'inactivo']);
                    }

                    //Se anula el movimiento en silos
                    $id_movi_silo = silos_movimiento::select('id as id_movi_silo')->where('id_recepcion_trigo', $datos[$i]['id_registro'])->first();
                    if($id_movi_silo){
                        $movi = silos_movimiento::find($id_movi_silo['id_movi_silo']);
                        $movi->deleted = '1';
                        $movi->usu_modifica = JWTAuth::user()->vendedor;
                        $movi->save();
                    }


                    // El trigo directo no alimenta saldos
                    if($datos[$i]['estado'] != 'directo') {
                        //Se consulta el saldo del silo
                        $saldo = silos_saldo::select('saldo')->where('id_silo', $movi->id_silo)->first();
                        $saldo = $request->visualiza == 'recepcion' ? $saldo['saldo'] - $movi->valor : $saldo['saldo'] + $movi->valor;
                        silos_saldo::where('id_silo', $movi->id_silo)->update(['saldo' => $saldo, 'usu_registro' => JWTAuth::user()->vendedor, 'updated_at' => date('Y-m-d H:i:s')]); 
                    }

                    $cont_reg++;
                }

                if(count($datos) == $cont_reg){
                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Muy bien! Movimientos anulados correctamente",
                        'estado' => 'ok',
                    ],200);
                }
                else{
                    DB::commit();
                    return response()->json([
                        "mensaje" => "¡Error! Vuelva a intentarlo",
                        'estado' => 'error',
                    ],200);                
                }
            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                        "mensaje" => $e->getMessage(),
                        "estado" => 'error'
                ],200);               
            }
    }
}
