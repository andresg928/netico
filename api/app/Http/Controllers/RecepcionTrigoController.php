<?php

namespace App\Http\Controllers;

use App\recepcion_trigo;
use Excel;
use Illuminate\Http\Request;
use App\Imports\RecepcionTrigoImport;

class RecepcionTrigoController extends Controller
{
    public function importarDatos(Request $request){

        try {
                if(!$request->file('documento_recepcion')){
                    throw new \Excepcion('Archivo no existe');
                }
                $file = $request->documento_recepcion;
                $data = array();
                $data['motonave'] = $request->motonave;
                $data['silo'] = $request->silo;
                Excel::import(new RecepcionTrigoImport($data), $file);

                return response()->json([
                    "estado" => "ok",
                    "mensaje" => 'El archivo se subió correctamente'
                ],200); 

        } catch (\Maatwebsite\Excel\ValidatorsExcepcion $e) {

            foreach ($failures as $failure) {
                $failure->row();
                $failure->attribute();
                $failure->errors();
                $failure->values();
            }

        }

    }

    public function listadoRecepcion(Request $request){

        $t='recepcion_trigo.';
        $registros = recepcion_trigo::select($t.'id_registro', $t.'deleted', $t.'fecha', $t.'motonave', $t.'placa', $t.'id_silo', 's.nombre as nom_sil', $t.'consecutivo', $t.'bl', $t.'tiquete1', $t.'peso_entrada1',
                                             $t.'peso_salida1', $t.'peso_neto1', $t.'hora_salida', $t.'acumulado1', $t.'tiquete2', $t.'peso_entrada2', $t.'peso_salida2', $t.'peso_neto2',
                                             $t.'acumulado2', $t.'hora_llegada', $t.'diferencia', $t.'tiempo_recorrido', $t.'usu_registra', $t.'usu_modifica', $t.'created_at', $t.'updated_at')
                                    ->join('silos as s', 's.id', '=', $t.'id_silo')
                                    ->where($t.'deleted', 0)
                                    ->whereBetween($t.'fecha', [$request->fecini, $request->fecfin])
                                    ->where($t.'motonave', 'like', '%' . $request->filtro_motonave . '%')
                                    ->where('s.nombre', 'like', '%' . $request->filtro_bodega . '%')
                                    ->orderBy($t.'fecha', 'ASC')
                                    ->orderBy($t.'tiquete1', 'ASC')
                                    ->get();
        return $registros;

    }

    public function downloadFile(){
        return response()->download(storage_path('app/public/formatos_importacion/FORMATO_RECIBO_TRIGO.xlsx'));
    }
}
