<?php

namespace App\Http\Controllers;

use App\Pesaje;
use App\Tarifa;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use App\Exports\PesajesExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\FacturacionController; 

class PesajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pesajes_pendientes = Pesaje::select('pesajes.id as id_pesaje', 'transaccion', 'tipo', 'peso_origen', 'peso_inicial','facturar_kilos',
        'peso_final', 'peso_neto', 'placa', 'estado', 'observaciones', 'pesajes.created_at as fecha','pesajes.updated_at as fecha_final','empresas.nombre as nom_emp',
        DB::raw('(CASE WHEN tipo = "trigo" THEN "Trigo" ELSE "Producto terminado" END) AS tipo_label, substring(pesajes.created_at,1,10) as fecha_trans_1,
                substring(pesajes.created_at,12,20) as hora_trans_1,substring(pesajes.updated_at,12,20) as hora_trans_2'),'u.name', 'pesajes.num_dcto', 'pesajes.total')
        ->where('estado', '=', 0)
        ->join('users as u', 'u.id', '=', 'pesajes.id_usuario')
        ->leftjoin('empresas', 'pesajes.nit_empresa', '=', 'empresas.nit')
        ->whereBetween('pesajes.fecha', [$request->input('fecha_inicial'), $request->input('fecha_final')])
        ->where('tipo', 'like', '%' . $request->input('tipo_producto') . '%')
        ->orderBy('transaccion', 'DESC')->get();


        return $pesajes_pendientes;
    }

    public function excelPesajes($fec_ini='', $fec_fin='', $tipo=''){   
        $datos = array();
        $datos['tipo_producto'] = $tipo;   
        $datos['fec_ini'] = $fec_ini;   
        $datos['fec_fin'] = $fec_fin;
        //dd($datos);   
        return Excel::download(new PesajesExport($datos), 'Pesajes.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerCalculos($tipo){
        $datos = array();
        //default

        //consulta informacion de resolucion y tarifas para controlar consecutivos y prefijos
        $tarifa = Tarifa::select('id as id_tarifa', 'precio', 'iva_porcentaje', 'rango_inicio', 'prefijo', 'rango_final', 'fecha_final', 'tipo_producto')
                        ->where('deleted', '0')->where('tipo_producto', $tipo)->first();

        //obtener consecutivo actual
        $consecutivo = Pesaje::where('tipo', $tipo)->where('id_tarifa', $tarifa['id_tarifa'])->orderBy('transaccion', 'DESC')->first();
        $date = Carbon::now();
        $fecha_actual = $date->format('Y-m-d');
        //comparo que no se pase los rangos inicial y final
        if($fecha_actual <= $tarifa['fecha_final']){

            if((int)$consecutivo['transaccion'] < (int)$tarifa['rango_final']){

                if($tarifa){
    
                    $datos['subtotal'] = 0;
                    $datos['iva'] = 0;
                    $datos['total'] = 0;
                    $datos['id_tarifa'] = $tarifa['id_tarifa'];
        
                    if($tarifa['iva_porcentaje'] > 0 || $tarifa['precio'] > 0){
        
                        $base_iva = '1.'.$tarifa['iva_porcentaje'];
                        $subtotal = round($tarifa['precio'] / $base_iva);
                        $iva = $subtotal * ($tarifa['iva_porcentaje']/100);
                        $total = round($subtotal + $iva);
        
                        //asigno datos al array
                        $datos['subtotal'] = $subtotal;
                        $datos['iva'] = $iva;
                        $datos['total'] = $total;
                    }
        
                    if($consecutivo){
                        $datos['consecutivo'] = $consecutivo['transaccion'] + 1;
                    } else {
                        $datos['consecutivo'] = $tarifa['rango_inicio'];
                    }
    
                    $datos['num_dcto'] = $tarifa['prefijo'].'-'.$datos['consecutivo'];
                    $datos['error'] = 'no';
    
                } else {
                    $datos['error'] = 'si';
                    $datos['mensaje'] = 'No se encontraron tarifas o resoluciones activas';
                }
    
            } else {
                $datos['error'] = 'si';
                $datos['mensaje'] = 'El número de factura '.$tarifa['rango_final'].'  no pertenece al rango resolución de facturación activa';
            }

        } else {
            $datos['error'] = 'si';
            $datos['mensaje'] = 'La fecha de la resolución de '.$tarifa['tipo_producto'].' no está en el rango permitido, por favor revisar en el módulo de TARIFAS';            
        }

        
        return $datos;
    }
    public function store(Request $request)
    {
        //verifico que la placa no esté en pesaje 1
        $valida_pesaje = Pesaje::where('placa', $request->input('placa'))->where('estado', 'peso_1')->first();
        if(!$valida_pesaje){

            $datos = $this->obtenerCalculos($request->tipo);

            if($datos['error'] == 'si'){
                return response()->json([
                    'mensaje' => $datos['mensaje'],
                    'error' => 'si',
                    "guardo" => false
                ],200);                
            }

            $pesaje = new Pesaje();
            $pesaje->id = Uuid::generate()->string;
            $pesaje->fecha = date('Y-m-d');
            $pesaje->tipo = $request->input('tipo');
            $pesaje->nit_empresa = $request->input('nit');
            $pesaje->placa = $request->input('placa');
            $pesaje->peso_origen = $request->input('peso_origen');
            $pesaje->peso_inicial = $request->input('peso_inicial');
            $pesaje->peso_final = $request->input('peso_final');
            $pesaje->peso_neto = $request->input('peso_neto');
            $pesaje->id_cargue = $request->input('id_cargue');
            $pesaje->estado = 'peso_1';
            $pesaje->observaciones = $request->input('observaciones');
            $pesaje->id_usuario = JWTAuth::user()->id;
            $pesaje->facturar_kilos = 0;
            $pesaje->subtotal = $datos['subtotal'];
            $pesaje->iva = $datos['iva'];
            $pesaje->total = $datos['total'];
            $pesaje->num_dcto = $datos['num_dcto'];
            $pesaje->transaccion = $datos['consecutivo'];
            $pesaje->id_tarifa = $datos['id_tarifa'];
            $saved = $pesaje->save();

            if($saved === true){
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    'error' => '',
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso",
                    'error' => '',
                    "guardo" => false
                ],500);                
            }
        }else{
            return response()->json([
                "mensaje" => "doble_ingreso",
                'error' => '',
                "guardo" => false
            ],200);               
        }

    }
    public function obtenerPesobasculaMettler(){
        /*$curl = curl_init();
        $url = "http://192.168.0.50:1702";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPGET, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);*/
        //$result = Curl::to('http://192.168.0.50:1702')->get();
        //dd($response);
        $result = 1;
        if($result){
            //obtiene el primer numero de una cadena de caracteres
            $resultado2=array();
            preg_match('/[0-9]+/', $result, $resultado2);
            $resultado2[0]=17150;
            if($resultado2[0]> 0){
                $respuesta = 'OK';
            }
            else{
                $respuesta = 'ERROR';
            }
            return response()->json([
                "respuesta" => $respuesta,
                "peso" => (int)$resultado2[0]
            ],200);  
            curl_close($curl);
        }
        else{
            return response()->json([
                "respuesta" => 'ERROR',
                "peso" => 0
            ],200);             
        }

    }

    public function obtenerFacturasxPlaca (Request $request) {

        $request->fecha = '20200212';

        $facturas = \DB::connection('sqlsrv')
                                    ->table('TRADE')
                                    ->select('trade.FECHA', 'p.placa', 'p.nrodcto', 'p.tipodcto')
                                    ->join('MTPEDIDO_REMISION as p', 'p.nrodcto', '=', DB::raw("replace(trade.nrodcto,' ', '')"))
                                    ->where(DB::raw("CONVERT(varchar(10), p.tipodcto)"), '=', DB::raw("CONVERT(varchar(10), trade.tipodcto)"))
                                    ->where('p.placa', '=', $request->placa)
                                    ->where('trade.fecha', '=', $request->fecha)
                                    ->whereIn('trade.tipodcto', ['RF','F1','F2','MO','PA','RP','RH','RS','N3','N4','N5','D1','NC'])
                                    ->orderBy('trade.fecha', 'DESC')->limit(50)->get();
        
        //$datos = array();
        $facturas = json_decode($facturas, true);
        $aux_facturas = array();

        for($i=0; $i<count($facturas); $i++){

            $nroddcto = $facturas[$i]['nrodcto'];
            $tipodcto = $facturas[$i]['tipodcto'];
            //obtengo el consecutivo para luego buscar esa llave el la matriz de $datos que la key es el tipodcto + nrodcto
            $aux_facturas[$i] = $tipodcto.$nroddcto;
            $datos[$tipodcto.$nroddcto] = $this->obtenerMovimientoPorFactura($nroddcto, $tipodcto);
        }

        //AGRUPO TOTALES
        $total_kilos = 0;

        for($j=0;$j<count($aux_facturas);$j++){

            for($k=0; $k < count($datos[$aux_facturas[$j]]); $k++){
                // dd($datos[$aux_facturas[$j]][$k]->peso_bruto);
                $total_kilos = $total_kilos + $datos[$aux_facturas[$j]][$k]->peso_bruto;
            }
        }
        //dd($total_kilos);
        if($total_kilos){
            return response()->json([
                "respuesta" => round($total_kilos, 2),
            ],200);
        } else {
            return response()->json([
                "respuesta" => "error_respuesta",
            ],200);
        }
    }

    public function obtenerMovimientoPorFactura($nrodcto, $tipodcto){

            $movimiento = \DB::connection('sqlsrv')->table('MVTRADE as m')
                                                    ->select('m.producto','p.DESCRIPCIO','p.MEAPSVR', 'm.CANTIDAD', DB::raw("CONVERT(decimal(10,2),round(p.MEAPSVR * m.CANTIDAD, 2)) as peso_bruto"))
                                                    ->join('MTMERCIA as p', 'p.codigo', '=', 'm.producto')
                                                    ->where('m.nrodcto', '=', $nrodcto)
                                                    ->where('m.tipodcto', '=', $tipodcto)->get();
            return $movimiento;

    }



    public function generarFactura($id, $imp = '')
    {
       //consulto los datos de la transaccion o sea del pesaje
       $pesaje = Pesaje::select('pesajes.id as id_pesaje','id_tarifa', 'pesajes.transaccion','num_dcto','subtotal','iva','total', 'pesajes.tipo', 'pesajes.peso_origen', 'pesajes.peso_inicial', 'pesajes.fecha as fecha_actual','u.name as usuario',
                                   'pesajes.peso_final', 'pesajes.peso_neto', 'pesajes.placa', 'pesajes.estado', 'pesajes.observaciones', 'pesajes.created_at as fecha','tipo as tipo_producto',
                                   DB::raw('(CASE WHEN pesajes.tipo = "trigo" THEN "Trigo" ELSE "Producto terminado" END) AS tipo_label, substring(pesajes.updated_at,1,10) as fecha_trans_2, substring(pesajes.created_at,12,20) as hora_trans_1, substring(pesajes.updated_at,12,20) as hora_trans_2')
                                )
       ->where('pesajes.id', '=', $id)
       ->join('users as u', 'u.id', '=', 'pesajes.id_usuario')
       ->orderBy('pesajes.transaccion', 'DESC')->get();

       //dd($pesaje);               
       // This  $data array will be passed to our PDF blade
        $pesaje[0]['empresa'] = 'ICOHARINAS SAS';
        $pesaje[0]['impresion'] = date('Y-m-d H:i:s');
        $pesaje[0]['nit'] = '890212673-6';
        $pesaje[0]['direccion'] = 'Dirección: Cra 17 Aut Palenque - Chimitá 59-131 Giron(Santander)';
        $pesaje[0]['telefono'] = 'Teléfonos: 6761000 - FAX 6761020';
        $pesaje[0]['reimprimir'] = '';
        //consulto los datos de resolucion
        $datos_resolucion = Tarifa::where('id',  $pesaje[0]['id_tarifa'])->first();
        $pesaje[0]['resolucion'] = $datos_resolucion['resolucion'];
        $pesaje[0]['fecha_inicio'] = $datos_resolucion['fecha_inicio'];
        $pesaje[0]['fecha_final'] = $datos_resolucion['fecha_final'];
        $pesaje[0]['rango_inicio'] = $datos_resolucion['rango_inicio'];
        $pesaje[0]['rango_final'] = $datos_resolucion['rango_final'];
        $pesaje[0]['precio'] = $datos_resolucion['precio'];

        //obtener fecha impresion en letras
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = Carbon::parse($pesaje[0]['fecha_trans_2']);
        $mes = $meses[($fecha->format('n')) - 1];
        $pesaje[0]['fecha_impresion'] = $fecha->format('d') . ' de ' . $mes . ' del año ' . $fecha->format('Y');

        if($imp == 'reimprimir') {
            $pesaje[0]['reimprimir'] = 'si';
        }
        
        $pdf = PDF::loadView('facturaPesaje', ['pesaje'=>$pesaje]);

        return $pdf->download('Factura_Báscula_'.$pesaje[0]['placa'].'.pdf');
    }    

    public function vehiculosPendientesPorPesajeFinal(){
 
       $pesajes_pendientes = Pesaje::select('pesajes.id as id_pesaje', 'transaccion', 'tipo', 'peso_origen', 'peso_inicial','pesajes.id_cargue',
                                   'peso_final', 'peso_neto', 'placa', 'estado', 'observaciones', 'pesajes.created_at as fecha','nit_empresa','empresas.nombre as nom_emp',
                                   DB::raw('(CASE WHEN tipo = "trigo" THEN "Trigo" ELSE "Producto terminado" END) AS tipo_label'))
       ->where('estado', '=', 'peso_1')
       ->where('estado', '=', 0)
       ->leftjoin('empresas', 'pesajes.nit_empresa', '=', 'empresas.nit')
       ->orderBy('transaccion', 'DESC')->get();

        
        return $pesajes_pendientes;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Pesaje  $pesaje
     * @return \Illuminate\Http\Response
     */
    public function show(Pesaje $pesaje)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pesaje  $pesaje
     * @return \Illuminate\Http\Response
     */
    public function edit(Pesaje $pesaje)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pesaje  $pesaje
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pesaje)
    {
        $pesaje = Pesaje::find($id_pesaje);
        $pesaje->peso_final = $request->input('peso_final');
        $pesaje->peso_neto = $request->input('peso_neto');
        $pesaje->estado = 'peso_2';
        $pesaje->peso_bruto_erp = $request->input('peso_bruto_erp');
        $pesaje->kilos_dif = $request->input('kilos_dif');
        $pesaje->nivel_acep = $request->input('nivel_acep');
        if($request->input('facturar')<0){
            //volver positivo
            $kilos_facturar = $request->input('facturar') * -1;
            $pesaje->facturar_kilos = $kilos_facturar;
        }
        else{
            $pesaje->facturar_kilos = 0;
        }

        $saved = $pesaje->save();

        if($saved === true){
            //se actualiza la info, si es producto terminado
            if($pesaje->tipo == 'producto_terminado'){
                $facturacion_ctrl = new FacturacionController();
                $facturacion_ctrl->actualizarFacturaGestionada($pesaje->placa);
            }


            return response()->json([
                "mensaje" => "registro_exitoso",
                "num_dcto" => $pesaje->num_dcto,
                "guardo" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pesaje  $pesaje
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pesaje $pesaje)
    {
        //
    }
}
