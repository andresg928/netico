<?php

namespace App\Http\Controllers;

use App\Tablero;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TableroController extends Controller
{

    public function registrarViaje(Request $request){

        //Registro nuevo
        if(!$request->id_viaje){
            $viaje = new Tablero();
            $viaje->id = Uuid::generate()->string;
            $viaje->usu_registro = JWTAuth::user()->vendedor;
            $viaje->fec_registro = date('Y-m-d H:i:s');
            $msj = 'Viaje registrado exitosamente.';
        }else{
            //Actualiza datos
            $viaje = Tablero::find($request->id_viaje);
            $viaje->usu_modifica = JWTAuth::user()->vendedor;
            $viaje->fec_modifica = date('Y-m-d H:i:s');
            $msj = 'Viaje actualizado exitosamente.';
            // Si elimina
            if($request->accion == 'eliminar'){
                $viaje->deleted = '1';
                $msj = 'Viaje eliminado exitosamente.';
            }
        }
        // Para cuando elimine no actulice estos datos
        if($request->accion != 'eliminar'){
            $viaje->fecha = $request->fecha;
            $viaje->viaje = $request->viaje;
            $viaje->kilos = $request->kilos;
        }


        $saved = $viaje->save();

        if($saved){
            return response()->json([
                "estado" => "ok",
                "mensaje" => $msj
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "mensaje" => 'Hubo un error al realizar la operación'
            ],200);           
        }
    }
    public function fechaVueCalendar($fecha){
        //Esto para que en el calendario cuadre por temas de zona horaria se agregó un dia
        $date = Carbon::parse($fecha);
        $newDate = $date->addDay();
        $formattedDate = $newDate->format('Y-m-d');
        return $formattedDate;
    }

    public function obtenerDiasMes($fechaInicio, $fechaFin){

        // Crear objetos DateTime para las fechas de inicio y fin
        $startDate = new \DateTime($fechaInicio);
        $endDate = new \DateTime($fechaFin);

        // Añadir un día al endDate para asegurarse de que el bucle incluya el último día
        $endDate->modify('+1 day');

        // Intervalo de un día
        $interval = new \DateInterval('P1D');

        $fecha=[];
        // Recorrer los días usando un bucle for
        for ($date = $startDate; $date < $endDate; $date->add($interval)) {
            $fecha[] =  $this->fechaVueCalendar($date->format('Y-m-d'));
        }
        return $fecha;
    }

    public function obtenerViajes(Request $request){

        $ctrl_coment = new ComentarioController();
        $fecha = $ctrl_coment->obtenerInicioFinMes($request->mes,$request->anio);
        $fec_ini=$fecha[0];
        $fec_fin=$fecha[1];


        $data = [];
        $viajes = Tablero::select('*', DB::raw('id as id_viaje'))->where('deleted', '0')
                    ->whereRaw("DATE_FORMAT(fecha,'%Y-%m-%d') BETWEEN '".$fec_ini."' and '".$fec_fin."'")
                    ->orderBy(DB::raw('CAST(kilos AS UNSIGNED)'), 'DESC')
                    ->get();

        for ($i=0; $i < count($viajes) ; $i++) { 
            $viajes[$i]['fecha_calendario'] = $this->fechaVueCalendar($viajes[$i]['fecha']);
        }

        $data['eventos'] = $viajes;
        $data['dias_mes'] = $this->obtenerDiasMes($fec_ini, $fec_fin);
        return $data;
    }
}
