<?php

namespace App\Http\Controllers;

use App\Silo;
use App\silos_saldo;
use App\silos_corte;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;

class SiloController extends Controller
{
    public function registrar(Request $request){

        if($request->id_silo){
            $silo = Silo::find($request->id_silo);
            $mensaje = 'Silo registrado modificado.';
        } else {
            $silo = new Silo();
            $silo->id = Uuid::generate()->string;
            $mensaje = 'Silo registrado correctamente.';
        }

        $silo->ubicacion = $request->ubicacion;
        $silo->nombre = $request->nombre;
        $silo->capacidad = $request->capacidad;
        $silo->saldo_inicial = $request->saldo_inicial;
        
        $silo->usu_registro = JWTAuth::user()->vendedor;
        $silo->usu_modifico = JWTAuth::user()->vendedor;
        $saved = $silo->save();

        if($saved === true){
            return response()->json([
                "estado" => "ok",
                "mensaje" => $mensaje
            ],200);
        } else {
            return response()->json([
                "estado" => "error",
                "id_despacho" => 'Error al registrar el silo, vuelva a intentarlo'
            ],200);            
        }
    }

    public function obtenerSilos(Request $request){
        $silos = Silo::select('id as id_silo', 'nombre', 'capacidad', 'saldo_inicial', 'ubicacion')->where('deleted', '0')->orderBy('ubicacion', 'ASC')->orderBy('nombre', 'ASC')->get();
        return $silos;
    }

    public function obtenerSilosPorNombre(Request $request){
        $silos = Silo::select('id as codigo', 'nombre')->where('deleted', '0')->where('ubicacion', $request->ubicacion)->orderBy('nombre', 'ASC')->get();
        return $silos;
    }

    public function obtenerInfoSiloxId($idSilo){
        $silo = Silo::select('id as codigo', 'nombre', 'ubicacion')->where('id', $idSilo)->first();
        return $silo;
    }

    public function ObtenerInfoSilosGraficos(Request $request){

        $silos = Silo::select('id as id_silo', 'nombre', 'capacidad', 'saldo_inicial', 'ubicacion')->where('deleted', '0')->orderBy('ubicacion', 'ASC')->orderBy('nombre', 'ASC')->get();

        $data = array();
        $total_trigo_principal = 0;
        $capacidad_trigo_principal = 0;
        $total_trigo_sucursal = 0;
        $capacidad_trigo_sucursal = 0;
        $data_aux = array();
        $j = 0;

        for ($i=0; $i < count($silos) ; $i++) {

            //Obtener saldo trigo
            $info_saldo = silos_saldo::select('saldo')->where('id_silo', $silos[$i]['id_silo'])->first();
            //Obtener Corte Silos
            $info_saldo_corte = silos_corte::select('saldo')->where('deleted', '0')->where('id_silo', $silos[$i]['id_silo'])->first();

            $saldo_corte = 0;
            if($info_saldo_corte){
                $saldo_corte = $info_saldo_corte['saldo'];
            }

            $saldo = 0;
            if($info_saldo){
                $saldo = $info_saldo['saldo'];
            }

            if($silos[$i]['ubicacion'] == 'Girón'){
                $data['sede_pricipal']['capacidad'][] = $silos[$i]['capacidad'];
                $data['sede_pricipal']['nombre_silo'][] = $silos[$i]['nombre'];
                $data['sede_pricipal']['saldo'][] = $saldo;
                $data['sede_pricipal']['corte_saldo'][] = $saldo_corte;
                $total_trigo_principal = $total_trigo_principal + $saldo;
                $capacidad_trigo_principal = $capacidad_trigo_principal + $silos[$i]['capacidad'];
            } else if ($silos[$i]['ubicacion'] == 'Santa marta') {
                $data['sucursal_1']['capacidad'][] = $silos[$i]['capacidad'];
                $data['sucursal_1']['nombre_silo'][] = $silos[$i]['nombre'];
                $data['sucursal_1']['saldo'][] = $saldo;
                $data['sucursal_1']['corte_saldo'][] = $saldo_corte;
                $total_trigo_sucursal = $total_trigo_sucursal + $saldo;
                $capacidad_trigo_sucursal = $capacidad_trigo_sucursal + $silos[$i]['capacidad'];

                //Info general de bodegas para resumir a la persona de santa marta
                //$colores1 = ['indigo'];

                $data_aux['bodega'] = $silos[$i]['nombre'];
                $data_aux['capacidad'] = $silos[$i]['capacidad'];
                $data_aux['saldo'] = $saldo;
                $data_aux['color1'] = 'indigo';
                $data['sucursal_1']['info_general'][] = $data_aux;
                $data_aux = [];
                $j++;

            }


        }

        $data['total_trigo_principal'] = $total_trigo_principal;
        $data['capacidad_trigo_principal'] = $capacidad_trigo_principal;
        $data['total_trigo_sucursal'] = $total_trigo_sucursal;
        $data['capacidad_trigo_sucursal'] = $capacidad_trigo_sucursal;

        return $data;
    }

}
