<?php

namespace App\Http\Controllers;

use App\despachos_novedades;
use App\Despacho;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\DespachoController;

class DespachosNovedadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function consultaGestionEntregas(Request $request){

        $t='despachos_novedades.';
        $datos = despachos_novedades::select('d.consecutivo', 'd.placa', 'd.conductor', 'dt.celular', 'dt.ciudad', $t.'novedad', $t.'observaciones', $t.'fecha_novedad')
                                     ->join('despachos_turnos as dt', 'dt.id', '=' ,$t.'id_turno')
                                     ->join('despachos as d', 'd.id', '=' ,'dt.id_despacho')
                                     ->orderBy($t.'fecha_novedad', 'DESC')
                                     ->whereRaw("DATE_FORMAT(despachos_novedades.fecha_novedad ,'%Y-%m-%d') BETWEEN '".$request->fecha_inicial."' and '".$request->fecha_final."'");
                                     if($request->estado !== 'Todos'){
                                        $datos->where($t.'novedad', $request->estado == 'Con novedad' ? 'si':'no');
                                     }
                                     $datos = $datos->get();
        return $datos;

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registrarNovedadDespacho(Request $request){

        DB::beginTransaction();
        try{
            //Varifica que no se haya gestionado la entrega, consultando si el id_turno y que este este vivo el registro con deleted 0
            $nov = despachos_novedades::where('id_turno', $request->id_turno)->where('deleted', '0')->first();
            if(count($nov)>0){
                DB::commit();
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => 'La entrega ya fue gestionada anteriormente.'
                ],200);   
            }

            $novedad = new despachos_novedades();
            $novedad->id = Uuid::generate()->string;
            $novedad->id_turno = $request->id_turno;
            $novedad->fecha_novedad = date("Y-m-d H:i:s");
            $novedad->novedad = $request->novedad == 1 ? 'si':'no';
            $novedad->observaciones = $request->observaciones;
            $saved = $novedad->save();

            if($saved === true){
                if($novedad->novedad == 'si'){
                    $despacho_ctrl = new DespachoController();
                    $datos = Despacho::find($request->id_despacho);
                    $datos['accion'] = 'gestionar_entrega';
                    $datos['novedad'] = $novedad->novedad;
                    $datos['fecha_hora_novedad'] = $novedad->fecha_novedad;
                    $datos['observaciones_novedad'] = $novedad->observaciones;
                    $despacho_ctrl->enviarProcesoColaTrabajo($datos);
                }
                DB::commit();
                return response()->json([
                    "estado" => 'ok',
                    "mensaje" => 'La novedad se ha registrado correctamente.'
                ],200);
            } else {
                DB::commit();
                return response()->json([
                    "estado" => 'error',
                    "mensaje" => 'Hubo un error, vuelva a intentarlo.'
                ],200);     
            }
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json([
                    "mensaje" => $e->getMessage(),
                    "estado" => 'error'
            ],200);               
        }
    }
}
