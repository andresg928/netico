<?php

namespace App\Exports;

use App\Pesaje;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PesajesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    public function __construct(array $datos)
    {
        $this->datos = $datos;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:M1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);
            },
        ];
    }
    public function headings(): array
    {
        return [
            '# Documento',
            'Fecha',
            'Tipo pesaje',
            'Placa',
            'Empresa',
            'Peso origen',
            'Peso inicial',
            'Hora inicio',
            'Peso final',
            'Hora final',
            'Peso neto',
            '(Kg) Facturar',
            'Observaciones',
            'Total',
            'Usuario'
        ];
    }
    public function collection()
    {
        $pesajes_pendientes = Pesaje::select(
            'pesajes.num_dcto',
            'pesajes.fecha',
             DB::raw('(CASE WHEN tipo = "trigo" THEN "Trigo" ELSE "Producto terminado" END) AS tipo_label'),
            'pesajes.placa',
            'empresas.nombre',
            'pesajes.peso_origen',
            'pesajes.peso_inicial',
             DB::raw('substring(pesajes.created_at,12,20) as hora_trans_1'),
            'pesajes.peso_final',
             DB::raw('substring(pesajes.updated_at,12,20) as hora_trans_2'),
            'pesajes.peso_neto',
            'facturar_kilos',
            'c.observaciones',
            'pesajes.total',
            'u.name'
        )
        ->leftjoin('empresas', 'pesajes.nit_empresa', '=', 'empresas.nit')
        ->whereBetween('pesajes.fecha', [$this->datos['fec_ini'], $this->datos['fec_fin']])
        ->where('tipo', 'like', '%' . $this->datos['tipo_producto'] . '%')
        ->join('users as u', 'u.id', '=', 'pesajes.id_usuario')
        ->leftjoin('cargues as c', 'c.id', '=', 'pesajes.id_cargue')
        ->orderBy('transaccion', 'DESC')->get();

        return $pesajes_pendientes;

    }
}
