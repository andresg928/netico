<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Http\Controllers\PedidoController;

class ClientesSinVentaExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    public function __construct()
    {
        //$this->datos = $datos;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12)->setBold(true);
            },
        ];
    }
    public function headings(): array
    {
        return [
            'NIT',
            'NOMBRE',
            'VENDEDOR',
            'CONTACTO',
            'TELEFONO',
            'DIRECCION',
            'COMENTARIO',
            'ULT_COMPRA'
        ];
    }
    public function collection(){
        $ctl_ped = new PedidoController();
        $datos = $ctl_ped->obtenerClientesNoVenta();
        return collect($datos);
    }
}
