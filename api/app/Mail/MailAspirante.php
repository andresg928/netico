<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailAspirante extends Mailable
{
    use Queueable, SerializesModels;
    public $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    public function build()
    {
        

        if($this->datos['accion'] == 'entrevista'){
            $asunto = 'Proceso de selección ICOHARINAS - Entrevista programada';
        }

            return $this->view('notificaAspirante')
                        ->cc($this->datos['email_aspirante'], 'Aspirante')
                        ->subject($asunto);
    }
}
