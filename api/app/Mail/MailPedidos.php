<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ClientesSinVentaExport;
use Carbon\Carbon;

class MailPedidos extends Mailable
{
    use Queueable, SerializesModels;
    public $datos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $asunto = $this->datos['placa'].'-'.$this->datos['conductor'].' | Vendedor : '.$this->datos['nombre_vendedor'];

        if($this->datos['accion'] == 'cerrar_despacho'){

            return $this->view('despachoCerrado')
                        ->cc($this->datos['email_cartera'], 'Cartera')
                        ->subject($asunto);

        } else if ($this->datos['accion'] == 'autorizar'){

            return $this->view('despachoCerrado')
                        //->from('auxiliarventas@icoharinas.com.co')
                        ->cc($this->datos['email_vendedor'], 'Vendedor')
                        ->cc($this->datos['email_facturacion'], 'Auxiliar Facturación')
                        ->subject($asunto);

        } else if($this->datos['accion'] == 'notificar_vendedor_despacho_gestionado'){

            $asunto = 'Despacho gestionado : '. $this->datos['placa'].'-'.$this->datos['conductor'].' | Vendedor : '.$this->datos['nombre_vendedor'];
            return $this->view('despachoCerrado')->subject($asunto);

        } else if ($this->datos['accion'] == 'terminar_cargue'){

            $asunto = $this->datos['placa'].' Facturar';

            return $this->view('cargueTerminado')
                        //->from('auxiliarventas@icoharinas.com.co')
                        //->cc($this->datos['email_vendedor'], 'Vendedor')
                        ->cc($this->datos['email_facturacion'], 'Facturación')
                        //->cc($this->datos['email_aux_ventas_2'], 'Auxiliar comercial')
                        ->subject($asunto);
        } else if ($this->datos['accion'] == 'notifica_certificado_calidad'){

            $asunto = $this->datos['asunto'];

            return $this->view('despachoCerrado')
                        ->cc($this->datos['copia_email_calidad'], 'Auxiliar Ventas')
                        ->subject($asunto);
        } else if ($this->datos['accion'] == 'notifica_clientes_sin_venta'){
            $asunto = 'Atención - Reporte de clientes sin venta mes de ('.$this->datos['mes_actual'].')';
            //$adjunto = $this->datos['reporte'];
            return $this->view('notificaReporteClientesSinVenta')
                        ->cc($this->datos['email_lider_ventas'], 'Líder Ventas')
                        ->cc($this->datos['email_cartera'], 'Jefe Cartera')
                        //->attachData($adjunto, 'ReporteClientes.xlsx')
                        ->attachData(Excel::raw(new ClientesSinVentaExport, \Maatwebsite\Excel\Excel::XLSX), 'Reporte_Clientes_'.date('Y-m-d').'.xlsx')
                        ->subject($asunto);
        } else if ($this->datos['accion'] == 'gestionar_entrega'){
            $novedad = $this->datos['novedad'] == 'si' ? 'CON novedad' : 'SIN novedad';
            $asunto = 'Atención - Entrega de mercancía '.$novedad;
            return $this->view('notificaNovedadEntrega')
                        ->subject($asunto);
        }

    }
}
