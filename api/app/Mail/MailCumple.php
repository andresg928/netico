<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailCumple extends Mailable
{
    use Queueable, SerializesModels;
    public $datos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            $asunto = '¡Feliz Cumpleaños!';
            return $this->view('mensajeCumple')
                        //->from('auxiliarventas@icoharinas.com.co')
                        //->cc($this->datos['email_vendedor'], 'Vendedor')
                        //->cc(rtrim($this->datos->EMAIL))
                        ->subject($asunto);

    }
}
