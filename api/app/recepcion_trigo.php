<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recepcion_trigo extends Model
{
    protected $table = 'recepcion_trigo';
    protected $primaryKey = 'id_registro';
    protected $keyType = 'string';
    protected $fillable = ['id_registro', 'fecha', 'placa', 'motonave','id_silo', 'consecutivo', 'bl', 'tiquete1', 'peso_entrada1', 'peso_salida1', 'peso_neto1', 'acumulado1', 
                            'hora_salida', 'tiquete2', 'peso_entrada2', 'peso_salida2', 'peso_neto2', 'acumulado2', 'hora_llegada', 'diferencia', 'tiempo_recorrido', 
                            'usu_registra'];
}
