<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class despachos_trigo extends Model
{
    protected $table = 'despachos_trigo';
    protected $primaryKey = 'id_registro';
    protected $keyType = 'string';
    protected $fillable = ['id_registro', 'deleted', 'motonave', 'bl','id_transportadora','nombre_transportadora', 'estado', 'fecha', 'placa', 'tiquete', 'id_silo', 'destino', 'peso_entrada', 
                            'peso_salida', 'peso_neto', 'acumulado', 'saldo', 'precintos', 'usu_registra', 'usu_modifica'];
}
