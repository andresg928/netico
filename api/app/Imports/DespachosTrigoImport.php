<?php

namespace App\Imports;

use App\despachos_trigo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use App\Http\Controllers\DespachosTrigoController;
use App\Http\Controllers\SilosMovimientoController;
use App\Http\Controllers\SiloController;
use Illuminate\Support\Facades\DB;

class DespachosTrigoImport implements ToModel, WithHeadingRow,WithCalculatedFormulas
{

    public function  __construct($data){
        $this->data = $data;
    }

    public function obtenerRegistroDespacho($fields){
        $registro = despachos_trigo::select('id_registro')->where('placa', $fields['placa'])->where('fecha', $fields['fecha'])->where('deleted', '0')->first();
        return $registro;
    }

    public function validarFilaExcel($row){
        if($row['item'] == '' || $row['fecha'] == '' || $row['placa'] == '' || $row['tiquete'] == '' || $row['transportadora'] == '' || $row['destino'] == '' || $row['tara'] == '' || $row['peso_bruto'] == '' || $row['peso_neto'] == '' || $row['acumulado'] == '' || $row['precintos'] == ''){
            return 'stop';
        }
        return 'next';
    }

    public function registraCargueBascula($data){

        // Esto sucede cuando se saca una parte de una bodega y la otra de otra bodega y la transporta la misma mula, en la columna item del excel deben poner la palabra complete para que pase por acá
        if($data['item'] == 'complete'){

            $cargue = DB::connection('mysql_bascula')->table('cargues')->where('placa', $data['placa'])->where('estado', 'activo')->orderBy('created_at','desc')->first();

            $cargue_update = DB::connection('mysql_bascula')->table('cargues')->where('placa', $data['placa'])->where('estado', 'activo')->update(['peso_origen'=> ($cargue->peso_origen + $data['peso_neto']) ]);

        } else {

            //$existe_registro = DB::connection('mysql_bascula')->table('cargues')->where('placa', $data['placa'])->where('estado', 'activo')->get();

            //if(count($existe_registro) == 0){
                DB::connection('mysql_bascula')->table('cargues')->insert([
                    'id' => Uuid::generate()->string,
                    'placa' => $data['placa'],
                    'nit_empresa' => $data['nit_transportadora'],
                    'peso_origen' => $data['peso_neto'],
                    'estado' => 'activo',
                    'id_usuario' => JWTAuth::user()->vendedor,
                    'observaciones' => $data['motonave'].'-'.$data['nombre_bodega'],
                    'documento' => '-',
                    'motonave' => $data['motonave'],
                    'id_despacho_trigo' => $data['id_despacho_trigo'],
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            //} 
        }
    }

    public function registrarPlaca($data){

        $vehiculo = DB::connection('mysql_bascula')->table('vehiculos')->where('placa', $data['placa'])->where('deleted', '0')->get();

        if(count($vehiculo) == 0){
            DB::connection('mysql_bascula')->table('vehiculos')->insert([
                    'id' => Uuid::generate()->string,
                    'placa' => $data['placa'],
                    'created_at' => date('Y-m-d H:i:s'),
            ]);               
        }
    }

    public function model(array $row)
    {
        $validar_fila = $this->validarFilaExcel($row);
        //dd($validar_fila);
        if($validar_fila == 'stop'){
            return null;
        }

        $desp_trigo_crtl = new DespachosTrigoController;
        $ctl_silos_mvto = new SilosMovimientoController();
        $silos_ctrl = new SiloController();

        $fields = array();
        $fields['placa'] = $row['placa'];
        $fields['fecha'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha']);
        $exist = $this->obtenerRegistroDespacho($fields);
        if($exist){return null;}

        $transportadora = $desp_trigo_crtl->obtenerDetalleTransportadora('nombre', $row['transportadora']);

        $data = array();
        $data['item'] = strtolower(trim($row['item']));
        $data['placa'] = $row['placa'];
        $data['nit_transportadora'] = $transportadora->nit;
        $data['peso_neto'] = $row['peso_neto'];
        $data['motonave'] = $this->data['motonave'];
        $this->registrarPlaca($data);
        $info_silo = $silos_ctrl->obtenerInfoSiloxId($this->data['silo']);
        $data['nombre_bodega'] = $info_silo['nombre'];
        $id_despachos_trigo = Uuid::generate()->string;
        $data['id_despacho_trigo'] = $id_despachos_trigo;
        $this->registraCargueBascula($data);
        //registrar movimiento
        $cantidad_ton = $row['peso_neto']/1000;
        $observaciones_movimiento = 'Placa '.$row['placa'].' motonave '.$this->data['motonave'];
        $ctl_silos_mvto->registrarMovimientoSinRequest($this->data['silo'], 'salida', $row['peso_neto'], $cantidad_ton, $observaciones_movimiento, $id_despachos_trigo);

        return new despachos_trigo([
            'id_registro' => $id_despachos_trigo,
            'fecha' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha']),
            'motonave' => $this->data['motonave'],
            'id_silo' => $this->data['silo'],
            'placa' => $row['placa'],
            'id_transportadora' => $transportadora->id,
            'nombre_transportadora' => $row['transportadora'],
            'bl' => $this->data['bl'],
            'tiquete' => $row['tiquete'],
            'destino' => $row['destino'],
            'peso_entrada' => $row['tara'],
            'peso_salida' => $row['peso_bruto'],
            'peso_neto' => $row['peso_neto'],
            'acumulado' => $row['acumulado'],
            'precintos' => $row['precintos'],
            'usu_registra' => JWTAuth::user()->vendedor,
        ]);
    }
    /*public function rules(): array{
        // Nombre de las columnas de excel a importar
        return [
            'fecha' => 'required',
            'placa' => 'required',
            'transportadora' => 'required',
            'tiquete' => 'required',
            'destino' => 'required',
            'tara' => 'required',
            'peso_bruto' => 'required',
            'peso_neto' => 'required',
            'acumulado' => 'required',
            'precintos' => 'required',
        ];
    }*/
    public function headingRow(): int
    {
        return 1;
    }
}
