<?php

namespace App\Imports;

use App\recepcion_trigo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Webpatser\Uuid\Uuid;
use App\Http\Controllers\SilosMovimientoController;
use  JWTAuth;

class RecepcionTrigoImport implements ToModel, WithHeadingRow, WithValidation,WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function  __construct($data)
    {
        $this->data = $data;
    }

    public function obtenerRegistroRecepcion($fields){
        $registro = recepcion_trigo::select('id_registro')->where('placa', $fields['placa'])
                                                          ->where('consecutivo', $fields['consecutivo'])
                                                          ->where('tiquete1', $fields['tiquete1'])
                                                          ->where('deleted', '0')
                                                          ->first();
        return $registro;
    }

    public function model(array $row)
    {
        $ctl_silos_mvto = new SilosMovimientoController();
        $fields = array();
        $fields['placa'] = $row['placa'];
        $fields['consecutivo'] = $row['consecutivo'];
        $fields['tiquete1'] = $row['tiquete1'];
        $exist = $this->obtenerRegistroRecepcion($fields);
        if($exist){return null;}
        $cantidad_ton = $row['peso_neto2']/1000;
        $id_recepcion_trigo = Uuid::generate()->string;
        $ctl_silos_mvto->registrarMovimientoSinRequest($this->data['silo'], 'descargue', $row['peso_neto2'], $cantidad_ton, '', $id_recepcion_trigo);

        return new recepcion_trigo([
            'id_registro' => $id_recepcion_trigo,
            'fecha' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha']),
            'motonave' => $this->data['motonave'],
            'id_silo' => $this->data['silo'],
            'placa' => $row['placa'],
            'consecutivo' => $row['consecutivo'],
            'bl' => $row['bl'],
            'tiquete1' => $row['tiquete1'],
            'peso_entrada1' => $row['peso_entrada1'],
            'peso_salida1' => $row['peso_salida1'],
            'peso_neto1' => $row['peso_neto1'],
            'acumulado1' => $row['acumulado1'],
            'hora_salida' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['hora_salida']),
            'tiquete2' => $row['tiquete2'],
            'peso_entrada2' => $row['peso_entrada2'],
            'peso_salida2' => $row['peso_salida2'],
            'peso_neto2' => $row['peso_neto2'],
            'acumulado2' => $row['acumulado2'],
            'hora_llegada' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['hora_llegada']),
            'diferencia' => $row['diferencia'],
            'tiempo_recorrido' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tiempo_recorrido']),
            'usu_registra' => JWTAuth::user()->vendedor,
        ]);
    }

    public function rules(): array{
        
        return [
            'fecha' => 'required',
            'placa' => 'required',
            'consecutivo' => 'required',
            'bl' => 'required',
            'tiquete1' => 'required',
            'peso_entrada1' => 'required',
            'peso_salida1' => 'required',
            'peso_neto1' => 'required',
            'acumulado1' => 'required',
            'hora_salida' => 'required',
            'tiquete2' => 'required',
            'peso_entrada2' => 'required',
            'peso_salida2' => 'required',
            'peso_neto2' => 'required',
            'acumulado2' => 'required',
            'hora_llegada' => 'required',
            'diferencia' => 'required',
            'tiempo_recorrido' => 'required',
        ];
    }
    public function headingRow(): int
    {
        return 1;
    }
}
