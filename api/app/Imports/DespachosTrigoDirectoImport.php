<?php

namespace App\Imports;

use App\despachos_trigo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Carbon\Carbon;
use App\Http\Controllers\DespachosTrigoController;
use App\Http\Controllers\SilosMovimientoController;
use App\Http\Controllers\SiloController;
use Illuminate\Support\Facades\DB;

class DespachosTrigoDirectoImport implements ToModel, WithHeadingRow,WithCalculatedFormulas
{

    public function  __construct($data){
        $this->data = $data;
    }

    public function obtenerRegistroDespacho($fields){
        $registro = despachos_trigo::select('id_registro')->where('placa', $fields['placa'])->where('fecha', $fields['fecha'])->where('deleted', '0')->first();
        return $registro;
    }

    public function validarFilaExcel($row){
        if($row['motonave'] == '' || $row['cliente'] == '' || $row['planta_destino'] == '' || $row['bl'] == '' || $row['producto'] == '' || $row['radicado'] == '' || $row['placa'] == '' || $row['documento'] == '' || $row['fecha_movimiento'] == '' || $row['peso_movimiento'] == '' || $row['tipo_despacho'] == '' || $row['transportador'] == ''){
            return 'stop';
        }
        return 'next';
    }

    public function registraCargueBascula($data){

        $existe_registro = DB::connection('mysql_bascula')->table('cargues')->where('placa', $data['placa'])->where('estado', 'activo')->get();

        if(count($existe_registro) == 0){

            DB::connection('mysql_bascula')->table('cargues')->insert([
                'id' => Uuid::generate()->string,
                'placa' => $data['placa'],
                'nit_empresa' => $data['nit_transportadora'],
                'peso_origen' => $data['peso_neto'],
                'estado' => 'activo',
                'id_usuario' => JWTAuth::user()->vendedor,
                'observaciones' => $data['motonave'].' - DIRECTO',
                'documento' => '-',
                'motonave' => $data['motonave'],
                'id_despacho_trigo' => $data['id_despacho_trigo'],
                'created_at' => date('Y-m-d H:i:s'),
            ]);

        }
      
    }

    public function registrarPlaca($data){

        $vehiculo = DB::connection('mysql_bascula')->table('vehiculos')->where('placa', $data['placa'])->where('deleted', '0')->get();

        if(count($vehiculo) == 0){
            DB::connection('mysql_bascula')->table('vehiculos')->insert([
                    'id' => Uuid::generate()->string,
                    'placa' => $data['placa'],
                    'created_at' => date('Y-m-d H:i:s'),
            ]);               
        }
    }

    public function model(array $row)
    {

        $dia = substr(strval($row['fecha_movimiento']), 0, 2);
        $mes = substr(strval($row['fecha_movimiento']), 3, 2);
        $año = substr(strval($row['fecha_movimiento']), 6, 4);

        //Para los dias que tiene el /, osea que no vienen de 2 caracteres
        if (strpos($dia, '/') !== false) {
            $dia='0'.intval(preg_replace('/[^0-9]+/', '', $dia), 10);
            $mes = substr(strval($row['fecha_movimiento']), 2, 2);
            $año = substr(strval($row['fecha_movimiento']), 5, 4);
        }

        $fecha = Carbon::parse($dia.'-'.$mes.'-'.$año);

        $validar_fila = $this->validarFilaExcel($row);

        if($validar_fila == 'stop'){
            return null;
        }

        $desp_trigo_crtl = new DespachosTrigoController;
        $ctl_silos_mvto = new SilosMovimientoController();

        $fields = array();
        $fields['placa'] = $row['placa'];
        $fields['fecha'] = $fecha->format('Y-m-d');
        $exist = $this->obtenerRegistroDespacho($fields);
        if($exist){return null;}

        $transportadora = $desp_trigo_crtl->obtenerDetalleTransportadora('nombre', $row['transportador']);

        $data = array();
        $data['placa'] = $row['placa'];
        $data['nit_transportadora'] = $transportadora->nit;
        $data['peso_neto'] = $row['peso_movimiento'] <=100 &&  $row['peso_movimiento'] >0 ? $row['peso_movimiento'] * 1000 : $row['peso_movimiento'];
        $data['motonave'] = $this->data['motonave'];
        $this->registrarPlaca($data);
        $id_despachos_trigo = Uuid::generate()->string;
        $data['id_despacho_trigo'] = $id_despachos_trigo;
        $this->registraCargueBascula($data);
        //registrar movimiento
        $cantidad_ton = $row['peso_movimiento'];
        $observaciones_movimiento = 'Placa '.$row['placa'].' motonave '.$this->data['motonave'];
        $ctl_silos_mvto->registrarMovimientoTrigoDirecto('DIRECTO', 'directo', $data['peso_neto'], $cantidad_ton, $observaciones_movimiento, $id_despachos_trigo);

        return new despachos_trigo([
            'id_registro' => $id_despachos_trigo,
            'fecha' => $fecha->format('Y-m-d'),
            'motonave' => $this->data['motonave'],
            'id_silo' => 'DIRECTO',
            'placa' => $row['placa'],
            'id_transportadora' => $transportadora->id,
            'nombre_transportadora' => $row['transportador'],
            'bl' => $row['bl'],
            'tiquete' => $row['radicado'],
            'destino' => 'ICOHARINAS',
            'peso_neto' => $data['peso_neto'],
            'precintos' => 'N/A',
            'estado' => 'directo',
            'usu_registra' => JWTAuth::user()->vendedor,
        ]);
    }

    public function headingRow(): int
    {
        // Desde que fila empieza a leer los datos
        return 2;
    }
}
