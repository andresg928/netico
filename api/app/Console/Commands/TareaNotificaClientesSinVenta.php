<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DespachoController;

class TareaNotificaClientesSinVenta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tarea:not-cli-sin-venta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se ejecuta correo electrónico cada inicio de mes, este contiene un adjunto con clientes que no compraron el mes anterior';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ctl_despacho = new DespachoController();
        $ctl_despacho->enviarInformeClientesSinVenta();
    }
}
