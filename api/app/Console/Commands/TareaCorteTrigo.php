<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\SilosCorteController;

class TareaCorteTrigo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:corte_trigo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que ejecutará una tarea todos los dias a las 06:00 am obteniendo el saldo de cada silo de trigo y lo envia a la tabla de cortes de trigo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $ctl_corte_silo = new SilosCorteController();
        $ctl_corte_silo->generarCorteTrigo();
        $ctl_corte_silo->obtenerCumpleaneros();
    }
}
