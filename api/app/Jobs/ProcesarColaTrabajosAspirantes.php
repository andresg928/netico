<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\AspiranteController;

class ProcesarColaTrabajosAspirantes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datos;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($datos = array())
    {
        //$this->datos = array();
        $this->datos[] = $datos;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //dd($this->datos[0]);
        $aspirante_ctr = new AspiranteController();
        $aspirante_ctr->enviarNotificacionEmail($this->datos[0]);
    }
}
