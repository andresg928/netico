<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->string('codigo_agrupa',10);
            $table->string('anio',4);
            $table->string('enero',7)->default(0);
            $table->string('febrero',7)->default(0);
            $table->string('marzo',7)->default(0);
            $table->string('abril',7)->default(0);
            $table->string('mayo',7)->default(0);
            $table->string('junio',7)->default(0);
            $table->string('julio',7)->default(0);
            $table->string('agosto',7)->default(0);
            $table->string('septiembre',7)->default(0);
            $table->string('octubre',7)->default(0);
            $table->string('noviembre',7)->default(0);
            $table->string('diciembre',7)->default(0);
            $table->string('usu_registra',10);
            $table->string('usu_modifica',10)->nullable();
            $table->primary(['codigo_agrupa', 'anio']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}
