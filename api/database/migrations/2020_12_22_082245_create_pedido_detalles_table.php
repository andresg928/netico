<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_detalles', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->string('id_pedido',36);
            $table->string('cod_producto', 10);
            $table->string('cod_grupo', 5)->nullable();
            $table->string('nom_producto', 100);
            $table->string('valor', 10);
            $table->string('peso', 10);
            $table->string('cantidad', 6);
            $table->integer('cantidad_despachada')->default(0);
            $table->string('unidad_medida', 15);
            $table->string('usu_mod', 40)->nullable();
            $table->string('nota', 100)->nullable();
            $table->string('nro_pedido_ofima', 15)->nullable();
            $table->string('cantidad_old', 6)->default(0);
            $table->string('usu_bodega', 20)->nullable();
            $table->string('fec_reg_bodega', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_detalles');
    }
}
