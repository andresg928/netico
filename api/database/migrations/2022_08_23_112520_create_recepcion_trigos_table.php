<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecepcionTrigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepcion_trigo', function (Blueprint $table) {
            $table->uuid('id_registro')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('id_silo', 36);
            $table->string('motonave', 30);
            $table->date('fecha');
            $table->string('placa', 7);
            $table->string('consecutivo', 12);
            $table->string('bl', 10);
            $table->string('tiquete1', 15);
            $table->integer('peso_entrada1');
            $table->integer('peso_salida1');
            $table->integer('peso_neto1');
            $table->integer('acumulado1');
            $table->time('hora_salida');
            $table->string('tiquete2', 15);
            $table->integer('peso_entrada2');
            $table->integer('peso_salida2');
            $table->integer('peso_neto2');
            $table->integer('acumulado2');
            $table->time('hora_llegada');
            $table->integer('diferencia');
            $table->time('tiempo_recorrido');
            $table->string('usu_registra', 12);
            $table->string('usu_modifica', 12)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepcion_trigo');
    }
}
