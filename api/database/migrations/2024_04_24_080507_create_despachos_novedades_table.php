<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachosNovedadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachos_novedades', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('id_turno', 36);
            $table->string('novedad', 2)->default('no');
            $table->text('observaciones')->nullable();
            $table->dateTime('fecha_novedad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despachos_novedades');
    }
}
