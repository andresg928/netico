<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->bigIncrements('consecutivo');
            $table->date('fecha_entrega')->nullable();
            $table->string('vendedor',10);
            $table->string('documento_vendedor',15)->nullable();
            $table->string('placa',9);
            $table->string('capacidad',3);
            $table->string('conductor',50);
            $table->string('nit_conductor',20)->nullable();
            $table->string('usu_mod',40)->nullable();
            $table->text('observaciones')->nullable();
            $table->string('correo_enviado',2)->default('no');
            $table->string('fec_cerrado',20)->nullable();
            $table->string('estado',20);
            $table->string('autorizado',2)->default('no');
            $table->string('usu_autorizado',40)->nullable();
            $table->string('fec_autorizado',20)->nullable();
            $table->string('cargado',2)->default('no');
            $table->string('fec_reg_cargado',25)->nullable();
            $table->string('usu_reg_cargado',20)->nullable();
            $table->string('compartido',2)->default('no');
            $table->string('compartido_cod_vendedor', 15)->nullable();
            $table->string('compartido_nom_vendedor', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despachos');
    }
}
