<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAspirantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirantes', function (Blueprint $table) {
            //Info Personal
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->string('documento_identidad',15);
            $table->date('fecha_expedicion');
            $table->string('apellido1',35);
            $table->string('apellido2',35)->nullable();
            $table->string('nombre1',35);
            $table->string('nombre2',35)->nullable();
            $table->string('nombre_completo',150);
            $table->string('celular',35);
            $table->string('correo',120);
            $table->string('cargo',120);
            //Procesos
            $table->string('preseleccion',2)->default('no');
            $table->dateTime('preseleccion_fecha_usu_reg')->nullable();
            $table->string('preseleccion_usu_reg',20)->nullable();

            $table->string('entrevista',2)->default('no');
            $table->string('entrevista_entrevistador',150)->nullable();
            $table->date('entrevista_fecha_cita')->nullable();
            $table->string('entrevista_hora_cita',10)->nullable();
            $table->dateTime('entrevista_fecha_usu_reg')->nullable();
            $table->string('entrevista_usu_reg',20)->nullable();
            $table->string('entrevista_aprobo',2)->default('no');
            $table->text('entrevista_observacion')->nullable();

            $table->string('pruebas',2)->default('no');
            $table->date('pruebas_fecha')->nullable();
            $table->string('pruebas_hora',10)->nullable();
            $table->string('pruebas_lugar',250)->nullable();
            $table->dateTime('pruebas_fecha_usu_reg')->nullable();
            $table->string('pruebas_usu_reg',20)->nullable();
            $table->string('pruebas_aprobo',2)->default('no');
            $table->text('pruebas_observacion')->nullable();

            $table->string('examen',2)->default('no');
            $table->date('examen_fecha')->nullable();
            $table->string('examen_hora',10)->nullable();
            $table->string('examen_lugar',250)->nullable();
            $table->dateTime('examen_fecha_usu_reg')->nullable();
            $table->string('examen_usu_reg',20)->nullable();
            $table->string('examen_aprobo',2)->default('no');
            $table->text('examen_observacion')->nullable();

            $table->string('contratacion',2)->default('no');
            $table->date('contratacion_fecha')->nullable();
            $table->string('contratacion_hora',10)->nullable();
            $table->date('contratacion_fecha_fin_periodo_prueba')->nullable();
            $table->dateTime('contratacion_fecha_usu_reg')->nullable();
            $table->string('contratacion_usu_reg',20)->nullable();
            $table->text('contratacion_observaciones')->nullable();
            $table->string('proceso_finalizado',2)->default('no');
            $table->string('adjunto',36)->default('no');
            $table->string('estado_proceso',30)->default('sin_gestionar');

            $table->string('usuario_registra',20);
            $table->string('usuario_modifica',20)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirantes');
    }
}
