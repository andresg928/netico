<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->bigIncrements('consecutivo');
            $table->string('nit',20);
            $table->string('cliente',100);
            $table->string('ciudad',60)->nullable();
            $table->string('estado',15)->default('abierto');
            $table->string('id_despacho',36);
            $table->text('observaciones')->nullable();
            $table->string('valor_total',11);
            $table->string('total_kilos',11);
            $table->string('bodega',2)->default('no');
            $table->string('certificado_calidad',2)->default('no');
            $table->text('observaciones_certificado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
