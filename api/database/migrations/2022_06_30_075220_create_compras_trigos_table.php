<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTrigosTable extends Migration
{

    public function up()
    {
        Schema::create('compras_trigo', function (Blueprint $table) {
            $table->uuid('id_registro')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->date('fecha_embarque1');
            $table->date('fecha_embarque2');
            $table->string('nit_proveedor', 15);
            $table->string('nombre_proveedor', 100);
            $table->string('origen_trigo', 40);
            $table->string('motonave', 40)->nullable();
            $table->string('adjunto', 45)->nullable();
            $table->string('tipo_trigo1', 25)->nullable();
            $table->string('proteina1', 5)->nullable();
            $table->integer('cantidad1')->nullable();
            $table->string('precio_base1', 15)->nullable();
            $table->integer('precio_final1')->nullable();
            $table->string('tipo_trigo2', 25)->nullable();
            $table->string('proteina2', 5)->nullable();
            $table->integer('cantidad2')->nullable();
            $table->string('precio_base2', 15)->nullable();
            $table->integer('precio_final2')->nullable();
            $table->string('tipo_trigo3', 25)->nullable();
            $table->string('proteina3', 5)->nullable();
            $table->integer('cantidad3')->nullable();
            $table->string('precio_base3', 15)->nullable();
            $table->integer('precio_final3')->nullable();
            $table->string('usu_registra', 15)->nullable();
            $table->string('usu_modifica', 15)->nullable();
            $table->string('usu_elimina', 15)->nullable();
            $table->string('inactivo', 2)->default('no');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('compras_trigo');
    }
}
