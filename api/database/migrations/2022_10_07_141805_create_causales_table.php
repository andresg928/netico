<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCausalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('causales', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('id_area', 36);
            $table->string('codigo', 15);
            $table->string('nombre', 100);
            $table->string('usu_registra', 40);
            $table->string('usu_modifica', 40)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('causales');
    }
}
