<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMercadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercados', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->date('fecha');
            $table->string('nit',15);
            $table->string('nombre',150);
            $table->string('marca', 50)->nullable();
            $table->string('nit_fabricante', 20)->nullable();
            $table->string('fabricante', 50)->nullable();
            $table->string('cod_zona',10);
            $table->string('nom_zona',70);
            $table->string('cod_producto',10);
            $table->string('nom_producto',150);
            $table->string('adjunto', 40)->nullable()->default('-');
            $table->integer('precio');
            $table->string('usu_registro',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercados');
    }
}
