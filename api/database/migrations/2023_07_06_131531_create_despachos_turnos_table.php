<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachosTurnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachos_turnos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted',1)->default(0);
            $table->string('id_despacho',36);
            $table->string('estado',15)->default('reportado');
            $table->string('id_ficho',3);
            $table->string('celular',15);
            $table->string('ciudad',40);
            $table->string('usuario_reporta',20);
            $table->string('fecha_hora_reporta',20);
            $table->string('usuario_anula',20)->nullable();
            $table->string('fecha_hora_anula',20)->nullable();
            $table->string('usuario_cargue',20)->nullable();
            $table->string('fecha_hora_cargue',20)->nullable();
            $table->string('usuario_en_espera',20)->nullable();
            $table->string('fecha_hora_en_espera',20)->nullable();
            $table->string('usuario_elimina',20)->nullable();
            $table->string('fecha_hora_elimina',20)->nullable();
            $table->string('usuario_finaliza',20)->nullable();
            $table->string('fecha_hora_finaliza',20)->nullable();
            $table->string('usuario_salida',20)->nullable();
            $table->string('fecha_hora_salida',20)->nullable();
            $table->string('usuario_autoriza_entrada',20)->nullable();
            $table->string('fecha_hora_autoriza_entrada',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despachos_turnos');
    }
}
