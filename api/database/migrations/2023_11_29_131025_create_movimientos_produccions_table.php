<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosProduccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos_produccions', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('id_maestro', 36);
            $table->date('fecha_produccion');
            $table->string('lote', 12)->nullable();
            $table->string('gestionado', 1)->default('0');
            $table->string('usu_gestiona', 20)->nullable();
            $table->dateTime('fec_hor_gestiona')->nullable();
            $table->string('nro_dcto_erp', 10)->nullable();
            $table->string('usu_registra', 20);
            $table->string('usu_modifica', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos_produccions');
    }
}
