<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitaClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visita_clientes', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->date('fecha');
            $table->string('cod_vendedor', 15);
            $table->string('nom_vendedor', 70);
            $table->string('nit_cliente', 20);
            $table->string('nom_cliente', 150);
            $table->string('presencial', 2)->default('si');
            $table->string('operacion', 20);
            $table->string('id_pedido', 36)->nullable();
            $table->string('estado', 15)->default('pendiente'); // pendiente
            $table->text('comentarios')->nullable();
            $table->string('valor_cobro',10)->nullable();
            $table->string('usu_registro',15)->nullable();
            $table->dateTime('fec_registro')->nullable();
            $table->string('usu_modifica',15)->nullable();
            $table->dateTime('fec_modifica')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visita_clientes');
    }
}
