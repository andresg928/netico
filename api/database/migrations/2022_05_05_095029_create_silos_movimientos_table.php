<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSilosMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silos_movimientos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default(0);
            $table->string('id_silo', 36);
            $table->string('tipo_movimiento', 20);
            $table->float('valor');
            $table->string('observaciones', 100)->nullable();
            $table->string('id_pesaje', 36)->nullable();
            $table->string('id_recepcion_trigo', 36)->nullable();
            $table->string('usu_registro', 15);
            $table->string('usu_modifica', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('silos_movimientos');
    }
}
