<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovedadSistemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novedad_sistema', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default(0);
            $table->string('tipo', 15);
            $table->string('mensaje', 200);
            $table->string('usu_registro', 10)->nullable();
            $table->string('usu_modifica', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novedad_sistema');
    }
}
