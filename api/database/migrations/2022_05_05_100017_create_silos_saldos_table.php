<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSilosSaldosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silos_saldos', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('id_silo', 36);
            $table->float('saldo');
            $table->string('usu_registro', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('silos_saldos');
    }
}
