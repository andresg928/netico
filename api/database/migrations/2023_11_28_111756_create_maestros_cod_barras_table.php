<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaestrosCodBarrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maestros_cod_barras', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('cod_barras', 60);
            $table->string('consecutivo', 6);
            $table->string('nombre_consecutivo', 150);
            $table->string('cod_producto', 9);
            $table->string('nom_producto', 150);
            $table->string('unidad_medida', 6);
            $table->integer('cantidad');
            $table->string('tipo_lote', 10);
            $table->text('observaciones')->nullable();
            $table->string('usu_registra', 20);
            $table->string('usu_modifica', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maestros_cod_barras');
    }
}
