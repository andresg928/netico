<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachosTrigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachos_trigo', function (Blueprint $table) {
            $table->uuid('id_registro')->unique()->index();
            $table->string('deleted', 1)->default('0');
            $table->string('motonave', 30);
            $table->string('bl', 20);
            $table->string('estado', 15)->default('en_transito');
            $table->date('fecha');
            $table->string('placa', 7);
            $table->string('tiquete', 15);
            $table->string('id_transportadora', 36);
            $table->string('nombre_transportadora', 150);
            $table->string('id_silo', 36);
            $table->string('destino', 36);
            $table->integer('peso_entrada')->default(0);
            $table->integer('peso_salida')->default(0);
            $table->integer('peso_neto')->default(0);
            $table->integer('acumulado')->default(0);
            $table->integer('saldo')->default(0);
            $table->string('precintos', 100);
            $table->string('usu_registra', 12);
            $table->string('usu_modifica', 12)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despachos_trigo');
    }
}
