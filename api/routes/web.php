<?php

Route::get('api/limpiar-proyecto', function () {
    echo Artisan::call('config:clear');
    echo Artisan::call('config:cache');
    echo Artisan::call('cache:clear');
    echo Artisan::call('route:clear');
    echo Artisan::call('view:clear');
 });

 // Consulta para landing conductores
 Route::get('api/despachos-turno/consulta-turno-conductores/', 'DespachosTurnoController@consultaTurnoVistaConductores');
 Route::get('api/despachos-turno/listado-reportados-conductor/', 'DespachosTurnoController@obtenerTurnosPendientes');
 Route::get('api/despachos/consulta-por-placa-conductor', 'DespachoController@consultaDespacho');
 
// XML
//Route::get('api/read-xml', 'DespachosTrigoController@readXML');
Route::get('api/silos-corte/moje', 'SilosMovimientoController@ActualizaCorteMoje');
//Route::get('api/cumple', 'SilosCorteController@obtenerCumpleaneros');

//USUARIOS
Route::post('api/usuarios/login', 'AuthController@inicioSesion');
Route::post('api/usuarios/registro', 'AuthController@register');
Route::get('api/usuarios/listado-usuarios', 'AuthController@listadoUsuarios');
Route::put('api/usuarios/actualiza-usuarios/{id}', 'AuthController@actualizaUsuario');
Route::put('api/usuarios/actualizar-clave', 'AuthController@actualizarClave');

Route::get('api/cartera/cupo', 'DespachoController@obtenerCupoDisponible');

//EMPRESAS
Route::post('api/empresas/registrar', 'EmpresaController@store');
Route::get('api/empresas/listado/', 'EmpresaController@index');

//ROLES
Route::post('api/rol/registrar', 'RolController@store');
Route::get('api/rol/listado/{tipo?}', 'RolController@index');
Route::get('api/rol/listar-permisos/', 'RolController@listarPermisos');
Route::put('api/rol/editar/{id_registro}', 'RolController@update');
Route::post('api/rol/asignar-permisos', 'RolController@asignarPermisosRol');
Route::get('api/permiso/verifica/', 'RolController@verificaPermiso');
Route::get('api/ultimo-dia/', 'PresupuestoController@obtenerUltimoDiaMes');
Route::get('api/mercado/descargar-documento/{archivo}', 'MercadoController@downloadFile');
// Descargar contrato compra trigo
Route::get('api/compras-trigo/descargar-documento/{archivo}', 'ComprasTrigoController@downloadFile');

// Descargar HV
Route::get('api/aspirante/descargar-hv/{archivo}', 'AspiranteController@downloadFile');

Route::get('api/cargue/obtener-inicio-fin-mes', 'PedidoController@obtenerDiaInicioFinMes');
Route::get('api/mercado/obtener-datos-global/', 'MercadoController@obtenerListaGlobal');
Route::get('api/logistica/file-import-recepcion-trigo/', 'RecepcionTrigoController@downloadFile');
Route::get('api/logistica/file-import-envio-trigo/', 'DespachosTrigoController@downloadFile');

// CONSUME PowerBi
// Api presupuestos power bi
Route::get('api/presupuestos/obtener/', 'PresupuestoVendedoresController@obtenerPresupuestosApiPowerBi');
Route::get('api/logistica/obtener-info-bascula/', 'CargueController@obtenerInfoBasculaApiPowerBi');
Route::get('api/logistica/obtener-info-peso-conductor/', 'CargueController@obtnerInfoPesoxConductorApiPowerBi');

//DESPACHOS NOVEDAD
Route::post('api/despachos-novedad/registrar', 'DespachosNovedadesController@registrarNovedadDespacho');

Route::group(['middleware' => 'auth.jwt'], function () {

    //USUARIOS
    Route::post('api/usuarios/cerrar-sesion', 'AuthController@logout');
    Route::post('api/usuarios/obtener-usuario', 'AuthController@getAuthUser');
    Route::get('api/usuarios/personal-activo-ofima', 'AuthController@obtenerPersonalActivoOfima');

    //CONDCUTORES OFIMA
    Route::get('api/conductores/listado/', 'DespachoController@obtenerInfoPlacaConductoresOfima');

    //DESPACHOS
    Route::post('api/despachos/registrar/', 'DespachoController@store');
    Route::get('api/despachos/listado/', 'DespachoController@obtenerInformacionDetallada');
    Route::get('api/despachos/info-x-consecutivo/', 'DespachoController@obtenerInfoDespachoxConsecutivo');
    Route::put('api/despachos/cerrar-despacho/', 'DespachoController@cerrarDespacho');
    Route::put('api/despachos/actualiza-despacho/', 'DespachoController@actualizaDespacho');
    Route::get('api/despachos/obtener-numero/', 'DespachoController@obtenerNumeroDespachosxEstado');
    Route::get('api/listado/despachos/', 'DespachoController@listadoDespachos');
    Route::put('api/despacho/ingresar-fecha-facturacion/', 'DespachoController@actualizaDespachoFechaFacturacion');
    Route::get('api/despachos/proyeccion-total-bultos', 'DespachoController@obtenerProductosDespachoPorFecha');
    Route::get('api/despachos/consulta-por-placa', 'DespachoController@consultaDespacho');
    Route::get('api/despachos/productos-por-despachos', 'DespachoController@obtenerProductosPorDespacho');
    Route::put('api/despachos/editar-detalle-pedido', 'DespachoController@editarDetallePedido');
    Route::put('api/despachos/terminar-cargue', 'DespachoController@terminarDespachoCargue');
    Route::put('api/despachos/compartir', 'DespachoController@compartirDespacho');
    Route::get('api/despachos/top-precio-productos', 'DespachoController@obtenerPrecioUltimosProductos');

    //CLIENTES
    Route::get('api/clientes/listado/', 'DespachoController@obtenerClientesOfima');
    Route::get('api/clientes/listado-clientes/', 'DespachoController@obtenerListadoClientes');

    // PRODUCTOS
    Route::get('api/productos/listado/', 'DespachoController@obtenerProductosOfima');

    //PEDIDOS
    Route::post('api/pedidos/registrar/', 'PedidoController@store');
    Route::get('api/pedidos/pedidos-x-despacho/', 'PedidoController@obtenerPedidosxDespacho');
    Route::get('api/pedidos/pedidos-detalle/', 'PedidoController@obtenerDetallePedido');
    Route::put('api/pedidos/eliminar-pedido/', 'PedidoController@eliminarPedido');
    Route::get('api/pedidos/historico/', 'PedidoController@obtenerHistoricoPedidos');
    Route::get('api/pedidos/encabezado/', 'PedidoController@obtenerEncabezadoPedido');
    Route::get('api/pedidos/obtener-cupo-nit/', 'PedidoController@obtenerCupoDisponiblexCliente');
    Route::post('api/pedido/registro/', 'PedidoController@ingresaPedidoOfima');
    Route::put('api/pedido/modifica-item-detalle/', 'PedidoController@modificaItemPedidoDetalle');
    Route::get('api/pedido/fecha-actual/', 'PedidoController@obtenerFechaActual');
    Route::get('api/pedido/informes/', 'PedidoController@obtenerDataInformes');
    Route::get('api/pedido/listado/', 'PedidoController@listadoPedidos');
    Route::get('api/pedido/precios-producto/', 'PedidoController@obtenerPreciosProducto');
    Route::get('api/pedido/obtener-total-bodega/', 'PedidoController@obtenerTotalPesoEnBodega');
    Route::get('api/pedido/obtener-pedidos-bodega/', 'PedidoController@obtenerPedidosEnBodega');
    Route::get('api/pedido/obtener-notas-credito/', 'PedidoController@obtenerInfoNotasCredito');
    Route::get('api/pedido/obtener-dias-habiles/', 'PedidoController@ejecutarDiasHabiles');
    Route::get('api/pedido/obtener-consecutivos-ofima/', 'PedidoController@obtenerConsecutivosxOrigen');
    Route::get('api/pedido/obtener-all-productos/', 'PedidoController@obtenerAllProductosOfima');

    //CARTERA
    Route::get('api/cartera/obtener-cartera/', 'DespachoController@obtenerCarteraFecha');
    Route::get('api/cartera/obtener-deuda/', 'DespachoController@obtenerDeudaCliente');
    //ZONAS
    Route::get('api/zonas/obtener-zonas/', 'PedidoController@obtenerZonas');
    Route::get('api/zonas/obtener-ciudad-zona/', 'PedidoController@obtenerCiudadxZona');

    //PRESUPUESTOS
    Route::get('api/presupuestos/grupos-productos/', 'PresupuestoController@obtenerGrupoProductos');
    Route::post('api/presupuestos/ingresar/', 'PresupuestoController@ingresarPresupuesto');
    Route::get('api/presupuestos/presupuesto-ventas-consolidado-mes/', 'PresupuestoController@obtenerVentasVsPresupuestoMes');
    Route::get('api/pedido/fecha-inicio-final/', 'PresupuestoController@obtenerFechaActual');
    Route::get('api/presupuestos/ventas-mes-mes/', 'PresupuestoController@obtenerVentasMes');
    Route::get('api/presupuestos/obtener-anios/', 'PresupuestoController@obtenerAniosFacturacion');

    //PRESUPUESTOS VENDEDORES
    Route::post('api/presupuestos-vendedores/ingresar/', 'PresupuestoVendedoresController@ingresarPresupuestoVendedor');
    Route::get('api/presupuestos-vendedores/obtener-presupuesto/', 'PresupuestoVendedoresController@obtenerGrupoProductosVendedor');
    Route::get('api/presupuestos/presupuesto-ventas-consolidado-mes-vendedor/', 'PresupuestoVendedoresController@obtenerVentasVsPresupuestoMes');
    Route::get('api/presupuestos/presupuesto-ventas-consilidaddo-mes-vendedor-bultos/', 'PresupuestoVendedoresController@obtenerPresupuestoConsolidadoPorVendedor');
    Route::get('api/presupuestos-vendedores/obtener-ventas-vendedores-fecha/', 'PresupuestoVendedoresController@obtenerVentasVendedoresFecha');

    // FINANCIERO
    Route::get('api/financiero/obtener-valores/', 'FinancieroController@ejecutarFuncionFinanciera');
    Route::get('api/financiero/obtener-anios-contables/', 'FinancieroController@obtenerAniosContables');
    Route::get('api/financiero/obtener-meses-contables/', 'FinancieroController@obtenerMesesContables');
    Route::get('api/financiero/obtener-info-periodos/', 'FinancieroController@obtenerInformacionPeridos');

    // VENDEDORES
    Route::get('api/usuarios/obtener-vendedores/', 'AuthController@obtenerVendedores');
    Route::get('api/usuarios/obtener-usuarios-x-rol/', 'AuthController@obtenerUsuariosPorRol');

    // COMENTARIOS
    Route::post('api/comentarios/ingresar/', 'ComentarioController@store');
    Route::get('api/comentarios/listado/', 'ComentarioController@listarComentarios');
    Route::get('api/comentarios/listado-fecha/', 'ComentarioController@obtenerComentariosFecha');
    Route::get('api/comentarios/obtener-comentario/', 'ComentarioController@obtenerComentario');

    // NOVEDADES AL SISTEMA
    Route::post('api/novedad-sistema/ejecutar/', 'NovedadSistemaController@ejecutarNovedad');
    Route::get('api/novedad-sistema/obtener/', 'NovedadSistemaController@obtenerNovedad');

    //MERCADO
    Route::get('api/mercado/obtener-productos-ofima/', 'MercadoController@obtenerProductosOfima');
    Route::get('api/mercado/obtener-zonas/', 'MercadoController@obtenerZonasVendedores');
    Route::post('api/competencia/registrar/', 'MercadoController@registrar');
    Route::post('api/mercado/subir-archivo/', 'MercadoController@subirEvidencia');
    Route::get('api/mercado/obtener-registros/', 'MercadoController@obtenerRegistros');
    Route::put('api/mercado/eliminar-registro/', 'MercadoController@eliminarRegistro');
    Route::get('api/mercado/graficas/', 'MercadoController@obtenerInfoGraficas');
    Route::get('api/mercado/ini-fin-mes/', 'MercadoController@obtenerDiaInicioFinMes');

    //FACTURAS PAGAS
    Route::post('api/facturas/registro/', 'FacturasPagasController@registroFacturaPaga');

    //SILOS
    Route::post('api/silos/registro/', 'SiloController@registrar');
    Route::get('api/silos/obtener/', 'SiloController@obtenerSilos');
    Route::get('api/silos/obtener-info-graficas/', 'SiloController@ObtenerInfoSilosGraficos');
    Route::get('api/silos/obtener-silos-nombre/', 'SiloController@obtenerSilosPorNombre');
    //SILO MOVIMIENTO
    Route::post('api/silos/registrar-movimiento/', 'SilosMovimientoController@registrarMovimiento');
    Route::get('api/silos/obtener-movimientos/', 'SilosMovimientoController@obtenerMovimientos');
    Route::put('api/silos/anular-movimiento/', 'SilosMovimientoController@anularMovimiento');
    Route::put('api/silos/anular-movimiento-multiple/', 'SilosMovimientoController@anularMovimientoMultiple');
    
    //COMPRAS TRIGO
    Route::post('api/compras-trigo/registrar-compra/', 'ComprasTrigoController@registrarCompra');
    Route::get('api/compras-trigo/obtener-proveedores/', 'ComprasTrigoController@ObtenerProveedoresTrigo');
    Route::get('api/compras-trigo/obtener-compras/', 'ComprasTrigoController@obtenerCompras');
    Route::put('api/compras-trigo/cambiar-estado-compra/', 'ComprasTrigoController@cambiarEstadoCompra');
    Route::get('api/compras-trigo/obtener-motonave-activa/', 'ComprasTrigoController@obtenerMotonaveActiva');
    Route::get('api/compras-trigo/obtener-ultimas-motonaves/', 'ComprasTrigoController@obtenerUltimasMotonaves');
    
    //LOGISTICA (RECEPCION TRIGO)
    Route::post('api/logistica/importar-recepcion-trigo/', 'RecepcionTrigoController@importarDatos');
    Route::get('api/logistica/listado-repecion-trigo/', 'RecepcionTrigoController@listadoRecepcion');

    //LOGISTICA (DESPACHOS DE TRIGO)
    Route::post('api/logistica/importar-despachos-trigo/', 'DespachosTrigoController@importarDatos');
    Route::get('api/logistica/obtener-despachos-trigo/', 'DespachosTrigoController@obtenerListadoDespachosTrigo');
    Route::get('api/logistica/obtener-trigo-transito/', 'DespachosTrigoController@obtenerTrigoEnTransito');
    Route::get('api/logistica/obtener-trigo-transito-detail/', 'DespachosTrigoController@obtenerTrigoTransitoDetallado');

    //RESPONSABLE
    Route::post('api/responsable/registro/', 'ResponsablesController@registrar');
    Route::get('api/responsable/listado/', 'ResponsablesController@obtenerResponsables');
    Route::put('api/responsable/inactivar/', 'ResponsablesController@inactivar');

    //AREAS
    Route::post('api/area/registro/', 'AreaController@registrar');
    Route::get('api/areas/listado/', 'AreaController@obtenerAreas');
    Route::put('api/area/actualizar/', 'AreaController@actualizar');
    Route::put('api/areas/inactivar/', 'AreaController@inactivar');

    //CAUSALES
    Route::post('api/causal/registro/', 'CausalesController@registrar');
    Route::get('api/causal/listado/', 'CausalesController@obtenerCausales');
    Route::put('api/causal/actualiza/', 'CausalesController@actualizar');

    //RESPONSABLES AREAS
    Route::post('api/responsable-area/registro/', 'ResponsablesAreasController@registrar');
    Route::put('api/responsable-area/eliminar/', 'ResponsablesAreasController@eliminar');

    //ASPIRANTES
    Route::post('api/aspirante/registro/', 'AspiranteController@regitrarAspirante');
    Route::put('api/aspirante/finalizar-proceso/', 'AspiranteController@finalizarProceso');
    Route::get('api/aspirante/listado/', 'AspiranteController@obtenerListadoAspirantes');
    Route::put('api/aspirante/cambia-estado-proceso/', 'AspiranteController@cambiarEstadoProceso');
    Route::get('api/aspirante/obtener-info-documento/', 'AspiranteController@obtenerInfoAspirante');

    // FICHOS
    Route::get('api/fichos/obtener-fichos/', 'FichoController@obtenerFichos');
    Route::put('api/fichos/liberar-fichos/', 'FichoController@liberarFichos');

    // DESPACHOS TURNO
    Route::post('api/despachos-turno/registrar/', 'DespachosTurnoController@registrarTurno');
    Route::get('api/despachos-turno/listado-reportados/', 'DespachosTurnoController@obtenerReportados');
    Route::put('api/despachos-turno/anular/', 'DespachosTurnoController@anularTurno');
    Route::put('api/despachos-turno/actualiza-estado/', 'DespachosTurnoController@actualizarEstadoTurno');
    Route::get('api/despachos-turno/obtener-ciudades/', 'DespachosTurnoController@obtenerCiudades');
    Route::get('api/despachos-turno/reimprimir-ticket/', 'DespachosTurnoController@reimprimirTicket');
    Route::get('api/despachos-turno/vehiculos-cargados/', 'DespachosTurnoController@obtenerVehiculosCargados');
    Route::get('api/despachos-turno/traza-despachos/', 'DespachosTurnoController@obtenerTrazaDespacho');

    // MAESTROS COD DE BARRAS
    Route::post('api/maestro-cod-barras/registrar/', 'MaestrosCodBarrasController@registrarMaestro');
    Route::get('api/maestro-cod-barras/listado/', 'MaestrosCodBarrasController@listarMaestros');
    Route::put('api/maestro-cod-barras/actualizar/', 'MaestrosCodBarrasController@actualizarMaestro');
    Route::put('api/maestro-cod-barras/eliminar/', 'MaestrosCodBarrasController@eliminarMaestro');

    // MOVIMIENTO DE PRODUCCION
    Route::post('api/movimiento-produccion/registrar/', 'MovimientosProduccionController@registrarMovimientoProduccion');
    Route::get('api/movimiento-produccion/listado/', 'MovimientosProduccionController@listarMovimientosProduccion');
    Route::put('api/movimiento-produccion/eliminar/', 'MovimientosProduccionController@eliminarRegistro');
    Route::get('api/movimiento-produccion/listado-por-getionar/', 'MovimientosProduccionController@listadoMovProduccionxGesionar');
    Route::get('api/movimiento-produccion/listado-productos-consolidado/', 'MovimientosProduccionController@obtenerConsolidadoProductos');
    Route::post('api/movimiento-produccion/ingresar-erp/', 'MovimientosProduccionController@ingresarProduccionERP');

    //DESPACHOS NOVEDAD
    Route::get('api/despachos-novedad/movimientos', 'DespachosNovedadesController@consultaGestionEntregas');

    // VISITAS
    Route::post('api/visita/registrar', 'VisitaClientesController@registrarVisita');
    Route::get('api/visita/obtener', 'VisitaClientesController@obtenerVisitas');
    Route::put('api/visita/actualizar', 'VisitaClientesController@modificarVisita');
    Route::put('api/visita/gestionar', 'VisitaClientesController@gestionarVisita');
    Route::get('api/visita/obtener-visitar-estado', 'VisitaClientesController@obtenerVisitarPorEstado');

    //TABLERO - VIAJES
    Route::post('api/tablero/registrar', 'TableroController@registrarViaje');
    Route::get('api/tablero/obtener', 'TableroController@obtenerViajes');
});

Auth::routes(['register' => false]);

// Route::get('/home', 'HomeController@index')->name('home');
